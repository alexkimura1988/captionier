//
//  SSPicker.m
//  FizFeliz
//
//  Created by Surender Rathore on 05/12/13.
//
//

#import "SSPicker.h"
#import "AppDelegate.h"

@implementation SSPicker
@synthesize pickerValues;

-(void)showPickerWithTitle:(NSString*)title OnView:(id)view
{
    self.pickerValues = @[@"hello"];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title
                                       delegate:Nil
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                              otherButtonTitles:nil];
    
    UIPickerView *pickerView = [[UIPickerView alloc] init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    CGRect pickerRect = pickerView.bounds;
    pickerRect.origin.y = 35;
    pickerView.frame = pickerRect;
    
    [actionSheet addSubview:pickerView];
    
    
    UIButton *startbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    startbtn.frame = CGRectMake(80,230, 170, 73);
    //[startbtn setBackgroundImage:[UIImage imageNamed:@"yourbutton.png"] forState:UIControlStateNormal];
    [startbtn addTarget:self action:@selector(pressedbuttonCancel:) forControlEvents:UIControlEventTouchUpInside];
    [actionSheet addSubview:startbtn];
    
 // [[UIApplication sharedApplication] delegate] ]
    if([view isKindOfClass:[UITabBar class]]) {
        UITabBar *tabBar = (UITabBar*)view;
        [actionSheet showFromTabBar:tabBar];
    }
    else if ([view isKindOfClass:[UIView class]]) {
        UIView *onView = (UIView*)view;
        [actionSheet showInView:onView];
    }
    
    
    
    
}


#pragma mark - UIPickerViewDelegate 

#pragma mark - UIActionSheetDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 1;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return @"sehllo";
}
@end
