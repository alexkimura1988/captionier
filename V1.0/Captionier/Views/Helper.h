//
//  Helper.h
//  Restaurant
//
//  Created by 3Embed on 14/09/12.
//
//

#import <Foundation/Foundation.h>

@interface Helper : NSObject

{
   
}


@property(nonatomic,assign)float _latitude;
@property(nonatomic,assign)float _longitude;
@property(nonatomic,assign)int showHeaderINInviteScreen;
@property(nonatomic,assign)int locate_ValueChanged;
@property(nonatomic,strong)NSString *location;
@property(nonatomic,assign)int videoControllerPoped;
@property(nonatomic,assign)int followersClicked;
@property(nonatomic,assign)BOOL isAddedNewPhoto;
@property(nonatomic,assign)int didCurrentUserFollowedSomebody;
@property(nonatomic,assign)int needToInilizeCamera;

+ (id)sharedInstance;

+(void)setToLabel:(UILabel*)lbl Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size Color:(UIColor*)color;
+(void)setButton:(UIButton*)btn Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size TitleColor:(UIColor*)t_color ShadowColor:(UIColor*)s_color;
+(void)showAlertWithTitle:(NSString*)title Message:(NSString*)message;
+(void)showErrorFor:(int)errorCode;
+ (NSString *)removeWhiteSpaceFromURL:(NSString *)url;
+ (NSString *)stripExtraSpacesFromString:(NSString *)string;
+ (NSString*)getCurrentDate;
+(NSString*)getCurrentTime;
+(NSString*)getDayDate;
+(BOOL)isPhone5;
@end
