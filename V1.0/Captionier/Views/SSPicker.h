//
//  SSPicker.h
//  FizFeliz
//
//  Created by Surender Rathore on 05/12/13.
//
//

#import <Foundation/Foundation.h>

@interface SSPicker : UIView <UIPickerViewDelegate,UIPickerViewDataSource>

@property(nonatomic,strong)NSArray *pickerValues;

-(void)showPickerWithTitle:(NSString*)title OnView:(id)view;
@end
