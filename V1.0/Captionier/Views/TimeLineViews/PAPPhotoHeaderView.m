//
//  PAPPhotoHeaderView.m
//  Anypic
//
//  Created by Héctor Ramos on 5/15/12.
//

#import "PAPPhotoHeaderView.h"
#import "PAPProfileImageView.h"
#import "TTTTimeIntervalFormatter.h"
#import "PAPPhotoCell.h"
#import "PAPUtility.h"
#import "PAPPhotoTimelineViewController.h"
#import "CommentsViewController.h"


@interface PAPPhotoHeaderView () 
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) PAPProfileImageView *avatarImageView;
@property (nonatomic, strong) UIButton *userButton;
@property (nonatomic,strong) UIButton *locationButton;
@property (nonatomic, strong) UILabel *timestampLabel;
@property (nonatomic, strong) TTTTimeIntervalFormatter *timeIntervalFormatter;
@property (nonatomic, strong) UIImageView *timelogo;
@end


@implementation PAPPhotoHeaderView
@synthesize containerView;
@synthesize avatarImageView;
@synthesize userButton;
@synthesize timestampLabel;
@synthesize timeIntervalFormatter;
@synthesize photo;
@synthesize user;
@synthesize buttons;
@synthesize likeButton;
@synthesize commentButton;
@synthesize delegate;
@synthesize moreButton;

@synthesize likeLabel;
@synthesize commentLabel;

#pragma mark - Initialization

- (id)initWithFrame:(CGRect)frame buttons:(PAPPhotoHeaderButtons)otherButtons {
    self = [super initWithFrame:frame];
    if (self) {
        [PAPPhotoHeaderView validateButtons:otherButtons];
        buttons = otherButtons;

        self.clipsToBounds = NO;
        self.containerView.clipsToBounds = NO;
        self.superview.clipsToBounds = NO;
        [self setBackgroundColor:[UIColor clearColor]];
        
        // translucent portion
        self.containerView = [[UIView alloc] initWithFrame:CGRectMake( 0.0f, 0.0f, 320, self.bounds.size.height)];
        
        [self addSubview:self.containerView];
        [self.containerView setBackgroundColor:[UIColor colorWithWhite:1 alpha:1.0f]];
        //self.containerView.alpha = .6;
        
        
        self.avatarImageView = [[PAPProfileImageView alloc] init];
        self.avatarImageView.frame = CGRectMake( 5.0f, 5.0f, 40.0f, 40.0f);
        self.avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width/2;
        self.avatarImageView.clipsToBounds = YES;
        [self.avatarImageView setClipsToBounds:YES];
        [self.avatarImageView.profileButton addTarget:self action:@selector(didTapUserButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        self.avatarImageView.layer.borderWidth = 2;
        self.avatarImageView.layer.borderColor = [UIColor colorWithWhite:0.705 alpha:1.000].CGColor;
        [self.containerView addSubview:self.avatarImageView];
        
        
        if (self.buttons && PAPPhotoHeaderButtonsLike) {
            
            likeLabel = [[UILabel alloc] initWithFrame:CGRectMake(205 -32, 25, 30, 20)];//203 -32, 33.f, 30, 20
            [Helper setToLabel:likeLabel Text:@"" WithFont:LATO_LIGHT FSize:14 Color:[UIColor colorWithWhite:0.402 alpha:1.000]];
            [likeLabel setBackgroundColor:[UIColor orangeColor]];
            likeLabel.textAlignment = NSTextAlignmentRight;
            likeLabel.backgroundColor = [UIColor clearColor];
            //likeLabel.userInteractionEnabled = YES;
            
            
            
        }
        if (self.buttons & PAPPhotoHeaderButtonsLike) {
            // like button
           likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
           //[containerView addSubview:self.likeButton];
            //[self.likeButton setFrame:CGRectMake(201, 25, 25, 22)];//201, 30.5f, 30.0f, 25.5f
           [self.likeButton setFrame:CGRectMake(201, 25, 30, 25.5f)];//188
           [self.likeButton setImage:[UIImage imageNamed:@"homescreen_like_btn_off.png"] forState:UIControlStateNormal];
           [self.likeButton setImage:[UIImage imageNamed:@"homescreen_like_btn_on.png"] forState:UIControlStateSelected];
           [self.likeButton addTarget:self action:@selector(didTapLikePhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.likeButton setSelected:NO];
        }
        
        
        if (self.buttons & PAPPhotoHeaderButtonsComment) {
            
            commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(228, 29.0f, 25, 20)];//250-32, 31.f, 30, 20
            [Helper setToLabel:commentLabel Text:@"" WithFont:LATO_LIGHT FSize:14 Color:[UIColor colorWithWhite:0.402 alpha:1.000]];
            commentLabel.textAlignment = NSTextAlignmentRight;
            commentLabel.backgroundColor = CLEAR_COLOR;
           //[containerView addSubview:self.commentLabel];
        }

        if (self.buttons & PAPPhotoHeaderButtonsComment) {
            
            commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
            //[containerView addSubview:self.commentButton];
            //[self.likeButton setFrame:CGRectMake(201, 25, 25, 22)];//201, 30.5f, 30.0f, 25.5f
            [self.commentButton setFrame:CGRectMake(253, 27,25.0f, 24)];//18.0
           // [self.commentButton setBackgroundColor:[UIColor greenColor]];
            [self.commentButton setImage:[UIImage imageNamed:@"home_screen_comment_off-568h"] forState:UIControlStateNormal];
            [self.commentButton setImage:[UIImage imageNamed:@"home_screen_comment_on-568h"] forState:UIControlStateHighlighted];
            [self.commentButton addTarget:self action:@selector(didTapCommentOnPhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.commentButton setSelected:NO];
            
            

        }
        
        
        if (self.buttons & PAPPhotoHeaderButtonsUser) {
            // This is the user's display name, on a button so that we can tap on it
            self.userButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [containerView addSubview:self.userButton];
            [self.userButton setBackgroundColor:[UIColor clearColor]];
            [[self.userButton titleLabel] setFont:[UIFont fontWithName:robo_bold size:16]];
            [self.userButton setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
            [self.userButton setTitleColor:[UIColor colorWithRed:0.341 green:0.832 blue:0.979 alpha:1.000] forState:UIControlStateHighlighted];
            [[self.userButton titleLabel] setLineBreakMode:NSLineBreakByTruncatingTail];
            [[self.userButton titleLabel] setShadowOffset:CGSizeMake( 0.0f, 1.0f)];
            userButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

        }
        
        
        self.locationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [containerView addSubview:self.locationButton];
        [self.locationButton setBackgroundColor:[UIColor clearColor]];
        [[self.locationButton titleLabel] setFont:[UIFont fontWithName:robo_bold size:13]];
        [self.locationButton setTitleColor:[UIColor colorWithRed:0.257 green:0.423 blue:1.000 alpha:1.000] forState:UIControlStateNormal];
        [self.locationButton setTitleColor:[UIColor colorWithRed:0.341 green:0.832 blue:0.979 alpha:1.000] forState:UIControlStateHighlighted];
        [[self.locationButton titleLabel] setLineBreakMode:NSLineBreakByTruncatingTail];
        [[self.locationButton titleLabel] setShadowOffset:CGSizeMake( 0.0f, 1.0f)];
        self.locationButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.locationButton addTarget:self action:@selector(locationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.locationButton.hidden = YES;

    self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //[containerView addSubview:self.moreButton];
    [self.moreButton setFrame:CGRectMake(310-23,25, 22, 23.5f)];
    //[self.moreButton setBackgroundColor:[UIColor orangeColor]];
    [self.moreButton setImage:[UIImage imageNamed:@"home_screen_more_option_off-568h"] forState:UIControlStateNormal];
    [self.moreButton setImage:[UIImage imageNamed:@"home_screen_more_option_on-568h"] forState:UIControlStateHighlighted];
    [self.moreButton addTarget:self action:@selector(didTapMoreButton:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        self.timeIntervalFormatter = [[TTTTimeIntervalFormatter alloc] init];
      
        // timestamp
        self.timestampLabel = [[UILabel alloc] initWithFrame:CGRectMake( 53.0f+18, 34.0f, containerView.bounds.size.width - 170, 18.0f)];
        //self.timestampLabel.backgroundColor = [UIColor redColor];
        [containerView addSubview:self.timestampLabel];
        [Helper setToLabel:self.timestampLabel Text:@"" WithFont:robo_medium FSize:12 Color:[UIColor grayColor]];
        [self.timestampLabel setBackgroundColor:[UIColor clearColor]];

        
        
        //timelogo
        
        _timelogo = [[UIImageView alloc] init];
        _timelogo.image = [UIImage imageNamed:@"home_time"];
        [containerView addSubview:_timelogo];
        [_timelogo setFrame:CGRectMake(53.0f,36, 15,15.0f)];
        
        
        
        //UIImage *timelogo = [UIImage imageNamed:@"home_time"];
        
        
    }

    return self;
}





#pragma mark - PAPPhotoHeaderView

- (void)setPhotoFile:(PFObject *)aPhoto {
    photo = aPhoto;

    // user's avatar
    PFUser *user = [self.photo objectForKey:kPAPPhotoUserKey];
    PFFile *profilePictureSmall = [user objectForKey:kPAPUserProfilePicSmallKey];
    [self.avatarImageView setFile:profilePictureSmall];

    NSString *authorName = [user objectForKey:kPAPUserDisplayNameKey];
    [self.userButton setTitle:authorName forState:UIControlStateNormal];
    
    CGFloat constrainWidth = containerView.bounds.size.width;

    if (self.buttons & PAPPhotoHeaderButtonsUser) {
        [self.userButton addTarget:self action:@selector(didTapUserButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self.moreButton addTarget:self action:@selector(didTapMoreButton:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.buttons & PAPPhotoHeaderButtonsComment) {
        constrainWidth = self.commentButton.frame.origin.x;
        [self.commentButton addTarget:self action:@selector(didTapCommentOnPhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (self.buttons & PAPPhotoHeaderButtonsLike) {
        constrainWidth = self.likeButton.frame.origin.x;
        [self.likeButton addTarget:self action:@selector(didTapLikePhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // we resize the button to fit the user's name to avoid having a huge touch area
    CGPoint userButtonPoint = CGPointMake(54.0f, 14.0f);
    constrainWidth -= userButtonPoint.x;
    CGSize constrainSize = CGSizeMake(constrainWidth, containerView.bounds.size.height - userButtonPoint.y*2.0f);
    CGSize userButtonSize = [self.userButton.titleLabel.text sizeWithFont:self.userButton.titleLabel.font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByTruncatingTail];
    
    
    CGRect textRect;
    
   
    
    if ([[aPhoto objectForKey:kPAPFeedLocationKey] length] == 0) {
        
        CGRect userButtonFrame = CGRectMake(userButtonPoint.x, 25 - userButtonSize.height/2, userButtonSize.width + 60, userButtonSize.height);
        [self.userButton setFrame:userButtonFrame];
        self.locationButton.hidden = YES;
        
        
        NSTimeInterval timeInterval = [[self.photo createdAt] timeIntervalSinceNow];
        NSString *timestamp = [self.timeIntervalFormatter stringForTimeInterval:timeInterval];
        [self.timestampLabel setText:timestamp];
        
        textRect = [timestamp boundingRectWithSize:CGSizeMake(70, 18)
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:@{NSFontAttributeName:self.timestampLabel.font}
                                           context:nil];
        
        CGSize timeStampSize = textRect.size;
        
        _timelogo.frame = CGRectMake(containerView.bounds.size.width - timeStampSize.width -15, 20, 10, 10);
        
        self.timestampLabel.frame = CGRectMake(CGRectGetMaxX(_timelogo.frame)+2, 25 - 9, timeStampSize.width, 18);
        
    }
    else{
        
        self.locationButton.hidden = NO;
        CGRect userButtonFrame = CGRectMake(userButtonPoint.x, 15 - userButtonSize.height/2, userButtonSize.width + 60, userButtonSize.height);
        [self.userButton setFrame:userButtonFrame];
        
        
        textRect = [[aPhoto objectForKey:kPAPFeedLocationKey] boundingRectWithSize:CGSizeMake(250, 20)
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:@{NSFontAttributeName:[UIFont fontWithName:robo_bold size:13]}
                                           context:nil];
        
        

        self.locationButton.frame = CGRectMake(userButtonPoint.x, CGRectGetMaxY(self.userButton.frame), 250, 20);
        [self.locationButton setTitle:[aPhoto objectForKey:kPAPFeedLocationKey] forState:UIControlStateNormal];
        
        
        NSTimeInterval timeInterval = [[self.photo createdAt] timeIntervalSinceNow];
        NSString *timestamp = [self.timeIntervalFormatter stringForTimeInterval:timeInterval];
        [self.timestampLabel setText:timestamp];
        
        textRect = [timestamp boundingRectWithSize:CGSizeMake(70, 18)
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:@{NSFontAttributeName:self.timestampLabel.font}
                                           context:nil];
        
        CGSize timeStampSize = textRect.size;
        
        _timelogo.frame = CGRectMake(containerView.bounds.size.width - timeStampSize.width -15, 10, 10, 10);
        
        self.timestampLabel.frame = CGRectMake(CGRectGetMaxX(_timelogo.frame)+2, 15 - 9, timeStampSize.width, 18);
    }
    
   
    
   
    
   
    
    
   

    [self setNeedsDisplay];
}

-(void)displayuser {
    
    PFFile *profilePictureSmall = [user objectForKey:kPAPUserProfilePicSmallKey];
    [self.avatarImageView setFile:profilePictureSmall];
    
    NSString *authorName = [user objectForKey:kPAPUserDisplayNameKey];
    [self.userButton setTitle:authorName forState:UIControlStateNormal];
    
    CGFloat constrainWidth = containerView.bounds.size.width;
    
    if (self.buttons & PAPPhotoHeaderButtonsUser) {
        [self.userButton addTarget:self action:@selector(didTapUserButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self.moreButton addTarget:self action:@selector(didTapMoreButton:) forControlEvents:UIControlEventTouchUpInside];
    
//    if (self.buttons & PAPPhotoHeaderButtonsComment) {
//        constrainWidth = self.commentButton.frame.origin.x;
//        [self.commentButton addTarget:self action:@selector(didTapCommentOnPhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//    }
    
    if (self.buttons & PAPPhotoHeaderButtonsLike) {
        constrainWidth = self.likeButton.frame.origin.x;
        [self.likeButton addTarget:self action:@selector(didTapLikePhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // we resize the button to fit the user's name to avoid having a huge touch area
    CGPoint userButtonPoint = CGPointMake(54.0f, 14.0f);
    constrainWidth -= userButtonPoint.x;
    CGSize constrainSize = CGSizeMake(constrainWidth, containerView.bounds.size.height - userButtonPoint.y*2.0f);
    CGSize userButtonSize = [self.userButton.titleLabel.text sizeWithFont:self.userButton.titleLabel.font constrainedToSize:constrainSize lineBreakMode:NSLineBreakByTruncatingTail];
    CGRect textRect;
    
    
    if ([[photo objectForKey:kPAPFeedLocationKey] length] == 0) {
        
        CGRect userButtonFrame = CGRectMake(userButtonPoint.x, 25 - userButtonSize.height/2, userButtonSize.width + 60, userButtonSize.height);
        [self.userButton setFrame:userButtonFrame];
        self.locationButton.hidden = YES;
        
    }
    else{
        
        self.locationButton.hidden = NO;
        CGRect userButtonFrame = CGRectMake(userButtonPoint.x, 15 - userButtonSize.height/2, userButtonSize.width + 60, userButtonSize.height);
        [self.userButton setFrame:userButtonFrame];
        
        
        textRect = [[photo objectForKey:kPAPFeedLocationKey] boundingRectWithSize:CGSizeMake(250, 20)
                                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                                        attributes:@{NSFontAttributeName:[UIFont fontWithName:robo_bold size:13]}
                                                                           context:nil];
        
        CGSize timeStampSize = textRect.size;
        
        self.locationButton.frame = CGRectMake(userButtonPoint.x, CGRectGetMaxY(self.userButton.frame), 250, 20);
        [self.locationButton setTitle:[photo objectForKey:kPAPFeedLocationKey] forState:UIControlStateNormal];
    }

    
    NSTimeInterval timeInterval = [[self.photo createdAt] timeIntervalSinceNow];
    NSString *timestamp = [self.timeIntervalFormatter stringForTimeInterval:timeInterval];
    [self.timestampLabel setText:timestamp];
    
     textRect = [timestamp boundingRectWithSize:CGSizeMake(70, 18)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:self.timestampLabel.font}
                                              context:nil];
    
    CGSize timeStampSize = textRect.size;
    
    
    
    
    _timelogo.frame = CGRectMake(containerView.bounds.size.width - timeStampSize.width -15, 20, 10, 10);
    
    self.timestampLabel.frame = CGRectMake(CGRectGetMaxX(_timelogo.frame)+2, 25 - 9, timeStampSize.width, 18);
    
    [self setNeedsDisplay];
    

}

- (void)setLikeStatus:(BOOL)liked {
    [self.likeButton setSelected:liked];
    
    if (liked) {
        [self.likeButton setTitleEdgeInsets:UIEdgeInsetsMake(-1.0f, 0.0f, 0.0f, 0.0f)];
        [[self.likeButton titleLabel] setShadowOffset:CGSizeMake(0.0f, -1.0f)];
    } else {
        [self.likeButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
        [[self.likeButton titleLabel] setShadowOffset:CGSizeMake(0.0f, 1.0f)];
    }
}

- (void)shouldEnableLikeButton:(BOOL)enable {
    if (enable) {
        [self.likeButton removeTarget:self action:@selector(didTapLikePhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        [self.likeButton addTarget:self action:@selector(didTapLikePhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
}

//-(void)buttonclickedComment:(UIButton*)sender{
//    if (delegate && [delegate respondsToSelector:@selector(cell:didTapComment:user:)]) {
//        [delegate cell:self didTapComment:sender user:self.photo];
//    }
//}
#pragma mark - ()

+ (void)validateButtons:(PAPPhotoHeaderButtons)buttons {
    if (buttons == PAPPhotoHeaderButtonsNone) {
        [NSException raise:NSInvalidArgumentException format:@"Buttons must be set before initializing PAPPhotoHeaderView."];
    }
}



- (void)didTapUserButtonAction:(UIButton *)sender {
    if (delegate && [delegate respondsToSelector:@selector(photoHeaderView:didTapUserButton:user:)]) {
        [delegate photoHeaderView:self didTapUserButton:sender user:[self.photo objectForKey:kPAPPhotoUserKey]];
    }
}

- (void)didTapMoreButton:(UIButton*)sender {
    
    if (delegate && [delegate respondsToSelector:@selector(photoHeaderView:didTapMoreButton:photo:)]) {
        [delegate photoHeaderView:self didTapMoreButton:sender photo:self.photo];
    }
}

- (void)didTapLikePhotoButtonAction:(UIButton *)button {
    if (delegate && [delegate respondsToSelector:@selector(photoHeaderView:didTapLikePhotoButton:photo:)]) {
        [delegate photoHeaderView:self didTapLikePhotoButton:button photo:self.photo];
    }
}

- (void)didTapCommentOnPhotoButtonAction:(UIButton *)sender {
    if (delegate && [delegate respondsToSelector:@selector(photoHeaderView:didTapCommentOnPhotoButton:photo:)]) {
        [delegate photoHeaderView:self didTapCommentOnPhotoButton:sender photo:self.photo];
    }
}
-(void)locationButtonClicked:(UIButton*)sender{
    
    if (delegate && [delegate respondsToSelector:@selector(photoHeaderView:didTapOnLocationButton:photo:)]) {
        [delegate photoHeaderView:self didTapOnLocationButton:sender photo:self.photo];
    }
}
-(void)handleSingleTapGesture:(UIGestureRecognizer*)gestureView{
    
    UILabel *label = (UILabel*)gestureView;
    
    if (delegate && [delegate respondsToSelector:@selector(photoHeaderView:didTapOnLikersLabel:photo:)]) {
        [delegate photoHeaderView:self didTapOnLikersLabel:label photo:self.photo];
    }
}




@end
