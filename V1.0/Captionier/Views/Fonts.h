//
//  Fonts.h
//  Anypic
//
//  Created by rahul Sharma on 20/08/13.
//
//

#import <Foundation/Foundation.h>

@protocol Constant <NSObject>



#define LABEL_LIKE_FONT   @""
//Fonts


#define HELVETICA_ROUNDEDBOLD  @"HelveticaRounded-Bold"

#define HELVETICA_ROUNDED_LTBOLD @"HelveticaRoundedLT-Bold"

#define HELVETICA_ROUNDEDLT_BOLDCOND  @"HelveticaRoundedLT-BoldCond"
#define HELVETICA_ROUNDED_BOLDCOND   @"HelveticaRounded-BoldCond"
#define HELVETICA_CONDENSED @"Helvetica-Condensed"
#define HELVETICANEUE_LIGHT @"HelveticaNeue-Light"
#define HELVETICA  @"Helvetica"

#define PROXIMANOVA_REGULAR     @"ProximaNova-Regular"
#define PROXIMANOVA_SEMIBOLD    @"ProximaNova-Semibold"
#define PROXIMANOVA_LIGHT       @"ProximaNova-Light"
#define HELVETICANEUE_BOLD      @"HelveticaNeue-Bold"
#define HELVETICA_BOLD          @"Helvetica-Bold"

#define     HelveticaNeue_BoldItalic        @"HelveticaNeue-BoldItalic"
#define     HelveticaNeue_Light             @"HelveticaNeue-Light"
#define     HelveticaNeue_Italic            @"HelveticaNeue-Italic"
#define     HelveticaNeue_UltraLightItalic @"HelveticaNeue-UltraLightItalic"
#define     HelveticaNeue_CondensedBold    @"HelveticaNeue-CondensedBold"
#define     HelveticaNeue_MediumItalic     @"HelveticaNeue-MediumItalic"
#define     HelveticaNeue_Thin              @"HelveticaNeue-Thin"
#define     HelveticaNeue_Medium            @"HelveticaNeue-Medium"
#define     HelveticaNeue_Thin_Italic      @"HelveticaNeue-Thin_Italic"
#define     HelveticaNeue_LightItalic      @"HelveticaNeue-LightItalic"
#define     HelveticaNeue_UltraLight       @"HelveticaNeue-UltraLight"
#define     HelveticaNeue_Bold              @"HelveticaNeue-Bold"
#define     HelveticaNeue                   @"HelveticaNeue"
#define     HelveticaNeue_CondensedBlack    @"HelveticaNeue-CondensedBlack"
#define     Lato                            @"Lato-Regular"
#define     LATO_LIGHT                      @"Lato-Light"
#define     robo                            @"Roboto-Regular"
#define     robo_light                      @"Roboto-Light"
#define     robo_medium                     @"Roboto-Medium"
#define     robo_bold                       @"Roboto-Bold"
#define     Aharoni_Bold                    @"Aharoni-Bold"














//Colors
#define cLikelabel  0x336699
#define cLikebuttonTitle   0x666666
#define cLikeButtonTitleSelector 0xffffff
#define cUserNameTitle 0x000000
#define cRateItLabelColor 0x666666
#define cCameraBottomViewColor 0x333333
#define cProfileScreenGrayColor 0xeeeaeb
#define cProfileBioColor 0x999999
#define cSettingsCellBGWhiteColor 0xffffff
#define cSettingsCellBGGrayColor 0xebe9e9
#define cDarkGrayColor 0x666666
#define cTitleColor 0xFFFFFF
#define cProfileUserNameTitle 0x57d6ff


#define LABEL_LIKE_COLOR        UIColorFromRGB(0X000000)//[UIColor colorWithRed:124.0/255.0 green:127.0/255.0 blue:137.0/255.0 alpha:1.0f]
#define TIMESTAMP_COLOR         UIColorFromRGB(cDarkGrayColor) //[UIColor colorWithRed:161.0/255.0 green:159.0/255.0 blue:156.0/255.0 alpha:1.0f]
#define BACKGROUND_COLOR        [UIColor colorWithRed:34.0/255.0 green:39.0/255.0 blue:46.0/255.0 alpha:1.0f]
#define APPBLUE_COLOR           [UIColor colorWithRed:18.0/255.0 green:18.0/255.0 blue:18.0/255.0 alpha:1.0f]
#define CELLBACKGROUND_COLOR    [UIColor colorWithRed:81.0/255.0 green:90.0/255.0 blue:95.0/255.0 alpha:1.0f]
#define DARKTABBAR_COLOR        [UIColor colorWithRed:55.0/255.0 green:61.0/255.0 blue:69.0/255.0 alpha:1.0f]



#define CLEAR_COLOR     [UIColor clearColor]
#define WHITE_COLOR     [UIColor whiteColor]
#define BLACK_COLOR     [UIColor blackColor]
#define GREEN_COLOR     [UIColor greenColor]
#define BLUE_COLOR     [UIColor blueColor]

@end