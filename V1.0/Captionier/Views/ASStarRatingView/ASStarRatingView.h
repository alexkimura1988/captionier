//
//  ASStarRatingView.h
//
//  Created by bl0ckme on 12/19/11.
//  Copyright (c) 2011 bl0ckme. All rights reserved.
//


@protocol ASStarRatingViewDelegate;
#import <UIKit/UIKit.h>

#define kDefaultMaxRating 5
#define kDefaultLeftMargin 1
#define kDefaultMidMargin 5
#define kDefaultRightMargin 1
#define kDefaultMinAllowedRating 0
#define kDefaultMaxAllowedRating kDefaultMaxRating
#define kDefaultMinStarSize CGSizeMake(11.5, 11.5)




@interface ASStarRatingView : UIView {
    UIImage *_notSelectedStar;
    UIImage *_selectedStar;
    UIImage *_halfSelectedStar;
    BOOL _canEdit;
    int _maxRating;
    float _leftMargin;
    float _midMargin;
    float _rightMargin;
    CGSize _minStarSize;
    float _rating;
    float _minAllowedRating;
    float _maxAllowedRating;
    NSMutableArray *_starViews;
}

@property (retain, nonatomic) UIImage *notSelectedStar;
@property (retain, nonatomic) UIImage *selectedStar;
@property (retain, nonatomic) UIImage *halfSelectedStar;
@property (assign, nonatomic) BOOL canEdit;
@property (assign, nonatomic) int maxRating;
@property (assign, nonatomic) float leftMargin;
@property (assign, nonatomic) float midMargin;
@property (assign, nonatomic) float rightMargin;
@property (assign, nonatomic) CGSize minStarSize;
@property (assign, nonatomic) float rating;
@property (assign, nonatomic) float minAllowedRating;
@property (assign, nonatomic) float maxAllowedRating;
@property (nonatomic,weak) id <ASStarRatingViewDelegate> delegate;
@end
@protocol ASStarRatingViewDelegate <NSObject>
@optional
-(void)didSelectStar:(ASStarRatingView*)sView Rating:(float)rating;

@end
