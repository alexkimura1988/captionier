//
//  Helper.m
//  Restaurant
//
//  Created by 3Embed on 14/09/12.
//
//

#import "Helper.h"

@implementation Helper


static Helper *helper;
@synthesize _latitude;
@synthesize _longitude;
@synthesize showHeaderINInviteScreen;
@synthesize location;
@synthesize locate_ValueChanged;
@synthesize videoControllerPoped;
@synthesize followersClicked;
@synthesize isAddedNewPhoto;
@synthesize didCurrentUserFollowedSomebody;
@synthesize needToInilizeCamera;



+ (id)sharedInstance {
	if (!helper) {
		helper  = [[self alloc] init];
	}
	
	return helper;
}


+(void)setToLabel:(UILabel*)lbl Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size Color:(UIColor*)color
{
    
    lbl.textColor = color;

    if (txt != nil) {
        lbl.text = txt;
    }
    
    
    if (font != nil) {
        lbl.font = [UIFont fontWithName:font size:_size];
    }
    
}

+(void)setButton:(UIButton*)btn Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size TitleColor:(UIColor*)t_color ShadowColor:(UIColor*)s_color
{
    [btn setTitle:txt forState:UIControlStateNormal];
    
    [btn setTitleColor:t_color forState:UIControlStateNormal];
    
    if (s_color != nil) {
        [btn setTitleShadowColor:s_color forState:UIControlStateNormal];
    }
    
    
    if (font != nil) {
        btn.titleLabel.font = [UIFont fontWithName:font size:_size];
    }
    else
    {
        btn.titleLabel.font = [UIFont systemFontOfSize:_size];
    }
}

+(void)showAlertWithTitle:(NSString*)title Message:(NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
  
}

+(void)showErrorFor:(int)errorCode
{
//    switch (errorCode) {
//        case SERVER_EXCEPTION:
//            [Helper showAlertWithTitle:@"Error" Message:@"Could not connect to sever. Please try again."];
//            break;
//            
//        case PASSWORD_NOT_MATCH:
//            [Helper showAlertWithTitle:@"Error" Message:@"The password you entered is incorrect. Please try again "];
//            break;
//        case SUCCESSFULL_RESPONSE:
//            ;
//            break;
//        case REQUEST_PARAMETER_BLANK:
//            [Helper showAlertWithTitle:@"Alert" Message:@"Please enter valid information"];
//            break;
//        case EMAIL_NOT_MATCH:
//            [Helper showAlertWithTitle:@"Alert" Message:@"User already exists"];
//            break;
//        case SEARCH_RESULT_NULL:
//            [Helper showAlertWithTitle:@"Message" Message:@"No Result Found"];
//            break;
//        case SERVICE_RESPONSE_NULL:
//            [Helper showAlertWithTitle:@"Message" Message:@"No Result Found"];
//            break;
//        default:
//            break;
//    }
}

+ (NSString *)removeWhiteSpaceFromURL:(NSString *)url {
	NSMutableString *string = [[NSMutableString alloc] initWithString:url] ;
	[string replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
    //	[string replaceOccurrencesOfString:@"www.museumhunters.com" withString:@"207.150.204.61" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
    //	NSLog(@"returnted : %@",string);
	return string;
}
+ (NSString *)stripExtraSpacesFromString:(NSString *)string {
	NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
	NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
	
	NSArray *parts = [string componentsSeparatedByCharactersInSet:whitespaces];
	NSArray *filteredArray = [parts filteredArrayUsingPredicate:noEmptyStrings];
	
	return [filteredArray componentsJoinedByString:@" "];
}
+(NSString*)getCurrentDate
{
    // Get current date time
    
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    
    // Get the date time in NSString
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];

    return dateInStringFormated;
    //NSLog(@"%@", dateInStringFormated);
    
    // Release the dateFormatter
    
    //[dateFormatter release];
}
+(NSString*)getCurrentTime
{
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    
    [dateFormatter setDateFormat:@"HH.mm a"];
    
    // Get the date time in NSString
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    
    return dateInStringFormated;

}
+(NSString*)getDayDate
{
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    
    [dateFormatter setDateFormat:@"EEEE dd-MMM-yyyy"];
    
    // Get the date time in NSString
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    
    return dateInStringFormated;
}
+(BOOL)isPhone5
{
    CGRect rect = [[UIScreen mainScreen] bounds];
    if (rect.size.height == 568) {
        return YES;
    }
    else
    {
        return NO;
    }
}
@end
