//
//  PAPPhotoCell.m
//  Anypic
//
//  Created by Héctor Ramos on 5/3/12.
//

#import "PAPPhotoCell.h"
#import "PAPUtility.h"
#import "STTweetLabel.h"
#import "VideoPlayerViewController.h"


#import "NSAttributedString+Attributes.h"
#import "OHASBasicMarkupParser.h"
#import "OHAttributedLabel.h"

@interface PAPPhotoCell() <OHAttributedLabelDelegate>


@end

@implementation PAPPhotoCell
@synthesize photoButton;
@synthesize labelDescrepiton;
@synthesize lblCategory;
@synthesize mediaIndicator;
@synthesize btnPlay;
@synthesize photo;
@synthesize delegate;


//Rakesh Added
@synthesize likeButton;
@synthesize commentButton;
@synthesize ShareButton;
@synthesize moreButton;

@synthesize commentLabel;
@synthesize likeLabel;
@synthesize lblMore;
@synthesize lblShare;

@synthesize labelCommentOne;
@synthesize labelCommentThree;
@synthesize labelCommentTwo;

@synthesize buttonUserOne;
@synthesize buttonUserTwo;
@synthesize buttonUserThree;

@synthesize commentview;
@synthesize imgline;

#pragma mark - NSObject

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        // Initialization code
        self.opaque = NO;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        self.clipsToBounds = NO;
        self.backgroundColor = [UIColor whiteColor];
        
        self.imageView.frame = CGRectMake( 0.0f, 0.0f, 320.0f, 320.0f);
        self.imageView.backgroundColor = [UIColor blackColor];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        self.photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.photoButton.frame = CGRectMake( 5.0f, 0.0f, 310.0f, 300.0f);
        self.photoButton.backgroundColor = [UIColor clearColor];
        //[self.contentView addSubview:self.photoButton];
        
        if (!likeButton) {
            
            likeLabel = [[UILabel alloc] initWithFrame:CGRectMake(22+8, 330.5f, 30, 25.5)];
            [Helper setToLabel:likeLabel Text:@"" WithFont:LATO_LIGHT FSize:14 Color:[UIColor colorWithWhite:0.402 alpha:1.000]];
            likeLabel.textAlignment = NSTextAlignmentRight;
            likeLabel.backgroundColor = [UIColor clearColor];
            likeLabel.userInteractionEnabled = NO;
        }
        
        if (!likeButton) {
            // like button
            likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
            //[self.contentView addSubview:self.likeButton];
            [self.likeButton setFrame:CGRectMake(7+10, 330.5f, 25.5f, 25.5f)];//7
            
            [self.likeButton setImage:[UIImage imageNamed:@"homescreen_like_btn_off.png"] forState:UIControlStateNormal];
            [self.likeButton setImage:[UIImage imageNamed:@"homescreen_like_btn_on.png"] forState:UIControlStateSelected];
            [self.likeButton addTarget:self action:@selector(didTapLikePhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            // [self.likeButton setSelected:NO];
        }
        
        
        if (!commentLabel) {
            
            //commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(95+20, 332.5f, 30, 20)];
            commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(22+8, 330.5f, 30, 25.5)];
            [Helper setToLabel:commentLabel Text:@"" WithFont:LATO_LIGHT FSize:14 Color:[UIColor colorWithWhite:0.402 alpha:1.000]];
            commentLabel.textAlignment = NSTextAlignmentRight;
            commentLabel.backgroundColor = CLEAR_COLOR;
            [self.contentView addSubview:self.commentLabel];
        }
        
        if (!commentButton) {
            // comments button
            commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.contentView addSubview:self.commentButton];
            //[self.commentButton setFrame:CGRectMake( 87+20, 330.5f, 25.5f, 25.5f)];
            [self.commentButton setFrame:CGRectMake(7+10, 330.5f, 25.5f, 25.5f)];
            [self.commentButton setBackgroundColor:[UIColor clearColor]];
            [self.commentButton setTitle:@"" forState:UIControlStateNormal];
            [self.commentButton setTitleColor:[UIColor colorWithRed:0.369f green:0.271f blue:0.176f alpha:1.0f] forState:UIControlStateNormal];
            [self.commentButton setTitleShadowColor:[UIColor colorWithWhite:1.0f alpha:0.750f] forState:UIControlStateNormal];
            [self.commentButton setTitleEdgeInsets:UIEdgeInsetsMake( -4.0f, 0.0f, 0.0f, 0.0f)];
            [[self.commentButton titleLabel] setShadowOffset:CGSizeMake( 0.0f, 1.0f)];
            [[self.commentButton titleLabel] setFont:[UIFont systemFontOfSize:12.0f]];
            // [[self.commentButton titleLabel] setMinimumFontSize:11.0f];
            [[self.commentButton titleLabel] setAdjustsFontSizeToFitWidth:YES];
            [self.commentButton setBackgroundImage:[UIImage imageNamed:@"homescreen_comment_btn_off"] forState:UIControlStateNormal];
            [self.commentButton setBackgroundImage:[UIImage imageNamed:@"homescreen_comment__btn_on"] forState:UIControlStateHighlighted];
            [self.commentButton addTarget:self action:@selector(buttonclickedComment:) forControlEvents:UIControlEventTouchUpInside];
            [self.commentButton setSelected:NO];
        }
        
        
        if (!ShareButton) {
            // comments button
            ShareButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.contentView addSubview:self.ShareButton];
            [self.ShareButton setFrame:CGRectMake( 167+30 , 330.5f, 25.5f, 25.5f)];
            [self.ShareButton setBackgroundColor:[UIColor clearColor]];
            [self.ShareButton setTitle:@"" forState:UIControlStateNormal];
            [self.ShareButton setTitleColor:[UIColor colorWithRed:0.369f green:0.271f blue:0.176f alpha:1.0f] forState:UIControlStateNormal];
            [self.ShareButton setTitleShadowColor:[UIColor colorWithWhite:1.0f alpha:0.750f] forState:UIControlStateNormal];
            [self.ShareButton setTitleEdgeInsets:UIEdgeInsetsMake( -4.0f, 0.0f, 0.0f, 0.0f)];
            [[self.ShareButton titleLabel] setShadowOffset:CGSizeMake( 0.0f, 1.0f)];
            [[self.ShareButton titleLabel] setFont:[UIFont systemFontOfSize:12.0f]];
            // [[self.commentButton titleLabel] setMinimumFontSize:11.0f];
            [[self.ShareButton titleLabel] setAdjustsFontSizeToFitWidth:YES];
            [self.ShareButton setBackgroundImage:[UIImage imageNamed:@"homescreen_share_btn_off.png"] forState:UIControlStateNormal];
            [self.ShareButton setBackgroundImage:[UIImage imageNamed:@"homescreen_share_btn_on.png"] forState:UIControlStateHighlighted];
            
            
            [self.ShareButton addTarget:self action:@selector(buttonclickedShare:) forControlEvents:UIControlEventTouchUpInside];
            [self.ShareButton setSelected:NO];
        }
        
        if (!moreButton) {
            // This is the user's display name, on a button so that we can tap on it
            self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.contentView addSubview:self.moreButton];
            [self.moreButton setFrame:CGRectMake(247+20,330.5f,  25.5f, 25.5f)];
            
            [self.moreButton addTarget:self action:@selector(buttonclickedMore:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [self.moreButton setBackgroundImage:[UIImage imageNamed:@"homescreen_more_btn_off.png"] forState:UIControlStateNormal];
            [self.moreButton setBackgroundImage:[UIImage imageNamed:@"homescreen_more_btn_on.png"] forState:UIControlStateHighlighted];
        }
        
        //commentview
        
        commentview = [[UIView alloc] initWithFrame:CGRectMake(5, 330, 310, 90)];//5, 355, 310, 90
        commentview.backgroundColor = [UIColor clearColor];
        //commentview.backgroundColor = [UIColor yellowColor];
        [self.contentView addSubview:commentview];
        
        
        //comment labels
        labelCommentOne = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(0, 0, 310, 20)];
        labelCommentOne.numberOfLines = 0;
        labelCommentOne.linkColor = UIColorFromRGB(0x2d5892);
        labelCommentOne.linkUnderlineStyle = kCTUnderlineStyleNone;
        labelCommentOne.font = [UIFont systemFontOfSize:13];
        labelCommentOne.lineBreakMode = NSLineBreakByWordWrapping;
        labelCommentOne.delegate = self;
        labelCommentOne.backgroundColor = [UIColor clearColor];
        [commentview addSubview:labelCommentOne];
        [labelCommentOne sizeToFit];
        
        labelCommentTwo = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(0, 20, 310, 20)];
        labelCommentTwo.numberOfLines = 0;
        labelCommentTwo.linkColor = UIColorFromRGB(0x2d5892);
        labelCommentTwo.linkUnderlineStyle = kCTUnderlineStyleNone;
        labelCommentTwo.font = [UIFont systemFontOfSize:13];
        labelCommentTwo.backgroundColor = [UIColor clearColor];
        labelCommentTwo.lineBreakMode = NSLineBreakByWordWrapping;
        labelCommentTwo.delegate = self;
        [commentview addSubview:labelCommentTwo];
        [labelCommentTwo sizeToFit];
        
        
        labelCommentThree = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(0, 60, 310, 20)];
        labelCommentThree.numberOfLines = 0;
        labelCommentThree.linkColor = UIColorFromRGB(0x2d5892);
        labelCommentThree.linkUnderlineStyle = kCTUnderlineStyleNone;
        labelCommentThree.font = [UIFont systemFontOfSize:13];
        labelCommentThree.lineBreakMode = NSLineBreakByWordWrapping;
        labelCommentThree.delegate = self;
        [commentview addSubview:labelCommentThree];
        labelCommentThree.backgroundColor = [UIColor clearColor];
        [labelCommentThree sizeToFit];
        
        
        buttonUserOne = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonUserOne.backgroundColor = [UIColor whiteColor];
        buttonUserOne.titleLabel.font = [UIFont boldSystemFontOfSize:13];
        [buttonUserOne setTitleColor:UIColorFromRGB(0x2d5892) forState:UIControlStateNormal];
        [buttonUserOne setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [buttonUserOne addTarget:self action:@selector(didTapUserButton:) forControlEvents:UIControlEventTouchUpInside];
        [commentview addSubview:buttonUserOne];
        
        buttonUserTwo = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonUserTwo.backgroundColor = [UIColor whiteColor];
        buttonUserTwo.titleLabel.font = [UIFont boldSystemFontOfSize:13];
        [buttonUserTwo setTitleColor:UIColorFromRGB(0x2d5892) forState:UIControlStateNormal];
        [buttonUserTwo setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [buttonUserTwo addTarget:self action:@selector(didTapUserButton:) forControlEvents:UIControlEventTouchUpInside];
        [commentview addSubview:buttonUserTwo];
        
        buttonUserThree = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonUserThree.backgroundColor = [UIColor whiteColor];
        buttonUserThree.titleLabel.font = [UIFont boldSystemFontOfSize:13];
        [buttonUserThree setTitleColor:UIColorFromRGB(0x2d5892) forState:UIControlStateNormal];
        [buttonUserThree setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [buttonUserThree addTarget:self action:@selector(didTapUserButton:) forControlEvents:UIControlEventTouchUpInside];
        [commentview addSubview:buttonUserThree];
        
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicator.frame = CGRectMake(320/2 - 20, 320/2 - 20, 40, 40);
        _activityIndicator.hidden = NO;
        _activityIndicator.color = [UIColor redColor];
        [self.activityIndicator startAnimating];
        self.activityIndicator.hidesWhenStopped = YES;
        [self.contentView addSubview:_activityIndicator];
        
        
        UIImage *btnPlayImage = [UIImage imageNamed:@"play_button.png"];
        self.btnPlay = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnPlay.frame = CGRectMake(280/2 - btnPlayImage.size.width/2 + 20, 280/2 - btnPlayImage.size.height/2, btnPlayImage.size.width, btnPlayImage.size.height);
        
        [self.btnPlay setBackgroundImage:btnPlayImage forState:UIControlStateNormal];
        [self.contentView addSubview:btnPlay];
        [self.contentView bringSubviewToFront:self.btnPlay];
        [self.btnPlay addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView bringSubviewToFront:self.imageView];
        [self.contentView bringSubviewToFront:self.labelDescrepiton];
        [self.contentView bringSubviewToFront:self.btnPlay];
        [self.contentView bringSubviewToFront:self.activityIndicator];
        // [self.labelDescrepitonBG bringSubviewToFront:imgline];
    }
    
    return self;
}


#pragma mark - UIView

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake( 0.0f, 0.0f, 320.0f, 320.0f);
    //self.photoButton.frame = CGRectMake( 5.0f, 0.0f, 310.0f, 310.0f);
    //self.labelDescrepiton.frame = CGRectMake(5,375, 310, 35);
    
    
    //comment one
    //CGSize nameSize = [self.buttonUserOne.titleLabel.text sizeWithFont:[UIFont systemFontOfSize:13] forWidth:210 lineBreakMode:NSLineBreakByTruncatingTail];
    
    CGSize nameSize = [self.buttonUserOne.titleLabel.text boundingRectWithSize:CGSizeMake(200, 18)
                                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                                    attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:13]}
                                                                       context:nil].size;
    
    [self.buttonUserOne setFrame:CGRectMake(0, 0, nameSize.width, nameSize.height)];
    
    CGSize  contentSize =  [self.labelCommentOne sizeThatFits:CGSizeMake(310,CGFLOAT_MAX)];
    self.labelCommentOne.frame = CGRectMake(0, 0, 310, contentSize.height);
    
    //comment two
    nameSize = [self.buttonUserTwo.titleLabel.text boundingRectWithSize:CGSizeMake(200, 18)
                                                                options:NSStringDrawingUsesLineFragmentOrigin
                                                             attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:13]}
                                                                context:nil].size;;
    
    [self.buttonUserTwo setFrame:CGRectMake(0,self.labelCommentOne.frame.size.height+self.labelCommentOne.frame.origin.y, nameSize.width, nameSize.height)];
    
    contentSize =  [self.labelCommentTwo sizeThatFits:CGSizeMake(310,CGFLOAT_MAX)];
    self.labelCommentTwo.frame = CGRectMake(0, self.labelCommentOne.frame.size.height+self.labelCommentOne.frame.origin.y, 310, contentSize.height);
    
    
    //comment three
    nameSize  = [self.buttonUserThree.titleLabel.text boundingRectWithSize:CGSizeMake(200, 18)
                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:13]}
                                                                   context:nil].size;;
    
    
    
    [self.buttonUserThree setFrame:CGRectMake(0,self.labelCommentTwo.frame.size.height+self.labelCommentTwo.frame.origin.y, nameSize.width, nameSize.height)];
    
    contentSize =  [self.labelCommentThree sizeThatFits:CGSizeMake(310,CGFLOAT_MAX)];
    self.labelCommentThree.frame = CGRectMake(0,self.labelCommentTwo.frame.size.height+self.labelCommentTwo.frame.origin.y, 310, contentSize.height);
    
    
    CGRect frame = commentview.frame;
    frame.size.height = CGRectGetMaxY(self.labelCommentThree.frame);
    commentview.frame = frame;
    
    
    frame = self.likeButton.frame;
    frame.origin.y = CGRectGetMaxY(self.commentview.frame);
    self.likeButton.frame = frame;
    
    frame = self.likeLabel.frame;
    frame.origin.y = CGRectGetMaxY(self.commentview.frame);
    self.likeLabel.frame = frame;
    
    frame = self.commentButton.frame;
    frame.origin.y = CGRectGetMaxY(self.commentview.frame);
    self.commentButton.frame = frame;
    
    frame = self.commentLabel.frame;
    frame.origin.y = CGRectGetMaxY(self.commentview.frame)+2.5;
    self.commentLabel.frame = frame;
    
    frame = self.ShareButton.frame;
    frame.origin.y = CGRectGetMaxY(self.commentview.frame);
    self.ShareButton.frame = frame;
    
    frame = self.moreButton.frame;
    frame.origin.y = CGRectGetMaxY(self.commentview.frame);
    self.moreButton.frame = frame;
    
    
    
    
}
-(void)playVideo:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(photoCell:didTapOnPlayButton:)]) {
        [delegate photoCell:self didTapOnPlayButton:self.photo];
    }
    
}
-(void)cretePlayer {
    
    if ([[self.photo objectForKey:kPAPPhotoMediaTypeKey]isEqualToString:kPAPPhotoMediaVideoKey ] && [self.photo objectForKey:kPAPPhotoVidoKey] != nil) { //Video
        
        //        UIImage *btnPlayImage = [UIImage imageNamed:@"play_button.png"];
        //        self.btnPlay = [UIButton buttonWithType:UIButtonTypeCustom];
        //        self.btnPlay.frame = CGRectMake(mainImageWidth/2 - btnPlayImage.size.width/2 + mainImageX, mainImageHeight/2 - btnPlayImage.size.height/2 +mainImageY, btnPlayImage.size.width, btnPlayImage.size.height);
        //        [self.btnPlay addTarget:self action:@selector(btnPlayAction:) forControlEvents:UIControlEventTouchUpInside];
        //        [self.btnPlay setBackgroundImage:btnPlayImage forState:UIControlStateNormal];
        //        [self addSubview:btnPlay];
        
        
        PFFile *theImage = [self.photo objectForKey:@"videoFile"];
        NSData *videoData = [theImage getData];
        
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init] ;
        [dateFormat setDateFormat:@"dd-MM-yyyy||HH:mm:SS"];
        NSDate *now = [[NSDate alloc] init] ;
        NSString *theDate = [dateFormat stringFromDate:now];
        
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"Default Album"];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
        
        NSString *videoPath = [[NSString alloc] initWithString:[NSString stringWithFormat:@"%@/%@.mov",documentsDirectory,theDate]] ;
        
        [videoData writeToFile:videoPath atomically:YES];
        
        //if (success) {
        VideoPlayerViewController *player = [[VideoPlayerViewController alloc] init];
        player.view.frame = self.imageView.frame;
        player.URL = [NSURL URLWithString:videoPath];
        [self.imageView addSubview:player.view];
        self.videoPlayer = player;
        player = nil;
        // }
        
        
        
    }
}


- (void)setLikeStatus:(BOOL)liked {
    
    [self.likeButton setSelected:liked];
    
    //    if (liked) {
    //        [self.likeButton setTitle:@"Liked" forState:UIControlStateSelected];
    //        self.likeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 23, 0, 0);
    //
    //    } else {
    //        [self.likeButton setTitle:@"Like" forState:UIControlStateSelected];
    //        self.likeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0);
    //    }
}

-(void)setComments:(NSArray*)objects Users:(NSArray*)users {
    
    
    labelCommentTwo.text = @"";
    labelCommentOne.text = @"";
    labelCommentThree.text = @"";
    [buttonUserThree setTitle:@"" forState:UIControlStateNormal];
    [buttonUserTwo setTitle:@"" forState:UIControlStateNormal];
    [buttonUserOne setTitle:@"" forState:UIControlStateNormal];
    
    
    [buttonUserOne setHidden:YES];
    [buttonUserTwo setHidden:YES];
    [buttonUserThree setHidden:YES];
    
    if (objects.count > 0) {
        
        buttonUserOne.hidden = NO;
        [buttonUserOne setTitle:users[0] forState:UIControlStateNormal];
        CGSize nameSize = [self.buttonUserOne.titleLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:13] forWidth:200 lineBreakMode:NSLineBreakByTruncatingTail];
        
        
        NSString *str = [NSString stringWithFormat:@"\"%@\" ",objects[0]];
        NSString *paddedString = [PAPPhotoCell padString:str username:users[0]];
        
        /*NSString *paddedString = [PAPPhotoCell padString:objects[0] username:users[0]];
         NSString *newOutput = [NSString stringWithFormat:@"\"%@\" ",paddedString];
         self.labelCommentOne.text = newOutput;*/
        self.labelCommentOne.text = paddedString;
        
        [self configureMentionLabel:labelCommentOne username:users[0]];
        
        if (objects.count > 1) {
            
            buttonUserTwo.hidden = NO;
            [buttonUserTwo setTitle:users[1] forState:UIControlStateNormal];
            CGSize nameSize = [self.buttonUserTwo.titleLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:13] forWidth:200 lineBreakMode:NSLineBreakByTruncatingTail];
            NSString *str = [NSString stringWithFormat:@"\"%@\" ",objects[1]];
            NSString *paddedString = [PAPPhotoCell padString:str username:users[1]];
            // NSString *paddedString = [PAPPhotoCell padString:objects[1] username:users[1]];
            
            labelCommentTwo.text = paddedString;
            [self configureMentionLabel:labelCommentTwo username:users[1]];
            
        }
        
        if (objects.count > 2) {
            
            buttonUserThree.hidden = NO;
            [buttonUserThree setTitle:users[2] forState:UIControlStateNormal];
            CGSize nameSize = [self.buttonUserThree.titleLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:13] forWidth:200 lineBreakMode:NSLineBreakByTruncatingTail];
            NSString *str = [NSString stringWithFormat:@"\"%@\" ",objects[2]];
            NSString *paddedString = [PAPPhotoCell padString:str username:users[2]];
            //NSString *paddedString = [PAPPhotoCell padString:objects[2] username:users[2]];
            //self.labelCommentThree.adjustsLetterSpacingToFitWidth = YES;
            
            
            //            NSAttributedString *attributedString =
            //            [[NSAttributedString alloc]
            //             initWithString:paddedString
            //             attributes:
            //             @{
            //
            //               NSKernAttributeName : @(-1.5f)
            //               }];
            
            
            self.labelCommentThree.text = paddedString;
            [self configureMentionLabel:labelCommentThree username:users[2]];
        }
    }
    
    
    [self setNeedsDisplay];
}

- (void)didTapLikePhotoButtonAction:(UIButton *)button {
    if (delegate && [delegate respondsToSelector:@selector(photoCell:didTapLikePhotoButton:photo:)]) {
        [delegate photoCell:self didTapLikePhotoButton:button photo:self.photo];
    }
}

-(void)didTapUserButton:(UIButton*)button{
    if (delegate && [delegate respondsToSelector:@selector(cell:didTapUserButton:)]) {
        [delegate cell:self didTapUserButton:button.titleLabel.text];
    }
}
-(void)buttonclickedMore:(UIButton*)sender{
    if (delegate && [delegate respondsToSelector:@selector(cell:didTapMore:user:)]) {
        [delegate cell:self didTapMore:sender user:self.photo];
    }
}
-(void)buttonclickedShare:(UIButton*)sender{
    if (delegate && [delegate respondsToSelector:@selector(cell:didTapShareButton:user:)]) {
        [delegate cell:self didTapShareButton:sender user:self.photo];
    }
}
-(void)buttonclickedComment:(UIButton*)sender{
    if (delegate && [delegate respondsToSelector:@selector(cell:didTapComment:user:)]) {
        [delegate cell:self didTapComment:sender user:self.photo];
    }
}
/* Static helper to get the height for a cell if it had the given name, content and horizontal inset */
+ (CGFloat)heightForCellWithName:(NSString *)name contentString:(NSString *)content cellInsetWidth:(CGFloat)cellInset {
    
    CGSize nameSize = [name sizeWithFont:[UIFont boldSystemFontOfSize:13] forWidth:200 lineBreakMode:NSLineBreakByTruncatingTail];
    NSString *paddedString = [PAPPhotoCell padString:content withFont:[UIFont systemFontOfSize:13] toWidth:nameSize.width];
    //CGFloat horizontalTextSpace = [PAPBaseTextCell horizontalTextSpaceForInsetWidth:cellInset];
    
    CGSize contentSize = [paddedString sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(310, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    
    // CGFloat singleLineHeight = [@"test" sizeWithFont:[UIFont systemFontOfSize:13]].height;
    
    // Calculate the added height necessary for multiline text. Ensure value is not below 0.
    // CGFloat multilineHeightAddition = contentSize.height + nameSize.height + singleLineHeight;
    
    return  contentSize.height;
}



/* Static helper to pad a string with spaces to a given beginning offset */
+ (NSString *)padString:(NSString *)string withFont:(UIFont *)font toWidth:(CGFloat)width {
    // Find number of spaces to pad
    
    NSString *trimmedComment = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSMutableString *paddedString = [[NSMutableString alloc] initWithString:@"tony"];
    //    while (true) {
    //        [paddedString appendString:@" "];
    //        if ([paddedString sizeWithFont:font].width >= width) {
    //            break;
    //        }
    //    }
    
    
    NSLog(@"pad %lu",(unsigned long)paddedString.length);
    
    // Add final spaces to be ready for first word
    [paddedString appendString:[NSString stringWithFormat:@" %@",trimmedComment]];
    return paddedString;
}
+ (NSString *)padString:(NSString *)string username:(NSString*)username{
    // Find number of spaces to pad
    
    //NSString *trimmedComment = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSMutableString *paddedString = [[NSMutableString alloc] initWithString:username];
    
    
    // Add final spaces to be ready for first word
    [paddedString appendString:[NSString stringWithFormat:@"  %@",string]];
    return paddedString;
}



#pragma OHAttributedLabel

-(void)configureMentionLabel:(OHAttributedLabel*)label username:(NSString*)username
{
    // Detect all "@xxx" mention-like strings using the "@\w+" regular expression
    NSRegularExpression* userRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B@\\w+" options:0 error:nil];
    
    NSMutableAttributedString* mas = [label.attributedText mutableCopy];
    
    
    [mas setTextColor:[UIColor whiteColor] range:NSMakeRange(0,username.length)];
    
    [userRegex enumerateMatchesInString:label.text options:0 range:NSMakeRange(0,label.text.length)
                             usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
     {
         // For each "@xxx" user mention found, add a custom link:
         NSString *taggedUser = [[label.text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
         NSString* linkURLString = [NSString stringWithFormat:@"user:%@",taggedUser]; // build the "user:" link
         [mas setLink:[NSURL URLWithString:linkURLString] range:match.range]; // add it
     }];
    
    NSRegularExpression* hashRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B#\\w+" options:0 error:nil];
    //NSMutableAttributedString* mas = [label.attributedText mutableCopy];
    [hashRegex enumerateMatchesInString:label.text options:0 range:NSMakeRange(0,label.text.length)
                             usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
     {
         // For each "@xxx" user mention found, add a custom link:
         NSString *hashTag = [[label.text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
         NSString* linkURLString = [NSString stringWithFormat:@"hash:%@",hashTag]; // build the "user:" link
         [mas setLink:[NSURL URLWithString:linkURLString] range:match.range]; // add it
     }];
    
    OHParagraphStyle* para = [OHParagraphStyle defaultParagraphStyle];
    //    para.firstLineHeadIndent = 30;
    //    para.headIndent = 5;
    //    para.tailIndent = -5;
    para.textAlignment = kCTTextAlignmentJustified;
    [mas setParagraphStyle:para];
    [OHASBasicMarkupParser processMarkupInAttributedString:mas];
    
    label.attributedText = mas;
    label.centerVertically = YES;
}
-(BOOL)attributedLabel:(OHAttributedLabel *)attributedLabel shouldFollowLink:(NSTextCheckingResult *)linkInfo
{
    if ([[linkInfo.URL scheme] isEqualToString:@"user"])
    {
        // We use this arbitrary URL scheme to handle custom actions
        // So URLs like "user:xxx" will be handled here instead of opening in Safari.
        // Note: in the above example, "xxx" is the 'resourceSpecifier' part of the URL
        NSString* taggeduser = [linkInfo.URL resourceSpecifier];
        
        // Display some message according to the user name clicked
        if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapTaggedUser:)]) {
            [self.delegate cell:self didTapTaggedUser:taggeduser];
        }
        
        
        // Prevent the URL from opening in Safari, as we handled it here manually instead
        return NO;
    }
    else if ([[linkInfo.URL scheme] isEqualToString:@"hash"]) {
        NSString* hash = [linkInfo.URL resourceSpecifier];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapHashTag:)]) {
            [self.delegate cell:self didTapHashTag:hash];
        }
        return NO;
    }
    else
    {
        if ([[UIApplication sharedApplication] canOpenURL:linkInfo.extendedURL])
        {
            // Execute the default behavior, which is opening the URL in Safari for URLs, starting a call for phone numbers, ...
            return YES;
        }
        else
        {
            //            [UIAlertView showWithTitle:@"Tap on link" message:[NSString stringWithFormat:@"Should open link %@", linkInfo.extendedURL]];
            return NO;
        }
    }
}


@end
