//
//  PAPPhotoCell.h
//  Anypic
//
//  Created by Héctor Ramos on 5/3/12.
//

@class PFImageView;
@class STTweetLabel;
@class VideoPlayerViewController;
@class OHAttributedLabel;
@protocol PAPPhotoCellViewDelegate;
@interface PAPPhotoCell : PFTableViewCell

@property (nonatomic, strong) UIButton *photoButton;
@property (nonatomic, strong) UIView *labelDescrepitonBG;
@property (nonatomic, strong) STTweetLabel *labelDescrepiton;
@property (nonatomic, strong) UIImageView *mediaIndicator;
@property (nonatomic, strong) UILabel *lblCategory;
@property (nonatomic, retain) VideoPlayerViewController *videoPlayer;
@property (nonatomic,strong) UIButton *btnPlay;
@property (nonatomic,weak) id <PAPPhotoCellViewDelegate> delegate;
@property (nonatomic,strong) PFObject *photo;
@property (nonatomic,strong) PFUser *user;

//Rakesh Added
@property (nonatomic,strong) UIButton *likeButton;
@property (nonatomic,strong) UIButton *commentButton;
@property (nonatomic,strong) UIButton *ShareButton;
@property (nonatomic,strong) UIButton *moreButton;


@property (nonatomic,strong) UILabel *likeLabel;
@property (nonatomic,strong) UILabel *commentLabel;
@property (nonatomic,strong) UILabel *lblShare;
@property (nonatomic,strong) UILabel *lblMore;

@property (nonatomic,strong) OHAttributedLabel *labelCommentOne;
@property (nonatomic,strong) OHAttributedLabel *labelCommentTwo;
@property (nonatomic,strong) OHAttributedLabel *labelCommentThree;

@property (nonatomic,strong) UIButton *buttonUserOne;
@property (nonatomic,strong) UIButton *buttonUserTwo;
@property (nonatomic,strong) UIButton *buttonUserThree;
@property(nonatomic,strong)  UIView *commentview;
@property(nonatomic,strong) UIImageView *imgline;
@property(nonatomic,strong) AVPlayer *vPlayer;
@property(nonatomic,strong) UIActivityIndicatorView *activityIndicator;


- (void)setLikeStatus:(BOOL)liked;
-(void)setComments:(NSArray*)objects Users:(NSArray*)users;


+ (CGFloat)heightForCellWithName:(NSString *)name contentString:(NSString *)content cellInsetWidth:(CGFloat)cellInset;



@end
@protocol PAPPhotoCellViewDelegate <NSObject>
@optional


-(void)photoCell:(PAPPhotoCell*)photoCell didTapOnPlayButton:(PFObject*)photo;

- (void)photoCell:(PAPPhotoCell *)photoHeaderView didTapLikePhotoButton:(UIButton *)button photo:(PFObject *)photo;

- (void)cell:(PAPPhotoCell *)cellView didTapUserButton:(NSString *)aUser;

- (void)cell:(PAPPhotoCell *)cellView didTapTaggedUser:(NSString*)user;

- (void)cell:(PAPPhotoCell *)cellView didTapHashTag:(NSString*)hashTag;

- (void)cell:(PAPPhotoCell *)cell didTapComment:(UIButton *)button user:(PFObject *)photo;

- (void)cell:(PAPPhotoCell *)cell didTapShareButton:(UIButton *)button user:(PFObject *)photo;

- (void)cell:(PAPPhotoCell *)cell didTapMore:(UIButton *)button user:(PFObject *)photo;

@end