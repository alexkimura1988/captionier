//
//  PAPBaseTextCell.m
//  Anypic
//
//  Created by Mattieu Gamache-Asselin on 5/15/12.
//

#import "PAPBaseTextCell.h"
#import "TTTTimeIntervalFormatter.h"
#import "PAPProfileImageView.h"
#import "PAPUtility.h"
#import "NSAttributedString+Attributes.h"
#import "OHASBasicMarkupParser.h"
#import "OHAttributedLabel.h"
#import "UILabel+DynamicHeight.h"

static TTTTimeIntervalFormatter *timeFormatter;

@interface PAPBaseTextCell ()<OHAttributedLabelDelegate> {
    BOOL hideSeparator; // True if the separator shouldn't be shown
}
//@property (nonatomic, strong) UIButton *nameButton;
/* Private static helper to obtain the horizontal space left for name and content after taking the inset and image in consideration */
+ (CGFloat)horizontalTextSpaceForInsetWidth:(CGFloat)insetWidth;
@end

@implementation PAPBaseTextCell

@synthesize mainView;
@synthesize cellInsetWidth;
@synthesize avatarImageView;
@synthesize avatarImageButton;
@synthesize nameButton;
@synthesize logo;
@synthesize contentLabel;
@synthesize timeLabel;
@synthesize separatorImage;
@synthesize delegate;
@synthesize user;
@synthesize photo;
@synthesize upvoteBtn;
@synthesize upvoteBtnLbl;


#pragma mark - NSObject

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];

    if (self) {
        // Initialization code
        if (!timeFormatter) {
            timeFormatter = [[TTTTimeIntervalFormatter alloc] init];
        }

        cellInsetWidth = 0.0f;
        hideSeparator = NO;
        self.clipsToBounds = YES;
        horizontalTextSpace =  [PAPBaseTextCell horizontalTextSpaceForInsetWidth:cellInsetWidth];
        
        self.opaque = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        self.backgroundColor = [UIColor clearColor];
        
        mainView = [[UIView alloc] initWithFrame:self.contentView.frame];
        [mainView setBackgroundColor:[UIColor whiteColor]];
        NSLog(@"weidth %f",mainView.frame.size.width);
        
        self.avatarImageView = [[PAPProfileImageView alloc] init];
//        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.width/2;
//        self.avatarImageView.clipsToBounds = YES;
//        [self.avatarImageView setClipsToBounds:YES];
        self.avatarImageView.layer.borderWidth = 2;
        self.avatarImageView.layer.borderColor =[UIColor colorWithWhite:0.705 alpha:1.0000].CGColor;
        [self.avatarImageView setBackgroundColor:[UIColor clearColor]];
        [self.avatarImageView setOpaque:YES];
        [mainView addSubview:self.avatarImageView];
        
        
        
//        self.logo = [[UIImageView alloc] init];
//        [self.logo setFrame:CGRectMake(255-33,vertBorderSpacing+5, 15,15.0f)];//53.0f,36, 15,15.0f
//        self.logo.image = [UIImage imageNamed:@"home_time"];
//        [self.mainView addSubview:self.logo];
        
        
        
                
        nameButton = [UIButton buttonWithType:UIButtonTypeCustom];
        nameButton.frame = CGRectMake(avatarDim+horiBorderSpacing, avatarY, 275, 20);
        [nameButton setBackgroundColor:[UIColor whiteColor]];
        nameButton.titleLabel.font = [UIFont fontWithName:robo_bold size:16];
        [nameButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [nameButton setTitleColor:[UIColor colorWithRed:0.341 green:0.832 blue:0.979 alpha:1.000] forState:UIControlStateHighlighted];
     
        [self.nameButton addTarget:self action:@selector(didTapUserButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        //[self.nameButton setTitle:@"surender" forState:UIControlStateNormal];
        [mainView addSubview:nameButton];
        
        contentLabel = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(avatarDim+horiBorderSpacing+25, avatarY+20+5, 270, 20)];
        contentLabel.backgroundColor = WHITE_COLOR;
        contentLabel.numberOfLines = 0;
        contentLabel.linkColor = UIColorFromRGB(0x2d5892);;
        contentLabel.linkUnderlineStyle = kCTUnderlineStyleNone;
        //contentLabel.font = [UIFont systemFontOfSize:10.5f];
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [Helper setToLabel:contentLabel Text:@"" WithFont:robo FSize:12 Color:[UIColor colorWithRed:142/255.0f green:145/255.0f blue:151/255.0f alpha:1.0f]];
       
        
        
        contentLabel.delegate = self;
        [mainView addSubview:contentLabel];
        //[contentLabel sizeToFit];
        
        
        self.timeLabel = [[UILabel alloc] init];
        [self.timeLabel setFont:[UIFont systemFontOfSize:11]];
        [self.timeLabel setTextColor:[UIColor grayColor]];
        [self.timeLabel setBackgroundColor:[UIColor clearColor]];
        [self.timeLabel setShadowColor:[UIColor colorWithWhite:1.0f alpha:0.70f]];
        [self.timeLabel setShadowOffset:CGSizeMake(0, 1)];
        [mainView addSubview:self.timeLabel];
        
        
        
        
        self.upvoteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //[self.upvoteBtn setBackgroundColor:[UIColor greenColor]];
        [self.upvoteBtn setBackgroundImage:[UIImage imageNamed:@"upvote_icon_on@2x.png"] forState:UIControlStateNormal];
        [self.upvoteBtn setBackgroundImage:[UIImage imageNamed:@"upvote_icon_on@2x.png"] forState:UIControlStateSelected];
        [self.upvoteBtn setBackgroundImage:[UIImage imageNamed:@"upvote_icon_off@2x.png"] forState:UIControlStateHighlighted];
        
        //[self.upvoteBtn setb]
        
        [self.upvoteBtn addTarget:self action:@selector(didTapUPvoteButton:) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:self.upvoteBtn];
        
        self.upvoteBtnLbl = [[UILabel alloc] init];
        //[self.upvoteBtnLbl setBackgroundColor:[UIColor yellowColor]];
        self.upvoteBtnLbl.textAlignment = NSTextAlignmentRight;
        [mainView addSubview:self.upvoteBtnLbl];
        
        
        self.avatarImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.avatarImageButton setBackgroundColor:[UIColor clearColor]];
        [self.avatarImageButton addTarget:self action:@selector(didTapUserButtonAction:) forControlEvents:UIControlEventTouchUpInside];

        [mainView addSubview:self.avatarImageButton];
        
        self.separatorImage = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"profile_screen_filter_line-568h"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 1, 0, .5)]];
        [mainView addSubview:separatorImage];
        
        
        self.logo = [[UIImageView alloc] init];
        self.logo.image = [UIImage imageNamed:@"home_time"];
        [self.mainView addSubview:self.logo];
        
        
        [self.contentView addSubview:mainView];
    }
    
    return self;
}


#pragma mark - UIView

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    [mainView setFrame:CGRectMake(0, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.contentView.frame.size.height)];
    
    // Layout avatar image
    //[self.avatarImageView setFrame:CGRectMake(avatarX, avatarY, avatarDim, avatarDim)];
    [self.avatarImageView setFrame:CGRectMake(8.0f,11.0f,45.0f,45.0f)];
//    self.upvoteBtn.frame = CGRectMake( ([UIScreen mainScreen].bounds.size.width)-80 , 28.0f, 30.0f, 30.0f);
//    self.upvoteBtnLbl.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width)-45, 28.0f, 50.0f, 30.0f);
    
    //self.avatarImageView.frame = CGRectMake( 5.0f, 11.0f, 45.0f, 45.0f);
    self.avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width/2;
    self.avatarImageView.clipsToBounds = YES;
    [self.avatarImageView setClipsToBounds:YES];
    

    [self.avatarImageButton setFrame:CGRectMake(avatarX, avatarY, avatarDim, avatarDim)];
    [nameButton setTitle:[self.user objectForKey:kPAPUserDisplayNameKey] forState:UIControlStateNormal];
    //NSLog(@"user %@",nameButton.titleLabel.text);
    // Layout the name button
    CGSize nameSize = [nameButton.titleLabel.text sizeWithFont:[UIFont fontWithName:robo_bold size:16] forWidth:275 lineBreakMode:NSLineBreakByTruncatingTail];
    [nameButton setFrame:CGRectMake(timeX+15, vertBorderSpacing, nameSize.width, nameSize.height)];
    
    CGSize  contentSize =  [self.contentLabel sizeThatFits:CGSizeMake(250,CGFLOAT_MAX)];
    self.contentLabel.frame = CGRectMake(timeX+15, vertBorderSpacing+nameSize.height, 250, contentSize.height);
    
    self.upvoteBtn.frame = CGRectMake( ([UIScreen mainScreen].bounds.size.width)-40 , 22.0f+(contentLabel.bounds.size.height), 30.0f, 30.0f);//-80
    self.upvoteBtnLbl.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width)-90, 24.0f+(contentLabel.bounds.size.height), 50.0f, 30.0f);//-45
    
    CGSize timeSize = [self.timeLabel.text sizeWithFont:[UIFont fontWithName:robo_light size:10.5f] forWidth:horizontalTextSpace lineBreakMode:NSLineBreakByTruncatingTail];
    [self.timeLabel setFrame:CGRectMake(236+40,vertBorderSpacing+5, 100, timeSize.height)];
    //[self.timeLabel setFrame:CGRectMake(timeX+8, contentLabel.frame.origin.y + contentLabel.frame.size.height + 5, timeSize.width, timeSize.height)];
    //[Helper setToLabel:timeLabel Text:@"" WithFont:robo_light FSize:10.5f Color:BLACK_COLOR];
    
    nameButton.backgroundColor = [UIColor clearColor];
    contentLabel.backgroundColor = [UIColor clearColor];
    timeLabel.backgroundColor = [UIColor clearColor];
    mainView.backgroundColor = [UIColor clearColor];
    
    // Layour separator
    [self.separatorImage setFrame:CGRectMake(0, self.frame.size.height-2, self.frame.size.width, 2)];
    [self.separatorImage setHidden:hideSeparator];
    
   
    [self.logo setFrame:CGRectMake(218+40                                                                                        ,vertBorderSpacing+5, 15,15.0f)];//53.0f,36, 15,15.0f
    
    
}

- (void)drawRect:(CGRect)rect {
    // Add a drop shadow in core graphics on the sides of the cell
    [super drawRect:rect];

}


#pragma mark - Delegate methods

/* Inform delegate that a user image or name was tapped */
- (void)didTapUserButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapUserButton:)]) {
        [self.delegate cell:self didTapUserButton:self.user];
    }    
}

- (void)didTapUPvoteButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapUPvoteButton::)]) {
        
        [self.delegate cell:self didTapUPvoteButton:self.user :self.photo];
        //[self.delegate cell:self didTapPvoteButton:self.user];
    }
}

#pragma mark - PAPBaseTextCell

/* Static helper to get the height for a cell if it had the given name and content */
+ (CGFloat)heightForCellWithName:(NSString *)name contentString:(NSString *)content {
    return [PAPBaseTextCell heightForCellWithName:name contentString:content cellInsetWidth:0];
}

/* Static helper to get the height for a cell if it had the given name, content and horizontal inset */
+ (CGFloat)heightForCellWithName:(NSString *)name contentString:(NSString *)content cellInsetWidth:(CGFloat)cellInset {
    

    
    CGSize size;
    
    CGFloat height = 0;
    
    size = [PAPBaseTextCell sizeOfMultiLineLabel:name font:[UIFont fontWithName:robo_bold size:16] size:270];
    
    height = height + size.height;
    
    size = [PAPBaseTextCell sizeOfMultiLineLabel:content font:[UIFont fontWithName:robo size:12] size:270];
    
    height = height + size.height;
    
    height = height + 33;//26
    
    return height;
    
}

/* Static helper to obtain the horizontal space left for name and content after taking the inset and image in consideration */
+ (CGFloat)horizontalTextSpaceForInsetWidth:(CGFloat)insetWidth {
    return (320) - (horiBorderSpacing+avatarDim+horiElemSpacing+horiBorderSpacing);
}

/* Static helper to pad a string with spaces to a given beginning offset */
+ (NSString *)padString:(NSString *)string withFont:(UIFont *)font toWidth:(CGFloat)width {
    // Find number of spaces to pad
    NSMutableString *paddedString = [[NSMutableString alloc] init];
    while (true) {
        [paddedString appendString:@" "];
        if ([paddedString sizeWithFont:font].width >= width) {
            break;
        }
    }
    
    // Add final spaces to be ready for first word
    [paddedString appendString:[NSString stringWithFormat:@" %@",string]];
    return paddedString;
}

- (void)setUser:(PFUser *)aUser {
    user = aUser;
    
    // Set name button properties and avatar image
     [self.avatarImageView setFile:[self.user objectForKey:kPAPUserProfilePicMediumKey]];//kPAPUserProfilePicSmallKey
    
    
}

-(void)setPhoto:(PFObject *)aPhoto
{
    photo = aPhoto;
}




- (void)setContentText:(NSString *)contentString {
    // If we have a user we pad the content with spaces to make room for the name
    if (self.user) {
        //CGSize nameSize = [self.nameButton.titleLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:13] forWidth:nameMaxWidth lineBreakMode:NSLineBreakByTruncatingTail];
        //NSString *paddedString = [PAPBaseTextCell padString:contentString withFont:[UIFont systemFontOfSize:13] toWidth:nameSize.width];
        NSString *newOutput = [NSString stringWithFormat:@"\"%@\" ",contentString];
        [self.contentLabel setText:newOutput];
        
       
        
    } else { // Otherwise we ignore the padding and we'll add it after we set the user
        NSString *newOutput = [NSString stringWithFormat:@"\"%@\" ",contentString];
        [self.contentLabel setText:newOutput];
       // [self.contentLabel setText:contentString];
    }
    
   
   
    [self configureMentionLabel:self.contentLabel];
   // [self setNeedsLayout];
}



/**
 *  <#Description#>
 *
 *  @param date <#date description#>
 */
- (void)setDate:(NSDate *)date {
    // Set the label with a human readable time
    [self.timeLabel setText:[timeFormatter stringForTimeIntervalFromDate:[NSDate date] toDate:date]];
    [self setNeedsDisplay];
}

- (void)setCellInsetWidth:(CGFloat)insetWidth {
    // Change the mainView's frame to be insetted by insetWidth and update the content text space
    cellInsetWidth = insetWidth;
    [mainView setFrame:CGRectMake(insetWidth, mainView.frame.origin.y, mainView.frame.size.width-2*insetWidth, mainView.frame.size.height)];
    horizontalTextSpace = [PAPBaseTextCell horizontalTextSpaceForInsetWidth:insetWidth];
    [self setNeedsDisplay];
}

/* Since we remove the compile-time check for the delegate conforming to the protocol
 in order to allow inheritance, we add run-time checks. */
- (id<PAPBaseTextCellDelegate>)delegate {
    return (id<PAPBaseTextCellDelegate>)delegate;
}

- (void)setDelegate:(id<PAPBaseTextCellDelegate>)aDelegate {
    if (delegate != aDelegate) {
        delegate = aDelegate;
    }
}

- (void)hideSeparator:(BOOL)hide {
    hideSeparator = hide;
}

#pragma OHAttributedLabel

-(void)configureMentionLabel:(OHAttributedLabel*)label
{
    // Detect all "@xxx" mention-like strings using the "@\w+" regular expression
    NSRegularExpression* userRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B@\\w+" options:0 error:nil];
    
    NSMutableAttributedString* mas = [label.attributedText mutableCopy];
    
    [userRegex enumerateMatchesInString:label.text options:0 range:NSMakeRange(0,label.text.length)
                             usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
     {
         // For each "@xxx" user mention found, add a custom link:
         NSString *taggedUser = [[label.text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
         NSString* linkURLString = [NSString stringWithFormat:@"user:%@",taggedUser]; // build the "user:" link
         [mas setLink:[NSURL URLWithString:linkURLString] range:match.range]; // add it
     }];
    
    NSRegularExpression* hashRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B#\\w+" options:0 error:nil];
    //NSMutableAttributedString* mas = [label.attributedText mutableCopy];
    [hashRegex enumerateMatchesInString:label.text options:0 range:NSMakeRange(0,label.text.length)
                             usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
     {
         // For each "@xxx" user mention found, add a custom link:
         NSString *hashTag = [[label.text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
         NSString* linkURLString = [NSString stringWithFormat:@"hash:%@",hashTag]; // build the "user:" link
         [mas setLink:[NSURL URLWithString:linkURLString] range:match.range]; // add it
     }];
    
    OHParagraphStyle* para = [OHParagraphStyle defaultParagraphStyle];
    //    para.firstLineHeadIndent = 30;
    //    para.headIndent = 5;
    //    para.tailIndent = -5;
    para.textAlignment = kCTTextAlignmentJustified;
    [mas setParagraphStyle:para];
    [OHASBasicMarkupParser processMarkupInAttributedString:mas];
    
    label.attributedText = mas;
    label.centerVertically = YES;
}
-(BOOL)attributedLabel:(OHAttributedLabel *)attributedLabel shouldFollowLink:(NSTextCheckingResult *)linkInfo
{
	if ([[linkInfo.URL scheme] isEqualToString:@"user"])
    {
		// We use this arbitrary URL scheme to handle custom actions
		// So URLs like "user:xxx" will be handled here instead of opening in Safari.
		// Note: in the above example, "xxx" is the 'resourceSpecifier' part of the URL
		NSString* taggeduser = [linkInfo.URL resourceSpecifier];
        
        // Display some message according to the user name clicked
        if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapTaggedUser:)]) {
            [self.delegate cell:self didTapTaggedUser:taggeduser];
        }
        
		
		// Prevent the URL from opening in Safari, as we handled it here manually instead
		return NO;
	}
    else if ([[linkInfo.URL scheme] isEqualToString:@"hash"]) {
        NSString* hash = [linkInfo.URL resourceSpecifier];
        NSLog(@"hashtag %@",hash);
        if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapHashTag:)]) {
            [self.delegate cell:self didTapHashTag:hash];
        }
        return NO;
    }
    else
    {
        if ([[UIApplication sharedApplication] canOpenURL:linkInfo.extendedURL])
        {
            // Execute the default behavior, which is opening the URL in Safari for URLs, starting a call for phone numbers, ...
            return YES;
        }
        else
        {
            //            [UIAlertView showWithTitle:@"Tap on link" message:[NSString stringWithFormat:@"Should open link %@", linkInfo.extendedURL]];
            return NO;
        }
	}
}

+(CGSize)sizeOfMultiLineLabel:(NSString*)text font:(UIFont*)font size:(CGFloat)width{
    
    
    
    //Label text
    NSString *aLabelTextString = text;
    
    //Label font
    UIFont *aLabelFont = font;
    
    //Width of the Label
    CGFloat aLabelSizeWidth = width;
    
    CGSize size;
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        //version < 7.0
        
        size = [aLabelTextString sizeWithFont:aLabelFont
                            constrainedToSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                lineBreakMode:NSLineBreakByWordWrapping];
    }
    else if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        //version >= 7.0
        
      

        //Return the calculated size of the Label
        size = [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{
                                                        NSFontAttributeName : aLabelFont
                                                        }
                                              context:nil].size;
        
    }
    
    return size;
    
}




@end
