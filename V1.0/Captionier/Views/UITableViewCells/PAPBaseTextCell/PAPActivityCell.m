//
//  PAPActivityCell.m
//  
//
//  Created by Mattieu Gamache-Asselin on 5/14/12.
//

#import "PAPActivityCell.h"
#import "TTTTimeIntervalFormatter.h"
#import "PAPProfileImageView.h"
#import "PAPActivityFeedViewController.h"
#import "NSAttributedString+Attributes.h"
#import "OHASBasicMarkupParser.h"
#import "OHAttributedLabel.h"
#import "SDWebImageManager.h"

static TTTTimeIntervalFormatter *timeFormatter;

@interface PAPActivityCell ()

/*! Private view components */
@property (nonatomic, strong) UIImageView *activityImageView;
@property (nonatomic,strong) UIImageView *logo1;
@property (nonatomic, strong) UIButton *activityImageButton;

/*! Flag to remove the right-hand side image if not necessary */
@property (nonatomic) BOOL hasActivityImage;

/*! Private setter for the right-hand side image */
- (void)setActivityImageFile:(NSString *)image;

/*! Button touch handler for activity image button overlay */
- (void)didTapActivityButton:(id)sender;

/*! Static helper method to calculate the space available for text given images and insets */
+ (CGFloat)horizontalTextSpaceForInsetWidth:(CGFloat)insetWidth;

@end

@implementation PAPActivityCell


#pragma mark - NSObject

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        horizontalTextSpace = [PAPActivityCell horizontalTextSpaceForInsetWidth:0];
        
        if (!timeFormatter) {
            timeFormatter = [[TTTTimeIntervalFormatter alloc] init];
        }

        // Create subviews and set cell properties
        self.opaque = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        self.hasActivityImage = NO; //No until one is set
        
        self.activityImageView = [[UIImageView alloc] init];
       //        self.activityImageView.layer.borderWidth = 2;
//        self.activityImageView.layer.borderColor =[UIColor colorWithWhite:0.705 alpha:1.0000].CGColor;
        
        [self.activityImageView setBackgroundColor:[UIColor clearColor]];
        
        [self.activityImageView setOpaque:YES];
        [self.mainView addSubview:self.activityImageView];
        
        self.activityImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.activityImageButton setBackgroundColor:[UIColor clearColor]];
        [self.activityImageButton addTarget:self action:@selector(didTapActivityButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.mainView addSubview:self.activityImageButton];
        
        self.separatorImage = [[UIImageView alloc] init];

        
        
        
    }
    return self;
}


#pragma mark - UIView

- (void)layoutSubviews {
    [super layoutSubviews];
       
    // Layout the activity image and show it if it is not nil (no image for the follow activity).
    // Note that the image view is still allocated and ready to be dispalyed since these cells
    // will be reused for all types of activity.
    [self.activityImageView setFrame:CGRectMake( [UIScreen mainScreen].bounds.size.width - 55.0f, self.frame.size.height/2 - 23, 46.0f,46.0f)];
    [self.activityImageButton setFrame:CGRectMake( [UIScreen mainScreen].bounds.size.width - 46.0f, 8.0f, 33.0f, 33.0f)];
    
     // Add activity image if one was set
    if (self.hasActivityImage) {
        [self.activityImageView setHidden:NO];
        [self.activityImageButton setHidden:NO];
    } else {
        [self.activityImageView setHidden:YES];
        [self.activityImageButton setHidden:YES];
    }

    // Change frame of the content text so it doesn't go through the right-hand side picture
    CGSize  contentSize =  [self.contentLabel sizeThatFits:CGSizeMake(200,CGFLOAT_MAX)];
    self.contentLabel.frame = CGRectMake(48+15, nameY+18, 200, contentSize.height);//45
    //[self.contentLabel setBackgroundColor:[UIColor redColor]];
    // Layout the timestamp label given new vertical 
    CGSize timeSize = [self.timeLabel.text sizeWithFont:[UIFont systemFontOfSize:11.0f] forWidth:[UIScreen mainScreen].bounds.size.width - 72.0f - 46.0f lineBreakMode:NSLineBreakByTruncatingTail];
    [self.timeLabel setFrame:CGRectMake(63.0f+15, self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 2.0f, timeSize.width, timeSize.height)];//46.0f
    
    [self.logo setFrame:CGRectMake(60.0f,self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 2.0f, 15,15.0f)];//53.0f,36, 15,15.0f
    //[self.logo setNeedsDisplay];
    //self.logo.image = [UIImage imageNamed:@"home_time"];
    
    [self.upvoteBtn setHidden:YES];
    [self.upvoteBtnLbl setHidden:YES];
    
}


#pragma mark - PAPActivityCell

- (void)setIsNew:(BOOL)isNew {
    if (isNew) {
        [self.mainView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundNewActivity.png"]]];
    } else {
        [self.mainView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundComments.png"]]];
    }
}


- (void)setActivity:(PFObject *)activity {
    // Set the activity property
    _activity = activity;
    if ([[activity objectForKey:kPAPActivityTypeKey] isEqualToString:kPAPActivityTypeFollow] || [[activity objectForKey:kPAPActivityTypeKey] isEqualToString:kPAPActivityTypeJoined]) {
        [self setActivityImageFile:nil];
    } else {
        
        [self setActivityImageFile:[[activity objectForKey:kPAPActivityPhotoKey] objectForKey:kPAPFeedMediaThumbnailLikKey]];
    }
    
    NSString *activityString = [PAPActivityFeedViewController stringForActivityType:(NSString*)[activity objectForKey:kPAPActivityTypeKey] object:activity];
    self.user = [activity objectForKey:kPAPActivityFromUserKey];
    
    // Set name button properties and avatar image
    
    [self.avatarImageView setFile:[self.user objectForKey:kPAPUserProfilePicMediumKey]];//kPAPUserProfilePicSmallKey
    self.avatarImageView.layer.cornerRadius =  self.avatarImageView.frame.size.width/2;
     self.avatarImageView.clipsToBounds = YES;
    [ self.avatarImageView setClipsToBounds:YES];
    self.avatarImageView.layer.borderWidth = 2;
    self.avatarImageView.layer.borderColor =[UIColor colorWithWhite:0.705 alpha:1.0000].CGColor;
    
    /************/
    
    NSString *nameString = NSLocalizedString(@"Someone", nil);
    if (self.user && [self.user objectForKey:kPAPUserDisplayNameKey] && [[self.user objectForKey:kPAPUserDisplayNameKey] length] > 0) {
        nameString = [self.user objectForKey:kPAPUserDisplayNameKey];
    }
    
    self.nameButton.titleLabel.font = [UIFont fontWithName:robo_bold size:16];
    [self.nameButton setTitle:nameString forState:UIControlStateNormal];
    [self.nameButton setTitle:nameString forState:UIControlStateHighlighted];
    
    // If user is set after the contentText, we reset the content to include padding
    if (self.contentLabel.text) {
        [self setContentText:self.contentLabel.text];
    }
        
    if (self.user) {
        
        self.contentLabel.font = [UIFont fontWithName:robo size:11];
        CGSize nameSize = [self.nameButton.titleLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:16.0f] forWidth:nameMaxWidth lineBreakMode:NSLineBreakByTruncatingTail];
        NSString *paddedString = [PAPBaseTextCell padString:activityString withFont:[UIFont systemFontOfSize:13.0f] toWidth:nameSize.width];
        [self.contentLabel setText:activityString];
    } else { // Otherwise we ignore the padding and we'll add it after we set the user
        [self.contentLabel setText:activityString];
    }

    self.textLabel.font = [UIFont fontWithName:robo_light size:12];
    [self.timeLabel setText:[timeFormatter stringForTimeIntervalFromDate:[NSDate date] toDate:[activity createdAt]]];
    

    [self setNeedsDisplay];
}

- (void)setCellInsetWidth:(CGFloat)insetWidth {
    [super setCellInsetWidth:insetWidth];
    horizontalTextSpace = [PAPActivityCell horizontalTextSpaceForInsetWidth:insetWidth];
}

// Since we remove the compile-time check for the delegate conforming to the protocol
// in order to allow inheritance, we add run-time checks.
- (id<PAPActivityCellDelegate>)delegate {
    return (id<PAPActivityCellDelegate>)_delegate;
}

- (void)setDelegate:(id<PAPActivityCellDelegate>)delegate {
    if(_delegate != delegate) {
        _delegate = delegate;
    }
}


#pragma mark - ()

+ (CGFloat)horizontalTextSpaceForInsetWidth:(CGFloat)insetWidth {
    return ([UIScreen mainScreen].bounds.size.width - (insetWidth * 2.0f)) - 72.0f;
}

+ (CGFloat)heightForCellWithName:(NSString *)name contentString:(NSString *)content {
    return [self heightForCellWithName:name contentString:content cellInsetWidth:0.0f];
}

+ (CGFloat)heightForCellWithName:(NSString *)name contentString:(NSString *)content cellInsetWidth:(CGFloat)cellInset {
    
    CGSize nameSize = [name sizeWithFont:[UIFont boldSystemFontOfSize:13.0f] forWidth:200.0f lineBreakMode:NSLineBreakByTruncatingTail];//boldSystemFontOfSize:13.0f
    
    
    //NSString *paddedString = [PAPBaseTextCell padString:content withFont:[UIFont systemFontOfSize:15.0f] toWidth:nameSize.width];
    //CGFloat horizontalTextSpace = [PAPActivityCell horizontalTextSpaceForInsetWidth:cellInset];
    
    CGSize contentSize = [content sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(265, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat singleLineHeight = [@"Test" sizeWithFont:[UIFont systemFontOfSize:11.0f]].height;
    
    // Calculate the added height necessary for multiline text. Ensure value is not below 0.
    CGFloat multilineHeightAddition = contentSize.height + nameSize.height + singleLineHeight;

    return  multilineHeightAddition + 20;
}

- (void)setActivityImageFile:(NSString *)imageFile {
    if (imageFile) {
        //[self.activityImageView setFile:imageFile];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        
        
        NSURL *mediaURL = [NSURL URLWithString:imageFile];
        
        [manager downloadWithURL:mediaURL
                         options:0
                        progress:^(NSUInteger receivedSize , long long expectesSize){
                            
                            //CGFloat progess = ((float)receivedSize/expectesSize);
                            //[progressView setProgress:progess animated:YES];
                        }
                       completed:^(UIImage *image , NSError *error , SDImageCacheType cacheType ,BOOL finished){
                           
                           //[progressView setProgress:1 animated:YES];
                           
                           //[progressView removeFromSuperview];
                           self.activityImageView.image = image;
                           
                       }];
        
        
        [self setHasActivityImage:YES];
    } else {
        [self setHasActivityImage:NO];
    }
}

- (void)didTapActivityButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapActivityButton:)]) {
        [self.delegate cell:self didTapActivityButton:self.activity];
    }    
}

@end
