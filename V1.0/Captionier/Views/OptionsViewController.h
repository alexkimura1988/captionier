//
//  OptionsViewController.h
//  FizFeliz
//
//  Created by Surender Rathore on 05/12/13.
//
//

#import <UIKit/UIKit.h>
@protocol OptionsViewControllerDelegate;
@interface OptionsViewController : UITableViewController
@property(nonatomic,strong)id <OptionsViewControllerDelegate> delegate;
@property(nonatomic,strong)NSArray *filterOptions;
@end
@protocol OptionsViewControllerDelegate <NSObject>
@optional
-(void)optionSelected:(NSString*)option;
@end