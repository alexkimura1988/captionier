//
//  AppDelegate.m
//  Anypic
//
//  Created by Héctor Ramos on 5/04/12.
//

#import "AppDelegate.h"

#import "Reachability.h"
#import "MBProgressHUD.h"
#import "PAPHomeViewController.h"
#import "PAPLogInViewController.h"
#import "UIImage+ResizeAdditions.h"
#import "PAPAccountViewController.h"
#import "PAPWelcomeViewController.h"
#import "PAPActivityFeedViewController.h"
#import "PAPPhotoDetailsViewController.h"
#import "CameraViewController.h"
#import "PAPAccountViewController.h"
#import "CommentsViewController.h"

#import "iRate.h"
#import "NotificationBar.h"
#import "LikersViewController.h"
#import "SearchOptionsViewController.h"
#import "SignUpViewController.h"
#import "NavigationViewController.h"
#import "HelpSreenViewController.h"
#import "WebViewController.h"
#import "CustomCameraViewController.h"
#import <Parse/PF_MBProgressHUD.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>

#import "AmazonTransfer.h"


NSString *SnapAndRunShouldUpdateAuthInfoNotification = @"SnapAndRunShouldUpdateAuthInfoNotification";

// preferably, the auth token is stored in the keychain, but since working with keychain is a pain, we use the simpler default system
NSString *kStoredAuthTokenKeyName = @"FlickrOAuthToken";
NSString *kStoredAuthTokenSecretKeyName = @"FlickrOAuthTokenSecret";

NSString *kGetAccessTokenStep = @"kGetAccessTokenStep";
NSString *kCheckTokenStep = @"kCheckTokenStep";

NSString *SRCallbackURLBaseString = @"mogram://auth";

#define TwitterApiKey             @"RMMrePixN7YjJrmXMgR7oiMUb"
#define TwitterApiSecret          @"SxpMhqEjc2Lx6Vud1Sg2M2lJ7z4GNhqWLUCHMr6YuPMK7lIYLq"

#define AmazonAccessKey           @"AKIAI5NXOWAV6LL3JAEA"
#define AmazonSecretKey           @"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"


@interface AppDelegate () <WebViewControllerDelegate> {
    NSMutableData *_data;
    BOOL firstLaunch;
}

@property (nonatomic, strong) PAPHomeViewController *homeViewController;
@property (nonatomic, strong) PAPActivityFeedViewController *activityViewController;
@property (nonatomic, strong) PAPWelcomeViewController *welcomeViewController;
@property (nonatomic, strong) CustomCameraViewController *cameraViewController;
@property (nonatomic, strong) PAPAccountViewController *accountViewController;
@property (nonatomic, strong) SearchOptionsViewController *searchViewController;

@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) NSTimer *autoFollowTimer;

@property (nonatomic, strong) Reachability *hostReach;
@property (nonatomic, strong) Reachability *internetReach;
@property (nonatomic, strong) Reachability *wifiReach;

- (void)setupAppearance;
- (BOOL)shouldProceedToMainInterface:(PFUser *)user;
- (BOOL)handleActionURL:(NSURL *)url;
@end

@implementation AppDelegate

@synthesize window;
@synthesize navController;
@synthesize tabBarController;
@synthesize networkStatus;

@synthesize homeViewController;
@synthesize activityViewController;
@synthesize welcomeViewController;
@synthesize accountViewController;
@synthesize cameraViewController;
@synthesize searchViewController;

@synthesize hud;
@synthesize autoFollowTimer;

@synthesize hostReach;
@synthesize internetReach;
@synthesize wifiReach;


#pragma mark - UIApplicationDelegate

void uncaughtExceptionHandler(NSException *exception) {
    //[Flurry startSession:@"VX2Q9RPNNFQGCD5KDP86"];
    [Flurry logError:@"Uncaught" message:@"Crash!" exception:exception];
    NSLog(@"The app has encountered an unhandled exception: %@", [exception debugDescription]);
    NSLog(@"Stack trace: %@", [exception callStackSymbols]);
    NSLog(@"desc: %@", [exception description]);
    NSLog(@"name: %@", [exception name]);
    NSLog(@"user info: %@", [exception userInfo]);
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    
    
    ///your stuff here
    
    //[self dismissViewControllerAnimated:YES completion:nil];
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    NSString * parseAppId = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"ParseApplicationId"];
    NSString * parseClientId = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"ParseClientId"];
    
    if ([parseAppId isEqualToString:@"Your-ParseApplicationId"]) {
        [Helper showAlertWithTitle:@"Parse" Message:@"Please add parse application id in info.plist file."];
    }
    else if([parseClientId isEqualToString:@"Your-ParseClientId"])
    {
        [Helper showAlertWithTitle:@"Parse" Message:@"Please add parse client id in info.plist file."];
    }
    
    // ****************************************************************************
    // Parse initialization
    [Parse setApplicationId:parseAppId clientKey:parseClientId];
    [PFFacebookUtils  initializeFacebook];
    [PFTwitterUtils initializeWithConsumerKey:TwitterApiKey consumerSecret:TwitterApiSecret];
    // ****************************************************************************
    
    NSString * flurryID = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FLURRY_ID"];
    
    if ([flurryID isEqualToString:@"Your-FLURRY_ID"]) {
        [Helper showAlertWithTitle:@"Flurry" Message:@"Please add flurry  id in info.plist file."];
    }
    else {
        //************************ Flurry **********************
        [Flurry startSession:flurryID];
        NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
        //************************ Flurry **********************
    }
    
    [PFQuery clearAllCachedResults];

    PFACL *defaultACL = [PFACL ACL];
    // Enable public read access by default, with any newly created PFObjects belonging to the current user
    [defaultACL setPublicReadAccess:YES];
    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];

    // Set up our app's global UIAppearance
    [self setupAppearance];

    // Use Reachability to monitor connectivity
    [self monitorReachability];

    self.welcomeViewController = [[PAPWelcomeViewController alloc] init];

    self.navController = [[UINavigationController alloc] initWithRootViewController:self.welcomeViewController];
    self.navController.navigationBarHidden = YES;

    self.window.rootViewController = self.navController;
    
//    [[UINavigationBar appearance] setTitleTextAttributes: @{NSFontAttributeName: [UIFont fontWithName:robo_light size:16.0f]}];
//    [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    [self.window makeKeyAndVisible];

    [self handlePush:launchOptions];
    
    
    [AmazonTransfer setConfigurationWithRegion:AWSRegionUSEast1 accessKey:AmazonAccessKey secretKey:AmazonSecretKey];
    
//    [Crashlytics startWithAPIKey:@"8c41e9486e74492897473de501e087dbc6d9f391"];

    return YES;
}



- (void)applicationDidEnterBackground:(UIApplication *)application
{
//    NSFileManager *fm = [NSFileManager defaultManager];
//    NSArray *list = [fm contentsOfDirectoryAtPath:NSTemporaryDirectory() error:nil];
//    for (NSString *item in list) {
//        [fm removeItemAtPath:[NSTemporaryDirectory() stringByAppendingPathComponent:item] error:nil];
//    }
    
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    //flicker
    if ([[url scheme] isEqualToString:@"mogram"]) {
//        if ([self flickrRequest].sessionInfo) {
//            // already running some other request
//            NSLog(@"Already running some other request");
//        }
//        else {
//            NSString *token = nil;
//            NSString *verifier = nil;
//            BOOL result = OFExtractOAuthCallback(url, [NSURL URLWithString:SRCallbackURLBaseString], &token, &verifier);
//            
//            if (!result) {
//                NSLog(@"Cannot obtain token/secret from URL: %@", [url absoluteString]);
//                return NO;
//            }
//            
//            [self flickrRequest].sessionInfo = kGetAccessTokenStep;
//            [flickrRequest fetchOAuthAccessTokenWithRequestToken:token verifier:verifier];
//        }
    }
    
    //facebook
    if ([self handleActionURL:url]) {
        return YES;
    }
    
    return  [FBAppCall handleOpenURL:url
                   sourceApplication:sourceApplication
                         withSession:[PFFacebookUtils session]];

    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken {
    [PFPush storeDeviceToken:newDeviceToken];

    if (application.applicationIconBadgeNumber != 0) {
        application.applicationIconBadgeNumber = 0;
    }
    if ([PFUser currentUser]) {
        // Make sure they are subscribed to their private push channel
        NSString *privateChannelName = [[PFUser currentUser] objectForKey:kPAPUserPrivateChannelKey];
        if (privateChannelName && privateChannelName.length > 0) {
            NSLog(@"Subscribing user to %@", privateChannelName);
            [[PFInstallation currentInstallation] addUniqueObject:privateChannelName forKey:kPAPInstallationChannelsKey];
        }
    }
    [[PFInstallation currentInstallation] saveInBackground];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
	if ([error code] != 3010) { // 3010 is for the iPhone Simulator
        NSLog(@"Application failed to register for push notifications: %@", error);
	}
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    
    //NSLog(@"userinfo %@",userInfo);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:PAPAppDelegateApplicationDidReceiveRemoteNotification object:nil userInfo:userInfo];
    
    NotificationBar *nBar = [NotificationBar sharedInstance];
    [nBar setMessage:@"" withUserInfo:userInfo];
    
    NSString *typeKey = [userInfo objectForKey:kPAPPushPayloadActivityTypeKey];
    NSString *photoObjectId = [userInfo objectForKey:kPAPPushPayloadPhotoObjectIdKey];
    // comment notification
    if ([typeKey isEqualToString:kPAPPushPayloadActivityCommentKey]) {
        //PFObject *object = [PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId];
        //NSLog(@"comment key matched");
        //[self shouldNavigateToPhoto:[PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId]];
        //[self shouldNavigateToPhoto:[PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId]];
         //[self shouldNavigateToSinglePhoto:[PFObject objectWithoutDataWithClassName:kPAPFeedClassKey objectId:photoObjectId]];
        return;
    }//like notification
    else if([typeKey isEqualToString:kPAPPushPayloadActivityLikeKey]) {
       // [self shouldNavigateToLikeController:[PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId]];
        //[self shouldNavigateToPhoto:[PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId]];
         [self shouldNavigateToSinglePhoto:[PFObject objectWithoutDataWithClassName:kPAPFeedClassKey objectId:photoObjectId]];
        return;
    }// tagged in photo
    else if ([typeKey isEqualToString:kPAPPushPayloadActivityTaggedInPhotoKey]) {
        //[self shouldNavigateToSinglePhoto:[PFObject objectWithoutDataWithClassName:kPAPFeedClassKey objectId:photoObjectId]];
        
        //[self shouldNavigateToPhoto:[PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId]];
        return;
    }//tagged in comment
    else if([typeKey isEqualToString:kPAPPushPayloadActivityTaggedInCommentKey]){
         //[self shouldNavigateToSinglePhoto:[PFObject objectWithoutDataWithClassName:kPAPFeedClassKey objectId:photoObjectId]];
        //[self shouldNavigateToPhoto:[PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId]];
        return;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    UIViewController *vc = self.welcomeViewController.presentedViewController;
    
    NSLog(@"sub %@",vc.view.subviews);
    
    for  (id view in vc.view.subviews) {
        if ([view isKindOfClass:[PF_MBProgressHUD class]]) {
            PF_MBProgressHUD *hd = (PF_MBProgressHUD*)view;
            [hd hide:YES];
        }
    }

    // Clear badge and update installation, required for auto-incrementing badges.
    if (application.applicationIconBadgeNumber != 0) {
        application.applicationIconBadgeNumber = 0;
        [[PFInstallation currentInstallation] saveInBackground];
    }

    // Clears out all notifications from Notification Center.
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    application.applicationIconBadgeNumber = 1;
    application.applicationIconBadgeNumber = 0;

    [[FBSession activeSession] handleDidBecomeActive];
}


#pragma mark - UITabBarControllerDelegate

- (BOOL)tabBarController:(UITabBarController *)aTabBarController shouldSelectViewController:(UIViewController *)viewController {
    // The empty UITabBarItem behind our Camera button should not load a view controller
    if (aTabBarController.selectedIndex == PAPCameraTabBarItemIndex) {
        [self gotoCamera];
        return NO;
    }
    return YES;
    
    
   // return ![viewController isEqual:aTabBarController.viewControllers[PAPCameraTabBarItemIndex]];
    
}

-(void) gotoCamera
{
   
    
    CustomCameraViewController *obj;
    //[self.navController setNavigationBarHidden:YES animated:YES];
    if (IS_IPHONE_5) {
         obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController" bundle:nil];
        
    }
    else {
         obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController_ip4" bundle:nil];
       
    }
    UINavigationController *navigationVC = [[UINavigationController alloc] initWithRootViewController:obj];
    navigationVC.navigationBarHidden = YES;
    [self.tabBarController presentViewController:navigationVC animated:YES completion:nil];
    
}
#pragma mark - PFLoginViewController

- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    if (username && password && username.length && password.length) {
        return YES;
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    return NO;
}

- (void) dismissWithOption:(BOOL)acceptedOrRejected
{
    [self.welcomeViewController.presentedViewController dismissViewControllerAnimated:NO completion:nil];

    if (acceptedOrRejected) {

        [[PFUser currentUser] setObject:@YES forKey:@"eulaAccepted"];
        [[PFUser currentUser] saveInBackground];
        
        [self completeLogin:[PFUser currentUser]];
    }
    else {

        [self logOut];
    }
    

}


- (void) completeLogin:(PFUser*)user
{
    // user has logged in - we need to fetch all of their Facebook data before we let them in
    if (![self shouldProceedToMainInterface:user]) {
        
        self.hud = [MBProgressHUD showHUDAddedTo:self.navController.presentedViewController.view animated:YES];
        self.hud.labelText = NSLocalizedString(@"Loading", nil);
        self.hud.dimBackground = YES;
        
        
    }
}

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    
    if (user) {
        NSString *privateChannelName = [NSString stringWithFormat:@"captionier_%@",[user objectId]];
        // Add the user to the installation so we can track the owner of the deviceu
        [[PFInstallation currentInstallation] setObject:[PFUser currentUser] forKey:kPAPInstallationUserKey];
        // Subscribe user to private channel
        [[PFInstallation currentInstallation] addUniqueObject:privateChannelName forKey:kPAPInstallationChannelsKey];
        // Save installation object
        [[PFInstallation currentInstallation] saveEventually];
        
        [user setObject:privateChannelName forKey:kPAPUserPrivateChannelKey];
        [user saveInBackground];
    }
    
    if ([PFUser currentUser].email.length == 0) {
        
        if ([PFFacebookUtils isLinkedWithUser:user]) {
            if ([user isNew]) {
                [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                    if (!error) {
                        [self facebookRequestDidLoad:result];
                    } else {
                        [self facebookRequestDidFailWithError:error];
                    }
                }];
            }
            else {
                if (![self shouldProceedToMainInterface:user]) {
                    
//                    self.hud = [MBProgressHUD showHUDAddedTo:self.navController.presentedViewController.view animated:YES];
//                    self.hud.labelText = NSLocalizedString(@"Loading", nil);
//                    self.hud.dimBackground = YES;
                }
            }
           
        }
        else if([PFTwitterUtils isLinkedWithUser:user]) {
            
            NSError *error;
            NSURL *verify = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:verify];
            [[PFTwitterUtils twitter] signRequest:request];
            NSURLResponse *response = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if (!error)
            {
                [self twitterRequestDidLoad:dictionary];
            } else
            {
                [self facebookRequestDidFailWithError:error];
            }
            
            
            NSLog(@"%@",dictionary);
            
            
        }
    }
    else {
        [self promptEULAToUser];
    }
    
    
   
    
}
-(void)promptEULAToUser{
    
    PFUser *user = [PFUser currentUser];
    
    BOOL eulaAccepted = NO;
    if([user objectForKey:@"eulaAccepted"])
    {
        eulaAccepted = [[user objectForKey:@"eulaAccepted"] boolValue];
    }
    if ([user isNew] || !eulaAccepted) {
        
        WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webView.title = @"Terms of Service";
        webView.weburl = TermsOfService;//[[NSUserDefaults standardUserDefaults] objectForKey:@"EulaURL"];;
        webView.delegate = self;
        // webView.hidesBottomBarWhenPushed = YES;
        //[self.navigationController pushViewController:webView animated:YES];
        UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:webView];
        [self.welcomeViewController.presentedViewController presentViewController:navC animated:NO completion:nil];
    }
    else {
        [self completeLogin:user];
    }
}


- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController
{
    
}


- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error;
{
    NSLog(@"error %@",[error localizedDescription]);
    [Helper showAlertWithTitle:@"Login failed!!" Message:@"invalid login credentials"];
    // [Helper showAlertWithTitle:@"Login Error" Message:@"Please add Parse application Id And Client Id in info.plist"];
}
#pragma mark - PFSignupViewConrtoller
// Sent to the delegate to determine whether the sign up request should be submitted to the server.
- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info {
    
    UIButton *btnCheckbox = (UIButton*)[signUpController.view viewWithTag:500];
    if (!btnCheckbox.isSelected) {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you accept terms and condition", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
        return NO;
    
    }
    
    UIImageView *profileImageView = (UIImageView*)[signUpController.view viewWithTag:123];
    if (!profileImageView.image) {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you have added a profile image", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
        return NO;
    }
    
    
    NSLog(@"info %@",info);
    NSRange whiteSpaceRange = [info[@"username"] rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you remove white space all username feild", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
        return NO;
    }
    
    
    BOOL informationComplete = YES;
    for (id key in info) {
        NSString *field = [info objectForKey:key];
        
        if (!field || field.length == 0) {
            informationComplete = NO;
            break;
        }
    }
    
    if (!informationComplete) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    }
    
    if (informationComplete) {
            [[ProgressIndicator sharedInstance] showPIOnView:self.navController.presentedViewController.presentedViewController.view withMessage:@"Signing up.."];
    }
    
    return informationComplete;
}

// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user {
    
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
    if (self.hud) {
        [MBProgressHUD hideHUDForView:self.navController.presentedViewController.view animated:NO];
    }
    self.hud = [MBProgressHUD showHUDAddedTo:self.navController.presentedViewController.presentedViewController.view animated:YES];
    self.hud.labelText = NSLocalizedString(@"Creating profile", nil);
    self.hud.dimBackground = YES;
    
    [user setObject:@YES forKey:@"eulaAccepted"];
    [user setObject:[user objectForKey:@"additional"] forKey:kPAPUserDisplayNameKey];
    [user saveEventually:^(BOOL successed , NSError *error){
        if (successed) {
            
            [PAPUtility saveProfilePictueblock:^(BOOL succedd, NSError*error){}];
            [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(dissmiss) userInfo:nil repeats:NO];
            
        }
    }];
    
    if (user) {
        
        NSString *privateChannelName = [NSString stringWithFormat:@"captionier_%@",[user objectId]];
        // Add the user to the installation so we can track the owner of the deviceu
        [[PFInstallation currentInstallation] setObject:[PFUser currentUser] forKey:kPAPInstallationUserKey];
        // Subscribe user to private channel
        [[PFInstallation currentInstallation] addUniqueObject:privateChannelName forKey:kPAPInstallationChannelsKey];
        // Save installation object
        [[PFInstallation currentInstallation] saveEventually];
        
        [user setObject:privateChannelName forKey:kPAPUserPrivateChannelKey];
        
        
    }
    
    //[self.welcomeViewController dismissViewControllerAnimated:YES completion:NULL];
    
}
-(void)dissmiss {
    [MBProgressHUD hideHUDForView:self.navController.presentedViewController.presentedViewController.view animated:NO];
    [self.welcomeViewController dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error {
    
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    NSLog(@"Failed to sign up...");
}

// Sent to the delegate when the sign up screen is dismissed.
- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController {
    NSLog(@"User dismissed the signUpViewController");
       /* self.signUpView.additionalField.text = @"";
        self.signUpView.usernameField.text = @"";
        self.signUpView.emailField.text = @"";
        self.signUpView.passwordField.text = @"";
        self.profileImageView.image = [UIImage imageNamed:@"default_profile_pic@2x.png"];*/
 
    
}

#pragma mark - PFLoginViewController

//- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
//    // user has logged in - we need to fetch all of their Facebook data before we let them in
//    if (![self shouldProceedToMainInterface:user]) {
//        self.hud = [MBProgressHUD showHUDAddedTo:self.navController.presentedViewController.view animated:YES];
//        self.hud.labelText = NSLocalizedString(@"Loading", nil);
//        self.hud.dimBackground = YES;
//    }
//    
//    // Subscribe to private push channel
//    if (user) {
//        NSString *privateChannelName = [NSString stringWithFormat:@"xtreme_%@",[user objectId]];
//        // Add the user to the installation so we can track the owner of the deviceu
//        [[PFInstallation currentInstallation] setObject:[PFUser currentUser] forKey:kPAPInstallationUserKey];
//        // Subscribe user to private channel
//        [[PFInstallation currentInstallation] addUniqueObject:privateChannelName forKey:kPAPInstallationChannelsKey];
//        // Save installation object
//        [[PFInstallation currentInstallation] saveEventually];
//        
//        [user setObject:privateChannelName forKey:kPAPUserPrivateChannelKey];
//    }
//    
//    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//        if (!error) {
//            [self facebookRequestDidLoad:result];
//        } else {
//            [self facebookRequestDidFailWithError:error];
//        }
//    }];
//}


#pragma mark - NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    _data = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [PAPUtility processFacebookProfilePictureData:_data];
}


#pragma mark - AppDelegate

- (BOOL)isParseReachable {
    return self.networkStatus != NotReachable;
}

- (void)presentLoginViewControllerAnimated:(BOOL)animated {
    
    PAPLogInViewController *loginViewController = [[PAPLogInViewController alloc] init];
    [loginViewController setDelegate:self];
    
    
    loginViewController.fields =  PFLogInFieldsUsernameAndPassword | PFLogInFieldsSignUpButton | PFLogInFieldsLogInButton| PFLogInFieldsPasswordForgotten | PFLogInFieldsTwitter;
    
    loginViewController.facebookPermissions = @[@"user_about_me",@"publish_actions",@"user_friends"];
    
   
    // Customize the Sign Up View Controller
    SignUpViewController *signUpViewController = [[SignUpViewController alloc] init];
    signUpViewController.delegate = self;
    signUpViewController.fields = PFSignUpFieldsDefault|PFSignUpFieldsAdditional;
    loginViewController.signUpController = signUpViewController;
    
    // Present Log In View Controller
    [self.welcomeViewController presentViewController:loginViewController animated:YES completion:NULL];
}

- (void)presentLoginViewController {
    
    [self presentLoginViewControllerAnimated:YES];
    
}


- (void)presentTabBarController {    
    
    
    self.tabBarController = [[UITabBarController alloc] init];
    //[[[self tabBarController]tabBar]setBarTintColor:[UIColor colorWithRed:238.0/255.0f green:238.0/255.0f blue:238.0/255.0f alpha:1.0f]];
    [[[self tabBarController] tabBar] setBackgroundImage:[UIImage imageNamed:@"tab_background-568h.png"]];
    
    [[UITabBar appearance] setSelectionIndicatorImage:[[UIImage alloc] init]];
    
    //** home contoller **//
    self.homeViewController = [[PAPHomeViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.homeViewController setFirstLaunch:firstLaunch];
    self.homeViewController.isNeedPullToRefresh = YES;
    UINavigationController *homeNavigationController = [[UINavigationController alloc] initWithRootViewController:self.homeViewController];
    UITabBarItem *homeTabBarItem = [[UITabBarItem alloc] initWithTitle:nil image:nil tag:0];
    
    // [homeTabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"home_screen_on"] withFinishedUnselectedImage:[UIImage imageNamed:@"home_screen_off"]];
    homeTabBarItem.selectedImage = [[UIImage imageNamed:@"home_screen_on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    homeTabBarItem.image = [[UIImage imageNamed:@"home_screen_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    homeTabBarItem.imageInsets = UIEdgeInsetsMake(5, -5, -5, 5);
    
    //** Search controller **//
    self.searchViewController = [[SearchOptionsViewController alloc] initWithNibName:@"SearchOptionsViewController" bundle:nil];
    
    UINavigationController *searchViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self.searchViewController];
    
    //set bar button item for profile controller
    UITabBarItem *searchTabBarItem = [[UITabBarItem alloc] initWithTitle:nil image:nil tag:1];
    
    //[searchTabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"search_icon_on"] withFinishedUnselectedImage:[UIImage imageNamed:@"search_icon_off"]];
    searchTabBarItem.selectedImage = [[UIImage imageNamed:@"search_icon_on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    searchTabBarItem.image = [[UIImage imageNamed:@"search_icon_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    searchTabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, -5, -5);
    
    
    
    //** camera controller **//
    CGRect rect = [[UIScreen mainScreen] bounds];
    
    if (rect.size.height == 568) {
        self.cameraViewController = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController" bundle:nil];
    }
    else
    {
        self.cameraViewController = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController-ip4" bundle:nil];
    }
    
    self.cameraViewController.hidesBottomBarWhenPushed = YES;
    UINavigationController *cameraNavigationController = [[UINavigationController alloc] initWithRootViewController:self.cameraViewController];
    cameraNavigationController.navigationBarHidden = YES;
    
    if ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        cameraNavigationController.navigationBar.translucent = NO;
    }
    //set bar button item for cameracontroller
    UITabBarItem *cameraTabBarItem = [[UITabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:@"camera_btn.png"] tag:2];
    //[cameraTabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tcamera_icon_on"] withFinishedUnselectedImage:[UIImage imageNamed:@"tcamera_icon_off"]];
    cameraTabBarItem.selectedImage = [[UIImage imageNamed:@"tcamera_icon_on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    cameraTabBarItem.image = [[UIImage imageNamed:@"tcamera_icon_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    cameraTabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    
    
    
    //** profile controller **//
    self.accountViewController = [[PAPAccountViewController alloc] initWithStyle:UITableViewStylePlain];
    accountViewController.showBackButton = NO;
    accountViewController.isNeedPullToRefresh = YES;
    [accountViewController setUser:[PFUser currentUser]];
    UINavigationController *profileViewNavigationController = [[UINavigationController alloc] initWithRootViewController:self.accountViewController];
    //set bar button item for profile controller
    UITabBarItem *profileTabBarItem = [[UITabBarItem alloc] initWithTitle:nil image:nil tag:3];
    //[profileTabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"profile_icon_on"] withFinishedUnselectedImage:[UIImage imageNamed:@"profile_icon_off"]];
    profileTabBarItem.selectedImage = [[UIImage imageNamed:@"profile_icon_on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    profileTabBarItem.image = [[UIImage imageNamed:@"profile_icon_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];//
    profileTabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, -5, -5);
    
    
    
    
    //** activifeed controller **//
    self.activityViewController = [[PAPActivityFeedViewController alloc] initWithStyle:UITableViewStylePlain];
    UINavigationController *activityFeedNavigationController = [[UINavigationController alloc] initWithRootViewController:self.activityViewController];
    //set bar button item for activityfeedcontroller
    UITabBarItem *activityFeedTabBarItem = [[UITabBarItem alloc] initWithTitle:@"" image:nil tag:4];
    //[activityFeedTabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"activity_icon_on"] withFinishedUnselectedImage:[UIImage imageNamed:@"activity_icon_off"]];
    activityFeedTabBarItem.selectedImage = [[UIImage imageNamed:@"activity_icon_on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    activityFeedTabBarItem.image = [[UIImage imageNamed:@"activity_icon_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [activityFeedNavigationController setTabBarItem:activityFeedTabBarItem];
    activityFeedTabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, -5, -5);
    
    
    
    
    //set barbutton items to NavigationController
    [homeNavigationController setTabBarItem:homeTabBarItem];
    [searchViewNavigationController setTabBarItem:searchTabBarItem];
    [cameraNavigationController setTabBarItem:cameraTabBarItem];
    [profileViewNavigationController setTabBarItem:profileTabBarItem];
    [activityFeedNavigationController setTabBarItem:activityFeedTabBarItem];
    
    
    
    self.tabBarController.delegate = self;
    self.tabBarController.viewControllers = @[ homeNavigationController,searchViewNavigationController,cameraNavigationController,activityFeedNavigationController,profileViewNavigationController];
    
    [self.navController setViewControllers:@[ self.welcomeViewController, self.tabBarController ] animated:NO];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
}

- (void)logOut {
    // clear cache
    [[PAPCache sharedCache] clear];

    // clear NSUserDefaults
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kPAPUserDefaultsCacheFacebookFriendsKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kPAPUserDefaultsActivityFeedViewControllerLastRefreshKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

    // Unsubscribe from push notifications by removing the user association from the current installation.
    NSString *privateChannelName = [NSString stringWithFormat:@"captionier_%@",[[PFUser currentUser] objectId]];
    [[PFInstallation currentInstallation] removeObject:privateChannelName forKey:kPAPInstallationChannelsKey];
    [[PFInstallation currentInstallation] saveInBackground];
    
    // Clear all caches
    [PFQuery clearAllCachedResults];
    
    // Log out
    [PFUser logOut];
    
    NSLog(@"%@",self.navController.viewControllers);
    
    
    // clear out cached data, view controllers, etc
    [self.navController popToRootViewControllerAnimated:YES];
    
    // [self presentLoginViewController];
    
    self.welcomeViewController = [[PAPWelcomeViewController alloc] init];
    
    self.navController = [[UINavigationController alloc] initWithRootViewController:self.welcomeViewController];
    self.navController.navigationBarHidden = YES;
    
    self.window.rootViewController = self.navController;
    
    self.homeViewController = nil;
    self.activityViewController = nil;
    

}




#pragma mark - ()

- (void)setupAppearance {
    
    if (SYSTEM_VERSION_GREATER_THAN(@"7")) {
         [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navigation_bar_ios7.png"] forBarMetrics:UIBarMetricsDefault];
         //[[UINavigationBar appearance]setBarTintColor:[UIColor colorWithRed:39.0f/255.0f green:50.0f/255.0f blue:72.0f/255.0f alpha:1.0f]];
    }
    else {
     //    [[UINavigationBar appearance]setBarTintColor:[UIColor colorWithRed:39.0f/255.0f green:50.0f/255.0f blue:72.0f/255.0f alpha:1.0f]];
     [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    }
   
    /*
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           UITextAttributeTextColor:UIColorFromRGB(cTitleColor),
                                                           UITextAttributeFont: [UIFont fontWithName:Aharoni_Bold size:18]
                                                           }];
    */
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor whiteColor], UITextAttributeTextColor,
                                [UIColor clearColor], UITextAttributeTextShadowColor, nil];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:attributes forState: UIControlStateNormal];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setBackButtonBackgroundImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

    - (BOOL)prefersStatusBarHidden {
        return YES;
    }
- (void)monitorReachability {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
   // self.hostReach = [Reachability reachabilityWithHostName:@"api.parse.com"];
   // [self.hostReach startNotifier];
    
    self.internetReach = [Reachability reachabilityForInternetConnection];
    [self.internetReach startNotifier];
    
    self.wifiReach = [Reachability reachabilityForLocalWiFi];
    [self.wifiReach startNotifier];
}

- (void)handlePush:(NSDictionary *)launchOptions {

    NSLog(@"handle push");
    // If the app was launched in response to a push notification, we'll handle the payload here
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationPayload) {
        [[NSNotificationCenter defaultCenter] postNotificationName:PAPAppDelegateApplicationDidReceiveRemoteNotification object:nil userInfo:remoteNotificationPayload];
        
        if (![PFUser currentUser]) {
            return;
        }
        
        
        NSString *typeKey = [remoteNotificationPayload objectForKey:kPAPPushPayloadActivityTypeKey];
        NSString *photoObjectId = [remoteNotificationPayload objectForKey:kPAPPushPayloadPhotoObjectIdKey];
        // comment notification
        if ([typeKey isEqualToString:kPAPPushPayloadActivityCommentKey]) {
           
           // [self shouldNavigateToPhoto:[PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId]];
            [self shouldNavigateToSinglePhoto:[PFObject objectWithoutDataWithClassName:kPAPFeedClassKey objectId:photoObjectId]];
            return;
        }//like notification
        else if([typeKey isEqualToString:kPAPPushPayloadActivityLikeKey]) {
            //[self shouldNavigateToLikeController:[PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId]];
            [self shouldNavigateToSinglePhoto:[PFObject objectWithoutDataWithClassName:kPAPFeedClassKey objectId:photoObjectId]];
            return;
        }// tagged in photo
        else if ([typeKey isEqualToString:kPAPPushPayloadActivityTaggedInPhotoKey]) {
            //[self shouldNavigateToSinglePhoto:[PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId]];
            [self shouldNavigateToSinglePhoto:[PFObject objectWithoutDataWithClassName:kPAPFeedClassKey objectId:photoObjectId]];
            return;
        }//tagged in comment
        else if([typeKey isEqualToString:kPAPPushPayloadActivityTaggedInCommentKey]){
            
            
            [self shouldNavigateToSinglePhoto:[PFObject objectWithoutDataWithClassName:kPAPFeedClassKey objectId:photoObjectId]];
            return;
        }
        else if([typeKey isEqualToString:kPAPPushPayloadActivityFollowKey]) {
            [self shouldNavigateToActivity ];
        }
       
        

        
        // If the push notification payload references a user, we will attempt to push their profile into view
        NSString *fromObjectId = [remoteNotificationPayload objectForKey:kPAPPushPayloadFromUserObjectIdKey];
        if (fromObjectId && fromObjectId.length > 0) {
            PFQuery *query = [PFUser query];
            query.cachePolicy = kPFCachePolicyCacheElseNetwork;
            [query getObjectInBackgroundWithId:fromObjectId block:^(PFObject *user, NSError *error) {
                if (!error) {
                    UINavigationController *homeNavigationController = self.tabBarController.viewControllers[PAPHomeTabBarItemIndex];
                    self.tabBarController.selectedViewController = homeNavigationController;
                    
//                    PAPAccountViewController *accountViewController = [[PAPAccountViewController alloc] initWithStyle:UITableViewStylePlain];
//                    accountViewController.user = (PFUser *)user;
//                    [homeNavigationController pushViewController:accountViewController animated:YES];
                }
            }];
        }
    }
}

- (void)autoFollowTimerFired:(NSTimer *)aTimer {
    [MBProgressHUD hideHUDForView:self.navController.presentedViewController.view animated:YES];
    [MBProgressHUD hideHUDForView:self.homeViewController.view animated:YES];
    [self.homeViewController loadObjects];
}

- (BOOL)shouldProceedToMainInterface:(PFUser *)user {
    if ([PAPUtility userHasValidFacebookData:[PFUser currentUser]]) {
        
        [MBProgressHUD hideHUDForView:self.navController.presentedViewController.view animated:YES];
        [self presentTabBarController];
        
        [self.navController dismissViewControllerAnimated:YES completion:nil];
        return YES;
    }
    else if([PFUser currentUser].email.length != 0) {
        [self.navController dismissViewControllerAnimated:YES completion:nil];
        return YES;
    }
    else if([[[PFUser currentUser] objectForKey:@"TwitterId"] length] > 0){
        [self presentTabBarController];
        
        [self.navController dismissViewControllerAnimated:YES completion:nil];
        return YES;
    }
    
    return NO;
}

- (BOOL)handleActionURL:(NSURL *)url {
//    if ([[url host] isEqualToString:kPAPLaunchURLHostTakePicture]) {
//        if ([PFUser currentUser]) {
//            return [self.tabBarController shouldPresentPhotoCaptureController];
//        }
//    } else {
//        if ([[url fragment] rangeOfString:@"^pic/[A-Za-z0-9]{10}$" options:NSRegularExpressionSearch].location != NSNotFound) {
//            NSString *photoObjectId = [[url fragment] substringWithRange:NSMakeRange(4, 10)];
//            if (photoObjectId && photoObjectId.length > 0) {
//                [self shouldNavigateToPhoto:[PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId]];
//                return YES;
//            }
//        }
//    }

    return NO;
}

// Called by Reachability whenever status changes.
- (void)reachabilityChanged:(NSNotification* )note {
    Reachability *curReach = (Reachability *)[note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);

    networkStatus = [curReach currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        NSLog(@"Network not reachable.");
    }
    
    if ([self isParseReachable] && [PFUser currentUser] && self.homeViewController.objects.count == 0) {
        // Refresh home timeline on network restoration. Takes care of a freshly installed app that failed to load the main timeline under bad network conditions.
        // In this case, they'd see the empty timeline placeholder and have no way of refreshing the timeline unless they followed someone.
        [self.homeViewController loadObjects];
    }
}

- (void)shouldNavigateToActivity {
    
     [self.tabBarController setSelectedIndex:3];
  
}
-(void)shouldNavigateToLikeController:(PFObject*)targetPhoto {
  
    for (PFObject *photo in self.homeViewController.objects) {
        if ([photo.objectId isEqualToString:targetPhoto.objectId]) {
  
            targetPhoto = photo;
            break;
        }
    }
    
    // if we have a local copy of this photo, this won't result in a network fetch
    [targetPhoto fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            
            UINavigationController *navC = (UINavigationController*)self.window.rootViewController;
            
            LikersViewController *commentView = [[LikersViewController alloc] initWithNibName:@"LikersViewController" bundle:Nil];
            commentView.photo = object;
            [navC pushViewController:commentView animated:YES];
            
            
        }
        else {
            NSLog(@"error : %@",[error localizedDescription]);
        }
    }];
}
-(void)shouldNavigateToSinglePhoto:(PFObject*)targetPhoto {
    
    for (PFObject *photo in self.homeViewController.objects) {
        if ([photo.objectId isEqualToString:targetPhoto.objectId]) {
          
            targetPhoto = photo;
            break;
        }
    }
    
    // if we have a local copy of this photo, this won't result in a network fetch
    [targetPhoto fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
          
            
            //UINavigationController *navC = (UINavigationController*)self.window.rootViewController;
            
            [self.tabBarController setSelectedIndex:3];
            
            UINavigationController *navC = self.tabBarController.viewControllers[3];
            
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:@"1" forKey:@"SingleObject"];
            
            //if (!homeViewController) {
                PAPHomeViewController *homeViewController = [[PAPHomeViewController alloc]initWithStyle:UITableViewStylePlain];
                homeViewController.selectedObject = object;
                homeViewController.isNeedPullToRefresh = NO;
                [navC pushViewController:homeViewController animated:YES];

//            }
//            else{
//                
//                homeViewController.selectedObject = object;
//                homeViewController.isNeedPullToRefresh = NO;
//                [navC pushViewController:homeViewController animated:YES];
//            }
            
            
        }
        else {
            NSLog(@"error : %@",[error localizedDescription]);
        }
    }];
    
}
- (void)facebookRequestDidLoad:(id)result {
    
    
    // This method is called twice - once for the user's /me profile, and a second time when obtaining their friends. We will try and handle both scenarios in a single method.
    PFUser *user = [PFUser currentUser];
    
    NSArray *data = [result objectForKey:@"data"];
    
    if (data) {
        // we have friends data
        NSMutableArray *facebookIds = [[NSMutableArray alloc] initWithCapacity:[data count]];
        for (NSDictionary *friendData in data) {
            if (friendData[@"id"]) {
                [facebookIds addObject:friendData[@"id"]];
            }
        }
        
        // cache friend data
        [[PAPCache sharedCache] setFacebookFriends:facebookIds];
        
        if (user) {
//            if (![self shouldProceedToMainInterface:user]) {
//                [self logOut];
//                return;
//            }
            [self promptEULAToUser];
        } else {
            NSLog(@"No user session found. Forcing logOut.");
            [self logOut];
        }
    } else {
        self.hud.labelText = NSLocalizedString(@"Creating Profile", nil);

        if (user) {
            NSString *facebookName = result[@"name"];
            
            if (facebookName && [facebookName length] != 0) {
                
                [user setObject:facebookName forKey:kPAPUserDisplayNameKey];
            } else {
                [user setObject:@"Someone" forKey:kPAPUserDisplayNameKey];
            }
            
            NSString *facebookId = result[@"id"];
            if (facebookId && [facebookId length] != 0) {
                [user setObject:facebookId forKey:kPAPUserFacebookIDKey];
            }
            
            if (result[@"username"]) {
                [user setObject:result[@"username"] forKey:@"username"];
            }
            else {
                [user setObject:[NSString stringWithFormat:@"%@%@",result[@"first_name"],result[@"last_name"]] forKey:@"username"];
            }
            
            [user saveEventually];
            
            
            if ([PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
                // Download user's profile picture
                NSURL *profilePictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [[PFUser currentUser] objectForKey:kPAPUserFacebookIDKey]]];
                NSURLRequest *profilePictureURLRequest = [NSURLRequest requestWithURL:profilePictureURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0f]; // Facebook profile picture cache policy: Expires in 2 weeks
                [NSURLConnection connectionWithRequest:profilePictureURLRequest delegate:self];
            }
        }
        
        [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                [self facebookRequestDidLoad:result];
            } else {
                [self facebookRequestDidFailWithError:error];
            }
        }];
    }
    
    
    
}

- (void)facebookRequestDidFailWithError:(NSError *)error {
    NSLog(@"Facebook error: %@", error);
    
    if ([PFUser currentUser]) {
        if ([[error userInfo][@"error"][@"type"] isEqualToString:@"OAuthException"]) {
            NSLog(@"The Facebook token was invalidated. Logging out.");
            [self logOut];
        }
    }
}


- (void)twitterRequestDidLoad:(NSDictionary *)result {
    
    
    // This method is called twice - once for the user's /me profile, and a second time when obtaining their friends. We will try and handle both scenarios in a single method.
    PFUser *user = [PFUser currentUser];
    
    NSArray *data = [result allKeys];
    
    if (result) {
        
        NSString *displayName = result[@"name"];
        NSString *username = [[displayName componentsSeparatedByString:@" "] componentsJoinedByString:@""];
        [user setObject:displayName forKey:kPAPUserDisplayNameKey];
        [user setObject:username forKey:@"username"];

        [user setObject:[result[@"id"] stringValue] forKey:@"TwitterId"];
        
        [user saveEventually];
        
        NSURL *profilePictureURL = [NSURL URLWithString:result[@"profile_image_url"]];
        NSURLRequest *profilePictureURLRequest = [NSURLRequest requestWithURL:profilePictureURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0f]; // Facebook profile picture cache policy: Expires in 2 weeks
        [NSURLConnection connectionWithRequest:profilePictureURLRequest delegate:self];
        
//        if (![self shouldProceedToMainInterface:user]) {
//            [self logOut];
//            return;
//        }
        
    }
    
}




+ (AppDelegate *)sharedDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}





#pragma mark -
#pragma mark iRate delegate methods

- (void)iRateUserDidRequestReminderToRateApp
{
    //reset event count after every 5 (for demo purposes)
    [iRate sharedInstance].eventCount = 0;
}


@end
