//
//  PAPConstants.h
//  Anypic
//
//  Created by Mattieu Gamache-Asselin on 5/25/12.
//

typedef enum {
    PAPHomeTabBarItemIndex = 0,
    PAPExploreTabBarItemIndex = 1,
    PAPCameraTabBarItemIndex = 2,
    
    PAPActivityTabBarItemIndex = 3,
    PAPProfileTabBarItemIndex = 4
} PAPTabBarControllerViewControllerIndex;


// Define an array of Facebook Ids for accounts to auto-follow on signup
#define kPAPAutoFollowAccountFacebookIds @[ ]

#pragma mark - NSUserDefaults
extern NSString *const kPAPUserDefaultsActivityFeedViewControllerLastRefreshKey;
extern NSString *const kPAPUserDefaultsCacheFacebookFriendsKey;

#pragma mark - Launch URLs

extern NSString *const kPAPLaunchURLHostTakePicture;


#pragma mark - NSNotification
extern NSString *const PAPAppDelegateApplicationDidReceiveRemoteNotification;
extern NSString *const PAPUtilityUserFollowingChangedNotification;
extern NSString *const PAPUtilityUserLikedUnlikedPhotoCallbackFinishedNotification;
extern NSString *const PAPUtilityDidFinishProcessingProfilePictureNotification;
extern NSString *const PAPTabBarControllerDidFinishEditingPhotoNotification;
extern NSString *const PAPTabBarControllerDidFinishImageFileUploadNotification;
extern NSString *const PAPPhotoDetailsViewControllerUserDeletedPhotoNotification;
extern NSString *const PAPPhotoDetailsViewControllerUserLikedUnlikedPhotoNotification;
extern NSString *const PAPPhotoDetailsViewControllerUserCommentedOnPhotoNotification;


#pragma mark - User Info Keys
extern NSString *const PAPPhotoDetailsViewControllerUserLikedUnlikedPhotoNotificationUserInfoLikedKey;
extern NSString *const kPAPEditPhotoViewControllerUserInfoCommentKey;


#pragma mark - Installation Class

// Field keys
extern NSString *const kPAPInstallationUserKey;


#pragma mark - PFObject Activity Class
// Class key
extern NSString *const kPAPActivityClassKey;

#pragma mark - PFObject PrivateFeed Class
NSString *const kPAPPrivateFeedClassKey;
NSString *const kPAPPrivateGroupClassKey;
NSString *const kPAPPrivateGroupActivityClassKey;



// Field keys
extern NSString *const kPAPActivityTypeKey;
extern NSString *const kPAPActivityFromUserKey;
extern NSString *const kPAPActivityToUserKey;
extern NSString *const kPAPActivityContentKey;
extern NSString *const kPAPActivityPhotoKey;
extern NSString *const kPAPActivityRatingCountKey;
extern NSString *const kPAPActivityUpvoteCommentsCount;
extern NSString *const kPAPActivityUpvoteCommentsUsersArrayKey;

// Type values
extern NSString *const kPAPActivityTypeLike;
//extern NSString *const kPAPActivityTypeUpvote;
extern NSString *const kPAPActivityTypeFollow;
extern NSString *const kPAPActivityTypeComment;
extern NSString *const kPAPActivityTypeJoined;
extern NSString *const kPAPActivityTypeRating;
extern NSString *const kPAPActivityTypeCommentTag;


#pragma mark - PFObject User Class
// Field keys
extern NSString *const kPAPUserDisplayNameKey;
extern NSString *const kPAPUserDisplayLocationKey;
extern NSString *const kPAPUserFacebookIDKey;
extern NSString *const kPAPUserPhotoIDKey;
extern NSString *const kPAPUserProfilePicSmallKey;
extern NSString *const kPAPUserProfilePicMediumKey;
extern NSString *const kPAPUserFacebookFriendsKey;
extern NSString *const kPAPUserAlreadyAutoFollowedFacebookFriendsKey;
extern NSString *const kPAPInstallationChannelsKey;
extern NSString *const kPAPUserPrivateChannelKey;
extern NSString *const kPAPPrivateGroupUsersKey;



#pragma mark - PFObject Photo Class
// Class key
extern NSString *const kPAPPhotoClassKey;

// Field keys
extern NSString *const kPAPPhotoPictureKey;
extern NSString *const kPAPPhotoThumbnailKey;
extern NSString *const kPAPPhotoUserKey;
extern NSString *const kPAPPhotoOpenGraphIDKey;
extern NSString *const kPAPPhotoCommentsCount;
extern NSString *const kPAPPhotoDescriptionTextKey;
extern NSString *const kPAPPhotoLikeCountKey;
extern NSString *const kPAPPhotoTotalRating;
extern NSString *const kPAPPhotoTotalUsersRated;
extern NSString *const kPAPPhotoCategory;
extern NSString *const kPAPPhotoUpperPrice;
extern NSString *const kPAPPhotoLowerPrice;
extern NSString *const kPAPPhotoBought;
extern NSString *const kPAPPhotoBlock;
extern NSString *const kPAPPhotoMediaTypeKey;
extern NSString *const kPAPPhotoVidoKey;
extern NSString *const kPAPPhotoAudioKey;
extern NSString *const kPAPPhotoMediaPhotoKey;
extern NSString *const kPAPPhotoMediaVideoKey;
extern NSString *const kPAPPhotoMediaAudioKey;
extern NSString *const kPAPPhotoCommentArrayKey;

#pragma mark - Cached Photo Attributes
// keys
extern NSString *const kPAPPhotoAttributesIsLikedByCurrentUserKey;
extern NSString *const kPAPPhotoAttributesIsRatedByCurrentUserKey;
extern NSString *const kPAPPhotoAttributesCurrentUserRatingKey;
extern NSString *const kPAPPhotoAttributesLikeCountKey;
extern NSString *const kPAPPhotoAttributesLikersKey;
extern NSString *const kPAPPhotoAttributesCommentCountKey;
extern NSString *const kPAPPhotoAttributesCommentersKey;

extern NSString *const kPAPhotoAttributesCommetnterUserskey;
extern NSString *const kPAPPhotoAttributesCommentsKey;


#pragma mark - Cached User Attributes
// keys
extern NSString *const kPAPUserAttributesPhotoCountKey;
extern NSString *const kPAPUserAttributesIsFollowedByCurrentUserKey;


#pragma mark - PFPush Notification Payload Keys

extern NSString *const kAPNSAlertKey;
extern NSString *const kAPNSBadgeKey;
extern NSString *const kAPNSSoundKey;

extern NSString *const kPAPPushPayloadPayloadTypeKey;
extern NSString *const kPAPPushPayloadPayloadTypeActivityKey;
extern NSString *const kPAPPushPayloadPayloadTypeChatKey;
extern NSString *const kPAPPushPayloadChatUserIdKey ;


extern NSString *const kPAPPushPayloadActivityTaggedInPhotoKey;
extern NSString *const kPAPPushPayloadActivityTaggedInCommentKey;
extern NSString *const kPAPPushPayloadActivityTypeKey;
extern NSString *const kPAPPushPayloadActivityLikeKey;
extern NSString *const kPAPPushPayloadActivityUpVoteKey;
extern NSString *const kPAPPushPayloadActivityCommentKey;
extern NSString *const kPAPPushPayloadActivityFollowKey;

extern NSString *const kPAPPushPayloadFromUserObjectIdKey;
extern NSString *const kPAPPushPayloadToUserObjectIdKey;
extern NSString *const kPAPPushPayloadPhotoObjectIdKey;


extern NSString *const kPAPSaveOriginalPhoto;

extern NSString *const kPAPFeedClassKey;;

extern NSString *const kPAPFeedUserKey;
extern NSString *const kPAPFeedCommentsCount;
extern NSString *const kPAPFeedLikeCountKey;
extern NSString *const kPAPFeedBlock;
extern NSString *const kPAPFeedMediaTypeKey;
extern NSString *const kPAPFeedMediaTypeVideoKey;
extern NSString *const kPAPFeedMediaTypeImageKey;
extern NSString *const kPAPFeedMediaThumbnailLikKey;
extern NSString *const kPAPFeedMediaLinkKey;
extern NSString *const kPAPFeedCommentersArrayKey;
extern NSString *const kPAPFeedLocationKey;
extern NSString *const kPAPFeedCommentsArrayKey;
extern NSString *const kPAPFeedLikedByArrayKey;
extern NSString *const kPAPFeedActivityArrayKey;
extern NSString *const kPAPFeedChannelKey;


extern NSString *const kPAPFeedPhotoCurrentLocaiton ;
extern NSString *const kPAPFeedPhotoTaggedLocation;
extern NSString *const kPAPFeedPhotoTaggedLocationName;

extern NSString *const kPAPFollowersClassKey;

extern NSString *const kPAPFollowersFollowedByKey;
extern NSString *const kPAPFollowersFollowingKey;