//
//  AmazonTransfer.h
//  
//
//  Created by rahul Sharma on 04/09/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>
#import <AWSS3/AWSS3TransferManager.h>



typedef void(^AmazonTransferCompletionBlock)(BOOL success, id result , NSError *error);
typedef void(^AmazonTransferProgressBlock)(NSInteger progressSize, NSInteger expectedSize);

@interface AmazonTransfer : NSObject

+ (void) setConfigurationWithRegion:(AWSRegionType)regionType
                         accessKey:(NSString*)accessKey
                         secretKey:(NSString*)secretKey;

+ (void) upload:(NSString*)localFilePath
         fileKey:(NSString*)fileKey
        toBucket:(NSString*)bucket
       mimeType:(NSString *) mimeType
   progressBlock:(AmazonTransferProgressBlock) progressBlock
 completionBlock:(AmazonTransferCompletionBlock) completionBlock;

+ (void) download:(NSString*)fileKey
       fromBucket:(NSString*)bucket
           toFile:(NSString*)localFilePath
   progressBlock:(AmazonTransferProgressBlock) progressBlock
 completionBlock:(AmazonTransferCompletionBlock) completionBlock;


@end


