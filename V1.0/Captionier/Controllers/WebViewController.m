//
//  WebViewController.m
//  Restaurant
//
//  Created by 3Embed on 25/09/12.
//
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

@synthesize weburl;
@synthesize webview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)backButtonAction:(id)sender {
   // [self.navigationController popViewControllerAnimated:YES];
    if (_delegate && [_delegate respondsToSelector:@selector(dismissWithOption:)]) {
        [_delegate dismissWithOption:NO];
    }
    else {
      [self dismissViewControllerAnimated:YES completion:nil];  
    }
    //
}

- (void)acceptButtonAction:(id)sender {
    // [self.navigationController popViewControllerAnimated:YES];
    
    if (_delegate && [_delegate respondsToSelector:@selector(dismissWithOption:)]) {
        [_delegate dismissWithOption:YES];
    }
    //[self dismissViewControllerAnimated:YES completion:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
      
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    //    backButton.titleLabel.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
    //    backButton.titleEdgeInsets = UIEdgeInsetsMake( 0.0f, 5.0f, 0.0f, 0.0f);
    //    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    //    [backButton setTitleColor:[UIColor colorWithRed:214.0f/255.0f green:210.0f/255.0f blue:197.0f/255.0f alpha:1.0] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
   // [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn_on.png"] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    UIButton *acceptButton = [UIButton buttonWithType:UIButtonTypeCustom];
    acceptButton.frame = CGRectMake( 0.0f, 0.0f,60, 30);
    acceptButton.titleLabel.font = [UIFont fontWithName:Aharoni_Bold size:16];
    acceptButton.titleEdgeInsets = UIEdgeInsetsMake( 5.0f, 5.0f, 0.0f, 0.0f);
    [acceptButton setTitle:@"Accept" forState:UIControlStateNormal];
    [acceptButton setTitleColor:[UIColor colorWithRed:214.0f/255.0f green:210.0f/255.0f blue:197.0f/255.0f alpha:1.0] forState:UIControlStateNormal];
    [acceptButton addTarget:self action:@selector(acceptButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (![[[PFUser currentUser] objectForKey:@"eulaAccepted"] boolValue])
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:acceptButton];
    }
    NSURL *url = [NSURL URLWithString:weburl];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webview loadRequest:requestObj];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:@""];
   
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.webview = nil;
   
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.webview stopLoading];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
      ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
      ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}

@end
