//
//  ProgressView.h
//  Anypic
//
//  Created by rahul Sharma on 14/08/13.
//
//

#import <UIKit/UIKit.h>

@interface ProgressView : UIView
@property(nonatomic,strong)UIActivityIndicatorView *activityIndicator;
@property(nonatomic,strong)UILabel *lblMessage;
-(void)setMessage:(NSString*)message;
-(void)hide;
+ (id)sharedInstance;
@end
