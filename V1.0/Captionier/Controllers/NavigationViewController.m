//
//  NavigationViewController.m
//  REMenuExample
//
//  Created by Roman Efimov on 4/18/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//
//  Sample icons from http://icons8.com/download-free-icons-for-ios-tab-bar
//

#import "NavigationViewController.h"
#import "PAPHomeViewController.h"
//#import "ExploreViewController.h"
#import "PAPActivityFeedViewController.h"
#import "PAPAccountViewController.h"
#import "CameraViewController.h"
#import "SearchOptionsViewController.h"
#import "PBJViewController.h"
#import "CustomCameraViewController.h"
#import "InviteFriendsViewController.h"
#import "FollowersFollowingViewController.h"
#import "GWExploreViewController.h"
#import "ExploreViewController.h"

@interface NavigationViewController ()

@property (strong, readwrite, nonatomic) REMenu *menu;

@end

@implementation NavigationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (REUIKitIsFlatMode()) {
        [self.navigationBar performSelector:@selector(setBarTintColor:) withObject:[UIColor colorWithRed:0/255.0 green:213/255.0 blue:161/255.0 alpha:1]];
        self.navigationBar.tintColor = [UIColor whiteColor];
    } else {
        self.navigationBar.tintColor = [UIColor colorWithRed:0 green:179/255.0 blue:134/255.0 alpha:1];
    }
    
    __typeof (self) __weak weakSelf = self;
    REMenuItem *homeItem = [[REMenuItem alloc] initWithTitle:@"Home"
                                                    subtitle:@""
                                                       image:[UIImage imageNamed:@"homescreen_home_icon_on.png"]
                                            highlightedImage:nil
                                                      action:^(REMenuItem *item) {
                                                          NSLog(@"Item: %@", item);
                                                          PAPHomeViewController *controller = [[PAPHomeViewController alloc] init];
                                                          [weakSelf setViewControllers:@[controller] animated:NO];
                                                      }];
    
    REMenuItem *exploreItem = [[REMenuItem alloc] initWithTitle:@"Explore"
                                                       subtitle:@""
                                                          image:[UIImage imageNamed:@"homescreen_explore_icon_on.png"]
                                               highlightedImage:nil
                                                         action:^(REMenuItem *item) {
                                                             NSLog(@"Item: %@", item);
                                                             SearchOptionsViewController *controller = [[SearchOptionsViewController alloc] init];
                                                             [weakSelf setViewControllers:@[controller] animated:NO];
                                                             controller.title = @"Explore";
                                                             
                                                             [weakSelf setViewControllers:@[controller] animated:NO];

                                                         }];
    
    REMenuItem *activityItem = [[REMenuItem alloc] initWithTitle:@"Activity"
                                                        subtitle:@""
                                                           image:[UIImage imageNamed:@"homescreen_activty_icon_on.png"]
                                                highlightedImage:nil
                                                          action:^(REMenuItem *item) {
                                                              NSLog(@"Item: %@", item);
                                                              PAPActivityFeedViewController *controller = [[PAPActivityFeedViewController alloc] init];
                                                              
                                                              [weakSelf setViewControllers:@[controller] animated:NO];
                                                          }];

    REMenuItem *cameraItem = [[REMenuItem alloc] initWithTitle:@"Camera"
                                                        subtitle:@""
                                                         image:[UIImage imageNamed:@"camera_menu_icon_off"]
                                                highlightedImage:nil
                                                          action:^(REMenuItem *item) {
                                                              NSLog(@"Item: %@", item);
                                                              if (IS_IPHONE_5) {
                                                                  CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController" bundle:nil];
                                                                  
                                                                  [weakSelf pushViewController:obj animated:YES];
                                                              }
                                                              else {
                                                                  CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController_ip4" bundle:nil];
                                                                  
                                                                  [weakSelf pushViewController:obj animated:YES];
                                                              }
                                                              [weakSelf setNavigationBarHidden:YES animated:NO];
                                                              
                                                          }];
    

    
//    REMenuItem *followItem = [[REMenuItem alloc] initWithTitle:@"Follow"
//                                                      subtitle:@""
//                                                         image:[UIImage imageNamed:@"following_menu_icon"]
//                                              highlightedImage:nil
//                                                        action:^(REMenuItem *item) {
//                                                            NSLog(@"Item: %@", item);
//                                                            FollowersFollowingViewController *followersVC = [[FollowersFollowingViewController alloc] init];
//                                                            followersVC.type = Following;
//                                                            followersVC.user = [PFUser currentUser];
//                                                            followersVC.title =  @"Follow";
//                                                            [weakSelf setViewControllers:@[followersVC] animated:NO];
//                                                        }];
//
//    
//    REMenuItem *inviteItem = [[REMenuItem alloc] initWithTitle:@"Invite"
//                                                      subtitle:@""
//                                                         image:[UIImage imageNamed:@"channel_video_btn"]
//                                              highlightedImage:nil
//                                                        action:^(REMenuItem *item) {
//                                                            NSLog(@"Item: %@", item);
//                                                            InviteFriendsViewController *controller = [[InviteFriendsViewController alloc] initWithNibName:@"InviteFriendsViewController" bundle:nil];
//                                                            controller.title = @"Invite";
//                                                            [weakSelf setViewControllers:@[controller] animated:NO];
//                                                        }];
//

//    REMenuItem *inviteItem = [[REMenuItem alloc] initWithTitle:@"Xtreme Channel"
//                                                      subtitle:@""
//                                                         image:[UIImage imageNamed:@"explore_gerday_way_icon_off"]
//                                              highlightedImage:nil
//                                                        action:^(REMenuItem *item) {
//                                                            NSLog(@"Item: %@", item);
//                                                            ExploreViewController *exploreViewVC = [[ExploreViewController alloc] initWithStyle:UITableViewStylePlain];
//                                                            exploreViewVC.category = nil;
//                                                            exploreViewVC.title = @"Xtreme Channel";
//
//                                                            [weakSelf setViewControllers:@[exploreViewVC] animated:NO];
//                                                        }];

    
//    REMenuItem *activityItem = [[REMenuItem alloc] initWithTitle:@"Activity"
//                                                        subtitle:@"Perform 3 additional activities"
//                                                           image:[UIImage imageNamed:@"Icon_Activity"]
//                                                highlightedImage:nil
//                                                          action:^(REMenuItem *item) {
//                                                              NSLog(@"Item: %@", item);
//                                                              CameraViewController *controller = [[CameraViewController alloc] init];
//                                                              
//                                                              [weakSelf setViewControllers:@[controller] animated:NO];
//                                                          }];
    
    //activityItem.badge = @"12";
    
    REMenuItem *profileItem = [[REMenuItem alloc] initWithTitle:@"Profile"
                                                          image:[UIImage imageNamed:@"homescreen_userhome_icon_on.png"]
                                               highlightedImage:nil
                                                         action:^(REMenuItem *item) {
                                                             NSLog(@"Item: %@", item);
                                                             PAPAccountViewController *controller = [[PAPAccountViewController alloc] init];
                                                             controller.user = [PFUser currentUser];
                                                             controller.showBackButton = NO;
                                                             [weakSelf setViewControllers:@[controller] animated:NO];
                                                         }];
    
    // You can also assign a custom view for any particular item
    // Uncomment the code below and add `customViewItem` to `initWithItems` array, for example:
    // self.menu = [[REMenu alloc] initWithItems:@[homeItem, exploreItem, activityItem, profileItem, customViewItem]]
    //
    /*
    UIView *customView = [[UIView alloc] init];
    customView.backgroundColor = [UIColor blueColor];
    customView.alpha = 0.4;
    REMenuItem *customViewItem = [[REMenuItem alloc] initWithCustomView:customView action:^(REMenuItem *item) {
        NSLog(@"Tap on customView");
    }];
    */
    
    homeItem.tag = 0;
    profileItem.tag = 1;
    cameraItem.tag = 2;
    exploreItem.tag = 3;
    activityItem.tag = 4;
    //followItem.tag = 4;
    //inviteItem.tag = 5;
    
    self.menu = [[REMenu alloc] initWithItems:@[homeItem, profileItem, cameraItem, exploreItem, activityItem]];
    
    // Background view
    //
    //self.menu.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    //self.menu.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //self.menu.backgroundView.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.600];

    //self.menu.imageAlignment = REMenuImageAlignmentRight;
    //self.menu.closeOnSelection = NO;
    //self.menu.appearsBehindNavigationBar = NO; // Affects only iOS 7
    if (!REUIKitIsFlatMode()) {
        self.menu.cornerRadius = 4;
        self.menu.shadowRadius = 4;
        self.menu.shadowColor = [UIColor blackColor];
        self.menu.shadowOffset = CGSizeMake(0, 1);
        self.menu.shadowOpacity = 1;
    }
    
    // Blurred background in iOS 7
    //
    //self.menu.liveBlur = YES;
    //self.menu.liveBlurBackgroundStyle = REMenuLiveBackgroundStyleDark;
    //self.menu.liveBlurTintColor = [UIColor redColor];
    
    self.menu.imageOffset = CGSizeMake(5, -1);
    self.menu.waitUntilAnimationIsComplete = NO;
    self.menu.badgeLabelConfigurationBlock = ^(UILabel *badgeLabel, REMenuItem *item) {
        badgeLabel.backgroundColor = [UIColor colorWithRed:0 green:179/255.0 blue:134/255.0 alpha:1];
        badgeLabel.layer.borderColor = [UIColor colorWithRed:0.000 green:0.648 blue:0.507 alpha:1.000].CGColor;
    };
}

- (void)toggleMenu
{
    if (self.menu.isOpen)
        return [self.menu close];
    
    [self.menu showFromNavigationController:self];
}

@end
