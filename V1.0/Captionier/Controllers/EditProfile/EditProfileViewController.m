//
//  EditProfileViewController.m
//  MealTrend
//
//  Created by rahul Sharma on 30/10/13.
//
//

#import "EditProfileViewController.h"
#import "PAPAccountViewController.h"
#import "EditProfileCell.h"

@interface EditProfileViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property(nonatomic,strong)PFImageView *profilePictureImageView;
@property(nonatomic,assign)BOOL isImageEdited;
@property(nonatomic,strong)NSMutableDictionary *userDetail;
@property(nonatomic,weak)IBOutlet UITableView *tbleView;
@property(nonatomic,assign)BOOL isKeyboardIsShown;
@end

@implementation EditProfileViewController
@synthesize displayName;
@synthesize displaypic;
@synthesize bioData;
@synthesize viewBG;
@synthesize object;
@synthesize labelPlaceHolder;
@synthesize profilePictureImageView;
@synthesize userDetail;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)backButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    PFUser *currentUser = [PFUser currentUser];

    userDetail = [[NSMutableDictionary alloc] init];
    [userDetail setObject:[currentUser objectForKey:kPAPUserDisplayNameKey] forKey:kPAPUserDisplayNameKey];
   
    [userDetail setObject:currentUser.username forKey:@"userName"];
    
    if ([currentUser objectForKey:@"bioData"]) {
    [userDetail setObject:[currentUser objectForKey:@"bioData"] forKey:@"bioData"];
    }
    if ([currentUser objectForKey:@"Gender"]) {
        [userDetail setObject:[currentUser objectForKey:@"Gender"] forKey:@"Gender"];
    }
    if ([currentUser objectForKey:@"Phone"]) {
        [userDetail setObject:[currentUser objectForKey:@"Phone"] forKey:@"Phone"];
    }
    if ([currentUser objectForKey:@"Website"]) {
        [userDetail setObject:[currentUser objectForKey:@"Website"] forKey:@"Website"];
    }
//    if ([currentUser objectForKey:@"Website"]) {
//        [userDetail setObject:[currentUser objectForKey:@"Website"] forKey:@"Website"];
//    }
    if (currentUser.email) {
        [userDetail setObject:currentUser.email forKey:@"Email"];
    }
    
    
    UIView *profileImageViewBG = [[UIView alloc] initWithFrame:CGRectMake(100, 10, 100, 130)];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tap.numberOfTapsRequired = 1;
    [profileImageViewBG addGestureRecognizer:tap];
    
    [self.viewBG addSubview:profileImageViewBG];
    [self.tbleView addSubview:profileImageViewBG];

    profilePictureImageView = [[PFImageView alloc] initWithFrame:CGRectMake(120, 10, 80, 80)];//0, 0, 80, 80
    //[viewBG addSubview:profilePictureImageView];
    [profilePictureImageView setContentMode:UIViewContentModeScaleAspectFill];
    PFFile *imageFile = [currentUser objectForKey:kPAPUserProfilePicMediumKey];
    if (imageFile) {
        [profilePictureImageView setFile:imageFile];
        profilePictureImageView.layer.cornerRadius = profilePictureImageView.frame.size.width/2;
        profilePictureImageView.clipsToBounds = YES;
        [profilePictureImageView setClipsToBounds:YES];
        [profilePictureImageView loadInBackground:^(UIImage *image, NSError *error) {
            if (!error) {
                [UIView animateWithDuration:0.200f animations:^{
                    //profilePictureBackgroundView.alpha = 1.0f;
                    // profilePictureStrokeImageView.alpha = 1.0f;
                    profilePictureImageView.alpha = 1.0f;
                }];
            }
        }];
    }
    
    //self.tbleView.tableHeaderView = profileImageViewBG;
    /**********/
    [profileImageViewBG addSubview:profilePictureImageView];
    self.tbleView.tableHeaderView = profileImageViewBG;
    
    UIButton *btnSelectImage = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSelectImage addTarget:self action:@selector(selectPicture:) forControlEvents:UIControlEventTouchUpInside];
    btnSelectImage.frame = CGRectMake(120, 5, 50, 80);
    [profileImageViewBG addSubview:btnSelectImage];
    
    //photo label
    UIView *lblview = [[UIView alloc] initWithFrame:CGRectMake(110, 90, 100, 50)];
    UILabel *imgLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    imgLbl.backgroundColor = CLEAR_COLOR;
    imgLbl.textAlignment = NSTextAlignmentCenter;
    [Helper setToLabel:imgLbl Text:@"Add/Edit Image" WithFont:robo FSize:12 Color:[UIColor grayColor]];
    [lblview addSubview:imgLbl];
    [profileImageViewBG addSubview:lblview];
   // self.tbleView.tableHeaderView =lblview;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked:)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelButtonClicked:)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    self.navigationItem.title = @"Edit Profile";
}
-(void)handleSingleTap:(UITapGestureRecognizer*)gesture{
    [self.view endEditing:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    _isKeyboardIsShown = NO;

}
-(void)viewDidAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
-(IBAction)editImage:(id)sender{
    [self selectPicture:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
     if (textField.tag == 700) {
        
        //[self moveViewDown];
        
        [textField resignFirstResponder];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Gender" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Male",@"Female",@"Not Specified", nil];
        actionSheet.tag = 201;
        [actionSheet showInView:self.view];
        return NO;
    }
    if (textField.tag == 400 || textField.tag == 500 || textField.tag == 600) {
        
//        CGRect viewFrame = self.tbleView.frame;
//        viewFrame.origin.y = -174;
//        
//        [UIView animateWithDuration:0.2 animations:^(void){
//            self.tbleView.frame = viewFrame;
//        }];
    }
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    [self saveEditedValues:textField];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    [self saveEditedValues:textField];
//    if (textField.tag >= 400) {
//        [self moveViewDown];
//    }
    
    return YES;
}



-(void)saveEditedValues:(UITextField*)textField{
    
    int tag = textField.tag;
    
    switch (tag) {
        case 100: //display name
        {
            [userDetail setObject:textField.text forKey:kPAPUserDisplayNameKey];
            break;
        }
        case 200:
        {
            [userDetail setObject:textField.text forKey:@"userName"];
            break;
        }
        case 300:
        {
            [userDetail setObject:textField.text forKey:@"Website"];
            break;
        }
        case 400:
        {
            [userDetail setObject:textField.text forKey:@"bioData"];
            break;
        }
        case 500:
        {
            [userDetail setObject:textField.text forKey:@"Email"];
            break;
        }
        case 600:
        {
            [userDetail setObject:textField.text forKey:@"Phone"];
            
            break;
        }
        case 700:
        {
            [userDetail setObject:textField.text forKey:@"Gender"];
            break;
        }
        default:
            break;
    }
}


#pragma TextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    if(textView.text.length >1 || text.length != 0)
    {
        [labelPlaceHolder setHidden:YES];
    }
    else
    {
        [labelPlaceHolder setHidden:NO];
    }
    
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    else {
        int limit = 139;
        
        return !([textView.text length]>limit && [text length] > range.length);
        
    }
    return YES;
    
    
    
    
//    
//    if([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
//        return NO;
//    }
//    else {
//        int limit = 139;
//        
//        return !([textView.text length]>limit && [text length] > range.length);
//
//    }
//    
//    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView.text.length == 0) {
        [labelPlaceHolder setHidden:NO];
    }
    else
    {
        [labelPlaceHolder setHidden:YES];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length == 0) {
        [labelPlaceHolder setHidden:NO];
    }
    else
    {
        [labelPlaceHolder setHidden:YES];
    }
}

/**
 *  This method will close the profile editing screen
 *
 *  @param sender closeBtn press
 */
-(IBAction)cancelButtonClicked:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^(void){
          }];
}



/**
 *  This method will save user updated details on parse
 *
 *  @param sender DoneBtn press
 */
-(IBAction)doneButtonClicked:(id)sender{
    
    //[PFUser enableAutomaticUser];
    
    [self.view endEditing:YES];
    
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:@"Please wait.."];
    
    [[PFUser currentUser] setObject:userDetail[kPAPUserDisplayNameKey] forKey:kPAPUserDisplayNameKey];
    if (userDetail[@"bioData"]) {
        [[PFUser currentUser] setObject:userDetail[@"bioData"] forKey:@"bioData"];
    }
    if (userDetail[@"Website"]) {
        [[PFUser currentUser] setObject:userDetail[@"Website"] forKey:@"Website"];
    }
    if (userDetail[@"Gender"]) {
        [[PFUser currentUser] setObject:userDetail[@"Gender"] forKey:@"Gender"];
    }
    if (userDetail[@"Email"]) {
        [[PFUser currentUser] setObject:userDetail[@"Email"] forKey:@"email"];
    }
    if (userDetail[@"userName"]) {
        [[PFUser currentUser] setObject:userDetail[@"userName"] forKey:@"username"];
    }
    if (userDetail[@"Phone"]) {
        [[PFUser currentUser] setObject:userDetail[@"Phone"] forKey:@"Phone"];
    }
    [[PFUser currentUser] saveInBackground];
    
    [[PFUser currentUser] refresh];
    if (_isImageEdited) {
        [PAPUtility saveProfilePictueblock:^(BOOL succedd, NSError*error){
            [pi hideProgressIndicator];
            [self dismissViewControllerAnimated:YES completion:^(void){
                if (self.onCompletion) {
                    self.onCompletion(YES);
                }
            }];
        }];
    }
    else {
        [pi hideProgressIndicator];
        
        [self dismissViewControllerAnimated:YES completion:^(void){
            if (self.onCompletion) {
                self.onCompletion(NO);
            }
        }];
       
    }

    
    
    
}
-(IBAction)selectPicture:(id)sender {
    
    
   UIActionSheet *acctionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Library", nil];
    acctionSheet.tag = 200;
    [acctionSheet showInView:self.view];
    
}
#pragma mark ActionSheetButton Methods


/**
 *  This method will open device camera to capture image
 *
 *  @param sender cameraBtn press
 */
-(void)cameraButtonClicked:(id)sender
{
    
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.allowsEditing = YES;
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerCameraDeviceFront;
    [self presentViewController:picker animated:YES completion:nil];
}

/**
 *  This method will open device image library to pick image
 *
 *  @param sender libraryBtn press
 */
-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    // picker.contentSizeForViewInPopover = CGSizeMake(400, 800);
    //    [self presentViewController:picker animated:YES completion:nil];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        
        
        
        //[popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}



/**
 *  This method will set default image if image is not selected from either of options
 *
 *  @param sender 
 */
-(void)removeImageClicked:(id)sender
{
    //UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    profilePictureImageView.image = [UIImage imageNamed:@"profile_bg"];
    //[self presentViewController:picker animated:YES completion:nil];
    
 
}


#pragma UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage  *albumImg = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    
    profilePictureImageView.image = albumImg;
    // profileImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"FacebookProfilePicture.jpg"];
    
    NSData *data = UIImageJPEGRepresentation(albumImg,1.0);
    [data writeToFile:getImagePath atomically:YES];
    
    _isImageEdited = YES;
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 200) {
        switch (buttonIndex) {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
//            case 2:
//            {
//                [self removeImageClicked:nil];
//                break;
//            }
            default:
                break;
        }
    }
    else if(actionSheet.tag == 201){
        switch (buttonIndex) {
            case 0:
            {
                
                [userDetail setObject:@"Male" forKey:@"Gender"];
                [self.tbleView reloadData];
                break;
            }
            case 1:
            {
                [userDetail setObject:@"Female" forKey:@"Gender"];
                [self.tbleView reloadData];
                break;
            }
            case 2:
            {
                [userDetail setObject:@"Not Specified" forKey:@"Gender"];
                [self.tbleView reloadData];
                break;
            }
            default:
                break;
        }
        [self moveViewDown];
    }
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 4;
    }
    else if(section == 1){
        return 3;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    EditProfileCell *cell = (EditProfileCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  //  UITextField *cell.textfeild;
    if (!cell) {
        cell = [[EditProfileCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont fontWithName:HELVETICANEUE_LIGHT size:18];
        cell.textLabel.textColor =  UIColorFromRGB(0x000000);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textfeild.delegate = self;
    }
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
            cell.textfeild.placeholder = @"display name";
            cell.textfeild.tag = 100;
            cell.textfeild.frame = CGRectMake(10, 5, 210, 35);
            cell.textfeild.text = userDetail[kPAPUserDisplayNameKey];
        }
        else if(indexPath.row == 1) {
           
            cell.textfeild.placeholder = @"username";
            cell.textfeild.tag = 200;
            cell.textfeild.frame = CGRectMake(10, 5, 210, 35);
            cell.textfeild.text = userDetail[@"userName"];
            cell.textfeild.userInteractionEnabled = NO;
        }
        else if(indexPath.row == 2) {
            
            cell.textfeild.placeholder = @"website";
            cell.textfeild.tag = 300;
            cell.textfeild.keyboardType = UIKeyboardTypeURL;
             cell.textfeild.text = userDetail[@"Website"];
        }
        else if(indexPath.row == 3) {
            
            cell.textfeild.placeholder = @"bio";
            cell.textfeild.tag = 400;
             cell.textfeild.text = userDetail[@"bioData"];
        }
    }
    else if(indexPath.section == 1){
        
        if (indexPath.row == 0) {
           
            cell.textfeild.placeholder = @"email";
            cell.textfeild.tag = 500;
            cell.textfeild.text = userDetail[@"Email"];
        }
        else if(indexPath.row == 1) {
            cell.textfeild.keyboardType = UIKeyboardTypePhonePad;
            cell.textfeild.placeholder = @"phone";
            cell.textfeild.tag = 600;
            cell.textfeild.text = userDetail[@"Phone"];
            
        }
        else if(indexPath.row == 2) {
          
            cell.textfeild.placeholder = @"Gender";
            cell.textfeild.tag = 700;
            cell.textfeild.text = userDetail[@"Gender"];
            
        }
    }
    
    return cell;
}





-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
        }
        else if(indexPath.row == 1) {
            
        }
        else if(indexPath.row == 2) {
            
        }
        else if(indexPath.row == 3) {
            
        }
    }
    else if(indexPath.section == 1){
        
        if (indexPath.row == 0) {
            
        }
        else if(indexPath.row == 1) {
            
        }
        else if(indexPath.row == 2) {
            
        }
    }
}
-(void)moveViewDown {
    CGRect viewFrame = self.tbleView.frame;
    viewFrame.origin.y = 0;
    
    [UIView animateWithDuration:0.1 animations:^(void){
        self.tbleView.frame = viewFrame;
    }];
}

#pragma mark - UIKeyBoardNotification Methods
-(void) keyboardWillHide:(NSNotification *)note
{
    // NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:0.1 animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        [self.tbleView setContentInset:edgeInsets];
        [self.tbleView setScrollIndicatorInsets:edgeInsets];
    }];
    
}
-(void) keyboardWillShow:(NSNotification *)note
{
    //CGRect keyboardEndFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGSize kbSize = [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    // NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    CGFloat height = UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation]) ? kbSize.height : kbSize.width;
    
    [UIView animateWithDuration:0.1 animations:^{
        UIEdgeInsets edgeInsets = [self.tbleView contentInset];
        edgeInsets.bottom = height;
        [self.tbleView setContentInset:edgeInsets];
        edgeInsets = [self.tbleView scrollIndicatorInsets];
        edgeInsets.bottom = height;
        [self.tbleView setScrollIndicatorInsets:edgeInsets];
    }];    _isKeyboardIsShown = YES;
}


@end
