//
//  EditProfileCell.m
//  MyAlbum
//
//  Created by Surender Rathore on 10/01/14.
//
//

#import "EditProfileCell.h"

@implementation EditProfileCell
@synthesize textfeild;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        textfeild = [[UITextField alloc] initWithFrame:CGRectMake(10, 5, 320, 35)];
        
        textfeild.borderStyle = UITextBorderStyleNone;
        textfeild.textColor = BLACK_COLOR;
        [self addSubview:textfeild];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
