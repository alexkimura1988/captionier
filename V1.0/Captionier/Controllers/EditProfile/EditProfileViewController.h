//
//  EditProfileViewController.h
//  MealTrend
//
//  Created by rahul Sharma on 30/10/13.
//
//

#import <UIKit/UIKit.h>


@interface EditProfileViewController : UIViewController
@property(nonatomic,weak) IBOutlet UITextField *displayName;
@property(nonatomic,weak) IBOutlet UITextView  *bioData;
@property(nonatomic,weak) IBOutlet PFImageView *displaypic;
@property(nonatomic,weak) IBOutlet UIView *viewBG;
@property(nonatomic,weak) IBOutlet UILabel *labelPlaceHolder;
@property(nonatomic,strong)PFObject *object;
@property (nonatomic, copy) void (^onCompletion)(BOOL isImagedEdited);
-(IBAction)cancelButtonClicked:(id)sender;
-(IBAction)doneButtonClicked:(id)sender;
-(IBAction)editImage:(id)sender;

@end
