//
//  HelpSreenViewController.m
//  Signature
//
//  Created by Rahul Sharma on 26/01/13.
//
//

#import "HelpSreenViewController.h"
#import "Helper.h"

@interface HelpSreenViewController ()

@end

@implementation HelpSreenViewController
@synthesize scrollView;
@synthesize selectedview;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)SkipButton
{
    
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    
    if (screenSize.size.height==480)
    {
        
    btnskip=[UIButton buttonWithType:UIButtonTypeCustom];
    [btnskip setFrame:CGRectMake(280, 25, 40, 40)];
   [btnskip setBackgroundColor:[UIColor clearColor]];
        //[Helper setButton:btnskip Text:@"Ende" WithFont:@"" FSize:15 TitleColor:[UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0]
          //ShadowColor:nil];
    [btnskip setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"close.png"]]];
    [btnskip addTarget:self action:@selector(btnSkipPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnskip];
    }
    else{
        

        btnskip=[UIButton buttonWithType:UIButtonTypeCustom];
        [btnskip setFrame:CGRectMake(280, 87, 40, 40)];
       
        [btnskip setBackgroundColor:[UIColor clearColor]];
        [btnskip setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"close.png"]]];
        [btnskip addTarget:self action:@selector(btnSkipPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnskip];

        
    }
}

-(void)btnSkipPressed
{
//    if (selectedview==0) {
//        
//     [self dismissViewControllerAnimated:YES completion:nil];
//    }
//    
//    if (selectedview==1)
//    {
//
//    }
//    
//    [self dismissViewControllerAnimated:YES completion:nil];
    
    
//    [(UINavigationController *)self.presentingViewController  popViewControllerAnimated:NO];
//    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    self.navigationController.navigationBarHidden=NO;
}


-(void)nextbutton
{
    CGRect screenSize = [[UIScreen mainScreen] bounds];
   
    if (screenSize.size.height==480)
    {
        
        buttonnext=[UIButton buttonWithType:UIButtonTypeCustom];
        [buttonnext setFrame:CGRectMake(215, 430, 80, 30)];
        [Helper setButton:buttonnext Text:@"Next" WithFont:@"" FSize:15 TitleColor:[UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0]
              ShadowColor:nil];
       
        [buttonnext addTarget:self action:@selector(buttonnextPressed) forControlEvents:UIControlEventTouchUpInside];
       // [self.view addSubview:buttonnext];

        
    btnnext=[UIButton buttonWithType:UIButtonTypeCustom];
    [btnnext setFrame:CGRectMake(275, 425, 40, 40)];
    [btnnext setTitle:@"" forState:UIControlStateNormal];
    [btnnext setBackgroundColor:[UIColor clearColor]];
    [btnnext setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"next_page.png"]]];
    [btnnext addTarget:self action:@selector(buttonnextPressed) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addSubview:btnnext];
        
    }else{
        
        buttonnext=[UIButton buttonWithType:UIButtonTypeCustom];
        [buttonnext setFrame:CGRectMake(215, 500, 80, 30)];
        [Helper setButton:buttonnext Text:@"Next" WithFont:@"" FSize:15 TitleColor:[UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0]
              ShadowColor:nil];
        [buttonnext addTarget:self action:@selector(buttonnextPressed) forControlEvents:UIControlEventTouchUpInside];
        //[self.view addSubview:buttonnext];
        
        
        btnnext=[UIButton buttonWithType:UIButtonTypeCustom];
        [btnnext setFrame:CGRectMake(275, 500, 40, 40)];
        [btnnext setTitle:@"" forState:UIControlStateNormal];
        [btnnext setBackgroundColor:[UIColor clearColor]];
        [btnnext setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"next_page.png"]]];
        [btnnext addTarget:self action:@selector(buttonnextPressed) forControlEvents:UIControlEventTouchUpInside];
       // [self.view addSubview:btnnext];
        
    }

}
-(void)buttonnextPressed
{
    CGFloat pageWidth;
    pageWidth = scrollView.frame.size.width;
     page = scrollView.contentOffset.x / scrollView.frame.size.width;
    NSLog(@"the page width is %f",pageWidth);
    NSLog(@"the page is %d",page);
    
     //scrollView.contentOffset= CGPointMake(320*page+1, 0);
    if (page==0) {
        scrollView.contentOffset= CGPointMake(320, 0);
    }
  else if (page==1) {
        scrollView.contentOffset= CGPointMake(640, 0);
    }
    
   else if (page==2) {
        scrollView.contentOffset= CGPointMake(960, 0);
    }
    
   else if (page==3) {
        scrollView.contentOffset= CGPointMake(960, 0);
    }
  
}


-(void)backbuttonCreation
{
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    
    if (screenSize.size.height==480)
    {
        buttonback=[UIButton buttonWithType:UIButtonTypeCustom];
        [buttonback setFrame:CGRectMake(34, 430, 80, 30)];
        [Helper setButton:buttonback Text:@"Back" WithFont:@"" FSize:15 TitleColor:[UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0]
              ShadowColor:nil];
        [buttonback addTarget:self action:@selector(buttonbackPressed) forControlEvents:UIControlEventTouchUpInside];
        //[self.view addSubview:buttonback];

        

    btnback=[UIButton buttonWithType:UIButtonTypeCustom];
    [btnback setFrame:CGRectMake(10, 425, 40, 40)];
    [btnback setTitle:@"" forState:UIControlStateNormal];
    [btnback setBackgroundColor:[UIColor clearColor]];
    [btnback setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pervious_page.png"]]];
    [btnback addTarget:self action:@selector(buttonbackPressed) forControlEvents:UIControlEventTouchUpInside];
   // [self.view addSubview:btnback];
        
    }
    else{
        
        
        buttonback=[UIButton buttonWithType:UIButtonTypeCustom];
        [buttonback setFrame:CGRectMake(34, 500, 80, 30)];
        [Helper setButton:buttonback Text:@"Back" WithFont:@"" FSize:15 TitleColor:[UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0]
              ShadowColor:nil];
         [buttonback addTarget:self action:@selector(buttonbackPressed) forControlEvents:UIControlEventTouchUpInside];
      //  [self.view addSubview:buttonback];
        
       
        
        btnback=[UIButton buttonWithType:UIButtonTypeCustom];
        [btnback setFrame:CGRectMake(15, 500, 40, 40)];
        [btnback setTitle:@"" forState:UIControlStateNormal];
        [btnback setBackgroundColor:[UIColor redColor]];
        [btnback setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pervious_page"]]];
        [btnback addTarget:self action:@selector(buttonbackPressed) forControlEvents:UIControlEventTouchUpInside];
       // [self.view addSubview:btnback];

        
    }
}

-(void)buttonbackPressed
{
    CGFloat pageWidth;
    pageWidth = scrollView.frame.size.width;
    page = scrollView.contentOffset.x / scrollView.frame.size.width;
    NSLog(@"the page width is %f",pageWidth);
    NSLog(@"the page is %d",page);
    
    //scrollView.contentOffset= CGPointMake(320*page+1, 0);
    if (page==0) {
        scrollView.contentOffset= CGPointMake(0, 0);
    }
    if (page==1) {
        scrollView.contentOffset= CGPointMake(0, 0);
    }
    
    if (page==2) {
        scrollView.contentOffset= CGPointMake(320, 0);
    }
    
    if (page==3) {
        scrollView.contentOffset= CGPointMake(640, 0);
    }
    

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self nextbutton];
    [self backbuttonCreation];
    if (page==0) {
        [self SkipButton];
        [btnback setHidden:YES];
         [buttonback setHidden:YES];
    }
    
      // [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"home_background.png"]]];
     //[self.view bringSubviewToFront:navImage];
    // [self.view bringSubviewToFront:skipBtn];
    [navBarView setHidden:YES];
    if (selectedview==1) {
        
        [self.navigationController setNavigationBarHidden:YES];
    }
    
   
    
   
    if (IS_IOS7) {
    
        
        CGRect screenSize = [[UIScreen mainScreen] bounds];
        scrollView.frame=CGRectMake(0, -20, 320, screenSize.size.height+20);
         NSLog(@"the screen size is %f",screenSize.size.height);
        
        UIImageView *image1=[[UIImageView alloc]initWithFrame:CGRectMake(320*0, 0, 320, scrollView.frame.size.height)];
        image1.image=[UIImage imageNamed:@"1st-screen.png"];
        NSLog(@"the scrollview size is %f",scrollView.frame.size.height);
        UIImageView *image2=[[UIImageView alloc]initWithFrame:CGRectMake(320*1, 0, 320, scrollView.frame.size.height)];
        image2.image=[UIImage imageNamed:@"2nd-screen.png"];
        
        UIImageView *image3=[[UIImageView alloc]initWithFrame:CGRectMake(320*2, 0, 320, scrollView.frame.size.height)];
        image3.image=[UIImage imageNamed:@"3rd-screen.png"];
        
        UIImageView *image4=[[UIImageView alloc]initWithFrame:CGRectMake(320*3, 0, 320, scrollView.frame.size.height)];
        image4.image=[UIImage imageNamed:@"4th-screen.png"];
        
        CGSize size = CGSizeMake(0, 0);
        [scrollView addSubview:image1];
        size.width+=320;
        [scrollView addSubview:image2];
        size.width+=320;
        [scrollView addSubview:image3];
        size.width+=320;
        [scrollView addSubview:image4];
        size.width+=320;
        [scrollView addSubview:image4];
        
        scrollView .showsHorizontalScrollIndicator=NO;
        scrollView.contentSize=size;

        
    }else{
        CGRect screenSize = [[UIScreen mainScreen] bounds];
        scrollView.frame=CGRectMake(0, -20, 320, screenSize.size.height);
        NSLog(@"the screen size is %f",screenSize.size.height);

        
    UIImageView *image1=[[UIImageView alloc]initWithFrame:CGRectMake(320*0, 0, 320, scrollView.frame.size.height)];
    image1.image=[UIImage imageNamed:@"1st-screen.png"];
    NSLog(@"the scrollview size is %f",scrollView.frame.size.height);
    UIImageView *image2=[[UIImageView alloc]initWithFrame:CGRectMake(320*1, 0, 320, scrollView.frame.size.height)];
    image2.image=[UIImage imageNamed:@"2nd-screen.png"];
    
    UIImageView *image3=[[UIImageView alloc]initWithFrame:CGRectMake(320*2, 0, 320, scrollView.frame.size.height)];
    image3.image=[UIImage imageNamed:@"3rd-screen.png"];
    
    UIImageView *image4=[[UIImageView alloc]initWithFrame:CGRectMake(320*3, 0, 320, scrollView.frame.size.height)];
    image4.image=[UIImage imageNamed:@"4th-screen.png"];
    
    CGSize size = CGSizeMake(0, 0);
    [scrollView addSubview:image1];
    size.width+=320;
     [scrollView addSubview:image2];
    size.width+=320;
     [scrollView addSubview:image3];
    size.width+=320;
     [scrollView addSubview:image4];
    size.width+=320;
     [scrollView addSubview:image4];
    
    scrollView .showsHorizontalScrollIndicator=NO;
    scrollView.contentSize=size;

    }
    
    UITapGestureRecognizer *tapGestureDisableView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    //tapGesture.delegate=self
    tapGestureDisableView.numberOfTapsRequired = 1;
    [scrollView addGestureRecognizer:tapGestureDisableView];
       
    
//    self.modelArray = [[NSMutableArray alloc] init];
//    
//        
//    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl
//                                                              navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
//
//    self.pageViewController.delegate=self;
//    self.pageViewController.dataSource =self;
//    
//    HelpScreenContainerViewController *contentViewController = [[HelpScreenContainerViewController alloc] initWithNibName:@"HelpScreenContainerViewController" bundle:nil];
//   // contentViewController.labelContents = [self.modelArray objectAtIndex:0];
//    NSArray *viewControllers = [NSArray arrayWithObject:contentViewController];
//    [self.pageViewController setViewControllers:viewControllers
//                                      direction:UIPageViewControllerNavigationDirectionForward
//                                       animated:NO
//                                     completion:nil];
//    [self addChildViewController:self.pageViewController];
//    [self.view addSubview:self.pageViewController.view];
//    [self.pageViewController didMoveToParentViewController:self];
//    
//    CGRect pageViewRect = self.view.bounds;
//   // pageViewRect = CGRectInset(pageViewRect, 40.0, 40.0);
//    self.pageViewController.view.frame = pageViewRect;
//    
//    self.view.gestureRecognizers = self.pageViewController.gestureRecognizers;
//    

}
//- (void)setViewControllers:(NSArray *)viewControllers
//                 direction:(UIPageViewControllerNavigationDirection)direction
//                  animated:(BOOL)animated
//                completion:(void (^)(BOOL finished))completion;
//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
//      viewControllerBeforeViewController:(UIViewController *)viewController
//{
//    NSUInteger currentIndex = 0;    //[self.modelArray indexOfObject:[(HelpScreenContainerViewController *)viewController labelContents]];
//    if(currentIndex == 0)
//    {
//        return nil;
//    }
//    HelpScreenContainerViewController *contentViewController = [[HelpScreenContainerViewController alloc] init];
//    //contentViewController.labelContents = [self.modelArray objectAtIndex:currentIndex - 1];
//    return contentViewController;
//}
//
//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
//       viewControllerAfterViewController:(UIViewController *)viewController
//{
//    NSUInteger currentIndex = 2;     //[self.modelArray indexOfObject:[(ContentViewController *)viewController labelContents]];
//    if(currentIndex == self.modelArray.count-1)
//    {
//        return nil;
//    }
//    HelpScreenContainerViewController *contentViewController = [[HelpScreenContainerViewController alloc] init];
//   // contentViewController.labelContents = [self.modelArray objectAtIndex:currentIndex + 1];
//    return contentViewController;
//}

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
   
    //[helpView removeFromSuperview];
    if(!isFrsTap){
        
        isFrsTap=YES;
        //
        //        [UIView animateWithDuration:0.3 animations:^() {
        //            navImage.alpha = 0.0;
        //        }];
        //
        //        [UIView animateWithDuration:0.3 animations:^() {
        //            navImage.alpha = 0.0;
        //        }];
        
        if(isLastPage){
            skipBtn.hidden=YES;
            exitBtn.hidden=NO;
        }else{
            skipBtn.hidden=NO;
            exitBtn.hidden=YES;
        }
        CGRect rect=CGRectMake(0, 0, 320, 44);
        //CGRect rect;
        rect.origin.y=-44;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        //[navBarView setFrame:rect];
        [UIView commitAnimations];
        
        
    }else{
        isFrsTap=NO;
        if(isLastPage){
            skipBtn.hidden=YES;
            exitBtn.hidden=NO;
        }else{
            skipBtn.hidden=NO;
            exitBtn.hidden=YES;
        }
        CGRect rect=CGRectMake(0, -44, 320, 44);
        rect.origin.y=0;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        //[navBarView setFrame:rect];
        [UIView commitAnimations];
        
    }
   }
-(IBAction)skipBtn:(id)sender
{

    [self dismissViewControllerAnimated:YES completion:nil];
   
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrolView {
    
    CGFloat pageWidth;
    pageWidth = scrollView.frame.size.width;
    // page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    page = scrollView.contentOffset.x / scrollView.frame.size.width;
    NSLog(@"the page is %d",page);
    if (page == 0) {
        btnskip.hidden = NO;
        lblSkipImg.hidden=NO;
        btnback.hidden=YES;
        buttonback.hidden=YES;
        btnnext.hidden=NO;
        buttonnext.hidden=NO;
       // [btnskip setTitle:@"Ende" forState:UIControlStateNormal];
    }
    else if(page == 1 ) {
        btnskip.hidden = YES;
        lblSkipImg.hidden=YES;
        btnback.hidden=NO;
        buttonback.hidden=NO;
        btnnext.hidden=NO;
        buttonnext.hidden=NO;
       // [btnskip setTitle:@"Ende" forState:UIControlStateNormal];
        
    }
    else if(page == 2 ) {
        btnskip.hidden = YES;
        lblSkipImg.hidden=YES;
        btnback.hidden=NO;
        buttonback.hidden=NO;
        btnnext.hidden=NO;
        buttonnext.hidden=NO;
        //[btnskip setTitle:@"Ende" forState:UIControlStateNormal];
        
    }
    
    else if(page == 3 ) {
        btnskip.hidden = NO;
        lblSkipImg.hidden=NO;
        btnnext.hidden=YES;
        buttonnext.hidden=YES;
       // [btnskip setTitle:@"Ende" forState:UIControlStateNormal];
        
    }

    
    else{
        btnskip.hidden = NO;
        lblSkipImg.hidden=NO;
        //[btnskip setTitle:@"Ende" forState:UIControlStateNormal];
    }
	

    
}


- (BOOL)shouldAutorotate {
   
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationPortrait;
}
//- (NSUInteger)supportedInterfaceOrientations {
//
//
//
//    return UIInterfaceOrientationLandscapeRight;
//}

@end
