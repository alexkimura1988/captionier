//
//  HelpSreenViewController.h
//  Signature
//
//  Created by Rahul Sharma on 26/01/13.
//
//

#import <UIKit/UIKit.h>

@interface HelpSreenViewController : UIViewController<UIScrollViewDelegate>
{
   IBOutlet  UIScrollView *scrollView;
     NSMutableArray *arrayImgs;
   IBOutlet UIImageView *navImage;
    IBOutlet UIButton *skipBtn;
     IBOutlet UIButton *exitBtn;
    IBOutlet UIView *navBarView;
    
    BOOL isFrsTap;
    BOOL isLastPage;
    
    int selectedview;
    int page;
    UIButton *btnskip;
    UIButton *buttonback;
    UIButton *btnback;
    UIButton *buttonnext;
    UIButton *btnnext;
    
    UILabel *lblSkipImg;
    
}

//@property (nonatomic, strong) UIPageViewController *pageViewController;
//@property (nonatomic, strong) NSMutableArray *modelArray;
@property (nonatomic, assign)int selectedview;
@property (nonatomic, strong) UIScrollView *scrollView;
-(IBAction)skipBtn:(id)sender;
@end
