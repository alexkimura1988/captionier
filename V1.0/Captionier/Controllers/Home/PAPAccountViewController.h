//
//  PAPAccountViewController.h
//  Anypic
//
//  Created by Héctor Ramos on 5/3/12.
//

#import "PAPPhotoTimelineViewController.h"

@interface PAPAccountViewController : PAPPhotoTimelineViewController

@property (nonatomic, strong) PFUser *user;
@property (nonatomic,assign)BOOL showHeaderView;
@property (nonatomic,assign)BOOL showBackButton;
-(void)refresh;
@end
