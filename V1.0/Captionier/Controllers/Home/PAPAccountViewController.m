//
//  PAPAccountViewController.m
//  Anypic
//
//  Created by Héctor Ramos on 5/2/12.
//

#import "PAPAccountViewController.h"
#import "PAPPhotoCell.h"
#import "TTTTimeIntervalFormatter.h"
#import "PAPLoadMoreCell.h"
#import "PAPFindFriendsViewController.h"
#import "SettingsViewController.h"
#import "EditProfileViewController.h"
#import "FollowersFollowingViewController.h"
#import "NMRangeSlider.h"
#import "OptionsViewController.h"
#import "PAPHomeViewController.h"
#import "SSPicker.h"
#import "AlbumGridViewController.h"
#import "SDWebImageDownloader.h"
#import "UIImageView+WebCache.h"
#import "NavigationViewController.h"
#import "STTweetLabel.h"
#import "PBJViewController.h"
#import "CustomCameraViewController.h"
#import "ChatView.h"
#import "UIImage+ImageEffects.h"
#import <FacebookSDK/FacebookSDK.h>


@interface PAPAccountViewController()<OptionsViewControllerDelegate,RangeSliderDelegate,FBFriendPickerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong) UIView *headerView;
@property(nonatomic, strong) UILabel *userDisplayNameLabel;
@property(nonatomic, strong) UILabel *userLocationLabel;
@property(nonatomic, strong) UILabel *userbioLabel;
@property (strong, nonatomic)  NMRangeSlider *labelSlider;
@property (strong, nonatomic)  UILabel *lowerLabel;
@property (strong, nonatomic)  UILabel *upperLabel;
@property (strong, nonatomic)  UIButton *followStatusButton;
@property (strong, nonatomic)  UILabel *filterByCategoryLabel;
@property (strong, nonatomic)  UILabel *photoCountLabel;
@property (nonatomic,strong)   NSString *selectedCategory;
@property (nonatomic,strong)   UIView *coverView;
@property (nonatomic,strong)   UIImageView *coverImage;
@property (nonatomic,strong)   UIImageView *starImg;
@property (nonatomic,strong) UILabel *followingCountLabel;
@property (nonatomic,strong) UILabel *followerCountLabel;
@property (nonatomic,strong) UILabel *postCountLabel;
@property (nonatomic, strong) PFImageView *profilePictureImageView;
@property (strong, nonatomic)  UIButton *segment1;
@property (strong, nonatomic)  UIButton *segment2;

@end

@implementation PAPAccountViewController
@synthesize headerView;
@synthesize user;
@synthesize userDisplayNameLabel;
@synthesize userbioLabel;
@synthesize labelSlider;
@synthesize lowerLabel;
@synthesize upperLabel;
@synthesize followStatusButton;
@synthesize filterByCategoryLabel;
@synthesize selectedCategory;
@synthesize photoCountLabel;
@synthesize showBackButton;
@synthesize showHeaderView;
@synthesize coverView;
@synthesize coverImage;
@synthesize starImg;
@synthesize followingCountLabel;
@synthesize postCountLabel;
@synthesize isNeedPullToRefresh;
@synthesize followerCountLabel;


#define loadAllVideos 0
#define loadLikedVideos 1

#pragma mark - Initialization

#pragma mark - UIViewController


/**
 *  Configures the navigation bar to use a custom button as its right button.
 */
- (void)customizeNavigationBarButtons {
    //UIImage *imgButton = [UIImage imageNamed:@"settings_edge"];
    
    UIButton *rightBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBarButton setImage:[UIImage imageNamed:@"homescreen_userhome_icon_off"] forState:UIControlStateNormal];
    [rightBarButton setImage:[UIImage imageNamed:@"homescreen_userhome_icon_on"] forState:UIControlStateHighlighted];
    [rightBarButton setFrame:CGRectMake(0, 0, 44, 44)];
    
    //[rightBarButton setTitle:@"" forState:UIControlStateNormal];
    //	rightBarButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    //	rightBarButton.titleLabel.textColor = [UIColor whiteColor];
    //	rightBarButton.titleLabel.shadowOffset = CGSizeMake(0,-1);
    //	rightBarButton.titleLabel.shadowColor = [UIColor darkGrayColor];
    [rightBarButton addTarget:self action:@selector(menuButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButton];
    
    [self customizeNavigationBackButton];
}

/**
 *  Configures the navigation bar to use a custom button as its left button.
 */
-(void)customizeNavigationBackButton {
    
    if (self.showBackButton) {
        UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn"];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
        [backButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateHighlighted];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    }
    else {
        //UIImage *backbuttonImage = [UIImage imageNamed:@"camera_nav_bar_icon_off"];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake( 0.0f, 0.0f,35, 35);
        [backButton addTarget:self action:@selector(titleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [backButton setBackgroundImage:[UIImage imageNamed:@"camera_nav_bar_icon_off"] forState:UIControlStateNormal];
        //[backButton setBackgroundImage:[UIImage imageNamed:@"camera_btn_on"] forState:UIControlStateHighlighted];
        // self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    }
}

/**
 *  go back to previous controller
 *
 *  @param sender backBtn pressed
 */
- (void)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 *  Configures the navigation bar to use a custom title with action as its title.
 */
-(void)customizeNavigationTitleView {
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"camera_nav_bar_icon_off"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    [backButton addTarget:self action:@selector(titleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backbuttonImage forState:UIControlStateNormal];
    //[backButton setBackgroundImage:[UIImage imageNamed:@"camera_btn_on"] forState:UIControlStateHighlighted];
    self.navigationItem.titleView = backButton;
    
}

/**
 *  This method will open camera screen
 *
 *  @param sender camera button pressed
 */
-(void)titleButtonAction:(id)sender {
    NavigationViewController *navigationController = (NavigationViewController *)self.navigationController;
    navigationController.menu.isOpen = YES;
    [navigationController toggleMenu];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if (IS_IPHONE_5) {
        CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController" bundle:nil];
        [self.navigationController pushViewController:obj animated:YES];
    }
    else {
        CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController_ip4" bundle:nil];
        
        [self.navigationController pushViewController:obj animated:YES];
    }
}

/**
 *  This method will goto  the previous controller
 *
 *  @param sender backButton pressed
 */
-(void)backButtonAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


/**
 *  This method will open edit screen to edit user information
 *
 *  @param sender editButton pressed
 */
-(void)editbuttonClicked:(id)sender
{
    
    EditProfileViewController *edit = [[EditProfileViewController alloc] initWithNibName:@"EditProfileViewController" bundle:nil];
    edit.onCompletion = ^(BOOL isImageEdited){
        if (isImageEdited) {
            [self refresh];
        }
        else {
            userDisplayNameLabel.text = [[PFUser currentUser] objectForKey:kPAPUserDisplayNameKey];
        }
    };
    UINavigationController *nav = [[NavigationViewController alloc] initWithRootViewController:edit];
    [self presentViewController:nav animated:YES completion:nil];
    
}

/**
 *  This method will open setting screen
 *
 *  @param sender setting icon pressed
 */
-(void)settingsbuttonClicked:(id)sender
{
    SettingsViewController *settings = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
    [self.navigationController pushViewController:settings animated:YES];
    
}

/**
 *  This method will open menu list
 *
 *  @param sender menu icon tapped
 */
-(void)menuButtonClicked:(id)sender
{
    NavigationViewController *navigationController = (NavigationViewController *)self.navigationController;
    //navigationController.menu.isOpen = YES;
    [navigationController toggleMenu];
    
}
-(void)viewWillAppear:(BOOL)animated {
    
    
    [self bioInfoFormatting];
    [self starVisibilityFrame];
    Helper *helper = [Helper sharedInstance];
    if( helper.didCurrentUserFollowedSomebody == 100) {
        [self userFollowedSomeOne:nil];
    }
    
    //[postCountLabel setText:@"0"];
    
    PFQuery *queryPostCount = [PFQuery queryWithClassName:kPAPFeedClassKey];
    [queryPostCount whereKey:kPAPFeedUserKey equalTo:self.user];
    [queryPostCount setCachePolicy:kPFCachePolicyCacheThenNetwork];
    [queryPostCount countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            [postCountLabel setText:[NSString stringWithFormat:@"%d", number]];
            //[followerCountLabel setText:[NSString stringWithFormat:@"%d Followers", number]];
        }
    }];
    
    PFQuery *queryFollowerCount = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [queryFollowerCount whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
    [queryFollowerCount whereKey:kPAPActivityToUserKey equalTo:self.user];
    [queryFollowerCount setCachePolicy:kPFCachePolicyCacheThenNetwork];
    [queryFollowerCount countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            [followerCountLabel setText:[NSString stringWithFormat:@"%d", number]];
            //[followerCountLabel setText:[NSString stringWithFormat:@"%d Followers", number]];
        }
    }];
    
    
    PFQuery *queryFollowingCount = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [queryFollowingCount whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
    [queryFollowingCount whereKey:kPAPActivityFromUserKey equalTo:self.user];
    [queryFollowingCount setCachePolicy:kPFCachePolicyCacheThenNetwork];
    [queryFollowingCount countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            [followingCountLabel setText:[NSString stringWithFormat:@"%d", number]];
        }
    }];
    
    
    
    
    
    PFQuery *queryLikeCount = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [queryLikeCount whereKey:kPAPActivityFromUserKey equalTo:self.user];
    [queryLikeCount whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeLike];
    [queryLikeCount countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            NSMutableAttributedString *titleText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d\nlikes", number]];
            
            NSInteger length = titleText.length;
            // Set the font to bold from the beginning of the string to the ","
            [titleText addAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Avenir Next" size:13],
                                       UITextAttributeTextColor:[UIColor whiteColor]} range:NSMakeRange(length - 5, 5)];
            
            
            // Normal font for the rest of the text
            [titleText addAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Avenir Next" size:14],
                                       UITextAttributeTextColor:[UIColor whiteColor]} range:NSMakeRange(0, length - 5)];
            
            NSMutableAttributedString *titleText_sel = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d\nlikes", number]];
            
            length = titleText_sel.length;
            // Set the font to bold from the beginning of the string to the ","
            [titleText_sel addAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Avenir Next" size:13],
                                           UITextAttributeTextColor:[UIColor blackColor]} range:NSMakeRange(length - 5, 5)];
            
            
            // Normal font for the rest of the text
            [titleText_sel addAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Avenir Next" size:14],
                                           UITextAttributeTextColor:[UIColor blackColor]} range:NSMakeRange(0, length - 5)];
            
            // Set the attributed string as the buttons' title text
            [_segment2 setAttributedTitle:titleText forState:UIControlStateNormal];
            [_segment2 setAttributedTitle:titleText_sel forState:UIControlStateSelected];
        }
    }];
    
    
}


/**
 *  This method is call to get the list of liked photos from the parse
 */
-(void)getLikedPhotos {
    PFQuery *followingActivitiesQuery = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [followingActivitiesQuery whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeLike];
    [followingActivitiesQuery whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
    followingActivitiesQuery.cachePolicy = kPFCachePolicyNetworkOnly;
    followingActivitiesQuery.limit = 10;
    [followingActivitiesQuery findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        
        NSLog(@"array : %@",array);
    }];
    
}


/**
 *  This method will refresh and update the profile details
 */
-(void)refresh {
    
    //set text on display name label
    userDisplayNameLabel.text = [[PFUser currentUser] objectForKey:kPAPUserDisplayNameKey];
    
    //update the new image
    PFImageView *profilePictureImageView = (PFImageView*)[self.headerView viewWithTag:100];
    UIView *alphaView =  (UIView*)[self.headerView viewWithTag:105];
    PFFile *imageFile = [[PFUser currentUser]  objectForKey:kPAPUserProfilePicMediumKey];
    if (imageFile) {
        [profilePictureImageView setFile:imageFile];
        [profilePictureImageView loadInBackground:^(UIImage *image, NSError *error) {
            if (!error) {
                [UIView animateWithDuration:0.200f animations:^{
                    //profilePictureBackgroundView.alpha = 1.0f;
                    // profilePictureStrokeImageView.alpha = 1.0f;
                    _profilePictureImageView.alpha = 1.0f;
                    
                    UIImage *profileImage = [image applyBlurWithRadius:10 tintColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.8] saturationDeltaFactor:1.0 maskImage:nil];
                    alphaView.backgroundColor = [UIColor colorWithPatternImage:profileImage];;
                    
                }];
            }
        }];
    }
}


/**
 *  This method is use to set profile image as its background image
 */
-(void)getCoverPic {
    
    PFQuery *query = [[PFQuery alloc] initWithClassName:@"CoverPhoto"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        if (!error) {
            PFObject *object = [array lastObject];
            
            if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"CoverImageName"] isEqualToString:[object objectForKey:@"photoName"]]) {
                PFFile *file = [object objectForKey:@"coverphoto"];
                
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                [manager downloadWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",file.url]]
                                 options:0
                                progress:^(NSUInteger receivedSize, long long expectedSize)
                 {
                     // progression tracking code
                 }
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                 {
                     if (image)
                     {
                         
                         coverImage.image = image;
                         
                         NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                         NSString *documents = [paths objectAtIndex:0];
                         NSString *finalPath = [documents stringByAppendingPathComponent:@"myImageName.png"];
                         NSData *data = UIImageJPEGRepresentation(image, 1);
                         [data writeToFile:finalPath atomically:YES];
                         [[NSUserDefaults standardUserDefaults] setObject:finalPath forKey:@"CoverImagePath"];
                         [[NSUserDefaults standardUserDefaults] setObject:[object objectForKey:@"photoName"] forKey:@"CoverImageName"];
                         
                     }
                 }];
            }
            else {
                // coverView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[NSUserDefaults standardUserDefaults] objectForKey:@"CoverImagePath"]]];
                coverImage.image = [UIImage imageWithContentsOfFile:[[NSUserDefaults standardUserDefaults] objectForKey:@"CoverImagePath"]];
            }
            
            
        }
    }];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (![self.user isDataAvailable]) {
        [self.user fetch];
    }
    [self starVisibilityFrame];
    
    if (showBackButton) {
        [self customizeNavigationBackButton];
    }
    else {
        //[self customizeNavigationBarButtons];
    }
    
    
    if (isNeedPullToRefresh) {
        
        if (NSClassFromString(@"UIRefreshControl")) {
            // Use the new iOS 6 refresh control.
            UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
            self.refreshControl = refreshControl;
            self.refreshControl.tintColor = [UIColor colorWithRed:73.0f/255.0f green:55.0f/255.0f blue:35.0f/255.0f alpha:1.0f];
            [self.refreshControl addTarget:self action:@selector(refreshControlValueChanged:) forControlEvents:UIControlEventValueChanged];
            self.pullToRefreshEnabled = NO;
        }
    }
    
    
    
    if (!self.user) {
        [NSException raise:NSInvalidArgumentException format:@"user cannot be nil"];
    }
    //self.navigationItem.title = AppTitle;
    
    
    
    
    self.headerView = [[UIView alloc] init];
    [self.headerView setBackgroundColor:[UIColor clearColor]]; // should be clear, this will be the container for our avatar, photo count, follower count, following count, and so on
    
    // if ([self.user isEqual:[PFUser currentUser]]) {
    self.headerView.frame =  CGRectMake( 0.0f, 0.0f, self.tableView.bounds.size.width,260+40);
    // }
    // else {
    //    self.headerView.frame =  CGRectMake( 0.0f, 0.0f, self.tableView.bounds.size.width,260);
    //}
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake( 0.0f, 0.0f, self.tableView.bounds.size.width,260)];
    alphaView.tag = 105;
    alphaView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"profile_bg"]];;
    [self.headerView addSubview:alphaView];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    
    //_profilePictureImageView = [[PFImageView alloc] initWithFrame:CGRectMake( 160-38,20, 150/2, 150/2)];
    _profilePictureImageView = [[PFImageView alloc] initWithFrame:CGRectMake( 160-38,60, 150/2, 150/2)];
    _profilePictureImageView.tag = 100;
    [headerView addSubview:_profilePictureImageView];
    _profilePictureImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _profilePictureImageView.layer.borderWidth = 1;
    [_profilePictureImageView setContentMode:UIViewContentModeScaleAspectFill];
    
    BOOL selfProfile = [user.objectId isEqualToString:[PFUser currentUser].objectId];
    
    
    PFFile *imageFile = [self.user objectForKey:kPAPUserProfilePicMediumKey];
    if (imageFile) {
        _profilePictureImageView.layer.cornerRadius = _profilePictureImageView.frame.size.width/2;
        _profilePictureImageView.clipsToBounds = YES;
        [_profilePictureImageView setClipsToBounds:YES];
        [_profilePictureImageView setFile:imageFile];
        [_profilePictureImageView loadInBackground:^(UIImage *image, NSError *error) {
            if (!error) {
                [UIView animateWithDuration:0.200f animations:^{
                    //profilePictureBackgroundView.alpha = 1.0f;
                    // profilePictureStrokeImageView.alpha = 1.0f;
                    _profilePictureImageView.alpha = 1.0f;
                    
                    UIImage *profileImage = [image applyBlurWithRadius:10 tintColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.8] saturationDeltaFactor:1.0 maskImage:[UIImage imageNamed:@"homescreen_profile_boarder"]];
                    alphaView.backgroundColor = [UIColor colorWithPatternImage:profileImage];;
                    
                }];
            }
        }];
    }
    
    // user name label
    userDisplayNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 100+40, 200, 22.0f)];
    userDisplayNameLabel.backgroundColor = CLEAR_COLOR;
    userDisplayNameLabel.tag = 11;
    userDisplayNameLabel.textAlignment = NSTextAlignmentCenter;
    [Helper setToLabel:userDisplayNameLabel Text:[self.user objectForKey:kPAPUserDisplayNameKey] WithFont:robo_light FSize:18 Color:WHITE_COLOR];
    [self.headerView addSubview:userDisplayNameLabel];
    self.navigationItem.title = [self.user objectForKey:kPAPUserDisplayNameKey];
    
    //  userDisplayNameLabel.backgroundColor = [UIColor purpleColor];
    
    _userLocationLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 127+40, 200, 22.0f)];
    _userLocationLabel.backgroundColor = CLEAR_COLOR;
    _userLocationLabel.tag = 11;
    _userLocationLabel.textAlignment = NSTextAlignmentCenter;
    [Helper setToLabel:_userLocationLabel Text:[self.user objectForKey:kPAPUserDisplayLocationKey] WithFont:Aharoni_Bold FSize:17 Color:WHITE_COLOR];
    [self.headerView addSubview:_userLocationLabel];
    
    //_userLocationLabel.backgroundColor = [UIColor yellowColor];
    
    
    if (selfProfile) {
        UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        editButton.frame = CGRectMake(230, 15, 80, 25);
        [editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        editButton.layer.borderColor = [UIColor whiteColor].CGColor;
        editButton.layer.borderWidth = 2.0;
        editButton.backgroundColor = [UIColor clearColor];
        
        [editButton setTitle:@"Edit" forState:UIControlStateNormal];
        [Helper setToLabel:editButton.titleLabel Text:@"Edit" WithFont:robo_medium FSize:12 Color:[UIColor whiteColor]];
        editButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        [editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [editButton addTarget:self action:@selector(editbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.headerView addSubview:editButton];
        
        UIImage *imgButton = [UIImage imageNamed:@"setting_icon@2x.png"];
        
        
        UIButton *rightbarbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        //[rightbarbutton setImage:[UIImage imageNamed:@"setting_icon"] forState:UIControlStateNormal];
        [rightbarbutton setImage:[UIImage imageNamed:@"setting_off"] forState:UIControlStateHighlighted];
        
        [rightbarbutton setFrame:CGRectMake(0, 0, 27, 25)];
        rightbarbutton.layer.borderWidth =0.0f;
        rightbarbutton.titleLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:15.0f];    rightbarbutton.layer.borderColor = [[UIColor whiteColor]CGColor];
        [rightbarbutton setImage:imgButton forState:UIControlStateNormal];
        //[rightbarbutton setFrame:CGRectMake(0, 0, imgButton.size.width/2, imgButton.size.height/2)];
        
        [rightbarbutton addTarget:self action:@selector(settingsbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:rightbarbutton]];
        
    }
    
    
    
    
    
    
    //follow status Button
    followStatusButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //[followStatusButton setTitleColor:UIColorFromRGB(cProfileUserNameTitle) forState:UIControlStateNormal];
    [followStatusButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [followStatusButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    //followStatusButton.frame = CGRectMake(100, 154, 120, 34);
    
    //followStatusButton setTitleColor:UIColorFromRGB(cLikeButtonTitleSelector) forState:UIControlStateHighlighted];
    followStatusButton.frame = CGRectMake(230, 15, 80, 25);//CGRectMake(160-63/2, 154, 126/2, 59/2);
    followStatusButton.backgroundColor = [UIColor clearColor];
    [followStatusButton setTitle:@"Following" forState:UIControlStateNormal];
    followStatusButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    followStatusButton.layer.borderColor = [UIColor whiteColor].CGColor;
    followStatusButton.layer.borderWidth = 2.0;
    //    [followStatusButton setBackgroundImage:[UIImage imageNamed:@"profile_screen_following_button_off-568h"] forState:UIControlStateNormal];
    //    [followStatusButton setBackgroundImage:[UIImage imageNamed:@"profile_screen_following_button_on-568h"] forState:UIControlStateHighlighted];
    followStatusButton.titleLabel.font = [UIFont fontWithName:robo_medium size:12];
    //self.chartButton.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0);
    
    if (!selfProfile) {
        [self.headerView addSubview:followStatusButton];
        
        
        
    }
    
    
    
    
    //photo icon
    UIImageView *photoIcon = [[UIImageView alloc] initWithFrame:CGRectMake(167/2 - 45, 3, 30, 30)];
    photoIcon.image = [UIImage imageNamed:@"view_image.png"];
    
    // photo count label
    photoCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0 , 74/2, 320/3 , 22.0f)];
    photoCountLabel.backgroundColor = CLEAR_COLOR;
    photoCountLabel.textAlignment = NSTextAlignmentCenter;
    [Helper setToLabel:photoCountLabel Text:@"" WithFont:Lato FSize:18 Color:WHITE_COLOR];
    
    
    //seperator image
    
    UIView *line= [[UIView alloc] initWithFrame:CGRectMake(0, 198, 320, 1)];
    line.backgroundColor = [UIColor colorWithRed:76/255.0 green:37/255.0 blue:136/255.0 alpha:1.0];
    
    //followers view
    
    UIView *followersView = [[UIView alloc] initWithFrame:CGRectMake(320/3, 195, 320/3, 60)];
    followersView.backgroundColor =CLEAR_COLOR;
    [self.headerView addSubview:followersView];
    
    followerCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 4, 320/3, 43)];//90-22, 4, 320/3, 43
    followerCountLabel.backgroundColor = CLEAR_COLOR;
    followerCountLabel.textAlignment = NSTextAlignmentCenter;
    [Helper setToLabel:followerCountLabel Text:@"" WithFont:robo_medium FSize:21 Color:WHITE_COLOR];
    [followersView addSubview:followerCountLabel];
    
    UIButton *btnFollower = [UIButton buttonWithType:UIButtonTypeCustom];
    btnFollower.frame = CGRectMake(0, 12, 320/3, 60);//56, 12, 320/2, 60
    [btnFollower setTitle:@"followers" forState:UIControlStateNormal];
    [btnFollower setTitleColor:[UIColor colorWithRed:141/255.0 green:148/255.0 blue:159/255.0 alpha:1.0] forState:UIControlStateNormal];
    btnFollower.titleLabel.font = [UIFont fontWithName:robo_medium size: 12];
    [btnFollower addTarget:self action:@selector(followersClicked:) forControlEvents:UIControlEventTouchUpInside];
    [followersView addSubview:btnFollower];
    
    
    //following view
    
    UIView *followingView = [[UIView alloc] initWithFrame:CGRectMake(2*320/3 , 195, 320/3, 60)];
    followingView.backgroundColor = CLEAR_COLOR;
    [self.headerView addSubview:followingView];
    
    followingCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0 , 4, 320/3, 43)];
    followingCountLabel.backgroundColor = CLEAR_COLOR;
    //followingCountLabel.backgroundColor =[UIColor yellowColor];
    followingCountLabel.textAlignment = NSTextAlignmentCenter;
    [Helper setToLabel:followingCountLabel Text:@"" WithFont:robo_medium FSize:21 Color:WHITE_COLOR];
    [followingView addSubview:followingCountLabel];
    
    UIButton *btnFollowing = [UIButton buttonWithType:UIButtonTypeCustom];
    btnFollowing.frame = CGRectMake(0, 12, 320/3, 60);
    [btnFollowing setTitle:@"following" forState:UIControlStateNormal];
    [btnFollowing setTitleColor:[UIColor colorWithRed:141/255.0 green:148/255.0 blue:159/255.0 alpha:1.0] forState:UIControlStateNormal];
    //[btnFollowing setBackgroundColor:[UIColor yellowColor]];
    btnFollowing.titleLabel.font = [UIFont fontWithName:robo_medium size: 12];
    [btnFollowing addTarget:self action:@selector(followingClicked:) forControlEvents:UIControlEventTouchUpInside];
    [followingView addSubview:btnFollowing];
    
    
    //postview
    
    UIView *postView = [[UIView alloc] initWithFrame:CGRectMake(0 , 195, 320/3, 60)];//0, 195, 320/3, 60
    postView.backgroundColor = CLEAR_COLOR;
    [self.headerView addSubview:postView];
    
    postCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0 , 4, 320/3 , 43)];
    postCountLabel.backgroundColor = CLEAR_COLOR;
    postCountLabel.textAlignment = NSTextAlignmentCenter;
    [Helper setToLabel:postCountLabel Text:@"0" WithFont:robo_medium FSize:21 Color:WHITE_COLOR];
    [postView addSubview:postCountLabel];
    
    //post button
    
    UIButton *btnPost = [UIButton buttonWithType:UIButtonTypeCustom];
    btnPost.frame = CGRectMake(0, 12, 320/3, 60);
    [btnPost setTitle:@"posts" forState:UIControlStateNormal];
    [btnPost setTitleColor:[UIColor colorWithRed:141/255.0 green:148/255.0 blue:159/255.0 alpha:1.0] forState:UIControlStateNormal];
    //btnPost.backgroundColor = [UIColor whiteColor];
    btnPost.titleLabel.font = [UIFont fontWithName:robo_medium size: 12];
    
    //[btnPost addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents:UIControlEventTouchUpInside];
    [postView addSubview:btnPost];
    
    if ([self.user isEqual:[PFUser currentUser]]) {
        
        
    }
    
    
    
    
    
    [followerCountLabel setText:@"0"];
    [followingCountLabel setText:@"0"];
    
    
    
    if (![[self.user objectId] isEqualToString:[[PFUser currentUser] objectId]]) {
        
        UIBarButtonItem *chatButton = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStyleDone target:self action:@selector(chatButton:)];
        //        UIBarButtonItem *chatButton = [[UIBarButtonItem alloc] initWithTitle:@"Chat" style:UIBarButtonItemStyleDone target:self action:@selector(chatButtonClicked:)];
        
        self.navigationItem.rightBarButtonItem = chatButton;
        
        //if (![[self.user objectForKey:@"username"] isEqualToString:@"XtremeBR"]) {
        {
            UIActivityIndicatorView *loadingActivityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [loadingActivityIndicatorView startAnimating];
            // self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:loadingActivityIndicatorView];
            
            // check if the currentUser is following this user
            PFQuery *queryIsFollowing = [PFQuery queryWithClassName:kPAPActivityClassKey];
            [queryIsFollowing whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
            [queryIsFollowing whereKey:kPAPActivityToUserKey equalTo:self.user];
            [queryIsFollowing whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
            [queryIsFollowing setCachePolicy:kPFCachePolicyCacheThenNetwork];
            [queryIsFollowing countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
                //self.navigationItem.rightBarButtonItem = nil;
                if (error && [error code] != kPFErrorCacheMiss) {
                    NSLog(@"Couldn't determine follow relationship: %@", error);
                    //self.navigationItem.rightBarButtonItem = nil;
                } else {
                    if (number == 0) {
                        [self configureFollowButton];
                    } else {
                        [self configureUnfollowButton];
                    }
                }
            }];
        }
        
    }
    
    UIImageView *tbbgImgVw = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_screen_bg"]];
    tbbgImgVw.frame = self.tableView.frame;
    self.tableView.backgroundView = tbbgImgVw;
    
    
    
}

#pragma mark - PFQueryTableViewController

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    self.tableView.tableHeaderView = self.headerView;
    //[self.tableView reloadData];
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
}



/**
 *  This method will add and resize the view  according to user's biodata and website added
 */
-(void)bioInfoFormatting
{
    
    if (!( [self.headerView viewWithTag:2000] || [self.headerView viewWithTag:4001])) {
        
        UIView *data = [[UIView alloc]initWithFrame:CGRectMake(0, 260, 320, 0)];
        data.backgroundColor = [UIColor blackColor];
        data.tag = 2000;
        [self.headerView addSubview:data];
        
        UILabel *melbl= [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 312, 25)];
        melbl.tag = 3000;
        melbl.backgroundColor =[UIColor clearColor];
        [Helper setToLabel:melbl Text:@"Me" WithFont:robo_medium FSize:18 Color:[UIColor whiteColor]];
        //[data addSubview:melbl];
        
        
        UITextView *biotxt = [[UITextView alloc] init];
        biotxt.tag = 4000;
        biotxt.textColor = [UIColor whiteColor];
        biotxt.scrollEnabled = NO;
        biotxt.editable = NO;
        biotxt.userInteractionEnabled = YES;
        biotxt.backgroundColor = [UIColor clearColor];
        biotxt.font = [UIFont fontWithName:robo_light size:14];
        biotxt.backgroundColor = CLEAR_COLOR;
        biotxt.text= [self.user objectForKey:@"bioData"];
        [data addSubview:biotxt];
        
        
        /****************/
        
        UIButton *buttonWebsite = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonWebsite setTitleColor:[UIColor colorWithRed:0.098 green:0.469 blue:0.998 alpha:1.000] forState:UIControlStateNormal];
        buttonWebsite.titleLabel.font = [UIFont systemFontOfSize:14];
        buttonWebsite.tag = 4001;
        //buttonWebsite.backgroundColor = [UIColor redColor];
        buttonWebsite.titleLabel.numberOfLines = 100;
        [buttonWebsite setTitle:[self.user objectForKey:@"Website"] forState:UIControlStateNormal];
        buttonWebsite.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [buttonWebsite addTarget:self action:@selector(buttonWebsiteClicked:) forControlEvents:UIControlEventTouchUpInside];
        [data addSubview:buttonWebsite];
        
        /************/
        
        
        
        
        
        CGSize size = [biotxt sizeThatFits:CGSizeMake(316, FLT_MAX)];
        biotxt.frame = CGRectMake(4, 0, 316, size.height);
        
        if ([[self.user objectForKey:@"bioData"] length] == 0) {
            size = CGSizeMake(0, 0);
            
        }
        CGSize nameSize; //= CGSizeMake(0, 0);
        if ([[self.user objectForKey:@"Website"] length]>0) {
            //            UIButton *buttonWebsite = [UIButton buttonWithType:UIButtonTypeCustom];
            //            [buttonWebsite setTitleColor:[UIColor colorWithRed:0.098 green:0.469 blue:0.998 alpha:1.000] forState:UIControlStateNormal];
            //            buttonWebsite.titleLabel.font = [UIFont systemFontOfSize:14];
            //            buttonWebsite.tag = 4001;
            //            //buttonWebsite.backgroundColor = [UIColor redColor];
            //            buttonWebsite.titleLabel.numberOfLines = 100;
            //            [buttonWebsite setTitle:[self.user objectForKey:@"Website"] forState:UIControlStateNormal];
            //            buttonWebsite.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //            [buttonWebsite addTarget:self action:@selector(buttonWebsiteClicked:) forControlEvents:UIControlEventTouchUpInside];
            //            [data addSubview:buttonWebsite];
            
            nameSize = [buttonWebsite.titleLabel.text boundingRectWithSize:CGSizeMake(300, FLT_MAX)
                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                                                   context:nil].size;
            
            if ([[self.user objectForKey:@"bioData"] length] == 0) {
                buttonWebsite.frame = CGRectMake(7,5, nameSize.width, nameSize.height);
                
                
                data.frame = CGRectMake(0, 260, 320, size.height + nameSize.height+10);
            }
            else{
                
                
                buttonWebsite.frame = CGRectMake(7,CGRectGetMaxY(biotxt.frame), nameSize.width, nameSize.height);
                data.frame = CGRectMake(0, 260, 320, size.height+nameSize.height+5);
                self.headerView.frame = CGRectMake(0, 0, 320, 260+data.frame.size.height);
            }
            
            
            
        }
        else{
            data.frame = CGRectMake(0, 260, 320, size.height+nameSize.height);
            self.headerView.frame = CGRectMake(0, 0, 320, 260+data.frame.size.height);
        }
        
        //data.frame = CGRectMake(0, 260, 320, size.height+nameSize.height);
        
        //self.headerView.frame = CGRectMake(0, 0, 320, 260+data.frame.size.height);
        
        self.tableView.tableHeaderView = self.headerView;
        
        
    }
    else {
        
        NSLog(@"view not present");
        UIView *data = [self.headerView viewWithTag:2000];
        //UILabel *label = (UILabel*)[data viewWithTag:3000];
        
        UITextView *biotxt = (UITextView*)[data viewWithTag:4000];
        biotxt.text= [self.user objectForKey:@"bioData"];
        
        
        CGSize size = [biotxt sizeThatFits:CGSizeMake(316, FLT_MAX)];
        biotxt.frame = CGRectMake(4, 0, 316, size.height);
        
        UIButton *buttonWebsite = (UIButton*)[data viewWithTag:4001];
        if ([[self.user objectForKey:@"bioData"] length] == 0) {
            size = CGSizeMake(0, 0);
            
            if ([[self.user objectForKey:@"Website"] length]> 0 ) {
                
                [buttonWebsite setTitle:[self.user objectForKey:@"Website"] forState:UIControlStateNormal];
                CGSize nameSize = [buttonWebsite.titleLabel.text boundingRectWithSize:CGSizeMake(300, FLT_MAX)
                                                                              options:NSStringDrawingUsesLineFragmentOrigin
                                                                           attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                                                              context:nil].size;
                
                buttonWebsite.frame = CGRectMake(7,5, nameSize.width, nameSize.height);
                
                
                data.frame = CGRectMake(0, 260, 320, size.height + nameSize.height+10);
                
            }
            //            else{
            //                data.frame = CGRectMake(0,260,0, 0);
            //            }
            
            
            
        }
        else{
            
            
            if ([[self.user objectForKey:@"Website"] length] == 0) {
                [buttonWebsite setTitle:@"" forState:UIControlStateNormal];
                
                data.frame = CGRectMake(0, 260, 320, size.height );
                
            }
            else{
                [buttonWebsite setTitle:[self.user objectForKey:@"Website"] forState:UIControlStateNormal];
                CGSize nameSize = [buttonWebsite.titleLabel.text boundingRectWithSize:CGSizeMake(300, FLT_MAX)
                                                                              options:NSStringDrawingUsesLineFragmentOrigin
                                                                           attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                                                              context:nil].size;
                
                buttonWebsite.frame = CGRectMake(7,CGRectGetMaxY(biotxt.frame), nameSize.width, nameSize.height);
                
                
                data.frame = CGRectMake(0, 260, 320, size.height + nameSize.height+5);}
        }
        /********/
        
        if (([[self.user objectForKey:@"bioData"] length] == 0) && ([[self.user objectForKey:@"Website"] length] == 0)) {
            self.headerView.frame = CGRectMake(0, 5 , 320,  260);
            data.frame =CGRectMake(0, 0, 0, 0);
            [buttonWebsite setTitle:@"" forState:UIControlStateNormal];
        }
        else{
            /******/
            
            self.headerView.frame = CGRectMake(0, 0, 320, 260+data.frame.size.height);
        }
        self.tableView.tableHeaderView = self.headerView;
    }
}



/**
 *  This method will convert website string as a hyper link
 *
 *  @param website string
 */
- (IBAction)buttonWebsiteClicked:(id)sender
{
    
    NSString *string = [self.user objectForKey:@"Website"];
    if ([string rangeOfString:@"http://"].location == NSNotFound) {
        string = [@"http://" stringByAppendingString:string];
    } else {
        NSLog(@"string contains bla!");
    }
    
    NSURL *url = [NSURL URLWithString:string];
    
    if (![[UIApplication sharedApplication] openURL:url]) {
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
    }
}

/**
 *  Parse query to load table either by all videos or all like videos
 *
 *  @return query
 */

- (PFQuery *)queryForTable {
    
    
    // self.loadVideos = 1;
    if (!self.user) {
        PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
        [query setLimit:0];
        return query;
    }
    PFQuery *query;
    if (self.loadVideos == loadAllVideos) {
        query = [PFQuery queryWithClassName:@"Feed"];
        query.cachePolicy = kPFCachePolicyNetworkOnly;
        if (self.objects.count == 0) {
            query.cachePolicy = kPFCachePolicyCacheThenNetwork;
        }
        
        
        [query whereKey:kPAPFeedUserKey equalTo:self.user];
        [query orderByDescending:@"createdAt"];
        [query includeKey:kPAPFeedUserKey];
        
        //[query whereKey:@"isPrivateGroup" doesNotMatchQuery:[query whereKey:@"isPrivateGroup" equalTo:@"yes"]];
    }
    else if(self.loadVideos == loadLikedVideos) {
        
        
        query = [PFQuery queryWithClassName:kPAPActivityClassKey];
        [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeLike];
        [query whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
        query.cachePolicy = kPFCachePolicyNetworkOnly;
        [query includeKey:kPAPActivityToUserKey];
        [query includeKey:kPAPActivityPhotoKey];
        [query orderByDescending:@"createdAt"];
        //followingActivitiesQuery.limit = 10;
        
        
    }
    
    
    return query;
}

-(void)selectPicture {
    UIActionSheet *acctionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Library", nil];
    acctionSheet.tag = 200;
    [acctionSheet showInView:self.tabBarController.tabBar];
    
}

#pragma mark ActionSheetButton Methods

/**
 *  This method will call once press camera is selected to take profile image
 *  it will set to front camera as default camera
 *
 *  @param sender btnPress
 */

-(void)cameraButtonClicked:(id)sender
{
    
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.allowsEditing = YES;
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerCameraDeviceFront;
    [self presentViewController:picker animated:YES completion:nil];
}


/**
 *  This method will select image from image library
 *
 *  @param sender btnPress
 */
-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    // picker.contentSizeForViewInPopover = CGSizeMake(400, 800);
    //    [self presentViewController:picker animated:YES completion:nil];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        
        
        
        //[popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}
#pragma UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage  *albumImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    _profilePictureImageView.image = albumImg;
    // profileImageView.contentMode = UIViewContentModeScaleAspectFill;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSData *imageData = UIImagePNGRepresentation(albumImg);
        PFFile *imageFile = [PFFile fileWithName:@"image.png" data:imageData];
        [imageFile saveInBackground];
        
        PFUser *cuser = [PFUser currentUser];
        [cuser setObject:imageFile forKey:kPAPUserProfilePicMediumKey];
        [cuser saveInBackground];
    });
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"FacebookProfilePicture.jpg"];
    
    NSData *data = UIImageJPEGRepresentation(albumImg,1.0);
    [data writeToFile:getImagePath atomically:YES];
    
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 200) {
        switch (buttonIndex) {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            default:
                break;
        }
    }
    else {
        [super actionSheet:actionSheet clickedButtonAtIndex:buttonIndex];
    }
    
}


/**
 *  This method will open chat screen for chatting
 *
 *  @param sender chatButton pressed
 */
-(void)chatButtonClicked:(id)sender{
    
    ChatView *chatView = [[ChatView alloc] initWithUser:self.user];
    [self.navigationController pushViewController:chatView animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *LoadMoreCellIdentifier = @"LoadMoreCell";
    
    PAPLoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:LoadMoreCellIdentifier];
    if (!cell) {
        cell = [[PAPLoadMoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LoadMoreCellIdentifier];
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
        cell.separatorImageTop.image = [UIImage imageNamed:@"SeparatorTimelineDark.png"];
        cell.hideSeparatorBottom = YES;
        cell.mainView.backgroundColor = [UIColor whiteColor];
    }
    return cell;
}



/**
 *  This method will open home screen with hastagged post details
 *
 *  @param hashTag  string
 */
-(void)loadViewWithHashTag:(NSString*)hashTag
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:hashTag forKey:@"HashTag"];
    [ud synchronize];
    PAPHomeViewController *home = [[PAPHomeViewController alloc] initWithStyle:UITableViewStylePlain];
    home.hashTagString = hashTag;
    [home setHashTag:YES];
    [self.navigationController pushViewController:home animated:YES];
}

#pragma mark - () Methods
-(void)photoCountIconClicked:(id)sender {
    AlbumGridViewController *albumGVC = [[AlbumGridViewController alloc] initWithNibName:@"AlbumGridViewController" bundle:Nil];
    albumGVC.user = self.user;
    [self.navigationController pushViewController:albumGVC animated:YES];
}
-(void)followStatusButtonClicked:(id)sender {
    
    EditProfileViewController *editProfile = [[EditProfileViewController alloc] initWithNibName:@"EditProfileViewController" bundle:nil];
    editProfile.onCompletion = ^(BOOL isImagesEdited){
        [self refresh];
    };
    UINavigationController *navigationC = [[UINavigationController alloc] initWithRootViewController:editProfile];
    [self.navigationController presentViewController:navigationC animated:YES completion:nil];
}

/**
 *  This method will open following screen to list all users whom current user is following.
 *
 *  @param gestureRecognizer
 */
-(void)followingClicked:(UIGestureRecognizer*)gestureRecognizer{
    
    FollowersFollowingViewController *followersVC = [[FollowersFollowingViewController alloc] init];
    followersVC.type = Following;
    followersVC.user = self.user;
    followersVC.title =  @"Following";
    [self.navigationController pushViewController:followersVC animated:YES];
    
}


/**
 *  This method will open follower screen to list all users who is following current user.
 *
 *  @param gestureRecognizer
 */
- (void)followersClicked:(UIGestureRecognizer*)gestureRecognizer
{
    [Flurry logEvent:@"FollowersAndFollowingButtonClicked"];
    
    FollowersFollowingViewController *followersVC = [[FollowersFollowingViewController alloc] init];
    followersVC.type = Followers;
    followersVC.user = self.user;
    followersVC.title = @"Followers";
    [self.navigationController pushViewController:followersVC animated:YES];
    
    
    
}

/**
 *  This method will make current user to follow new user and after that is send push notification to that designated user
 *
 *  @param sender followButton press
 */
- (void)followButtonAction:(id)sender {
    UIActivityIndicatorView *loadingActivityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [loadingActivityIndicatorView startAnimating];
    // self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:loadingActivityIndicatorView];
    
    [self configureUnfollowButton];
    
    [PAPUtility followUserEventually:self.user block:^(BOOL succeeded, NSError *error) {
        [loadingActivityIndicatorView stopAnimating];
        //if (!error) {
        [self configureUnfollowButton];
        [self sendPushToFollowingUser:self.user];
        // }
        
    }];
}


/**
 *  This method will unfollow the current user following user
 *
 *  @param sender button pressed
 */
- (void)unfollowButtonAction:(id)sender {
    UIActivityIndicatorView *loadingActivityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [loadingActivityIndicatorView startAnimating];
    
    
    [self configureFollowButton];
    [loadingActivityIndicatorView stopAnimating];
    
    
    [PAPUtility unfollowUserEventually:self.user For:Following];
    
}


/**
 *  This method will configure follow user button
 */

- (void)configureFollowButton {
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Follow" style:UIBarButtonItemStyleBordered target:self action:@selector(followButtonAction:)];
    [followStatusButton setTitle:@"Follow" forState:UIControlStateNormal];
    [followStatusButton removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
    [followStatusButton addTarget:self action:@selector(followButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [[PAPCache sharedCache] setFollowStatus:NO user:self.user];
}


/**
 *  This method will unconfigured the follow user button
 */
- (void)configureUnfollowButton {
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Unfollow" style:UIBarButtonItemStyleBordered target:self action:@selector(unfollowButtonAction:)];
    
    [followStatusButton setTitle:@"Following" forState:UIControlStateNormal];
    
    [followStatusButton removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
    [followStatusButton addTarget:self action:@selector(unfollowButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [[PAPCache sharedCache] setFollowStatus:YES user:self.user];
}
#pragma mark - RangeSlider Method
-(void)rangeSliderTouchEnd {
    
    
    self.lowerLabel.font = [UIFont fontWithName:LATO_LIGHT size:13];
    self.upperLabel.font = [UIFont fontWithName:LATO_LIGHT size:13];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:Nil];
    
    [self loadObjects];
}
- (void) updateSliderLabels
{
    // You get get the center point of the slider handles and use this to arrange other subviews
    
    CGPoint lowerCenter;
    lowerCenter.x = (self.labelSlider.lowerCenter.x + self.labelSlider.frame.origin.x +38);
    lowerCenter.y = (self.labelSlider.center.y - 25);
    self.lowerLabel.center = lowerCenter;
    self.lowerLabel.font = [UIFont fontWithName:Lato size:17];
    self.lowerLabel.text = [NSString stringWithFormat:@"$%d", (int)self.labelSlider.lowerValue];
    
    CGPoint upperCenter;
    upperCenter.x = (self.labelSlider.upperCenter.x + self.labelSlider.frame.origin.x +38);
    upperCenter.y = (self.labelSlider.center.y - 25 );
    self.upperLabel.font = [UIFont fontWithName:Lato size:17];
    self.upperLabel.center = upperCenter;
    self.upperLabel.text = [NSString stringWithFormat:@"$%d", (int)self.labelSlider.upperValue];
}

// Handle control value changed events just like a normal slider
- (IBAction)labelSliderChanged:(NMRangeSlider*)sender
{
    [self updateSliderLabels];
}



/**
 *  This method will open category controller to select category of post
 *
 *  @param sender categoty button clicked
 */
-(void)filterByCategoryButtonClicked:(id)sender {
    
    OptionsViewController *optionVC = [[OptionsViewController alloc] init];
    optionVC.delegate = self;
    optionVC.filterOptions = @[@"All",@"Dates",@"Work",@"Hobby",@"Day to day",@"Food and Drink",@"Home",@"Personal",@"Other"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:optionVC];
    [self presentModalViewController:navController animated:YES];
    
}
-(void)optionSelected:(NSString *)option{
    
    filterByCategoryLabel.text = option;
    if ([option isEqualToString:@"All"]) {
        self.selectedCategory = @"";
    }
    else {
        self.selectedCategory = option;
    }
    
    [self loadObjects];
    
}
-(void)segmentedControlValueDidChange:(UIButton *)segment
{
    
    if ([segment isEqual:_segment1]) {
        _segment1.selected = YES;
        _segment2.selected = NO;
        self.loadVideos = loadAllVideos;
        
    }
    else {
        _segment1.selected = NO;
        _segment2.selected = YES;
        self.loadVideos = loadLikedVideos;
        
    }
    
    [self loadObjects];
    
}


/**
 *  This method invoke when user follow someone it will return following user count.
 *
 *  @param note
 */
-(void)userFollowedSomeOne:(NSNotification*)note{
    
    //UILabel *followingCountLabel = (UILabel*)[self.headerView viewWithTag:500];
    
    
    PFQuery *queryFollowingCount = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [queryFollowingCount whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
    [queryFollowingCount whereKey:kPAPActivityFromUserKey equalTo:self.user];
    [queryFollowingCount setCachePolicy:kPFCachePolicyCacheThenNetwork];
    [queryFollowingCount countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            [followingCountLabel setText:[NSString stringWithFormat:@"%d", number]];
        }
    }];
    
}



/**
 *  This method will send push notification
 *
 *  @param toUser followingUser
 */
-(void)sendPushToFollowingUser:(PFUser*)toUser{
    
    NSString *message = [NSString stringWithFormat:@"%@ started Following you",[[PFUser currentUser] username]];
    
    NSDictionary *payload =
    [NSDictionary dictionaryWithObjectsAndKeys:
     message, kAPNSAlertKey,
     @"Increment",kAPNSBadgeKey,
     kPAPPushPayloadActivityFollowKey, kPAPPushPayloadActivityTypeKey,
     @"sms-received.wav",kAPNSSoundKey,
     nil];
    
    // Send the push
    PFPush *push = [[PFPush alloc] init];
    if ([toUser objectForKey:kPAPUserPrivateChannelKey]) {
        [push setChannel:[toUser objectForKey:kPAPUserPrivateChannelKey]];
        [push setData:payload];
        [push sendPushInBackground];
    }
    
}


-(void)starVisibilityFrame
{
    
    PFQuery *query = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [query orderByDescending:kPAPActivityUpvoteCommentsCount];
    [query whereKey:kPAPActivityUpvoteCommentsCount greaterThan:[NSNumber numberWithInt:5]];
    //[[query whereKey:kPAPActivityToUserKey equalTo:[PFUser currentUser].objectId] ;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSLog(@"All data:%@",objects);
            for (PFObject *obj  in objects) {
                //if ([[PFUser currentUser].objectId  isEqual:[[obj objectForKey:kPAPActivityToUserKey] objectId]]){
                //if ([user.objectId  isEqual:[[obj objectForKey:kPAPActivityToUserKey] objectId]]){
                
                starImg = [[UIImageView alloc]initWithFrame:CGRectMake(172, 50, 25, 25)];
                starImg.image = [UIImage imageNamed:@"selected_star.png"];
                [self.headerView addSubview:starImg];
                
                // }
            }
        }
    }];
}

@end