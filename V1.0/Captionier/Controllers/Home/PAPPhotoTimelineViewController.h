//
//  PAPPhotoTimelineViewController.h
//  Anypic
//
//  Created by Héctor Ramos on 5/3/12.
//

#import "PAPPhotoHeaderView.h"
#import "PAPPhotoCell.h"
#import <MessageUI/MessageUI.h>

typedef enum {
    
    VIDEO_MEDIA = 0,
    IMAGE_MEDIA,
    AUDIO_MEDIA
    
}MediaType;
@interface PAPPhotoTimelineViewController : PFQueryTableViewController <PAPPhotoHeaderViewDelegate,PAPPhotoCellViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    
    
    NSString *filePath1;
}
- (PAPPhotoHeaderView *)dequeueReusableSectionHeaderView;
@property(nonatomic,strong)PFObject *selectedObject;
@property(nonatomic,assign)BOOL isNeedPullToRefresh;
@property (nonatomic, strong) NSMutableSet *reusableSectionHeaderViews;
@property (nonatomic, strong) NSMutableDictionary *outstandingSectionHeaderQueries;
@property (nonatomic,assign)  int loadVideos;
@property(nonatomic) MediaType mediatype;

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
@end
