//
//  MapAnnotationView.m
//  InstaChurch
//
//  Created by 3Embed on 04/02/15.
//
//

#import "MapAnnotationView.h"

@implementation MapAnnotationView
@synthesize coordinate=_coordinate;
@synthesize title=_title;
-(id) initWithTitle:(NSString *) title AndCoordinate:(CLLocationCoordinate2D)coordinate
{
    self = [super init];
    _title = title;
    _coordinate = coordinate;
    return self;
}
@end

