//
//  PAPPhotoTimelineViewController.m
//  Anypic
//
//  Created by Héctor Ramos on 5/2/12.
//

#import "PAPPhotoTimelineViewController.h"
#import "PAPPhotoCell.h"
#import "PAPAccountViewController.h"
#import "PAPPhotoDetailsViewController.h"
#import "PAPUtility.h"
#import "PAPLoadMoreCell.h"
#import "STTweetLabel.h"
#import "ASStarRatingView.h"
#import "PAPHomeViewController.h"
#import "CommentsViewController.h"

#import "ReportInappropriateOptionsVC.h"
#import "LikersViewController.h"
#import "NotificationBar.h"
#import "VideoPlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "DACircularProgressView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UploadProgress.h"
#import "ProgressView.h"
#import "HelpSreenViewController.h"
#import "SDWebImageDownloader.h"
#import "SDWebImageManager.h"
#import "PAPLocationViewController.h"
#import "PrivateDetailGroupViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>

@interface PAPPhotoTimelineViewController () <UIActionSheetDelegate,AVAudioPlayerDelegate, UIScrollViewDelegate>
{
    UIDocumentInteractionController *dic;
    BOOL shareOnFB;
    BOOL shareOnTwitter;
    PFObject *feedObject;
    PFObject *privateFeed;
}

@property (nonatomic, assign) BOOL shouldReloadOnAppear;
@property (nonatomic, assign) BOOL shouldCallCommentsQuery;
@property (nonatomic,strong) PFObject *selectObject;
@property (nonatomic, strong)VideoPlayerViewController *player;
@property (nonatomic, strong) PAPPhotoCell *pCell;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (nonatomic, strong) MPMoviePlayerController  *moviePlayer;
@property (nonatomic,assign) BOOL isQueryForHashTag;
@property (nonatomic, strong) AVPlayerLayer *vLayer;
@property (nonatomic, strong) AVPlayer *vplayer;
@property (nonatomic, strong) NSMutableDictionary *playerItem;
@property (nonatomic, strong)     AVPlayerLayer *vidLayer;
@property (nonatomic, strong) UIActivityIndicatorView *videoLoadProgressView;
@property (nonatomic, strong) NSMutableArray *observedObjects;
@property (nonatomic,strong) NSString *shortUrl;

@property (nonatomic, assign) BOOL isViewHidden;

@end

@implementation PAPPhotoTimelineViewController
@synthesize reusableSectionHeaderViews;
@synthesize shouldReloadOnAppear;
@synthesize outstandingSectionHeaderQueries;
@synthesize selectObject;
@synthesize selectedObject;
@synthesize isNeedPullToRefresh;
@synthesize player;
@synthesize moviePlayer;
@synthesize loadVideos;
@synthesize mediatype;
@synthesize shortUrl;


#pragma mark - Initialization




- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PAPTabBarControllerDidFinishEditingPhotoNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PAPUtilityUserFollowingChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PAPPhotoDetailsViewControllerUserLikedUnlikedPhotoNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PAPUtilityUserLikedUnlikedPhotoCallbackFinishedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PAPPhotoDetailsViewControllerUserCommentedOnPhotoNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PAPPhotoDetailsViewControllerUserDeletedPhotoNotification object:nil];
    
    
    for (AVPlayerItem *item in _observedObjects) {
        [item removeObserver:self forKeyPath:@"status"];
        [item removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
        [item removeObserver:self forKeyPath:@"playbackBufferEmpty"];
        [item removeObserver:self forKeyPath:@"playbackBufferFull"];
        
    }
}

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        
        self.outstandingSectionHeaderQueries = [NSMutableDictionary dictionary];
        
        // The className to query on
        self.parseClassName = kPAPFeedClassKey;
        //self.parseClassName = kPAPPrivateFeedClassKey;
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        if ([ud objectForKey:@"SingleObject"]) {
            self.paginationEnabled = NO;
        }
        else {
            self.paginationEnabled = YES;
        }
        // Whether the built-in pagination is enabled
        
        
        // Whether the built-in pull-to-refresh is enabled
        if (NSClassFromString(@"UIRefreshControl")) {
            self.pullToRefreshEnabled = NO;
        } else {
            self.pullToRefreshEnabled = YES;
        }
        
        // The number of objects to show per page
        self.objectsPerPage = 10;
        
        // Improve scrolling performance by reusing UITableView section headers
        self.reusableSectionHeaderViews = [NSMutableSet setWithCapacity:3];
        
        self.shouldReloadOnAppear = NO;
        
        self.shouldCallCommentsQuery = YES;
    }
    return self;
}



/**
 *  customized back leftbutton on navigation bar
 */
-(void)customizeNavigationBackButton {
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
    //    [backButton setBackgroundImage:[UIImage imageNamed:<#ImageNameOn#>] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
}

/**
 *  move to previous controller
 *
 *  @param sender back btn pressed
 */
-(void)backButtonAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UIViewController

- (void)viewDidLoad {
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone]; // PFQueryTableViewController reads this in viewDidLoad -- would prefer to throw this in init, but didn't work
    
    [super viewDidLoad];
    
    
    // UIView *texturedBackgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
    // texturedBackgroundView.backgroundColor = [UIColor whiteColor];
    // self.tableView.backgroundView = texturedBackgroundView;
    
    //isNeedPullToRefresh = YES;
    if (isNeedPullToRefresh) {
        
        if (NSClassFromString(@"UIRefreshControl")) {
            // Use the new iOS 6 refresh control.
            UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
            self.refreshControl = refreshControl;
            self.refreshControl.tintColor = [UIColor colorWithRed:73.0f/255.0f green:55.0f/255.0f blue:35.0f/255.0f alpha:1.0f];
            [self.refreshControl addTarget:self action:@selector(refreshControlValueChanged:) forControlEvents:UIControlEventValueChanged];
            self.pullToRefreshEnabled = NO;
        }
    }
    else {
        [self customizeNavigationBackButton];
    }
    
    _observedObjects = [NSMutableArray new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidPublishPhoto:) name:PAPTabBarControllerDidFinishEditingPhotoNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userFollowingChanged:) name:PAPUtilityUserFollowingChangedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidDeletePhoto:) name:PAPPhotoDetailsViewControllerUserDeletedPhotoNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLikeOrUnlikePhoto:) name:PAPPhotoDetailsViewControllerUserLikedUnlikedPhotoNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLikeOrUnlikePhoto:) name:PAPUtilityUserLikedUnlikedPhotoCallbackFinishedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidCommentOnPhoto:) name:PAPPhotoDetailsViewControllerUserCommentedOnPhotoNotification object:nil];
    
    
    
    //    //TO check existing font in system
    //    for (NSString* family in [UIFont familyNames])
    //                {
    //                    NSLog(@"%@",family);
    //
    //
    //                    for (NSString* name in [UIFont fontNamesForFamilyName: family])
    //                    {
    //                        NSLog(@"%@", name);
    //                       // TELogInfo(@"  %@", name);
    //                    }
    //                }
    //
    
    
    
    //[self.tableView setBackgroundColor:[UIColor redColor]];
    shareOnFB = NO;
    shareOnTwitter = NO;
    NSLog(@"viewdidload - timeline %@", self);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _isViewHidden = NO;
    
    if (self.shouldReloadOnAppear) {
        self.shouldReloadOnAppear = NO;
        [self loadObjects];
    }
    
    //    NotificationBar *bar = [NotificationBar sharedInstance];
    //    [bar setMessage:@"message me"];
}
-(void)viewWillDisappear:(BOOL)animated {
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    
    _isViewHidden = YES;
    //[_vplayer removeObserver:self forKeyPath:@"currentItem.playbackLikelyToKeepUp"];
    [self stopVideoPlayer];
    //_vplayer = nil;
    //[_vidLayer removeFromSuperlayer];
    //_vidLayer = nil;
    //[_playerItem removeAllObjects];
    //_playerItem = nil;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger sections = self.objects.count;
    if (self.paginationEnabled && sections != 0)
        sections++;
    return sections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == self.objects.count) {
        // Load More section
        return nil;
    }
    
    PAPPhotoHeaderView *headerView = [self dequeueReusableSectionHeaderView];
    
    if (!headerView) {
        headerView = [[PAPPhotoHeaderView alloc] initWithFrame:CGRectMake( 0.0f, 0.0f, self.view.bounds.size.width, 50) buttons:PAPPhotoHeaderButtonsDefault];
        headerView.delegate = self;
        //[headerView setBackgroundColor:[UIColor redColor]];
        [self.reusableSectionHeaderViews addObject:headerView];
    }
    
    PFObject *photo = [self.objects objectAtIndex:section];
    if(loadVideos == 1){
        
        
        PFObject *object  = [photo objectForKey:kPAPActivityPhotoKey];
        //PFUser *user1 = [photo objectForKey:kPAPActivityToUserKey];
        headerView.user = [photo objectForKey:kPAPActivityToUserKey];
        [headerView displayuser];
        photo = object;
        headerView.photo = photo;
        
        
        
    }
    else if (_isQueryForHashTag) {
        PFObject *object  = [photo objectForKey:kPAPActivityPhotoKey];
        
        headerView.user = [photo objectForKey:kPAPActivityFromUserKey];
        [headerView displayuser];
        photo = object;
        headerView.photo = photo;
    }
    else {
       // PFUser *userDetails = [photo objectForKey:@"user"];
        //[headerView setPhotoFile:userDetails];
       [headerView setPhotoFile:photo];
    }
    
    
    return headerView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == self.objects.count) {
        return 0.0f;
    }
    return 50.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake( 0.0f, 0.0f, self.tableView.bounds.size.width, 16.0f)];
    // footerView.backgroundColor = [UIColor greenColor];
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == self.objects.count) {
        return 0.0f;
    }
    return 05.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    if (self.shouldCallCommentsQuery) {
    //        [self calculateHeightForCells];
    //    }
    
    if (indexPath.section >= self.objects.count) {
        // Load More Section
        return 44.0f;
    }
    
    PFObject *object = self.objects[indexPath.section];
    if (loadVideos == 1 || _isQueryForHashTag) {
        object = [object objectForKey:kPAPActivityPhotoKey];
    }
    
    NSArray *commentersArray = [object objectForKey:kPAPFeedCommentersArrayKey];
    NSArray *commentsArray = [object objectForKey:kPAPFeedCommentsArrayKey];
    
    CGFloat height = 0;
    
    
    for(int i = 0 ; i< commentersArray.count ;i++){
        
        NSString *commenter = commentersArray[i];
        NSString *comment = commentsArray[i];
        
        height =  height + [PAPPhotoCell heightForCellWithName:commenter contentString:comment cellInsetWidth:0];
    }
    
    return 365.0f + height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == self.objects.count && self.paginationEnabled) {
        // Load More Cell
        [self loadNextPage];
    }
}


#pragma mark - PFQueryTableViewController
/**
 *  parse query to get data from parse
 *
 *  @return query
 */
- (PFQuery *)queryForTable {
    
    [self removePlayerObservers];
    
    if (![PFUser currentUser]) {
        PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
        [query setLimit:0];
        return query;
    }
    PFQuery *query;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"SingleObject"] && selectedObject != nil) {
        
        PFQuery *photosFromCurrentUserQuery = [PFQuery queryWithClassName:self.parseClassName];
        [photosFromCurrentUserQuery whereKey:@"objectId" equalTo:self.selectedObject.objectId];
        //[photosFromCurrentUserQuery whereKeyExists:kPAPPhotoPictureKey];
        
        query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects: photosFromCurrentUserQuery, nil]];
        [query includeKey:kPAPPhotoUserKey];
        //[query orderByDescending:@"SingleObject"];
        
        [ud removeObjectForKey:@"SingleObject"];
    }
    else if ([ud objectForKey:@"FilterBy"]) {
        PFQuery *photosFromCurrentUserQuery = [PFQuery queryWithClassName:self.parseClassName];
        
        [photosFromCurrentUserQuery whereKey:@"category" containsString:[ud objectForKey:@"FilterBy"]];
        
        
        //[photosFromCurrentUserQuery whereKeyExists:kPAPPhotoPictureKey];
        
        query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects: photosFromCurrentUserQuery, nil]];
        [query includeKey:kPAPPhotoUserKey];
        [query orderByDescending:@"createdAt"];
        
        [ud removeObjectForKey:@"FilterBy"];
    }
    else if ([ud objectForKey:@"HashTag"]) {
        
        _isQueryForHashTag = YES;
        
        query = [PFQuery queryWithClassName:@"Activity"];
        [query whereKey:@"content" containsString:[ud objectForKey:@"HashTag"]];
        [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeComment];
        query.cachePolicy = kPFCachePolicyNetworkOnly;
        [query includeKey:kPAPActivityFromUserKey];
        [query includeKey:kPAPActivityPhotoKey];
        [query orderByDescending:@"createdAt"];
        
        [ud removeObjectForKey:@"HashTag"];
        [ud synchronize];
        
    }
    else {
        
        
        
        PFQuery *followingActivitiesQuery = [PFQuery queryWithClassName:kPAPActivityClassKey];
        [followingActivitiesQuery whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
        [followingActivitiesQuery whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
        followingActivitiesQuery.cachePolicy = kPFCachePolicyNetworkOnly;
        followingActivitiesQuery.limit = 1000;
        
        PFQuery *photosFromFollowedUsersQuery = [PFQuery queryWithClassName:self.parseClassName];
        [photosFromFollowedUsersQuery whereKey:kPAPFeedUserKey matchesKey:kPAPActivityToUserKey inQuery:followingActivitiesQuery];
        [photosFromFollowedUsersQuery whereKeyExists:kPAPFeedMediaLinkKey];
        [photosFromFollowedUsersQuery whereKeyDoesNotExist:@"isPrivateGroup"];
        
        
        PFQuery *photosFromCurrentUserQuery = [PFQuery queryWithClassName:self.parseClassName];
        [photosFromCurrentUserQuery whereKey:kPAPFeedUserKey equalTo:[PFUser currentUser]];
        
        [photosFromCurrentUserQuery whereKeyExists:kPAPFeedMediaLinkKey];
        
        query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:photosFromFollowedUsersQuery, photosFromCurrentUserQuery, nil]];
        query.limit = 5;
        [query includeKey:kPAPPhotoUserKey];
        [query whereKeyDoesNotExist:@"isPrivateGroup"];
        [query orderByDescending:@"createdAt"];
        
        
        //************************************Private Group ***************************************//
        
        
        
        
        PFQuery *feedFromCurrentUserQuery = [PFQuery queryWithClassName:kPAPPrivateFeedClassKey];
        //[feedFromCurrentUserQuery whereKey:kPAPPrivateGroupUsersKey equalTo:[PFUser currentUser]];
        [feedFromCurrentUserQuery whereKeyExists:kPAPFeedMediaLinkKey];
        [feedFromCurrentUserQuery includeKey:kPAPPrivateGroupUsersKey];
        feedFromCurrentUserQuery.limit = 5;
        [feedFromCurrentUserQuery orderByDescending:@"createdAt"];
        
        //PFQuery *privateGrpQuery =  [ PFQuery queryWithClassName:kPAPPrivateGroupClassKey];
        //[privateGrpQuery whereKeyExists:kPAPFeedMediaLinkKey];
        
//        query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:feedFromCurrentUserQuery,privateGrpQuery,nil]];
//        query.limit = 5;
//        [query includeKey:kPAPPrivateGroupClassKey];
//        [query orderByDescending:@"createdAt"];
        
        
        
    }
    
    
    
    
    //        PFQuery *followingActivitiesQuery = [PFQuery queryWithClassName:kPAPFollowersClassKey];
    //        [followingActivitiesQuery whereKey:kPAPFollowersFollowingKey equalTo:[PFUser currentUser]];
    //        followingActivitiesQuery.cachePolicy = kPFCachePolicyNetworkOnly;
    //        followingActivitiesQuery.limit = 1000;
    //
    //        PFQuery *photosFromFollowedUsersQuery = [PFQuery queryWithClassName:kPAPFeedClassKey];
    //        [photosFromFollowedUsersQuery whereKey:kPAPFeedUserKey matchesKey:kPAPFollowersFollowingKey inQuery:followingActivitiesQuery];
    //        [photosFromFollowedUsersQuery whereKeyExists:kPAPFeedMediaLinkKey];
    //
    //        PFQuery *photosFromCurrentUserQuery = [PFQuery queryWithClassName:kPAPFeedClassKey];
    //        [photosFromCurrentUserQuery whereKey:kPAPFeedUserKey equalTo:[PFUser currentUser]];
    //
    //        [photosFromCurrentUserQuery whereKeyExists:kPAPFeedMediaLinkKey];
    //
    //        query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:photosFromFollowedUsersQuery, photosFromCurrentUserQuery, nil]];
    //        [query includeKey:kPAPFeedUserKey];
    //        [query orderByDescending:@"createdAt"];
    
    
    // A pull-to-refresh should always trigger a network request.
    [query setCachePolicy:kPFCachePolicyNetworkOnly];
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    //
    // If there is no network connection, we will hit the cache first.
    if (self.objects.count == 0 || ![[UIApplication sharedApplication].delegate performSelector:@selector(isParseReachable)]) {
        
        [query setCachePolicy:kPFCachePolicyCacheThenNetwork];
    }
    
    /*
     This query will result in an error if the schema hasn't been set beforehand. While Parse usually handles this automatically, this is not the case for a compound query such as this one. The error thrown is:
     
     Error: bad special key: __type
     
     To set up your schema, you may post a photo with a caption. This will automatically set up the Photo and Activity classes needed by this query.
     
     You may also use the Data Browser at Parse.com to set up your classes in the following manner.
     
     Create a User class: "User" (if it does not exist)
     
     Create a Custom class: "Activity"
     - Add a column of type pointer to "User", named "fromUser"
     - Add a column of type pointer to "User", named "toUser"
     - Add a string column "type"
     
     Create a Custom class: "Photo"
     - Add a column of type pointer to "User", named "user"
     
     You'll notice that these correspond to each of the fields used by the preceding query.
     */
    
    return query;
}

- (void)objectsDidLoad:(NSError *)error {
    
    
    [super objectsDidLoad:error];
    
    
    _vplayer = nil;
    [_playerItem removeAllObjects];
    _playerItem = nil;
    _pCell = nil;
    [_vidLayer removeObserver:self forKeyPath:@"readyForDisplay"];
    [_vidLayer removeFromSuperlayer];
    _vidLayer = nil;
    
    if (NSClassFromString(@"UIRefreshControl")) {
        [self.refreshControl endRefreshing];
    }
    
    
    if (self.objects.count == 0) {
        UIView *bgView = [[UIView alloc] initWithFrame:self.view.bounds];
        //UILabel *label = [[UILabel alloc] initWithFrame:bgView.bounds];
        UILabel *label = [[UILabel alloc]init];
        [label setFrame:CGRectMake( 0.0f,100.0f+50, 320.0f, 455)];
        label.text = @"No posts to display";
        label.textAlignment = NSTextAlignmentCenter;
        [bgView addSubview:label];
        bgView.backgroundColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        [self.tableView setBackgroundView:bgView];
    }
    else {
        self.tableView.backgroundView = nil;
    }
    
    
}

/**
 *  calculating height of cell according to number of comments and its length
 */

-(void)calculateHeightForCells{
    
    self.shouldCallCommentsQuery = NO;
    
    for (int i = 0; i < self.objects.count; i++) {
        PFObject *photo = self.objects[i];
        
        PFQuery *queryComments = [PFQuery queryWithClassName:kPAPActivityClassKey];
        [queryComments whereKey:kPAPActivityPhotoKey equalTo:photo];
        [queryComments whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeComment];
        //[queryComments orderByDescending:kPAPActivityUpvoteCommentsCount];
        [queryComments setLimit:3];
        [queryComments includeKey:kPAPActivityFromUserKey];
        [queryComments includeKey:kPAPActivityPhotoKey];
        //[queryComments includeKey:kPAPActivityUpvoteCommentsCount];
        //[queryComments orderByDescending:kPAPActivityUpvoteCommentsCount];
        
        
        [queryComments findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
            if (!error) {
                [[PAPCache sharedCache] setCommentsForPhoto:photo Comments:array];
            }
        }];
    }
    
    
    
}

- (PFObject *)objectAtIndexPath:(NSIndexPath *)indexPath {
    // overridden, since we want to implement sections
    if (indexPath.section < self.objects.count) {
        return [self.objects objectAtIndex:indexPath.section];
    }
    
    return nil;
}



/**
 *  video player
 *
 *  @param indexPath
 */
- (void) setupVideoPlayer:(NSIndexPath*)indexPath
{
    PFObject *object = [self.objects objectAtIndex:indexPath.section];
    if ([object.parseClassName isEqualToString:kPAPActivityClassKey]) {
        object = [object objectForKey:kPAPActivityPhotoKey];
        object = [object fetchIfNeeded];
    }
    NSString *mediaLink = [object objectForKey:kPAPFeedMediaLinkKey];
    
    AVPlayerItem *newItem = nil;
    
    if (_playerItem) {
        newItem = [_playerItem objectForKey:[@(indexPath.section) stringValue]];
    }
    if (!newItem) {
        //NSLog(@"new item - %i - %@", indexPath.section, mediaLink);
        newItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:mediaLink]]];
        if (!_playerItem) {
            _playerItem = [[NSMutableDictionary alloc] init];
        }
        if (newItem) {
            [_playerItem setObject:newItem forKey:[@(indexPath.section) stringValue]];
        }
        
        if (!_vplayer) {
            _vplayer = [AVPlayer playerWithPlayerItem:newItem];
            _vplayer.actionAtItemEnd = AVPlayerActionAtItemEndPause;
            //[_vplayer play];
            
            if (_vidLayer) {
                [_vidLayer removeObserver:self forKeyPath:@"readyForDisplay"];
                _vidLayer = nil;
            }
            _vidLayer = [AVPlayerLayer playerLayerWithPlayer:_vplayer];
            [_vidLayer setFrame:CGRectMake(0, 0, 320, 320)];
            [_vidLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
            
            [_vidLayer  addObserver:self forKeyPath:@"readyForDisplay" options:NSKeyValueObservingOptionNew context:nil];
            
            
            //[_vplayer addObserver:self forKeyPath:@"status" options:0 context:nil];
            [self setupPlayerObserversForItem:newItem];
            
        }
        
        
        
    }
    
}


/**
 *  actions for video player
 */
-(void)setupPlayerObserversForItem:(AVPlayerItem*)item{
    
    _pCell.btnPlay.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidStartPlaying:) name:AVPlayerItemNewAccessLogEntryNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemPlayingStalled:) name:AVPlayerItemPlaybackStalledNotification object:nil];
    
    
    NSLog(@" %s add observer : %@", __PRETTY_FUNCTION__, item);
    
    [item  addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    [item  addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew context:nil];
    [item addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
    [item addObserver:self forKeyPath:@"playbackBufferFull" options:NSKeyValueObservingOptionNew context:nil];
    
    [_observedObjects addObject:item];
}



-(void)removePlayerObservers{
    
    
    
    @try{
        
        
        [_playerItem enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            AVPlayerItem *item = (AVPlayerItem*)obj;
            [item removeObserver:self forKeyPath:@"status"];
            [item removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
            [item removeObserver:self forKeyPath:@"playbackBufferEmpty"];
            [item removeObserver:self forKeyPath:@"playbackBufferFull"];
            
            NSLog(@" %s remove observer : %@", __PRETTY_FUNCTION__, item);
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemNewAccessLogEntryNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemPlaybackStalledNotification object:nil];
            
            
        }];
          _pCell.btnPlay.hidden = NO;
        [_vplayer.currentItem removeObserver:self forKeyPath:@"status"];
        [_vplayer.currentItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
        [_vplayer.currentItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
        [_vplayer.currentItem removeObserver:self forKeyPath:@"playbackBufferFull"];
        
        NSLog(@" %s remove observer : %@", __PRETTY_FUNCTION__, _vplayer);
        
    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
    }
    
}
//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
//
//    if (object == _vplayer && [keyPath isEqualToString:@"status"]) {
//        if (_vplayer.status == AVPlayerStatusFailed) {
//            NSLog(@"AVPlayer Failed");
//
//        } else if (_vplayer.status == AVPlayerStatusReadyToPlay) {
//            NSLog(@"AVPlayerStatusReadyToPlay");
//            [_vplayer play];
//
//
//        } else if (_vplayer.status == AVPlayerItemStatusUnknown) {
//            NSLog(@"AVPlayer Unknown");
//
//        }
//    }
//}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (_isViewHidden)
        return;
    NSLog(@"%@ %@", keyPath, change);
    
    if ([object isEqual:_vplayer.currentItem] && [keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        
        if ((change[NSKeyValueChangeNewKey] != [NSNull null]) && [change[NSKeyValueChangeNewKey] boolValue]) {
            
            NSInteger pcellIndex = _pCell ? [self.tableView indexPathForCell:_pCell].section : -1;
            AVPlayerItem *playerItem = (AVPlayerItem*)[_playerItem objectForKey:[@(pcellIndex) stringValue]];
            if (playerItem && [playerItem isEqual:_vplayer.currentItem]) {
                NSLog(@"player1");
                [self hideCellActivityIndicator];
                _pCell.btnPlay.hidden = YES;
                [_vplayer play];
                
                if (![[[_pCell.imageView.layer sublayers] objectAtIndex:0] isEqual:_vidLayer]) {
                    
                    [_pCell.imageView.layer insertSublayer:_vidLayer atIndex:0];
                    
                }
                
                
                
            }
        }
        
    }
    else if ([object isEqual:_vplayer.currentItem] && [keyPath isEqualToString:@"playbackBufferFull"]){
        if ((change[NSKeyValueChangeNewKey] != [NSNull null]) && [change[NSKeyValueChangeNewKey] boolValue]) {
            NSInteger pcellIndex = _pCell ? [self.tableView indexPathForCell:_pCell].section : -1;
            AVPlayerItem *playerItem = (AVPlayerItem*)[_playerItem objectForKey:[@(pcellIndex) stringValue]];
            if (playerItem && [playerItem isEqual:_vplayer.currentItem]) {
                NSLog(@"player2");
                [self hideCellActivityIndicator];
                _pCell.btnPlay.hidden = YES;
                [_vplayer play];
                if (![[[_pCell.imageView.layer sublayers] objectAtIndex:0] isEqual:_vidLayer]) {
                    
                    [_pCell.imageView.layer insertSublayer:_vidLayer atIndex:0];
                    
                }
            }
            
        }
    }
    else if ([object isEqual:_vplayer.currentItem] && [keyPath isEqualToString:@"status"]){
        if ((change[NSKeyValueChangeNewKey] != [NSNull null]) && ((AVPlayerItemStatus)change[NSKeyValueChangeNewKey] == AVPlayerItemStatusReadyToPlay)) {
            NSInteger pcellIndex = _pCell ? [self.tableView indexPathForCell:_pCell].section : -1;
            AVPlayerItem *playerItem = (AVPlayerItem*)[_playerItem objectForKey:[@(pcellIndex) stringValue]];
            if (playerItem && [playerItem isEqual:_vplayer.currentItem]) {
                NSLog(@"player2");
                [self hideCellActivityIndicator];
                 _pCell.btnPlay.hidden = YES;
                [_vplayer play];
                
                if (![[[_pCell.imageView.layer sublayers] objectAtIndex:0] isEqual:_vidLayer]) {
                    
                    [_pCell.imageView.layer insertSublayer:_vidLayer atIndex:0];
                    
                }
            }
            
        }
    }
    else if ([object isEqual:_vidLayer] && [keyPath isEqualToString:@"readyForDisplay"]) {
        if ((change[NSKeyValueChangeNewKey] != [NSNull null]) && [change[NSKeyValueChangeNewKey] boolValue]) {
            
            NSInteger pcellIndex = _pCell ? [self.tableView indexPathForCell:_pCell].section : -1;
            AVPlayerItem *playerItem = (AVPlayerItem*)[_playerItem objectForKey:[@(pcellIndex) stringValue]];
            if (playerItem && [playerItem isEqual:_vplayer.currentItem]) {
                NSLog(@"player2");
                [_pCell.imageView.layer insertSublayer:_vidLayer atIndex:0];
            }
            
        }
    }
    else if ([object isEqual:_vplayer.currentItem] && [keyPath isEqualToString:@"playbackBufferEmpty"]) {
        [self showCellActivityIncicator];
    }
    
    //    else if ([object isEqual:_vplayer.currentItem] && [keyPath isEqualToString:@"playbackBufferEmpty"]){
    //
    //        if (!_videoLoadProgressView) {
    //            _videoLoadProgressView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    //
    //        }
    //        else {
    //
    //            [_videoLoadProgressView removeFromSuperview];
    //        }
    //        _videoLoadProgressView.center = _pCell.center;
    //        [_pCell addSubview:_videoLoadProgressView];
    //        [_videoLoadProgressView startAnimating];
    //
    //    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section < self.objects.count) {
        PFObject *feed = [self.objects objectAtIndex:indexPath.section];
        PAPPhotoCell *photoCell = (PAPPhotoCell*)cell;
        
        if ([[feed parseClassName] isEqualToString:kPAPActivityClassKey])
        {
            feed = [feed objectForKey:kPAPActivityPhotoKey];
            feed = [feed fetchIfNeeded];
        }
        
        //_pCell = photoCell;
        NSURL *mediaURL = [NSURL URLWithString:[feed objectForKey:kPAPFeedMediaLinkKey]];
        
        
        if ([[feed objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeImageKey]) {
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            
            DACircularProgressView *progressView = [[DACircularProgressView alloc] initWithFrame:photoCell.btnPlay.frame];
            progressView.roundedCorners = YES;
            progressView.trackTintColor = [UIColor clearColor];
            // [photoCell.contentView addSubview:progressView];
            photoCell.imageView.image = [UIImage imageNamed:@"PlaceholderPhoto.png"];
            
            
            [manager downloadWithURL:mediaURL
                             options:SDWebImageProgressiveDownload
                            progress:^(NSUInteger receivedSize , long long expectesSize){
                                
                                CGFloat progess = ((float)receivedSize/expectesSize);
                                [progressView setProgress:progess animated:YES];
                            }
                           completed:^(UIImage *image , NSError *error , SDImageCacheType cacheType ,BOOL finished){
                               
                               [progressView setProgress:1 animated:YES];
                               
                               [progressView removeFromSuperview];
                               photoCell.imageView.image = image;
                               
                               [photoCell.activityIndicator stopAnimating];
                               [photoCell.activityIndicator setHidden:YES];
                               //[photoCell.imageView setNeedsDisplay];
                               
                           }];
        }
        else {
            
            if (!_pCell) {
                //                _pCell = photoCell;
                
                NSLog(@"willdisplaycell : %@", _vplayer.currentItem);
                
                [self checkVisibilityOfCell:photoCell inScrollView:self.tableView];
                
                
            }
            
            
            
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)aObject {
    static NSString *CellIdentifierImage = @"ImageCell";
    static NSString *CellIdentifierVideo = @"VideoCell";
    
    if (indexPath.section == self.objects.count) {
        // this behavior is normally handled by PFQueryTableViewController, but we are using sections for each object and we must handle this ourselves
        UITableViewCell *cell = [self tableView:tableView cellForNextPageAtIndexPath:indexPath];
        return cell;
    } else {
        PAPPhotoCell *cell = nil;
        
        PFObject *object = aObject;
        if ([[aObject parseClassName] isEqualToString:kPAPFeedClassKey])
        {
            object = aObject;
        }
        else if ([[aObject parseClassName] isEqualToString:kPAPActivityClassKey])
        {
            object = [aObject objectForKey:kPAPActivityPhotoKey];
            object = [object fetchIfNeeded];
        }
        else if ([[aObject parseClassName] isEqualToString:kPAPPrivateFeedClassKey])
        {
            object = aObject;
//            object = [aObject objectForKey:kPAPActivityPhotoKey];
//            object = [object fetchIfNeeded];
        }
        
        if ([[object objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeImageKey]) {
            cell = (PAPPhotoCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierImage];
        }
        else {
            NSString *ident = [NSString stringWithFormat:@"%@_%li", CellIdentifierVideo, (long)indexPath.section];
            cell = (PAPPhotoCell *)[tableView dequeueReusableCellWithIdentifier:ident];
            
            //cell = nil;
        }
        
        //cell = nil;
        
        if (cell == nil) {
            
            if ([[object objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeImageKey]) {
                cell = [[PAPPhotoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierImage];
                cell.imageView.image = [UIImage imageNamed:@"PlaceholderPhoto.png"];
                
            }
            else {
                
                NSString *ident = [NSString stringWithFormat:@"%@_%i", CellIdentifierVideo, indexPath.section];
                
                
                cell = [[PAPPhotoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ident] ;
                cell.imageView.image = [UIImage imageNamed:@"PlaceholderPhoto.png"];
                
                //NSString *mediaLink = [object objectForKey:kPAPFeedMediaLinkKey];
                
                
                
            }
            //[cell.photoButton addTarget:self action:@selector(didTapOnPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.delegate = self;
            
            
            
            
        }
        
        cell.photo = object;
        
        if ([[aObject parseClassName] isEqualToString:kPAPActivityClassKey]){
            cell.user = [aObject objectForKey:kPAPActivityFromUserKey];
        }
        else{
            cell.user = [object objectForKey:kPAPFeedUserKey];
        }
        
        if ([[object objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeVideoKey]) {
            
            [self setupVideoPlayer:indexPath];
            [cell.btnPlay setHidden:NO];
            [cell.activityIndicator startAnimating];
            [cell.contentView bringSubviewToFront:cell.activityIndicator];
            [cell.activityIndicator setHidden:NO];
            
        }
        else {
            [cell.btnPlay setHidden:YES];
            
            [cell.activityIndicator setHidden:YES];
            
        }
        
        
        //set comments
        int totalComments = [[object objectForKey:kPAPFeedCommentsCount] integerValue];
        if (totalComments <= 1 ) {
            cell.commentLabel.text = [NSString stringWithFormat:@"%d",totalComments];
        }
        else {
            
            cell.commentLabel.text = [NSString stringWithFormat:@"%d",totalComments];
            
        }
        
        
        //set likes
        int totalLikes = [[object objectForKey:kPAPFeedLikeCountKey] integerValue];
        if (totalLikes <= 1 ) {
            cell.likeLabel.text = [NSString stringWithFormat:@"%d",totalLikes];
        }
        else {
            cell.likeLabel.text = [NSString stringWithFormat:@"%d",totalLikes];
        }
        
        
        
        NSArray *commentsArray = [object objectForKey:kPAPFeedCommentsArrayKey];
        NSArray *commentersArray = [object objectForKey:kPAPFeedCommentersArrayKey];
        [cell setComments:commentsArray Users:commentersArray];
        
        
        NSDictionary *attributesForPhoto = [[PAPCache sharedCache] attributesForPhoto:object];
        
        NSLog(@"attributes : %d   , %@",indexPath.section , attributesForPhoto);
        
        if (attributesForPhoto) {
            
            [cell setLikeStatus:[[PAPCache sharedCache] isPhotoLikedByCurrentUser:object]];
            
            
            
        } else {
            
            @synchronized(self) {
                // check if we can update the cache
                NSNumber *outstandingSectionHeaderQueryStatus = [self.outstandingSectionHeaderQueries objectForKey:[NSNumber numberWithInt:indexPath.section]];
                if (!outstandingSectionHeaderQueryStatus) {
                    PFQuery *query = [PAPUtility queryForActivitiesOnPhoto:object cachePolicy:kPFCachePolicyNetworkOnly];
                    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                        
                        @synchronized(self) {
                            [self.outstandingSectionHeaderQueries removeObjectForKey:[NSNumber numberWithInt:indexPath.section]];
                            
                            if (error) {
                                return;
                            }
                            
                            //[cell setComments:objects];
                            
                            
                            BOOL isLikedByCurrentUser = NO;
                            
                            
                            for(PFObject *obj in objects){
                                
                                PFUser *user = [obj  objectForKey:kPAPActivityFromUserKey];
                                if ([user.objectId isEqualToString:[[PFUser currentUser] objectId]]) {
                                    
                                    if ([[obj objectForKey:kPAPActivityTypeKey] isEqualToString:kPAPActivityTypeLike]) {
                                        [cell setLikeStatus:YES];
                                        isLikedByCurrentUser = YES;
                                        
                                    }
                                    
                                    
                                    
                                }
                            }
                            
                            
                            if (!isLikedByCurrentUser) {
                                
                                [cell setLikeStatus:NO];
                            }
                            
                            
                            [[PAPCache sharedCache] setPhotoIsLikedByCurrentUser:object liked:isLikedByCurrentUser];
                            
                            
                        }
                    }];
                }
            }
        }
        
        
        
        //        //cell.photoButton.tag = indexPath.section;
        //        if ([[object objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeVideoKey]) {
        //            cell.btnPlay.hidden = NO;
        //            [_vplayer pause];
        //        }
        //        else {
        //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //                //[self photoCell:cell didTapOnPlayButton:object];
        //            });
        //        }
        //
        //}
        return cell;
    }
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *LoadMoreCellIdentifier = @"LoadMoreCell";
    
    PAPLoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:LoadMoreCellIdentifier];
    if (!cell) {
        cell = [[PAPLoadMoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LoadMoreCellIdentifier];
        cell.selectionStyle =UITableViewCellSelectionStyleGray;
        cell.separatorImageTop.image = [UIImage imageNamed:@"SeparatorTimelineDark.png"];
        cell.hideSeparatorBottom = YES;
        cell.mainView.backgroundColor = [UIColor whiteColor];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (![cell isKindOfClass:[PAPLoadMoreCell class]] && indexPath.section < self.objects.count) {
        //[[(PAPPhotoCell*)cell vPlayer] pause];
        //[[[cell.imageView.layer sublayers] objectAtIndex:0] setHidden:YES];
        
    }
    
    //[self stopVideoPlayer];
    
}

/**
 *  once video reach its assigned limit it stop playing
 */
-(void)stopVideoPlayer {
    
    
    // if (_vplayer.rate > 0 && !_vplayer.error) {
    // player is playing
    
    if (self.objects.count > 0) {
        
        NSIndexPath *ip = [self.tableView indexPathForCell:_pCell];
        PFObject *object = self.objects[ip.section];
        
        if ([[object objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeVideoKey]) {
            [_pCell.btnPlay setHidden:NO];
            [_vplayer pause];
        }
        [self removePlayerObservers];
        
    }
    
    
    //[self removePlayerObservers];
    
    // }
    
    
    
}

/**
 *  To let the video start playing
 */
-(void)startVideoPlayer
{
    //[self setupPlayerObservers];
    NSLog(@"player3");
    
    [_vplayer play];
}

/**
 *  displaying hastagged user post on home view controller
 *
 *  @param hashTag hashtag userDetails
 */

-(void)loadViewWithHashTag:(NSString*)hashTag
{
    self.shouldReloadOnAppear = NO;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:hashTag forKey:@"HashTag"];
    [ud synchronize];
    PAPHomeViewController *home = [[PAPHomeViewController alloc] initWithStyle:UITableViewStylePlain];
    
    home.hashTagString = hashTag;
    [home setHashTag:YES];
    [self.navigationController pushViewController:home animated:YES];
}

-(void)getUserDetail:(NSString*)username {
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" containsString:username];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        if (!error) {
            // NSLog(@"array %@",array);
            if (array.count > 0) {
                PFUser *user = [array lastObject];
                [self navigateToUserAccountDetial:user];
            }
            
        }
    }];
}

#pragma mark - PAPPhotoTimelineViewController

- (PAPPhotoHeaderView *)dequeueReusableSectionHeaderView {
    for (PAPPhotoHeaderView *sectionHeaderView in self.reusableSectionHeaderViews) {
        if (!sectionHeaderView.superview) {
            // we found a section header that is no longer visible
            return sectionHeaderView;
        }
    }
    
    return nil;
}


#pragma mark - PAPPhotoHeaderViewDelegate
-(void)navigateToUserAccountDetial:(PFUser*)user {
    
    if ([self isKindOfClass:[PAPAccountViewController class]]) {
        return;
    }
    
    self.shouldReloadOnAppear = YES;
    PAPAccountViewController *accountViewController = [[PAPAccountViewController alloc] initWithStyle:UITableViewStylePlain];
    accountViewController.showBackButton = YES;
    [accountViewController setUser:user];
    [self.navigationController pushViewController:accountViewController animated:YES];
    
}
- (void)photoHeaderView:(PAPPhotoHeaderView *)photoHeaderView didTapUserButton:(UIButton *)button user:(PFUser *)user {
    
    [self navigateToUserAccountDetial:user];
}


/**
 *  perform when user press like button
 *  update like count
 *  send push to user
 *
 *  @param photoHeaderView view where button is located
 *  @param button          like button
 *  @param aPhoto          complete details of post
 */
- (void)photoHeaderView:(PAPPhotoHeaderView *)photoHeaderView didTapLikePhotoButton:(UIButton *)button photo:(PFObject *)aPhoto {
    
    [photoHeaderView.likeButton setEnabled:NO];
    
    PFObject *photo = aPhoto;
    if ([[aPhoto parseClassName] isEqualToString:kPAPFeedClassKey])
    {
        photo = aPhoto;
    }
    else if ([[aPhoto parseClassName] isEqualToString:kPAPActivityClassKey])
    {
        photo = [aPhoto objectForKey:kPAPActivityPhotoKey];
        photo = [photo fetchIfNeeded];
        
    }
    
    BOOL liked = [[PAPCache sharedCache] isPhotoLikedByCurrentUser:photo]; //!button.selected;
    [photoHeaderView setLikeStatus:liked];
    
    
    [[PAPCache sharedCache] setPhotoIsLikedByCurrentUser:photo liked:liked];
    
    NSString *originalButtonTitle = photoHeaderView.likeLabel.text;
    
    
    if (!liked) {
        [PAPUtility likePhotoInBackground:photo block:^(BOOL succeeded, NSError *error) {
            
            
            if (!succeeded) {
                [Flurry logEvent:@"LikedFailed"];
                [photoHeaderView.likeButton setEnabled:YES];
                //[actualHeaderView.likeButton setTitle:originalButtonTitle forState:UIControlStateNormal];
                [photoHeaderView.likeLabel setText:originalButtonTitle];
                
            }
            else
            {
            
                int previousCount = [[photo objectForKey:kPAPFeedLikeCountKey] integerValue];
                [photo incrementKey:kPAPFeedLikeCountKey];
                [photo saveInBackground];
                [photoHeaderView.likeButton setEnabled:YES];
                ;
                
                [photoHeaderView.likeLabel setText:[NSString stringWithFormat:@"%d",(previousCount+1)]];
                [photoHeaderView setLikeStatus:succeeded];
                [[PAPCache sharedCache] setPhotoIsLikedByCurrentUser:photo liked:succeeded];
                
                [Flurry logEvent:@"LikedSucceesfully"];
                // Explicitly add the photographer in case he didn't comment
                
                if ([[photo objectForKey:kPAPFeedUserKey] objectForKey:kPAPUserPrivateChannelKey]) {
                    
                    NSMutableSet *channelSet = [NSMutableSet setWithCapacity:1];
                    
                    [channelSet addObject:[[photo objectForKey:kPAPFeedUserKey] objectForKey:kPAPUserPrivateChannelKey]];
                    
                    
                    // Create push payload
                    if (channelSet.count > 0) {
                        
                        
                        // Create notification message
                     //   NSString *userFirstName = [PAPUtility firstNameForDisplayName:[[PFUser currentUser] objectForKey:kPAPUserDisplayNameKey]];
                         NSString *userFirstName = [[PFUser currentUser]username];
                        
                        
                        NSString *message;
                        if ([[photo objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeVideoKey ]){
                            message = [NSString stringWithFormat:@"%@ liked your video", userFirstName];
                        }
                        else if ([[photo objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeImageKey]){
                            message = [NSString stringWithFormat:@"%@ liked your photo", userFirstName];
                        }
                        
                        
                        NSDictionary *payload =
                        [NSDictionary dictionaryWithObjectsAndKeys:
                         message, kAPNSAlertKey,
                         @"Increment",kAPNSBadgeKey,
                         kPAPPushPayloadPayloadTypeActivityKey, kPAPPushPayloadPayloadTypeKey,
                         kPAPPushPayloadActivityLikeKey, kPAPPushPayloadActivityTypeKey,
                         [[PFUser currentUser] objectId], kPAPPushPayloadFromUserObjectIdKey,
                         [photo objectId], kPAPPushPayloadPhotoObjectIdKey,
                         @"sms-received.wav",kAPNSSoundKey,
                         nil];
                        
                        // Send the push
                        PFPush *push = [[PFPush alloc] init];
                        [push setChannels:[channelSet allObjects]];
                        [push setData:payload];
                        [push sendPushInBackground];}
                }
                
            }
            
        }];
    } else {
        
        [PAPUtility unlikePhotoInBackground:photo block:^(BOOL succeeded, NSError *error) {
            PAPPhotoHeaderView *actualHeaderView = (PAPPhotoHeaderView *)[self tableView:self.tableView viewForHeaderInSection:button.tag];
            
            if (!succeeded) {
                // [actualHeaderView.likeButton setTitle:originalButtonTitle forState:UIControlStateNormal];
                [actualHeaderView.likeLabel setText:originalButtonTitle];
                [photoHeaderView.likeButton setEnabled:YES];
            }
            else {
                
                [[PAPCache sharedCache] setPhotoIsLikedByCurrentUser:photo liked:NO];
                
                int previousCount = [[photo objectForKey:kPAPFeedLikeCountKey] integerValue];
                if (previousCount == 0) {
                    [photoHeaderView.likeLabel setText:[NSString stringWithFormat:@"%d",0]];
                }
                else {
                    
                    
                    
                    [photo setObject:[NSNumber numberWithInt:(previousCount-1)] forKey:kPAPFeedLikeCountKey];
                    [photo saveEventually:^(BOOL successed , NSError *error){
                        [photoHeaderView.likeButton setEnabled:YES];
                    }];
                    //[actualHeaderView shouldEnableLikeButton:YES];
                    [photoHeaderView.likeLabel setText:[NSString stringWithFormat:@"%d",(previousCount-1)]];
                    [photoHeaderView setLikeStatus:!succeeded];
                }
            }
        }];
    }
    
}

/**
 *  tap on location name  to see  its location
 *
 *  @param photoHeaderView cell header view
 *  @param locationbutton  location name
 *  @param photo           complete description about user detail who posted that post
 */
-(void)photoHeaderView:(PAPPhotoHeaderView *)photoHeaderView didTapOnLocationButton:(UIButton *)locationbutton photo:(PFObject *)photo{
    
    PAPLocationViewController *locationController = [[PAPLocationViewController alloc] init];
    locationController.object = photo;
    [self.navigationController pushViewController:locationController animated:YES];
    
}


/**
 *  push notification sound
 *
 *  @return sound
 */
#pragma mark - PhotoCellViewDelegate
-(void)playNotificationSound{
    
    //play sound
    SystemSoundID	pewPewSound;
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"record" ofType:@"wav"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL, & pewPewSound);
    AudioServicesPlaySystemSound(pewPewSound);
}
#pragma mark - PAPPhotoCellViewDelegate

/**
 *  tap on like button placed which is at bottom of post
 *
 *  @param photoHeaderView photoHeaderView description
 *  @param button          like button
 *  @param aPhoto          complete details of post
 */
-(void)photoCell:(PAPPhotoCell *)photoHeaderView didTapLikePhotoButton:(UIButton *)button photo:(PFObject *)aPhoto{
    
    //[self playNotificationSound];
    
    [Flurry logEvent:@"LikeButtonClicked"];
    
    [photoHeaderView.likeButton setEnabled:NO];
    
    PFObject *photo = aPhoto;
    if ([[aPhoto parseClassName] isEqualToString:kPAPFeedClassKey])
    {
        photo = aPhoto;
    }
    else if ([[aPhoto parseClassName] isEqualToString:kPAPActivityClassKey])
    {
        photo = [aPhoto objectForKey:kPAPActivityPhotoKey];
        photo = [photo fetchIfNeeded];
    }
    else if ([[aPhoto parseClassName] isEqualToString:kPAPPrivateFeedClassKey])
    {
              photo = aPhoto;
    }
    
    BOOL liked = [[PAPCache sharedCache] isPhotoLikedByCurrentUser:photo]; //!button.selected;
    [photoHeaderView setLikeStatus:liked];
    
    
    [[PAPCache sharedCache] setPhotoIsLikedByCurrentUser:photo liked:liked];
    
    NSString *originalButtonTitle = photoHeaderView.likeLabel.text;
    
    
    if (!liked) {
        [PAPUtility likePhotoInBackground:photo block:^(BOOL succeeded, NSError *error) {
            
            
            if (!succeeded) {
                [Flurry logEvent:@"LikedFailed"];
                [photoHeaderView.likeButton setEnabled:YES];
                //[actualHeaderView.likeButton setTitle:originalButtonTitle forState:UIControlStateNormal];
                [photoHeaderView.likeLabel setText:originalButtonTitle];
                
            }
            else
            {
                
                
                
                int previousCount = [[photo objectForKey:kPAPFeedLikeCountKey] integerValue];
                [photo incrementKey:kPAPFeedLikeCountKey];
                [photo saveInBackground];
                [photoHeaderView.likeButton setEnabled:YES];
                ;
                
                
                
                [photoHeaderView.likeLabel setText:[NSString stringWithFormat:@"%d",(previousCount+1)]];
                [photoHeaderView setLikeStatus:succeeded];
                [[PAPCache sharedCache] setPhotoIsLikedByCurrentUser:photo liked:succeeded];
                
                [Flurry logEvent:@"LikedSucceesfully"];
                // Explicitly add the photographer in case he didn't comment
                
                NSString *currentOid = [PFUser currentUser].objectId;
                NSLog(@"******userinfo %@",currentOid);
                NSString *anotherOid = [[photo objectForKey:kPAPFeedUserKey]objectId];
                NSLog(@"******userinfo %@",anotherOid);
                
                // if ([[photo objectForKey:kPAPFeedUserKey] objectForKey:kPAPUserPrivateChannelKey])
                if ([photoHeaderView.user  objectForKey:kPAPUserPrivateChannelKey]){
                    
                    //                    NSString *currentOid = [PFUser currentUser].objectId;
                    //                    NSLog(@"******userinfo %@",currentOid);
                    //                    NSString *anotherOid = [[photo objectForKey:kPAPFeedUserKey]objectId];
                    //                    NSLog(@"******userinfo %@",anotherOid);
                    
                    if (![currentOid isEqualToString:anotherOid]) {
                        
                        
                        
                        NSMutableSet *channelSet = [NSMutableSet setWithCapacity:1];
                        
                        /******A ********/
                        
                        //                    NSString *chkId = [PFUser currentUser].objectId;
                        //                    NSLog(@"******userinfo %@",chkId);
                        //                    NSString * str = [[photo objectForKey:kPAPFeedUserKey]objectId];
                        //                     NSLog(@"******userinfo %@",str);
                        //
                        //
                        //                    }
                        
                        /******A ********/
                        
                        [channelSet addObject:[photoHeaderView.user objectForKey:kPAPUserPrivateChannelKey]];
                        
                        
                        // Create push payload
                        if (channelSet.count > 0) {
                            
                            // Create notification message
                            /*NSString *userFirstName = [PAPUtility firstNameForDisplayName:[[PFUser currentUser] objectForKey:kPAPUserDisplayNameKey]];*/ //elimate text after space
                            
                          // NSString *currentUserName = [[PFUser currentUser] objectForKey:kPAPUserDisplayNameKey];
                             NSString *currentUserName = [[PFUser currentUser] username];
                            // Create notification message
                            
                            
                            /*if ([currentUserName isEqualToString:userFirstName]) {
                             
                             }
                             else
                             {*/
                            
                            NSString *message;
                            if ([[photo objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeVideoKey ]){
                                message = [NSString stringWithFormat:@"%@ liked your video", currentUserName];
                            }
                            else if ([[photo objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeImageKey]){
                                message = [NSString stringWithFormat:@"%@ liked your photo", currentUserName];
                            }
                            
                            
                            NSDictionary *payload =
                            [NSDictionary dictionaryWithObjectsAndKeys:
                             message, kAPNSAlertKey,
                             @"Increment",kAPNSBadgeKey,
                             kPAPPushPayloadPayloadTypeActivityKey, kPAPPushPayloadPayloadTypeKey,
                             kPAPPushPayloadActivityLikeKey, kPAPPushPayloadActivityTypeKey,
                             [[PFUser currentUser] objectId], kPAPPushPayloadFromUserObjectIdKey,
                             [photo objectId], kPAPPushPayloadPhotoObjectIdKey,
                             @"sms-received.wav",kAPNSSoundKey,
                             nil];
                            
                            // Send the push
                            PFPush *push = [[PFPush alloc] init];
                            [push setChannels:[channelSet allObjects]];
                            [push setData:payload];
                            [push sendPushInBackground];
                        }
                    }}
            }
            
        }];
    } else {
        
        [PAPUtility unlikePhotoInBackground:photo block:^(BOOL succeeded, NSError *error) {
            PAPPhotoHeaderView *actualHeaderView = (PAPPhotoHeaderView *)[self tableView:self.tableView viewForHeaderInSection:button.tag];
            
            if (!succeeded) {
                // [actualHeaderView.likeButton setTitle:originalButtonTitle forState:UIControlStateNormal];
                [actualHeaderView.likeLabel setText:originalButtonTitle];
                [photoHeaderView.likeButton setEnabled:YES];
            }
            else {
                
                [[PAPCache sharedCache] setPhotoIsLikedByCurrentUser:photo liked:NO];
                
                int previousCount = [[photo objectForKey:kPAPFeedLikeCountKey] integerValue];
                if (previousCount == 0) {
                    [photoHeaderView.likeLabel setText:[NSString stringWithFormat:@"%d",0]];
                }
                else {
                    
                    
                    
                    [photo setObject:[NSNumber numberWithInt:(previousCount-1)] forKey:kPAPFeedLikeCountKey];
                    [photo saveEventually:^(BOOL successed , NSError *error){
                        [photoHeaderView.likeButton setEnabled:YES];
                    }];
                    //[actualHeaderView shouldEnableLikeButton:YES];
                    [photoHeaderView.likeLabel setText:[NSString stringWithFormat:@"%d",(previousCount-1)]];
                    [photoHeaderView setLikeStatus:!succeeded];
                }
            }
        }];
    }
}


/**
 *  <#Description#>
 *
 *  @param photoCell <#photoCell description#>
 *  @param feed      video
 */
-(void)photoCell:(PAPPhotoCell *)photoCell didTapOnPlayButton:(PFObject *)feed {
    
    ////    if (player) {
    ////        [self stopVideoPlayer];
    ////    }
    //
    //    //_pCell = photoCell;
    //
    //    NSURL *mediaURL = [NSURL URLWithString:[feed objectForKey:kPAPFeedMediaLinkKey]];
    //
    //
    //    if ([[feed objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeImageKey]) {
    //
    //        SDWebImageManager *manager = [SDWebImageManager sharedManager];
    //
    //        DACircularProgressView *progressView = [[DACircularProgressView alloc] initWithFrame:photoCell.btnPlay.frame];
    //        progressView.roundedCorners = YES;
    //        progressView.trackTintColor = [UIColor clearColor];
    //        [photoCell.contentView addSubview:progressView];
    //
    //
    //        [manager downloadWithURL:mediaURL
    //                         options:0
    //                        progress:^(NSUInteger receivedSize , long long expectesSize){
    //
    //                            CGFloat progess = ((float)receivedSize/expectesSize);
    //                            [progressView setProgress:progess animated:YES];
    //                        }
    //                       completed:^(UIImage *image , NSError *error , SDImageCacheType cacheType ,BOOL finished){
    //
    //                           [progressView setProgress:1 animated:YES];
    //
    //                           [progressView removeFromSuperview];
    //                           photoCell.imageView.image = image;
    //
    //                       }];
    //    }
    //    else {
    //
    ////        UIActivityIndicatorView *av = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    ////        av.center = photoCell.imageView.center;
    ////        av.tag = 121;
    ////        [photoCell.contentView addSubview:av];
    ////        [av startAnimating];
    //
    //        [_pCell.vPlayer play];
    //
    //
    //    }
    
    if ([_pCell isEqual:photoCell]) {
        _pCell.btnPlay.hidden = YES;
        [self hideCellActivityIndicator];
        if (_vplayer.currentItem) {
            [_vplayer.currentItem seekToTime:kCMTimeZero];
            [_vplayer play];
        }
        else {
            _pCell = nil;
            [self checkVisibilityOfCell:photoCell inScrollView:self.tableView];
        }
    }
    else {
        [self checkVisibilityOfCell:photoCell inScrollView:self.tableView];
        
    }
    
    //    if (_vplayer) {
    //
    //
    //
    //        //[_pCell.vPlayer pause];
    //       // [self startVideoPlayer];
    //    }
    //    else
    //    {
    //       [self startVideoPlayer];
    //    }
    
    
    
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
//    if ([keyPath isEqualToString:@"rate"]) {
//
//        NSLog(@"rate changed : %@", [NSValue valueWithCGRect:_vidLayer.frame]);
//
//        if ([_vplayer rate]) {
//
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//                UIActivityIndicatorView *av = (UIActivityIndicatorView*)[_pCell.contentView viewWithTag:121];
//                [av stopAnimating];
//                [av removeFromSuperview];
//            });
//
//            NSLog(@"inserting layer : %@", [self.tableView indexPathForCell:_pCell]);
//            if (![[[_pCell.imageView.layer sublayers] objectAtIndex:0] isEqual:_vidLayer]) {
//
//                [_pCell.imageView.layer insertSublayer:_vidLayer atIndex:0];
//                _vidLayer.zPosition = 1;
//            }
//
//        }
//        else {
//            [_vidLayer removeFromSuperlayer];
//        }
//    }
//}

- (void) itemDidStartPlaying:(NSNotification*)notif
{
    NSLog(@"itemDidStartPlaying : %@, %@", notif, [NSValue valueWithCGRect:_vidLayer.frame]);
    
    // [self hideCellActivityIndicator];
    
    
    //[_pCell.btnPlay setHidden:YES];
    
    
    //    if (![[[_pCell.imageView.layer sublayers] objectAtIndex:0] isEqual:_vidLayer]) {
    //
    //        _vidLayer.shouldRasterize = YES;
    //        [_vidLayer removeFromSuperlayer];
    //
    //        _vidLayer.contents = (__bridge id)(_pCell.imageView.image.CGImage);
    //
    //
    //            [_pCell.imageView.layer insertSublayer:_vidLayer atIndex:0];
    //
    //
    //
    //
    //    }
    //    [_pCell.imageView setNeedsDisplay];
    //    [_pCell.imageView.layer display];
    ////    [_vidLayer setNeedsDisplay];
    //    [_vidLayer displayIfNeeded];
    //    if ( [_vplayer.currentItem isPlaybackLikelyToKeepUp]){
    //
    //        NSLog(@"show indicator");
    //        [_vplayer play];
    //    }
    //    else{
    //        NSLog(@"hide indicator");
    //    }
    
}


- (void) itemPlayingStalled:(NSNotification*)notif
{
    NSLog(@"itemDidStallPlaying");
    
    //
    //    if ( [_vplayer.currentItem isPlaybackLikelyToKeepUp]){
    //
    //        NSLog(@"show indicator");
    //        [_vplayer play];
    //        [_videoLoadProgressView removeFromSuperview];
    //    }
    //    else{
    //        NSLog(@"hide indicator");
    //        if (!_videoLoadProgressView) {
    //            _videoLoadProgressView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    //
    //        }
    //        else {
    //
    //            [_videoLoadProgressView removeFromSuperview];
    //        }
    //        _videoLoadProgressView.center = _pCell.center;
    //        [_pCell addSubview:_videoLoadProgressView];
    //        [_videoLoadProgressView startAnimating];
    //    }
    
}

-(void)itemDidFinishPlaying:(NSNotification*)notif {
    NSLog(@"finish playing");
    
    //[_vplayer seekToTime:kCMTimeZero];
    //[_vplayer play];
    
    [_vplayer pause];
    [self removePlayerObservers];
    //[[_vplayer currentItem] seekToTime:kCMTimeZero];
    
    [_pCell.btnPlay setHidden:NO];
    
    
    //    [_pCell.contentView bringSubviewToFront:_pCell.btnPlay];
}


/**
 *  move to comment controller to send new comment
 *
 *  @param photoHeaderView photoHeaderView description
 *  @param button          comment button
 *  @param photo           detailed information about post and its user
 */
- (void)photoHeaderView:(PAPPhotoHeaderView *)photoHeaderView didTapCommentOnPhotoButton:(UIButton *)button photo:(PFObject *)photo
{
    self.shouldReloadOnAppear = YES;
    CommentsViewController *commentView = [[CommentsViewController alloc] initWithNibName:@"CommentsViewController" bundle:nil];
    commentView.photo = photo;
    commentView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:commentView animated:YES];
    
}


/**
 *  more action give option to delete the post if that post belongs to current user 
 *  else give option to report about post
 *
 *  @param photoHeaderView  photoHeaderView description
 *  @param button           more button
 *  @param photo            detailed information about post and its user
 */

- (void)photoHeaderView:(PAPPhotoHeaderView *)photoHeaderView didTapMoreButton:(UIButton *)button photo:(PFObject *)photo
{
    if ([[photo parseClassName] isEqualToString:kPAPFeedClassKey])
    {
        self.selectObject = photo;
    }
    else if ([[photo parseClassName] isEqualToString:kPAPActivityClassKey])
    {
        self.selectObject = [photo objectForKey:kPAPActivityPhotoKey];
        self.selectObject = [self.selectObject fetchIfNeeded];
    }
    
    UIActionSheet *actionSheet = Nil;
    
    
    if ([[(PFUser*)[photo objectForKey:kPAPFeedUserKey] objectId] isEqualToString:[PFUser currentUser].objectId]) {
        NSLog(@"curent user ");
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Delete", nil];
        actionSheet.tag = 100;
    }
    else {
        
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Report", nil];
        actionSheet.tag =400;
        
    }
    
    [actionSheet showFromTabBar:self.tabBarController.tabBar];

    //[actionSheet showInView:self.view];
    
}

/**
 *  move to comment controller to send new comment
 *
 *  @param cell   cell description
 *  @param button comment button
 *  @param photo  detailed information about post and its user
 */
- (void)cell:(PAPPhotoCell *)cell didTapComment:(UIButton *)button user:(PFObject *)photo{
    
    self.shouldReloadOnAppear = YES;
    CommentsViewController *commentView = [[CommentsViewController alloc] initWithNibName:@"CommentsViewController" bundle:nil];
    commentView.photo = photo;
    commentView.user = cell.user;
    commentView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:commentView animated:YES];
    
}


/**
 *  tap on share button gives different ways to share particulare post
 *
 *  @param cell   cell description
 *  @param button share button
 *  @param photo  detailed information about post and its user
 */
- (void)cell:(PAPPhotoCell *)cell didTapShareButton:(UIButton *)button user:(PFObject *)photo{
    
    if ([[photo parseClassName] isEqualToString:kPAPFeedClassKey])
    {
        self.selectObject = photo;
    }
    else if ([[photo parseClassName] isEqualToString:kPAPActivityClassKey])
    {
        self.selectObject = [photo objectForKey:kPAPActivityPhotoKey];
        self.selectObject = [self.selectObject fetchIfNeeded];
    }
    
    
    UIActionSheet *actionSheet;
    if ([[self.selectObject objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeVideoKey]) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Facebook", @"Twitter",@"Email", nil];
        actionSheet.tag = 500;
    }
    else{
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Facebook", @"Twitter", @"Instagram",@"Email", nil];
        actionSheet.tag = 300;
    }
    
   [actionSheet showFromTabBar:self.tabBarController.tabBar];
    //[actionSheet showInView:self.view];
}


/**
 *  more action give option to delete the post if that post belongs to current user
 *  else give option to report about post
 *
 *  @param cell   button located at bottom of the post
 *  @param button more button
 *  @param photo  detailed information about post and its user
 */
- (void)cell:(PAPPhotoCell *)cell didTapMore:(UIButton *)button user:(PFObject *)photo{
    
    
    if ([[photo parseClassName] isEqualToString:kPAPFeedClassKey])
    {
        self.selectObject = photo;
        privateFeed = photo;
    }
    else if ([[photo parseClassName] isEqualToString:kPAPActivityClassKey])
    {
        self.selectObject = [photo objectForKey:kPAPActivityPhotoKey];
        privateFeed = [photo objectForKey:kPAPActivityPhotoKey];
        self.selectObject = [self.selectObject fetchIfNeeded];
    }
     else if ([[photo parseClassName] isEqualToString:kPAPPrivateFeedClassKey])
     {
         self.selectedObject = photo;
         privateFeed = photo;
     }
    
    UIActionSheet *actionSheet = Nil;
    
    
    if ([[(PFUser*)[photo objectForKey:kPAPFeedUserKey] objectId] isEqualToString:[PFUser currentUser].objectId]) {
        NSLog(@"curent user ");
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Delete", nil];
        actionSheet.tag = 100;
    }
    else {
        
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Report", nil];
        actionSheet.tag =400;
        
    }
    
    //[actionSheet showInView:self.view];
     [actionSheet showInView:self.tabBarController.tabBar];
}


/**
 *   Hide the activity indicator playing on cell
 */
-(void)hideCellActivityIndicator{
    [_pCell.activityIndicator setHidden:YES];
    [_pCell.activityIndicator stopAnimating];
}

/**
 *  start activity indicator
 */
-(void)showCellActivityIncicator{
    [_pCell.contentView bringSubviewToFront:_pCell.activityIndicator];
    [_pCell.activityIndicator setHidden:NO];
    [_pCell.activityIndicator startAnimating];
}


#pragma mark - PAPBaseTextCellDelegate

/**
 *  tapping on username
 *
 *  @param cellView <#cellView description#>
 *  @param aUser    user details
 */
- (void)cell:(PAPBaseTextCell *)cellView didTapUserButton:(NSString *)aUser {
    
    if ([self isKindOfClass:[PAPAccountViewController class]]) {
        return;
    }
    
    [self findUserWithuserName:aUser];
    
}

/**
 *  tapping on tagUser
 *
 *  @param cellView <#cellView description#>
 *  @param user     taggerUser details
 */
- (void)cell:(PAPBaseTextCell *)cellView didTapTaggedUser:(NSString *)user{
    
    [self findUserWithuserName:user];
}
-(void)cell:(PAPBaseTextCell *)cellView didTapHashTag:(NSString *)hashTag{
    [self loadViewWithHashTag:hashTag];
}

-(void)findUserWithuserName:(NSString*)username {
    // NSLog(@"user %@",user);
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" containsString:username];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        if (!error) {
            // NSLog(@"array %@",array);
            if (array.count > 0) {
                PFUser *user = [array lastObject];
                [self navigateToUserAccountDetial:user];
            }
            
        }
    }];
}

#pragma AudioDelegate

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    [_pCell.contentView bringSubviewToFront:_pCell.btnPlay];
}
#pragma mark - UIActionSheetDelegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    // NSLog(@"buttonindex %d",buttonIndex);
    
    if (actionSheet.tag == 100) {  // current user- delete aciton
        if (actionSheet.cancelButtonIndex != buttonIndex)  {
            if ([[privateFeed parseClassName] isEqualToString:kPAPPrivateFeedClassKey])
            {
                
                PFQuery *query = [PFQuery queryWithClassName:kPAPPrivateFeedClassKey];
                [query whereKey:kPAPFeedMediaLinkKey equalTo:[privateFeed objectForKey:kPAPFeedMediaLinkKey]];
                [query findObjectsInBackgroundWithBlock:^(NSArray *activities, NSError *error) {
                    if (!error) {
                        for (PFObject *activity in activities) {
                            [activity delete];
                        }
                    }
                    
                    // Delete photo
                    [self.selectObject deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        [self loadObjects];
                    }];
                }];
                
                
            }
            else
            {
                
                PFQuery *query = [PFQuery queryWithClassName:kPAPActivityClassKey];
                [query whereKey:kPAPActivityPhotoKey equalTo:self.selectObject];
                [query findObjectsInBackgroundWithBlock:^(NSArray *activities, NSError *error) {
                    if (!error) {
                        for (PFObject *activity in activities) {
                            [activity delete];
                        }
                    }
                    
                    // Delete photo
                    [self.selectObject deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        [self loadObjects];
                    }];
                }];
                
                
            }
        }
    }
    else if(actionSheet.tag == 400) {  // other user action
        
        NSLog(@"button index %d",buttonIndex);
        
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            
            if (buttonIndex == 0) {  // delete
                
                [self reportPhoto];
                
            }
            
        }
        
        
        
    }
    
    else if (actionSheet.tag ==300) {
        
        if (buttonIndex ==0) {
            shareOnFB = YES;
            shareOnTwitter = NO;
            [self shareOnFB];
            
            
            //[self postOnFacebook:self.selectObject];
        }
        else if (buttonIndex==1){
            shareOnTwitter =YES;
            shareOnFB = NO;
            //[self postOnTwitter:self.selectObject];
            [self shareOnTwitter];
            
        }
        else if (buttonIndex==2){
            
            [self shareOnInstagram];
        }
        else if (buttonIndex==3){
            
            [self sendEmail];
        }
    }
    else if (actionSheet.tag == 500){
        
        if (buttonIndex ==0) {
            shareOnFB = YES;
            shareOnTwitter = NO;
            //            [self postOnFacebook:self.selectObject];
            [self shareOnFB];
        }
        else if (buttonIndex==1){
            shareOnTwitter =YES;
            shareOnFB = NO;
            
            // [self postOnTwitter:self.selectObject];
            [self shareOnTwitter];
        }
        else if (buttonIndex==2){
            
            [self sendEmail];
        }
        
    }
    
}


#pragma mark - Email
/**
 *  send email with video/image
 */
-(void)sendEmail
{
    
    MFMailComposeViewController *comp=[[MFMailComposeViewController alloc]init];
    [comp setMailComposeDelegate:self];
    if([MFMailComposeViewController canSendMail]) {
        [comp setToRecipients:[NSArray arrayWithObjects:@" ", nil]];
        [comp setSubject:@"A message from Captionier"];
        NSData *mediaData = nil;
        NSString *mediaFileLink = [self.selectObject objectForKey:kPAPFeedMediaLinkKey];
        //[comp setMessageBody:mediaFileLink isHTML:NO];
        [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        if ([[selectObject objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeImageKey]) {
            NSInteger index = [self.objects indexOfObject:self.selectObject];
            PAPPhotoCell *cell = (PAPPhotoCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]];
            mediaData = UIImageJPEGRepresentation(cell.imageView.image, 1);
            
            if (mediaData) {
                [comp addAttachmentData:mediaData mimeType:@"image/jpeg" fileName:@"image.jpeg"];
            }
            else {
                [comp setMessageBody:mediaFileLink isHTML:NO];
                
            }
            
        }
        else {
            
            [comp setMessageBody:mediaFileLink isHTML:NO];
            
        }
        
        
        
        [self presentViewController:comp animated:YES completion:nil];
    }
    else {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your device is not currently connected to an email account."delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alrt show];
    }
}


//
//
//        if ([MFMailComposeViewController canSendMail])
//        {
//
//            MFMailComposeViewController *  mailComposecontroller=[[MFMailComposeViewController alloc]init];
//            mailComposecontroller.mailComposeDelegate=self;
//
//
//            [mailComposecontroller setToRecipients:[NSArray arrayWithObjects:@" ", nil]];
//
//            [mailComposecontroller setSubject:@"A message from Status Life"];
//
//            NSString *mediaFileLink = [self.selectObject objectForKey:kPAPFeedMediaLinkKey];
//            NSData *mediaData = nil;
//            if ([[selectObject objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeImageKey]) {
//                NSInteger index = [self.objects indexOfObject:self.selectObject];
//                PAPPhotoCell *cell = (PAPPhotoCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]];
//                mediaData = UIImageJPEGRepresentation(cell.imageView.image, 1);
//
//                if (mediaData) {
//                    [mailComposecontroller addAttachmentData:mediaData mimeType:@"image/jpeg" fileName:@"image.jpeg"];
//                }
//                else {
//                    [mailComposecontroller setMessageBody:mediaFileLink isHTML:NO];
//
//                }
//
//
//
//            }
//            else {
//
//                [mailComposecontroller setMessageBody:mediaFileLink isHTML:NO];
//
//            }
//
//
//
//
//
//
//
//            [self presentViewController:mailComposecontroller animated:YES completion:^{
//            }];
//
//
//
//        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
//                                                            message:@"Your device is not currently connected to an email account."
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles: nil];
//            [alert show];
//        }
//




//}
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //  ////NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            //  ////NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            //NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            // ////NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    // UINavigationController *navigationController = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).navigationcontroller;
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}


/**
 *  sharing post on twitter
 */
- (void)shareOnTwitter
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:@"Posting.." On:self.view];
    [self postToSocialMedia:self.selectObject];
    
}

/**
 *  This method will post media link on configured twitter account
 *
 *  @param feed mediaLink
 */

-(void)postOnTwitter:(PFObject*)feed
{
    NSString *mediaLink = [self getWebLinkForFeed:feed];
    
    
    
    // Create an account store object.
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    
    // Create an account type that ensures Twitter accounts are retrieved.
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    // Request access from the user to use their Twitter accounts.
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        if(granted) {
            // Get the list of Twitter accounts.
            NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
            
            if ([accountsArray count] > 0) {
                // Grab the initial Twitter account to tweet from.
                ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                SLRequest *postRequest = nil;
                //ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                NSLog(@"Twitter Login User:%@",twitterAccount.username);
                NSLog(@"Twitter AccountType:%@",twitterAccount.accountType);
                
                // Post Text
                
                NSString *posttext = [NSString stringWithFormat:@"Shared via @Captionier %@",mediaLink];
                
                NSDictionary *message = @{@"status": posttext, @"wrap_links": @"true"};
                
                // URL
                NSURL *requestURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
                
                // Request
                postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:requestURL parameters:message];
                
                // Set Account
                postRequest.account = twitterAccount;
                
                // Post
                [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    NSLog(@"Twitter HTTP response: %li", (long)[urlResponse statusCode]);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (shareOnFB) {
                            [self postOnFacebook:feed];
                        }
                        else {
                            [self donePosting];
                        }
                    });
                    
                }];
                
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"There is no Twitter account configured" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
                [alert show];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (shareOnFB) {
                        [self postOnFacebook:feed];
                    }
                    else {
                        [self donePosting];
                    }
                });
            }
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (shareOnFB) {
                    [self postOnFacebook:feed];
                }
                else {
                    [self donePosting];
                }
            });
        }
    }];
    
}

/**
 *  Method to share media on instagram
 */

-(void)shareOnInstagram
{
    
    if ([[selectObject objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeImageKey]) {
        NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
        
        if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
            //imageToUpload is a file path with .ig file extension
            
            NSString *savePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/sharing.igo"];
            
            NSFileManager *fm = [NSFileManager defaultManager];
            if ([fm fileExistsAtPath:savePath])
            {
                [fm removeItemAtPath:savePath error:nil];
            }
            
            NSInteger index = [self.objects indexOfObject:self.selectObject];
            PAPPhotoCell *cell = (PAPPhotoCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]];
            UIImage *image = cell.imageView.image;
            
            NSData *imageD = UIImageJPEGRepresentation(image, 1);
            
            [imageD writeToFile:savePath atomically:YES];
            
            dic = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
            dic.UTI = @"com.instagram.exclusivegram";
            dic.delegate = nil;
            
            dic.annotation = [NSDictionary dictionaryWithObject:@"Shared via @Captionier" forKey:@"InstagramCaption"];
            [dic presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
            
        }
        else
        {
            [Helper showAlertWithTitle:@"Message" Message:@"You don't have Instagram installed. Download instagram app to get more functionality."];
            
        }
        
    }
    else {
        [Helper showAlertWithTitle:@"Message" Message:@"Videos can not be directly shared to Instagram"];
        return;
    }
    
    
    
}



/**************************** Short Link ************************************/

#pragma mark  Facebook Sharing
-(void)shareOnFB
{
    shareOnFB = YES;
    [self postToSocialMedia:self.selectObject];
    
    
}

/**
 *  This method is to convert media link into tinylink
 */

- (NSString*)getWebLinkForFeed:(PFObject*)feed
{
    if (shortUrl == nil || shortUrl.length == 0) {
        
       // NSString *link = [NSString stringWithFormat:@"http://108.166.190.172:81/vind/picogram/fbgallerys1.php?id=%@&uid=%@", feed.objectId, [PFUser currentUser].objectId];
        NSString *link = [NSString stringWithFormat:@"http://postmenu.cloudapp.net/picogram/fbgallerys1.php?id=%@&uid=%@", feed.objectId, [PFUser currentUser].objectId];
        
        // http://postmenu.cloudapp.net/picogram/fbgallerys1.php?id=%@&uid=%@
        
        NSString *apiEndpoint = [NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@",link];
        shortUrl = [NSString stringWithContentsOfURL:[NSURL URLWithString:apiEndpoint]
                                            encoding:NSASCIIStringEncoding
                                               error:nil];
    }
    
    
    return shortUrl;
}


/**
 *  This method will let the media link generate in main thread
 *
 *  @param feed media path
 */
- (void) postToSocialMedia:(PFObject*)feed
{
    if (shareOnTwitter) {
        
        //  [self postOnTwitter:feed];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self postOnTwitter:feed];
        });
        
    }
    else if (shareOnFB) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self postOnFacebook:feed];
        });
    }
}


//- (void) postOnFacebook:(PFObject*)feed
//{
//    feedObject = feed;
//
//    NSString *mediaLink = [self getWebLinkForFeed:feed];
//
//
//
//    //mediaLink = shortURL;
//
//    NSString *caption = @"Checkout this cool app";
//
//    NSString *picturelink = [feed objectForKey:kPAPFeedMediaLinkKey];
//    if (!kPAPFeedMediaTypeImageKey) {
//        picturelink = [feed objectForKey:kPAPFeedMediaThumbnailLikKey];
//    }
//
//
//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                   picturelink, @"picture",
//                                   @"Created from 24fit", @"caption",
//                                   caption, @"description",
//                                   mediaLink, @"link",
//                                   nil];
//
//    [self makeFBPostWithParams:params];
//}


/**
 *  Post the media with its description on facebook
 *
 *  @param params mediatype,caption,mediaLink
 */
- (void) makeFBPostWithParams:(NSDictionary*)params
{
    if ([[PFFacebookUtils session] isOpen]) {
        
        [FBRequestConnection startWithGraphPath:@"/me/feed/" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            
            
            // [[ProgressIndicator sharedInstance] hideProgressIndicator];
            
            NSString *message = @"Posted Successfully";
            if (result!= nil) {
                [Flurry logEvent:@"PhotoSharedOnFacebook"];
                
            }
            
            [self donePosting];
            
            
        }];
    }
    else {
        
        [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES completionHandler:^(FBSession *session,
                                                                                                                                                                       FBSessionState status,
                                                                                                                                                                       NSError *error){
            
            if (!error) {
                [FBRequestConnection startWithGraphPath:@"/me/feed/" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                    
                    
                    //  [[ProgressIndicator sharedInstance] hideProgressIndicator];
                    
                    NSString *message = @"Posted Successfully";
                    if (result!= nil) {
                        [Flurry logEvent:@"PhotoSharedOnFacebook"];
                        
                    }
                    
                    [self donePosting];
                    
                }];
            }
            else {
                [[[UIAlertView alloc] initWithTitle:@"Facebook" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                
            }
            
            
        }];
        
        
    }
}





/****************************************************/

#pragma mark  Facebook Sharing

//- (NSString*)getWebLinkForFeed:(PFObject*)feed
//{
//    NSString *link = [NSString stringWithFormat:@"http://108.166.190.172:81/vind/vind/fbgallerys1.php?id=%@&uid=%@", feed.objectId, [PFUser currentUser].objectId];
//
//    return link;
//}
//
//- (void) makeFBPostWithParams:(NSDictionary*)params
//{
//    if ([[PFFacebookUtils session] isOpen]) {
//
//        [FBRequestConnection startWithGraphPath:@"/me/feed/" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//
//
//            // [[ProgressIndicator sharedInstance] hideProgressIndicator];
//
//            NSString *message = @"Posted Successfully";
//            if (result!= nil) {
//                [Flurry logEvent:@"PhotoSharedOnFacebook"];
//
//            }
////            if (error) {
////                message = [error localizedDescription];
////                [[[UIAlertView alloc] initWithTitle:@"Facebook" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
////
////            }
//            [self donePosting];
//
//
//        }];
//    }
//    else {
//
//        [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES completionHandler:^(FBSession *session,
//                                                                                                                                                                       FBSessionState status,
//                                                                                                                                                                       NSError *error){
//
//            if (!error) {
//                [FBRequestConnection startWithGraphPath:@"/me/feed/" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//
//
//                    //  [[ProgressIndicator sharedInstance] hideProgressIndicator];
//
//                    NSString *message = @"Posted Successfully";
//                    if (result!= nil) {
//                        [Flurry logEvent:@"PhotoSharedOnFacebook"];
//
//                    }
////                    if (error) {
////                        message = [error localizedDescription];
////                        [[[UIAlertView alloc] initWithTitle:@"Facebook" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
////
////                    }
//                    [self donePosting];
//
//                }];
//            }
//            else {
//                [[[UIAlertView alloc] initWithTitle:@"Facebook" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
//
//            }
//
//
//        }];
//
//
//    }
//}



/**
 *  generating media link with its description
 *
 *  @param feed mediaLink
 */
- (void) postOnFacebook:(PFObject*)feed
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:@"Posting.." On:self.view];
    
    NSString *mediaLink = [self getWebLinkForFeed:feed];
    
    NSString *caption = @"Checkout this cool app";
    
    
    NSString *picturelink = [feed objectForKey:kPAPFeedMediaLinkKey];
    if ([[feed objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeVideoKey]) {
        picturelink = [feed objectForKey:kPAPFeedMediaThumbnailLikKey];
    }
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   picturelink, @"picture",
                                   @"Created from Captionier", @"caption",
                                   caption, @"description",
                                   mediaLink, @"link",
                                   @"Captionier",@"name",
                                   nil];
    
    [self makeFBPostWithParams:params];
}

/**
 *  hidding progress indicator once sharing  is done
 */
- (void) donePosting
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    //[self.navigationController popToRootViewControllerAnimated:YES];
}



/**
 *  Goto report controller to list report type
 */
-(void)reportPhoto {
    
    if ([[privateFeed parseClassName] isEqualToString:kPAPPrivateFeedClassKey])
    {
        self.shouldReloadOnAppear = YES;
        ReportInappropriateOptionsVC *reportVC = [[ReportInappropriateOptionsVC alloc] initWithNibName:@"ReportInappropriateOptionsVC" bundle:nil];
        reportVC.object = privateFeed;
        reportVC.mediaType = [[privateFeed objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeVideoKey] ;
        [self.navigationController pushViewController:reportVC animated:YES];
    }
    else{
    
    self.shouldReloadOnAppear = YES;
    ReportInappropriateOptionsVC *reportVC = [[ReportInappropriateOptionsVC alloc] initWithNibName:@"ReportInappropriateOptionsVC" bundle:nil];
    reportVC.object = self.selectObject;
    reportVC.mediaType = [[self.selectObject objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeVideoKey] ;
    [self.navigationController pushViewController:reportVC animated:YES];
    }
}

/*
 #pragma mark - PAPPhotoCellViewDelegate
 -(void)photoCell:(PAPPhotoCell *)photoHeaderView didTapLikePhotoButton:(UIButton *)button photo:(PFObject *)photo{
 
 BOOL liked = !button.selected;
 [photoHeaderView setLikeStatus:liked];
 
 NSString *originalTotalLikes = photoHeaderView.totalLikes.text;
 
 NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
 [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
 
 NSNumber *likeCount = [numberFormatter numberFromString:button.titleLabel.text];
 if (liked) {
 likeCount = [NSNumber numberWithInt:[likeCount intValue] + 1];
 [[PAPCache sharedCache] incrementLikerCountForPhoto:photo];
 } else {
 if ([likeCount intValue] > 0) {
 likeCount = [NSNumber numberWithInt:[likeCount intValue] - 1];
 }
 [[PAPCache sharedCache] decrementLikerCountForPhoto:photo];
 }
 
 [[PAPCache sharedCache] setPhotoIsLikedByCurrentUser:photo liked:liked];
 
 //[button setTitle:@"Liked" forState:UIControlStateNormal];
 
 if (liked) {
 [PAPUtility likePhotoInBackground:photo block:^(BOOL succeeded, NSError *error) {
 //PAPPhotoCell *cell = (PAPPhotoCell*)[sel<#(NSInteger)#>f tableView:<#(UITableView *)#> estimatedHeightForFooterInSection:]
 //PAPPhotoHeaderView *actualHeaderView = (PAPPhotoHeaderView *)[self tableView:self.tableView viewForHeaderInSection:button.tag];
 //[actualHeaderView shouldEnableLikeButton:YES];
 //[actualHeaderView setLikeStatus:succeeded];
 
 int previousCount = [[photo objectForKey:kPAPFeedLikeCountKey] integerValue];
 [photo setObject:[NSNumber numberWithInt:(previousCount+1)] forKey:kPAPFeedLikeCountKey];
 [photo saveInBackground];
 
 
 [photoHeaderView.totalLikes setText:[NSString stringWithFormat:@"%d Likes",(previousCount+1)]];
 [photoHeaderView setLikeStatus:succeeded];
 
 
 if (!succeeded) {
 photoHeaderView.totalLikes.text = originalTotalLikes;
 }
 }];
 } else {
 [PAPUtility unlikePhotoInBackground:photo block:^(BOOL succeeded, NSError *error) {
 //            PAPPhotoHeaderView *actualHeaderView = (PAPPhotoHeaderView *)[self tableView:self.tableView viewForHeaderInSection:button.tag];
 //            [actualHeaderView shouldEnableLikeButton:YES];
 int previousCount = [[photo objectForKey:kPAPFeedLikeCountKey] integerValue];
 [photo setObject:[NSNumber numberWithInt:(previousCount-1)] forKey:kPAPFeedLikeCountKey];
 [photo saveInBackground];
 
 [photoHeaderView.totalLikes setText:[NSString stringWithFormat:@"%d Likes",(previousCount-1)]];
 [photoHeaderView setLikeStatus:!succeeded];
 
 if (!succeeded) {
 photoHeaderView.totalLikes.text = originalTotalLikes;
 }
 }];
 }
 }
 -(void)photoCell:(PAPPhotoCell *)photoHeaderView didTapCommentOnPhotoButton:(UIButton *)button photo:(PFObject *)photo{
 
 
 CommentsViewController *commentView = [[CommentsViewController alloc] initWithNibName:@"CommentsViewController" bundle:nil];
 commentView.photo = photo;
 [self.navigationController pushViewController:commentView animated:YES];
 
 
 //    PAPPhotoDetailsViewController *photoDetailsVC = [[PAPPhotoDetailsViewController alloc] initWithPhoto:photo];
 //    [self.navigationController pushViewController:photoDetailsVC animated:YES];
 
 }
 -(void)photoCell:(PAPPhotoCell *)photoCell didSelectStar:(ASStarRatingView *)statView Rating:(float)rating photo:(PFObject *)photo{
 int ratin = (int)rating;
 [PAPUtility ratePhotoInBackground:photo rating:ratin  block:^(BOOL succeeded, NSError *error) {
 
 
 int previousCount = [[photo objectForKey:kPAPFeedTotalRating] integerValue];
 int previousUsersCount = [[photo objectForKey:kPAPFeedTotalUsersRated] integerValue];
 [photo setObject:[NSNumber numberWithInt:(previousCount+ (int)rating)] forKey:kPAPFeedTotalRating];
 [photo setObject:[NSNumber numberWithInt:(previousUsersCount +1)] forKey:kPAPFeedTotalUsersRated];
 
 [photo saveInBackground];
 statView.canEdit = NO;
 
 PFQuery *querylike = [PFQuery queryWithClassName:kPAPActivityClassKey];
 [querylike whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
 [querylike whereKey:kPAPActivityTypeKey containsString:@"like"] ;
 
 //[query whereKey:kPAPActivityTypeKey containsString:kPAPActivityTypeRating];
 [querylike whereKey:kPAPActivityPhotoKey equalTo:photo];
 
 
 PFQuery *queryRating = [PFQuery queryWithClassName:kPAPActivityClassKey];
 [queryRating whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
 
 [queryRating whereKey:kPAPActivityTypeKey containsString:kPAPActivityTypeRating] ;
 //[query whereKey:kPAPActivityTypeKey containsString:kPAPActivityTypeRating];
 [queryRating whereKey:kPAPActivityPhotoKey equalTo:photo];
 
 PFQuery *mainQuery = [PFQuery orQueryWithSubqueries:@[querylike,queryRating]];
 
 [mainQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
 
 //NSLog(@"arrcount %d",objects.count);
 
 BOOL isLikedByCurrentUser = NO;
 BOOL isRatedByCurrentUser = NO;
 float  rating = 0;
 
 for(PFObject *obj in objects){
 
 PFUser *user = [obj  objectForKey:kPAPActivityFromUserKey];
 if ([user.objectId isEqualToString:[[PFUser currentUser] objectId]]) {
 if ([[obj objectForKey:kPAPActivityTypeKey] isEqualToString:@"like"]) {
 [photoCell setLikeStatus:YES];
 isLikedByCurrentUser = YES;
 //[[PAPCache sharedCache] setAttributesForPhoto:object likedByCurrentUser:YES];
 photoCell.starView.canEdit = NO;
 }
 else if ([[obj objectForKey:kPAPActivityTypeKey] isEqualToString:kPAPActivityTypeRating]) {
 isRatedByCurrentUser = YES;
 rating = [[obj objectForKey:kPAPActivityRatingCountKey] floatValue];
 photoCell.starView.rating = rating;
 photoCell.starView.canEdit = NO;
 }
 
 
 }
 }
 
 [[PAPCache sharedCache] setAttributesForPhoto:photo likedByCurrentUser:isLikedByCurrentUser ratedByCurrentUser:isRatedByCurrentUser Rating:rating];
 
 
 }];
 
 
 }];
 }
 */
#pragma mark - ()

- (NSIndexPath *)indexPathForObject:(PFObject *)targetObject {
    for (int i = 0; i < self.objects.count; i++) {
        PFObject *object = [self.objects objectAtIndex:i];
        if ([[object objectId] isEqualToString:[targetObject objectId]]) {
            return [NSIndexPath indexPathForRow:0 inSection:i];
        }
    }
    
    return nil;
}

- (void)userDidLikeOrUnlikePhoto:(NSNotification *)note {
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

- (void)userDidCommentOnPhoto:(NSNotification *)note {
    //    [self.tableView beginUpdates];
    //    [self.tableView endUpdates];
    [self loadObjects];
}

- (void)userDidDeletePhoto:(NSNotification *)note {
    // refresh timeline after a delay
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC);
    dispatch_after(time, dispatch_get_main_queue(), ^(void){
        [self loadObjects];
    });
}

- (void)userDidPublishPhoto:(NSNotification *)note {
    
    if (self.objects.count > 0) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
    
    [self loadObjects];
}

- (void)userFollowingChanged:(NSNotification *)note {
    NSLog(@"User following changed.");
    self.shouldReloadOnAppear = YES;
}


- (void)didTapOnPhotoAction:(UIButton *)sender {
    
    PFObject *photo = [self.objects objectAtIndex:sender.tag];
    if ([[photo parseClassName] isEqualToString:kPAPActivityClassKey]) {
        photo = [photo objectForKey:kPAPActivityPhotoKey];
        photo = [photo fetchIfNeeded];
    }
    if (photo) {
        //        PAPPhotoDetailsViewController *photoDetailsVC = [[PAPPhotoDetailsViewController alloc] initWithPhoto:photo];
        //        [self.navigationController pushViewController:photoDetailsVC animated:YES];
        
        self.shouldReloadOnAppear = YES;
        CommentsViewController *commentView = [[CommentsViewController alloc] initWithNibName:@"CommentsViewController" bundle:nil];
        commentView.photo = photo;
        [self.navigationController pushViewController:commentView animated:YES];
        
    }
}

/**
 *  Reloading data
 *
 *  @param refreshControl refreshControl description
 */
- (void)refreshControlValueChanged:(UIRefreshControl *)refreshControl {
    [self loadObjects];
}

- (void)setVideoImage:(PAPPhotoCell*)cell
{
    AVAsset *asset = [[_vplayer currentItem] asset];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    [imageGenerator setAppliesPreferredTrackTransform:YES];
    CMTime time = [[_vplayer currentItem] currentTime];
    CGImageRef imgRef = [imageGenerator copyCGImageAtTime:time
                                               actualTime:NULL
                                                    error:NULL];
    imgRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    
    NSLog(@"videoimage ip - %@", [self.tableView indexPathForCell:cell]);
    [cell.imageView setImage:[UIImage imageWithCGImage:imgRef]];
    
    
}

- (void)checkVisibilityOfCell:(UITableViewCell *)cell inScrollView:(UIScrollView *)aScrollView {
    CGRect cellRect = [aScrollView convertRect:cell.frame toView:aScrollView.superview];
    if ([cell isKindOfClass:[PAPLoadMoreCell class]]) {
        return;
    }
    
    BOOL completelyVisible = CGRectContainsRect(aScrollView.frame, cellRect);
    
    if (!completelyVisible) {
        completelyVisible = CGRectContainsPoint(aScrollView.frame, CGPointMake(CGRectGetMidX(cellRect), CGRectGetMidY(cellRect)));
        if (completelyVisible) {
            completelyVisible = CGRectContainsPoint(aScrollView.frame, CGPointMake(CGRectGetMinX(cellRect), CGRectGetMaxY(cellRect)));
            if (!completelyVisible) {
                completelyVisible = CGRectContainsPoint(aScrollView.frame, CGPointMake(CGRectGetMinX(cellRect), CGRectGetMinY(cellRect)));
            }
        }
    }
    if (completelyVisible) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        PFObject *object = [self.objects objectAtIndex:indexPath.section];
        if ([[object parseClassName] isEqualToString:kPAPActivityClassKey])
        {
            object = [object objectForKey:kPAPActivityPhotoKey];
            object = [object fetchIfNeeded];
        }
        
        PAPPhotoCell *photoCell = (PAPPhotoCell*)cell;
        //NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        if ([[object objectForKey:kPAPFeedMediaTypeKey] isEqualToString:kPAPFeedMediaTypeImageKey]) {
            
            [_vplayer pause];
            [_vplayer.currentItem seekToTime:kCMTimeZero];
            
            return;
        }
        //NSLog(@"completely visible scroll - %li %@", (long)indexPath.section, _vplayer);
        
        NSInteger pcellIndex = _pCell ? [self.tableView indexPathForCell:_pCell].section : -1;
        
        if (_pCell && (pcellIndex != indexPath.section)) {
            //[_pCell.vPlayer pause];
            _pCell.btnPlay.hidden = NO;
            [_vplayer pause];
            [_vplayer.currentItem seekToTime:kCMTimeZero];
            
            // NSLog(@"stop playing scroll : %li - %@", (long)pcellIndex, [_vplayer currentItem]);
            [self setVideoImage:_pCell];
            
        }
        if (pcellIndex != indexPath.section) {
            _pCell = photoCell;
            
            if (!_vplayer) {
                [self setupVideoPlayer:indexPath];
            }
            
            AVPlayerItem *playerItem = (AVPlayerItem*)[_playerItem objectForKey:[@(indexPath.section) stringValue]];
            if (!playerItem) {
                [self setupVideoPlayer:indexPath];
                
                
                playerItem = (AVPlayerItem*)[_playerItem objectForKey:[@(indexPath.section) stringValue]];
                if (playerItem) {
                    [_playerItem setObject:playerItem forKey:[@(indexPath.section) stringValue]];
                }
                
                
            }
            if (playerItem) {
                //[_vplayer replaceCurrentItemWithPlayerItem:playerItem];
                [self videoPlayerChangeCurrentItem:playerItem];
            }
            
            [_vidLayer setNeedsDisplay];
            [_vidLayer displayIfNeeded];
            
            
            
            
            
            // NSLog(@"start playing : %i - %@", indexPath.section, [_vplayer currentItem]);
            
            // if ([[self.navigationController topViewController] isEqual:self]) {
            NSLog(@"player4");
            _pCell.btnPlay.hidden = YES;
            [_vplayer play];
            [self hideCellActivityIndicator];
            // }
            
        }
        else {
            _pCell = (PAPPhotoCell*)cell;
            AVPlayerItem *cItem = _vplayer.currentItem;
            AVPlayerItem *playerItem = (AVPlayerItem*)[_playerItem objectForKey:[@(indexPath.section) stringValue]];
            if ([cItem isEqual:playerItem]) {
                //if ([[self.navigationController topViewController] isEqual:self]) {
                NSLog(@"player5");
                _pCell.btnPlay.hidden = YES;
                [_vplayer play];
                [self hideCellActivityIndicator];
                // }
            }
            else {
                
                [self videoPlayerChangeCurrentItem:playerItem];
                //                [_vplayer replaceCurrentItemWithPlayerItem:playerItem];
                //if ([[self.navigationController topViewController] isEqual:self]) {
                NSLog(@"player6");
                _pCell.btnPlay.hidden = YES;
                [_vplayer play];
                [self hideCellActivityIndicator];
                // }
            }
            
        }
    }
    
    
}

- (void) videoPlayerChangeCurrentItem:(AVPlayerItem*)newItem {
    
    [self removePlayerObservers];
    
    [_vplayer replaceCurrentItemWithPlayerItem:newItem];
    
    [self setupPlayerObserversForItem:newItem];
    
}

/**
 *  Pause video when it scrolled
 *
 *  @param scrollView <#scrollView description#>
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
     _pCell.btnPlay.hidden = NO;
    [_vplayer pause];
    //[_vplayer seekToTime:kCMTimeZero];
    
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView {
    if(![aScrollView isDecelerating] && ![aScrollView isDragging]){
        
        NSArray* cells = self.tableView.visibleCells;
        
        NSUInteger cellCount = [cells count];
        if (cellCount == 0)
            return;
        
        // Check the visibility of the first cell
        [self checkVisibilityOfCell:[cells firstObject] inScrollView:aScrollView];
        if (cellCount == 1)
            return;
        
        // Check the visibility of the last cell
        [self checkVisibilityOfCell:[cells lastObject] inScrollView:aScrollView];
        if (cellCount == 2)
            return;
    }
}


/**
 *  scrolling 
 *
 *  @param aScrollView <#aScrollView description#>
 *  @param decelerate  <#decelerate description#>
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)aScrollView willDecelerate:(BOOL)decelerate{
    if(!decelerate){
        
        NSArray* cells = self.tableView.visibleCells;
        
        NSUInteger cellCount = [cells count];
        if (cellCount == 0)
            return;
        
        // Check the visibility of the first cell
        [self checkVisibilityOfCell:[cells firstObject] inScrollView:aScrollView];
        if (cellCount == 1)
            return;
        
        // Check the visibility of the last cell
        [self checkVisibilityOfCell:[cells objectAtIndex:1] inScrollView:aScrollView];
        if (cellCount == 2)
            return;
        [self checkVisibilityOfCell:[cells lastObject] inScrollView:aScrollView];
        return;
    }
}


//- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
//{
//    NSArray* cells = self.tableView.visibleCells;
//
//    NSUInteger cellCount = [cells count];
//    if (cellCount == 0)
//        return;
//
//    // Check the visibility of the first cell
//    [self checkVisibilityOfCell:[cells firstObject] inScrollView:aScrollView];
//    if (cellCount == 1)
//        return;
//
//    // Check the visibility of the last cell
//    [self checkVisibilityOfCell:[cells lastObject] inScrollView:aScrollView];
//    if (cellCount == 2)
//        return;
//}

//- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
//    
//    
//    
//    // All of the rest of the cells are visible: Loop through the 2nd through n-1 cells
//    //for (NSUInteger i = 1; i < cellCount - 1; i++)
//     //   [[cells objectAtIndex:i] notifyCompletelyVisible];
//}



@end
