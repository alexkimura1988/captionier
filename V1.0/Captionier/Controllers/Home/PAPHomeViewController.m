//
//  PAPHomeViewController.m
//  Anypic
//
//  Created by Héctor Ramos on 5/2/12.
//

#import "PAPHomeViewController.h"
#import "PAPSettingsActionSheetDelegate.h"
#import "PAPSettingsButtonItem.h"
#import "PAPFindFriendsViewController.h"
#import "MBProgressHUD.h"
#import "NotificationBar.h"
#import "HelpScreensViewController.h"

#import "PBJViewController.h"
#import "NavigationViewController.h"

#import "REMenu.h"
#import "HelpSreenViewController.h"
#import "CustomCameraViewController.h"
#import "PushScheduleControllerViewController.h"
#import "PrivateGroupListViewController.h"


@interface PAPHomeViewController ()
{
    BOOL shouldReload;
}
@property (nonatomic, strong) PAPSettingsActionSheetDelegate *settingsActionSheetDelegate;
@property (nonatomic, strong) UIView *blankTimelineView;
@end

@implementation PAPHomeViewController
@synthesize firstLaunch;
@synthesize settingsActionSheetDelegate;
@synthesize blankTimelineView;
@synthesize hashTagString;
@synthesize hashTag;


#pragma mark - UIViewController

/**
 *  go back to previous controller
 *
 *  @param sender btn pressed
 */
- (void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


/**
 *  refreshing the objects
 *
 *  @param sender refresh btn clicked
 */
-(void)btnRefreshClicked:(id)sender
{
    [Flurry logEvent:@"Refresh_Button_Clicked"];
    [self loadObjects];
    
}



-(void)privateGroupClicked:(id)sender{
    
    PrivateGroupListViewController *groupCV = [[PrivateGroupListViewController alloc]initWithNibName:@"PrivateGroupListViewController" bundle:nil];
    groupCV.hidesBottomBarWhenPushed = YES;
    groupCV.user =[PFUser currentUser];
    [self.navigationController pushViewController:groupCV animated:YES];
}

/**
 *  Configures the navigation bar right side button as custome button .
 */
- (void)customizeNavigationBarButtons {
	UIImage *imgButton = [UIImage imageNamed:@"privateGrp_icon_off@2x.png"];
	
	UIButton *rightBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[rightBarButton setBackgroundImage:imgButton forState:UIControlStateNormal];
    [rightBarButton setBackgroundImage:[UIImage imageNamed:@"privateGrp_icon_on@2x.png"] forState:UIControlStateHighlighted];
	[rightBarButton setFrame:CGRectMake(0, 0, imgButton.size.width, imgButton.size.height)];
	
	rightBarButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    [rightBarButton addTarget:self action:@selector(privateGroupClicked:) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButton];
   
}

//- (void)viewWillLayoutSubviews
//{
//    [super viewWillLayoutSubviews];
//    NavigationViewController *navigationController = (NavigationViewController *)self.navigationController;
//    [navigationController.menu setNeedsLayout];
//}


//-(void)viewWillAppear:(BOOL)animated
//{
//    //[self FirstLaunchApplication];
//
//    if (shouldReload) {
//        [self loadObjects];
//    }
//    shouldReload = YES;
//}






-(void)FirstLaunchApplication
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults boolForKey:@"IsFirstLaunch"] == NO) {
        
        
        HelpSreenViewController *helpscreen=[[HelpSreenViewController alloc]initWithNibName:@"HelpSreenViewController" bundle:nil];
        [userDefaults setBool:YES forKey:@"IsFirstLaunch"];
        helpscreen.selectedview=1;
        [userDefaults synchronize];
        // [self presentViewController:helpscreen animated:YES completion:nil];
        [self .navigationController pushViewController:helpscreen animated:YES];
        
        
    }else{
        
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    shouldReload = NO;
    
    // To unhide statusBar
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
   
    
    [self customizeNavigationBarButtons];
    
   
    
    if (self.isNeedPullToRefresh) {
        //[self customizeNavigationBarButtons];
    }
    

    if (self.isHashTag) {
        
     
        self.title = hashTagString;
        UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn.png"];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    
        [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
     
        //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
    }
    
    else
    {
        self.navigationItem.title = AppTitle;
//        UIImageView *navilogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insta_logo1"]];//home_logo
//        navilogo.frame = CGRectMake(0, 0, 100, 44);
//        self.navigationItem.titleView = navilogo;
        
//        self.navigationItem.title = AppTitle;
//        UIImageView *bgImgVw = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_screen_bg"]];
//        bgImgVw.frame = [[UIScreen mainScreen] bounds];

        
    }

    

    
    
    UIImageView *bgImgVw = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_screen_bg"]];
    bgImgVw.frame = self.tableView.frame;
    self.tableView.backgroundView = bgImgVw;
    
    NSLog(@"viewdidload - home %@", self);

}


/**
 *  This method will open camera screen
 */

-(void) gotoCamera
{
    NavigationViewController *navigationController = (NavigationViewController *)self.navigationController;
    navigationController.menu.isOpen = YES;
       [navigationController toggleMenu];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if (IS_IPHONE_5) {
        CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController" bundle:nil];
        [self.navigationController pushViewController:obj animated:YES];
    }
    else {
        CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController_ip4" bundle:nil];
        
        [self.navigationController pushViewController:obj animated:YES];
    }

}

#pragma mark - PFQueryTableViewController


- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];

    if (self.objects.count == 0 && ![[self queryForTable] hasCachedResult] & !self.firstLaunch) {
        self.tableView.scrollEnabled = NO;
        
        if (!self.blankTimelineView.superview) {
            self.blankTimelineView.alpha = 0.0f;
            self.tableView.tableHeaderView = self.blankTimelineView;
            
            [UIView animateWithDuration:0.200f animations:^{
                self.blankTimelineView.alpha = 1.0f;
            }];
        }
    } else {
        self.tableView.tableHeaderView = nil;
        self.tableView.scrollEnabled = YES;
    }    
}


#pragma mark - ()

- (void)settingsButtonAction:(id)sender {
    self.settingsActionSheetDelegate = [[PAPSettingsActionSheetDelegate alloc] initWithNavigationController:self.navigationController];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self.settingsActionSheetDelegate cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"My Profile",@"Find Friends",@"Log Out", nil];
    [actionSheet showInView:self.view];
    //[actionSheet showFromTabBar:self.tabBarController.tabBar];
}

/**
 *  This method will invite friends screen
 *
 *  @param sender btn pressed
 */
- (void)inviteFriendsButtonAction:(id)sender {
    PAPFindFriendsViewController *detailViewController = [[PAPFindFriendsViewController alloc] init];
    [self.navigationController pushViewController:detailViewController animated:YES];
}
@end
