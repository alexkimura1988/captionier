//
//  PushScheduleControllerViewController.m
//  VIND
//
//  Created by Rathore on 26/11/14.
//
//

#import "PushScheduleControllerViewController.h"
#import "AlarmSelectViewController.h"

@interface PushScheduleControllerViewController ()
@property(nonatomic,strong)IBOutlet UIDatePicker *datePicker;
@property(nonatomic,strong)IBOutlet UIButton *buttonDeleteAlarm;
@property(nonatomic,strong)IBOutlet UIButton *buttonSelectAlarmRepeatTime;
@property(nonatomic,strong)NSDate *selectedDate;
@property(nonatomic,strong)NSMutableData *responseData;
@property(nonatomic,assign)BOOL isOneTimePush;
@property(nonatomic,strong)NSString *pushRepeatDays;
@property(nonatomic,assign)int serviceType;
@end



#define SetPush 0
#define GetPush  1
#define DeletePush 2

@implementation PushScheduleControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    NSArray  *arrayAlarmSchedule = @[@"Every Sunday",@"Every Monday",@"Every Tuesday",@"Every Wednesday",@"Every Thursday",@"Every Friday",@"Every Saturday"];
    
    NSString *alarmDay = @"";
    _pushRepeatDays = @"";
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
   
    
    if ([ud boolForKey:arrayAlarmSchedule[1]]) {
        alarmDay = [alarmDay stringByAppendingString:@" Mon"];
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"1,"];
    }
    else {
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"0,"];
    }
    
    
    if ([ud boolForKey:arrayAlarmSchedule[2]]) {
        alarmDay = [alarmDay stringByAppendingString:@" Tue"];
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"2,"];
    }
    else {
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"0,"];
    }
    
    
    if ([ud boolForKey:arrayAlarmSchedule[3]]) {
        alarmDay = [alarmDay stringByAppendingString:@" Wed"];
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"3,"];
    }
    else {
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"0,"];
    }
    
    if ([ud boolForKey:arrayAlarmSchedule[4]]) {
        alarmDay = [alarmDay stringByAppendingString:@" Thu"];
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"4,"];
    }
    else {
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"0,"];
    }
    
    if ([ud boolForKey:arrayAlarmSchedule[5]]) {
        alarmDay = [alarmDay stringByAppendingString:@" Fri"];
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"5,"];
    }
    else {
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"0,"];
    }
    
    if ([ud boolForKey:arrayAlarmSchedule[6]]) {
        alarmDay = [alarmDay stringByAppendingString:@" Sat"];
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"6,"];
    }
    else {
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"0,"];
    }
    
    if ([ud boolForKey:arrayAlarmSchedule[0]]) {
        alarmDay = [alarmDay stringByAppendingString:@"Sun"];
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"7"];
    }
    else {
        _pushRepeatDays = [_pushRepeatDays stringByAppendingString:@"0"];
    }
    
    
    //
    if (alarmDay.length == 0) {
        _isOneTimePush = YES;
        [_buttonSelectAlarmRepeatTime setTitle:@"Never" forState:UIControlStateNormal];
    }
    else if(alarmDay.length ==  27){
        _isOneTimePush = NO;
        [_buttonSelectAlarmRepeatTime setTitle:@"Everyday" forState:UIControlStateNormal];
    }
    else {
         _isOneTimePush = NO;
        [_buttonSelectAlarmRepeatTime setTitle:alarmDay forState:UIControlStateNormal];
    }
    
    if ([ud objectForKey:@"SDate"]) {
        _datePicker.date = [ud objectForKey:@"SDate"];
    }

}
-(void)viewWillDisappear:(BOOL)animated{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(IBAction)buttonDeleteAlarmClicked:(id)sender{
  
   /*
    NSString *date;
    if (_selectedDate == nil) {
        date = [self getUTCDate:[NSDate date]];
    }
    else{
        date = [self getUTCDate:_selectedDate];
    }
    
    // Create the request.
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://107.170.66.211/apps/elfnmagic/process.php/sendPush"]];
    
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    
    
    NSString *stringData = [NSString stringWithFormat:@"date_time=%@&message=%@",date,@""];
    // Convert your data and set your request's HTTPBody property
   
    NSData *requestBodyData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    request.HTTPBody = requestBodyData;
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
   */
    
    
    
    [self deleteSchedule];
    


}
-(IBAction)buttonSetScheduleClicked:(id)sender{
    
        [self setSchedule];
}


-(IBAction)buttonSelectAlarmRepeatTime:(id)sender{
    
    AlarmSelectViewController *alarm = [[AlarmSelectViewController alloc] initWithNibName:@"AlarmSelectViewController" bundle:nil];
    [self.navigationController pushViewController:alarm animated:YES];
    
}
//listen to changes in the date picker and just log them
- (IBAction)datePickerDateChanged:(UIDatePicker *)paramDatePicker{
    
    _selectedDate = paramDatePicker.date;
    NSLog(@"Selected date = %@", paramDatePicker.date);
    
}

#pragma mark - DateFormats

- (NSString*)getUTCDate:(NSDate*)date
{
    // Purpose: Return a string of the current date-time in UTC (Zulu) time zone in ISO 8601 format.
    return [self toStringFromDateTime:date];
}
- (NSString*)toStringFromDateTime:(NSDate*)datetime
{
    // Purpose: Return a string of the specified date-time in UTC (Zulu) time zone in ISO 8601 format.
    // Example: 2013-10-25T06:59:43.431Z
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:SS.SSS'Z'"];
    NSString* dateTimeInIsoFormatForZuluTimeZone = [dateFormatter stringFromDate:datetime];
    return dateTimeInIsoFormatForZuluTimeZone;
}
-(NSString*)getGMTDateFormat:(NSDate*)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    NSString *timeStamp = [dateFormatter stringFromDate:date];
    return timeStamp;
}

-(NSString*)getGMTTimeFormat:(NSDate*)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    NSString *timeStamp = [dateFormatter stringFromDate:date];
    return timeStamp;
}
#pragma mark Services
-(void)setSchedule{
    
    
    _serviceType = SetPush;
    
    
    
    int pushtype = 2; //reccurring push
    if (_isOneTimePush) {
        pushtype = 1; // one time push
    }
    
    
    NSString *date;
    if (_selectedDate == nil) {
        
        if (pushtype == 1) {
            date = [self getGMTDateFormat:[NSDate date]];
        }
        else {
            date = [self getGMTTimeFormat:[NSDate date]];
        }
        
    }
    else {
        
        if (pushtype == 1) {
            date = [self getGMTDateFormat:_selectedDate];
        }
        else {
            date = [self getGMTTimeFormat:_selectedDate];
        }
    }
    
    // Create the request.
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://104.131.171.65/elfminder.me/app/process.php/schedulePush"]];
    
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    
    
    NSString *stringData = [NSString stringWithFormat:@"date_time=%@&message=%@&push_type=%d&recurring_days=%@&user_id=%@",date,@"",pushtype,_pushRepeatDays,@""];
    // Convert your data and set your request's HTTPBody property
    
    NSData *requestBodyData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    request.HTTPBody = requestBodyData;
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
}
-(void)getSchedule{
    
}
-(void)deleteSchedule{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"ScheduleID"]) {
        
        _serviceType = DeletePush;
        
        NSString *sID = [[NSUserDefaults standardUserDefaults] objectForKey:@"ScheduleID"];
        
        // Create the request.
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://104.131.171.65/elfminder.me/app/process.php/removeScheduledPush"]];
        
        // Specify that it will be a POST request
        request.HTTPMethod = @"POST";
        
        
        NSString *stringData = [NSString stringWithFormat:@"schedule_id=%@",sID];
        // Convert your data and set your request's HTTPBody property
        
        NSData *requestBodyData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
        request.HTTPBody = requestBodyData;
        
        // Create url connection and fire request
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    else{
        [Helper showAlertWithTitle:@"Message" Message:@"You haven't setup any schedule"];
    }
    
    
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
   _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
    NSString *response = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
    NSLog(@"res %@",response);
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    if (_serviceType == SetPush) {
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:_responseData
                                                              options:0 error:NULL];
        [self pareseSetSchedue:jsonObject];
    }
    else  if (_serviceType == DeletePush) {
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:_responseData
                                                                   options:0 error:NULL];
        [self parseDeleteSchedule:jsonObject];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}

-(void)pareseSetSchedue:(NSDictionary*)array{
    NSLog(@"response %@",array);
    
    if (array.count > 0) {
       
        if ([array[@"flag"] integerValue] == 0) {
            
            //save schedule date and ID
            if (_selectedDate != nil) {
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                [ud setObject:_selectedDate forKey:@"SDate"];
                [ud synchronize];
            }
            
             [[NSUserDefaults standardUserDefaults] setObject:array[@"schedule_id"] forKey:@"ScheduleID"];
            [Helper showAlertWithTitle:@"Message" Message:@"Your Reminder is successfully set."];
        }
        else if ([array[@"flag"] integerValue] == 1) {
            [Helper showAlertWithTitle:@"Message" Message:array[@"msg"]];
        }
       
    }
    
    
    
}
-(void)parseDeleteSchedule:(NSDictionary*)dictionary{
    
    if (dictionary.count > 0) {
        
        if ([dictionary[@"flag"] integerValue] == 0) {
            
            //delete schedule date and ID
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SDate"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ScheduleID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [Helper showAlertWithTitle:@"Message" Message:@"Your Reminder has been deleted successfully."];
            
            _datePicker.date = [NSDate date];
            
        }
        
    }
}
@end
