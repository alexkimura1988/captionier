

#import <Parse/Parse.h>
#import "ChatView.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface ChatView()
{
	NSTimer *timer;
	BOOL isLoading;

	NSString *chatroom;

	NSMutableArray *users;
	NSMutableArray *messages;
	NSMutableDictionary *avatars;

	UIImageView *outgoingBubbleImageView;
	UIImageView *incomingBubbleImageView;
}
@property(nonatomic,strong)PFUser *chatUser;
@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation ChatView
@synthesize chatUser;
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)initWith:(NSString *)chatroom_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	self = [super init];
	chatroom = chatroom_;
	return self;
}

/*!
 *  initilize the chat with the other user
 *
 *  @param chatuser Parse User object
 *
 *  @return class object
 */
- (id)initWithUser:(PFUser*)chatuser_{
    
    self = [super init];
	self.chatUser = chatuser_;
	return self;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewDidLoad];
	self.title = [self.chatUser objectForKey:kPAPUserDisplayNameKey];

	users = [[NSMutableArray alloc] init];
	messages = [[NSMutableArray alloc] init];
	avatars = [[NSMutableDictionary alloc] init];

	self.sender = [PFUser currentUser].objectId;

	outgoingBubbleImageView = [JSQMessagesBubbleImageFactory outgoingMessageBubbleImageViewWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
	incomingBubbleImageView = [JSQMessagesBubbleImageFactory incomingMessageBubbleImageViewWithColor:[UIColor jsq_messageBubbleGreenColor]];
    
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    self.inputToolbar.contentView.leftBarButtonItem = nil;

	isLoading = NO;
	[self loadMessages];
	timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(loadMessages) userInfo:nil repeats:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidAppear:(BOOL)animated
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewDidAppear:animated];

	self.collectionView.collectionViewLayout.springinessEnabled = YES;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewWillDisappear:(BOOL)animated
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewWillDisappear:animated];
	[timer invalidate];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)loadMessages
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (isLoading == NO)
	{
		isLoading = YES;
		JSQMessage *message_last = [messages lastObject];

//		PFQuery *query = [PFQuery queryWithClassName:PF_CHAT_CLASS_NAME];
//		[query whereKey:PF_CHAT_ROOM equalTo:chatroom];
//		if (message_last != nil) [query whereKey:PF_CHAT_CREATEDAT greaterThan:message_last.date];
//		[query includeKey:PF_CHAT_USER];
//		[query orderByAscending:PF_CHAT_CREATEDAT];
        

        PFQuery *meSender = [PFQuery queryWithClassName:@"Chats"];
        [meSender whereKey:@"senderUser" equalTo:[PFUser currentUser]];
        [meSender whereKey:@"toUser" equalTo:self.chatUser];
        
        
        PFQuery *otherSender = [PFQuery queryWithClassName:@"Chats"];
        [otherSender whereKey:@"senderUser" equalTo:self.chatUser];
        [otherSender whereKey:@"toUser" equalTo:[PFUser currentUser]];
        
        PFQuery *query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:meSender, otherSender, nil]];
        if (message_last != nil) [query whereKey:@"createdAt" greaterThan:message_last.date];
        [query includeKey:@"toUser"];
        [query includeKey:@"senderUser"];
        [query orderByAscending:@"createdAt"];
        //query.cachePolicy = kPFCachePolicyCacheThenNetwork;
        
		[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
		{
			if (error == nil)
			{
				for (PFObject *object in objects)
				{
					PFUser *user = [object objectForKey:@"senderUser"];
					[users addObject:user];

					JSQMessage *message = [[JSQMessage alloc] initWithText:object[@"message"] sender:user.objectId date:object.createdAt];
					[messages addObject:message];
				}
				if ([objects count] != 0) [self finishReceivingMessage];
			}
			else {
                NSLog(@"error: Network error.");
             
            }
			isLoading = NO;
		}];
	}
}

#pragma mark - JSQMessagesViewController method overrides

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text sender:(NSString *)sender date:(NSDate *)date
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	PFObject *object = [PFObject objectWithClassName:@"Chats"];
    object[@"senderUser"] = [PFUser currentUser];
    object[@"toUser"] = self.chatUser;
	object[@"message"] = text;
	[object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
	{
		if (error == nil)
		{
			[JSQSystemSoundPlayer jsq_playMessageSentSound];
			[self loadMessages];
		}
		else {
            NSLog(@"Error : Network error");
        }
        
	}];
	[self finishSendingMessage];
    
    
    //NSString *userFirstName = [PAPUtility firstNameForDisplayName:[[PFUser currentUser] objectForKey:kPAPUserDisplayNameKey]];
    NSString *userFirstName = [PAPUtility firstNameForDisplayName:[[PFUser currentUser]username]];
    NSString *message = [NSString stringWithFormat:@"%@:%@", userFirstName, text];
    
    // Truncate message if necessary and ensure we have enough space
    // for the rest of the payload
    if (message.length > 100) {
        message = [message substringToIndex:99];
        message = [message stringByAppendingString:@"..."];
    }
    
    NSDictionary *payload =
    [NSDictionary dictionaryWithObjectsAndKeys:
     message, kAPNSAlertKey,
     kPAPPushPayloadPayloadTypeActivityKey, kPAPPushPayloadPayloadTypeKey,
     kPAPPushPayloadPayloadTypeChatKey, kPAPPushPayloadActivityTypeKey,
     [[PFUser currentUser] objectId], kPAPPushPayloadFromUserObjectIdKey,
     self.chatUser.objectId,kPAPPushPayloadChatUserIdKey,
     @"sms-received.wav",kAPNSSoundKey,
     nil];
    
    // Send the push
    PFPush *push = [[PFPush alloc] init];
    [push setChannel:[self.chatUser objectForKey:kPAPUserPrivateChannelKey]];
    [push setData:payload];
    [push sendPushInBackground];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)didPressAccessoryButton:(UIButton *)sender
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	NSLog(@"didPressAccessoryButton");
}

#pragma mark - JSQMessages CollectionView DataSource

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return [messages objectAtIndex:indexPath.item];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (UIImageView *)collectionView:(JSQMessagesCollectionView *)collectionView bubbleImageViewForItemAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	JSQMessage *message = [messages objectAtIndex:indexPath.item];
	if ([[message sender] isEqualToString:self.sender])
	{
		return [[UIImageView alloc] initWithImage:outgoingBubbleImageView.image highlightedImage:outgoingBubbleImageView.highlightedImage];
	}
	else return [[UIImageView alloc] initWithImage:incomingBubbleImageView.image highlightedImage:incomingBubbleImageView.highlightedImage];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (UIImageView *)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageViewForItemAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    return nil;
//	PFUser *user = [users objectAtIndex:indexPath.item];
//    [user fetchIfNeeded];
//	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blank_avatar"]];
//	if (avatars[user.objectId] == nil)
//	{
//		PFFile *filePicture = [user objectForKey:kPAPUserProfilePicSmallKey];
//        
//        
//		[filePicture getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error)
//		{
//			if (error == nil)
//			{
//				avatars[user.objectId] = [UIImage imageWithData:imageData];
//				[imageView setImage:avatars[user.objectId]];
//			}
//		}];
//	}
//	else [imageView setImage:avatars[user.objectId]];
//
//	imageView.layer.cornerRadius = imageView.frame.size.width/2;
//	imageView.layer.masksToBounds = YES;
//
//	return imageView;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (indexPath.item % 3 == 0)
	{
		JSQMessage *message = [messages objectAtIndex:indexPath.item];
		return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
	}
	return nil;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	JSQMessage *message = [messages objectAtIndex:indexPath.item];
	if ([message.sender isEqualToString:self.sender])
	{
		return nil;
	}
	
	if (indexPath.item - 1 > 0)
	{
		JSQMessage *previousMessage = [messages objectAtIndex:indexPath.item - 1];
		if ([[previousMessage sender] isEqualToString:message.sender])
		{
			return nil;
		}
	}

	PFUser *user = [users objectAtIndex:indexPath.item];
	return [[NSAttributedString alloc] initWithString:user[kPAPUserDisplayNameKey]];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return nil;
}

#pragma mark - UICollectionView DataSource

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return [messages count];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
	
	JSQMessage *message = [messages objectAtIndex:indexPath.item];
	if ([message.sender isEqualToString:self.sender])
	{
		cell.textView.textColor = [UIColor blackColor];
	}
	else
	{
		cell.textView.textColor = [UIColor whiteColor];
	}
	
	cell.textView.linkTextAttributes = @{NSForegroundColorAttributeName:cell.textView.textColor,
										 NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle | NSUnderlinePatternSolid)};
	
	return cell;
}

#pragma mark - JSQMessages collection view flow layout delegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
				   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (indexPath.item % 3 == 0)
	{
		return kJSQMessagesCollectionViewCellLabelHeightDefault;
	}
	return 0.0f;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
				   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	JSQMessage *message = [messages objectAtIndex:indexPath.item];
	if ([[message sender] isEqualToString:self.sender])
	{
		return 0.0f;
	}
	
	if (indexPath.item - 1 > 0)
	{
		JSQMessage *previousMessage = [messages objectAtIndex:indexPath.item - 1];
		if ([[previousMessage sender] isEqualToString:[message sender]])
		{
			return 0.0f;
		}
	}
	return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
				   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return 0.0f;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)collectionView:(JSQMessagesCollectionView *)collectionView
				header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	NSLog(@"didTapLoadEarlierMessagesButton");
}

@end
