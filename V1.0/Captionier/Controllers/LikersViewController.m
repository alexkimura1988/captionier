//
//  LikersViewController.m
//  FizFeliz
//
//  Created by Surender Rathore on 16/12/13.
//
//

#import "LikersViewController.h"
#import "PAPFindFriendsCell.h"
#import "PAPProfileImageView.h"
#import "PAPAccountViewController.h"

@interface LikersViewController ()<PAPFindFriendsCellDelegate>
@property(nonatomic,weak)IBOutlet UITableView *tbleView;
@property(nonatomic,strong) NSArray *likers;
@end

@implementation LikersViewController
@synthesize photo;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


/**
 *  Configures the navigation bar left button as custome button
 *
 */
-(void)customizeNavigationBackButton {
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
       self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
}

/**
 *  go back to previous controller
 *
 *  @param sender backBtn pressed
 */
-(void)backButtonAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"Likes";
    [self customizeNavigationBackButton];
    [self loadPhotoLikers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 *  Parse queries to load most liked photos
 */
-(void)loadPhotoLikers {
    
    PFQuery *queryLikes = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [queryLikes whereKey:kPAPActivityPhotoKey equalTo:photo];
    [queryLikes whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeLike];
    [queryLikes includeKey:kPAPActivityFromUserKey];
    [queryLikes findObjectsInBackgroundWithBlock:^(NSArray *objects,NSError *error){
        if (!error) {
            _likers = [[NSArray alloc] initWithArray:objects];
           // NSLog(@"likers %@",_likers);
            [self.tbleView reloadData];
        }
    }];
}

#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _likers.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    PAPFindFriendsCell *cell = (PAPFindFriendsCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[PAPFindFriendsCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.delegate = self;
        cell.followButton.hidden = YES;
        
    }
    PFObject *object = _likers[indexPath.row];
    PFUser *user = [object objectForKey:kPAPActivityFromUserKey];
   // NSLog(@"user %@",user);
    [cell.avatarImageView setFrame:CGRectMake(0, 2, 40, 40)];
   
    
    [cell setUser:user];
    CGRect frame = cell.nameButton.frame;
    frame.origin.y = 10;
    frame.origin.x = 45;
    cell.nameButton.frame = frame;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}


#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
	[tableViewCell setSelected:NO animated:YES];
    
    
}

#pragma mark - PAPFindFriendsCellDelegate

/**
 *  This method is invoked when username is tapped
 *  Open tapped user name account
 *
 *  @param cellView
 *  @param aUser    tappedUserName
 */
- (void)cell:(PAPFindFriendsCell *)cellView didTapUserButton:(PFUser *)aUser {
    // Push account view controller
    PAPAccountViewController *accountViewController = [[PAPAccountViewController alloc] initWithStyle:UITableViewStylePlain];
    accountViewController.showBackButton = YES;
    [accountViewController setUser:aUser];
    [self.navigationController pushViewController:accountViewController animated:YES];
}

@end
