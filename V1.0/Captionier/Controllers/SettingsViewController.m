//
//  SettingsViewController.m
//  Anypic
//
//  Created by rahul Sharma on 08/08/13.
//
//

#import "SettingsViewController.h"
#import "PAPFindFriendsViewController.h"
#import "PAPConstants.h"
#import "AppDelegate.h"
#import "WebViewController.h"
#import "InviteFriendsViewController.h"
#import "FollowersFollowingViewController.h"
#import "FBLoginHandler.h"

@interface SettingsViewController ()<FBLoginHandlerDelegate>
@property(nonatomic,strong)NSArray *settingOptions;
@property(nonatomic,strong)NSArray *termsOptions;
@property(nonatomic,strong) IBOutlet UITableView *tbleView;
@property (nonatomic, strong) ABPeoplePickerNavigationController *addressBook;
@property (nonatomic, strong) NSString *selectedEmailAddress;
@end

@implementation SettingsViewController
@synthesize settingOptions;
@synthesize termsOptions;
@synthesize tbleView;
@synthesize addressBook;



#define MogramBlogUrl   @"http://108.166.190.172:81/mogram/mogramBlog.html"
#define MogramSupportUrl @"http://108.166.190.172:81/mogram/mogramsupport.html"
#define MogramHelpCenter @"http://108.166.190.172:81/mogram/mogramHelpCenter.html"
#define mogramTermsOfService @"http://108.166.190.172:81/mogram/mogramTermsOfService.html"
#define mogramPrivacyPolicy @"http://108.166.190.172:81/mogram/mogramPrivacyPolicy.html"

#define PrivacyPolicy @"http://www.wmg.com/privacy/wmg"

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/**
 *  To go previous controller
 *
 *  @param sender backBtn press
 */
- (void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)customizeNavigationBarButtons {
	UIImage *imgButton = [UIImage imageNamed:@"cahrt_btn.png"];
	
	UIButton *rightBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[rightBarButton setFrame:CGRectMake(0, 0, 70, 35)];
    [rightBarButton setTitle:@"Logout" forState:UIControlStateNormal];
	
	rightBarButton.titleLabel.font = [UIFont fontWithName:Lato size:17];
    [rightBarButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBarButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [rightBarButton addTarget:self action:@selector(logOut:) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButton];
	
	
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Settings";
    [self.view setBackgroundColor:WHITE_COLOR];
    [self customizeNavigationBarButtons];
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    //[backButton setBackgroundImage:[UIImage imageNamed:@"back_btn_on.png"] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    settingOptions = @[@"Invite from contacts",@"Invite with facebook",@"Support",@"Report problem",@"Terms of service", @"Privacy Policy"];

    
    //settingOptions = @[@"Invite from contacts",@"Invite with facebook",@"Save Original photo",@"Support",@"Report problem",@"Terms of service"];
    termsOptions = @[@"Support",@"Report problem",@"Help center",@"Mogram blog",@"Privacy policy",@"Terms of service"];
    
    
    [self sendRequestForEULA];
    
    UIImageView *bgImgVw = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_screen_bg"]];
    bgImgVw.frame = self.tbleView.frame;
    self.tbleView.backgroundView = bgImgVw;

}

-(void)sendRequestForEULA {
    PFQuery *query = [PFQuery queryWithClassName:@"EULA"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        if (!error) {
            PFObject *object = [array lastObject];
            PFFile *file = [object objectForKey:@"eula"];
            [[NSUserDefaults standardUserDefaults] setObject:file.url forKey:@"EulaURL"];
            
            
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidUnload
{
    self.tbleView = nil;
}
#pragma mark -
#pragma mark TableView DataSourse Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     
  
        return settingOptions.count;
   
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont fontWithName:robo_light size:28];
        cell.textLabel.textColor = UIColorFromRGB(0x666666);
        
        UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(10, 54, 300, 1)];
        line.image = [UIImage imageNamed:@"profile_screen_filter_line-568h"];
        [cell.contentView addSubview:line];
        
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    //cell.textLabel.font = [UIFont fontWithName:<#(NSString *)#> size:<#(CGFloat)#>]
    cell.textLabel.text = settingOptions[indexPath.row];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
	[tableViewCell setSelected:NO animated:YES];
 
    switch (indexPath.row) {
        case 0:
        {
            [self inviteFriendsButtonAction:nil];
            break;
        }
        case 1:
        {
            if ([FBSession activeSession].isOpen) {
                FollowersFollowingViewController *followersVC = [[FollowersFollowingViewController alloc] init];
                followersVC.type = FacebookFriends;
                followersVC.user = [PFUser currentUser];
                followersVC.title =  @"Following";
                [self.navigationController pushViewController:followersVC animated:YES];
            }
            else {
                FBLoginHandler *fbLoginHandler = [FBLoginHandler sharedInstance];
                fbLoginHandler.delegate = self;
                [fbLoginHandler loginWithFacebook];
                
            }
            
            
            break;
        }
        case 2:
        {
           
            [self sendEmail:1];
            
            break;
        }
        case 3:
        {
            [self sendEmail:0];
            break;
        }
        case 4:
        {
            WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
            webView.title = @"Terms of Service";
            webView.weburl = TermsOfService;
            UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:webView];
            [self.navigationController presentViewController:navC animated:YES completion:nil];


            break;

            
            break;
        }
        case 5:
        {
            
            WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
            webView.title = @"Privacy Policy";
            webView.weburl =@"http://107.170.105.134/TNC/Captionier_Privacy_Policy/";
            UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:webView];
            [self.navigationController presentViewController:navC animated:YES completion:nil];
            
            
            break;
            
            
            break;
        }
        case 6:
        {
            
            WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
            webView.title = @"Donate";
            webView.weburl = @"http://instachurch.net/appwebviews/donate.php";
            UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:webView];
            [self.navigationController presentViewController:navC animated:YES completion:nil];
            break;

        }
        case 7:
        {
            WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
            webView.title = @"Donate";
            webView.weburl = @"http://instachurch.net/appwebviews/donate.php";
            UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:webView];
            [self.navigationController presentViewController:navC animated:YES completion:nil];
            break;
        }
        case 8:
        {
            WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
            webView.title = @"Terms of Service";
            webView.weburl = mogramTermsOfService;
             webView.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:webView animated:YES];
            break;
        }
        
            
        default:
            break;
    }
        
    
  
    
}


#pragma Logout

/**
 *  This method will take conformation from user to logout
 *
 *  @param sender logout btn press
 */

-(void)logOut:(id)sender
{
    UIActionSheet *actionsheet = [[UIActionSheet alloc] initWithTitle:@"Are you sure you want to logout?" delegate:self cancelButtonTitle:@"No" destructiveButtonTitle:@"Logout" otherButtonTitles:nil, nil];
    actionsheet.tag = 100;
    [actionsheet showFromTabBar:self.tabBarController.tabBar];
//    [actionsheet showInView:self.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100) {
        if (actionSheet.destructiveButtonIndex == buttonIndex) {
            [(AppDelegate *)[[UIApplication sharedApplication] delegate] logOut];
        }
    }
    else {
        if (buttonIndex == actionSheet.cancelButtonIndex) {
            return;
        }
        
        if (buttonIndex == 0) {
            [self presentMailComposeViewController:self.selectedEmailAddress];
        } else if (buttonIndex == 1) {
            [self presentMessageComposeViewController:self.selectedEmailAddress];
        }
    }
  
    
}



/**
 *  This method will open email composer 
 *  set email subject & recipents
 *
 *  @param type 0 or 1
 */

-(void)sendEmail:(int)type
{
    if ([MFMailComposeViewController canSendMail])
    {
        
        MFMailComposeViewController *  mailComposecontroller=[[MFMailComposeViewController alloc]init];
        mailComposecontroller.mailComposeDelegate=self;
        if (type == 0) {
            [mailComposecontroller setSubject:@"Report a problem"];
            [mailComposecontroller setToRecipients:[NSArray arrayWithObjects:@"info@3embed.com", nil]];
        }
        else {
            [mailComposecontroller setSubject:@"Support Query"];
            [mailComposecontroller setToRecipients:[NSArray arrayWithObjects:@"info@3embed.com", nil]];
        }
        
        
               
        
        [self presentViewController:mailComposecontroller animated:YES completion:^{
        }];
        
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Your device is not currently connected to an email account."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //  ////NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            //  ////NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            //NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            // ////NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    // UINavigationController *navigationController = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).navigationcontroller;
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}

/**
 *  This method will pick contacts from device and perfrom corresponding functions i.e
 *  send email or
 *  send text message
 *
 *  @param sender
 */
- (void)inviteFriendsButtonAction:(id)sender {
    
    
    addressBook = [[ABPeoplePickerNavigationController alloc] init];
    addressBook.peoplePickerDelegate = self;
    
    if ([MFMailComposeViewController canSendMail] && [MFMessageComposeViewController canSendText]) {
        addressBook.displayedProperties = [NSArray arrayWithObjects:[NSNumber numberWithInt:kABPersonEmailProperty], [NSNumber numberWithInt:kABPersonPhoneProperty], nil];
    } else if ([MFMailComposeViewController canSendMail]) {
        addressBook.displayedProperties = [NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonEmailProperty]];
    } else if ([MFMessageComposeViewController canSendText]) {
        addressBook.displayedProperties = [NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonPhoneProperty]];
    }
    
    //[self presentModalViewController:addressBook animated:YES];
    [self presentViewController:addressBook animated:YES completion:nil];
}

#pragma mark - ABPeoplePickerDelegate

/* Called when the user cancels the address book view controller. We simply dismiss it. */
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier NS_AVAILABLE_IOS(8_0){
    
    if (property == kABPersonEmailProperty) {
        
        ABMultiValueRef emailProperty = ABRecordCopyValue(person,property);
        NSString *email = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emailProperty,identifier);
        self.selectedEmailAddress = email;
        
        if ([MFMailComposeViewController canSendMail] && [MFMessageComposeViewController canSendText]) {
            // ask user
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Invite %@",@""] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Email", @"iMessage", nil];
            //[actionSheet showFromTabBar:self.tabBarController.tabBar];
            [actionSheet showInView:addressBook.view];
        } else if ([MFMailComposeViewController canSendMail]) {
            // go directly to mail
            [self presentMailComposeViewController:email];
        } else if ([MFMessageComposeViewController canSendText]) {
            // go directly to iMessage
            [self presentMessageComposeViewController:email];
        }
        
    } else if (property == kABPersonPhoneProperty) {
        ABMultiValueRef phoneProperty = ABRecordCopyValue(person,property);
        NSString *phone = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(phoneProperty,identifier);
        
        if ([MFMessageComposeViewController canSendText]) {
            [self presentMessageComposeViewController:phone];
        }
    }
    
    //return NO;
    
    
}

///* Called when a member of the address book is selected, we return YES to display the member's details. */
//- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
//    return YES;
//}

/* Called when the user selects a property of a person in their address book (ex. phone, email, location,...)
 This method will allow them to send a text or email inviting them to Anypic.  */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    
    if (property == kABPersonEmailProperty) {
        
        ABMultiValueRef emailProperty = ABRecordCopyValue(person,property);
        NSString *email = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emailProperty,identifier);
        self.selectedEmailAddress = email;
        
        if ([MFMailComposeViewController canSendMail] && [MFMessageComposeViewController canSendText]) {
            // ask user
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Invite %@",@""] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Email", @"iMessage", nil];
            //[actionSheet showFromTabBar:self.tabBarController.tabBar];
            [actionSheet showInView:addressBook.view];
        } else if ([MFMailComposeViewController canSendMail]) {
            // go directly to mail
            [self presentMailComposeViewController:email];
        } else if ([MFMessageComposeViewController canSendText]) {
            // go directly to iMessage
            [self presentMessageComposeViewController:email];
        }
        
    } else if (property == kABPersonPhoneProperty) {
        ABMultiValueRef phoneProperty = ABRecordCopyValue(person,property);
        NSString *phone = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(phoneProperty,identifier);
        
        if ([MFMessageComposeViewController canSendText]) {
            [self presentMessageComposeViewController:phone];
        }
    }
    
    return NO;
}


#pragma mark - MFMessageComposeDelegate

/* Simply dismiss the MFMessageComposeViewController when the user sends a text or cancels */
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
     [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UIActionSheetDelegate


- (void)presentMailComposeViewController:(NSString *)recipient {
   
    
    MFMailComposeViewController *comp=[[MFMailComposeViewController alloc]init];
    [comp setMailComposeDelegate:self];
    if([MFMailComposeViewController canSendMail]) {
        [comp setToRecipients:[NSArray arrayWithObjects:recipient, nil]];
        [comp setSubject:@"Join me on Captionier"];
        // NSData *mediaData = nil;
        // NSString *mediaFileLink = [self.selectObject objectForKey:kPAPFeedMediaLinkKey];
        [comp setMessageBody:@"<h2>Share your pictures, share your story.</h2><p><a href=\"https://itunes.apple.com/us/app/picogram/id825452469?ls=1&mt=8\">Picogram</a> is the easiest way to share videos with your friends. Get the app and share your fun videos with the world.</p><p><a href=\"http://appscrip.com\">Picogram</a> is fully powered by <a href=\"http://parse.com\">Parse</a>.</p>" isHTML:YES];
        [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        [self presentViewController:comp animated:YES completion:nil];
    }
    else {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Your device is not currently connected to an email account."delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alrt show];
    }

}

- (void)presentMessageComposeViewController:(NSString *)recipient {
    // Create the compose text message view controller
    MFMessageComposeViewController *composeTextViewController = [[MFMessageComposeViewController alloc] init];
    
    // Send the destination phone number and a default text
    [composeTextViewController setMessageComposeDelegate:self];
    [composeTextViewController setRecipients:[NSArray arrayWithObjects:recipient, nil]];
    [composeTextViewController setBody:@"Check out Captionier! https://itunes.apple.com/us/app/picogram/id825452469?ls=1&mt=8"];
    
    [self dismissViewControllerAnimated:NO completion:^(){
        [self presentViewController:composeTextViewController animated:NO completion:nil];
    }];
}

-(void)didFacebookUserLogin:(BOOL)login withDetail:(NSDictionary *)userInfo{
    if (login) {
        NSLog(@"Do something when user is loggin with data %@",userInfo);
        
        [[PFUser currentUser] setObject:userInfo[@"FacebookId"] forKey:kPAPUserFacebookIDKey];
        [[PFUser currentUser] saveEventually];
        
        [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                //[self facebookRequestDidLoad:result];
                NSArray *data = [result objectForKey:@"data"];
                
                if (data) {
                    // we have friends data
                    NSMutableArray *facebookIds = [[NSMutableArray alloc] initWithCapacity:[data count]];
                    for (NSDictionary *friendData in data) {
                        if (friendData[@"id"]) {
                            [facebookIds addObject:friendData[@"id"]];
                        }
                    }
                    
                    // cache friend data
                    [[PAPCache sharedCache] setFacebookFriends:facebookIds];
                    
                    
                    FollowersFollowingViewController *followersVC = [[FollowersFollowingViewController alloc] init];
                    followersVC.type = FacebookFriends;
                    followersVC.user = [PFUser currentUser];
                    followersVC.title =  @"Following";
                    [self.navigationController pushViewController:followersVC animated:YES];
                }
            } else {
                //[self facebookRequestDidFailWithError:error];
            }
        }];
    }
}

@end
