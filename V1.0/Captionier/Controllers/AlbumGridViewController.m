//
//  AlbumGridViewController.m
//  FriendPickerSample
//
//  Created by Surender Rathore on 08/12/13.
//
//

#import "AlbumGridViewController.h"
#import "EditProfileViewController.h"
#import "SDWebImageDownloader.h"
#import "UIImageView+WebCache.h"
#import "PAPHomeViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface AlbumGridViewController ()
@property(nonatomic,retain)IBOutlet UICollectionView *collectionViewPack;
@property(nonatomic,strong)NSArray *albumImages;
@end

@implementation AlbumGridViewController
@synthesize collectionViewPack;
@synthesize albumId;
@synthesize albumImages;
@synthesize target;
@synthesize user;

static NSString * const kCellReuseIdentifier = @"collectionViewCell";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/**
 *  Configures the navigation bar left button as custome button
 *
 */
-(void)customizeNavigationBackButton {
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
  
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
}


/**
 *  go back to previous controller
 *
 *  @param sender backBtn pressed
 */
-(void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    //Create collection View
    [self.collectionViewPack registerNib:[UINib nibWithNibName:@"CollectionViewItem" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifier];
    self.collectionViewPack.backgroundColor = [UIColor clearColor];
    
    self.title = @" ";
    [super viewDidLoad];
    [self customizeNavigationBackButton];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(95, 95)];
    flowLayout.minimumInteritemSpacing = 0;
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [self.collectionViewPack setCollectionViewLayout:flowLayout];
    [self.collectionViewPack setAllowsSelection:YES];
    self.collectionViewPack.delegate=self;
    
    [self getThumbnailPics];
}

/**
 *  query to get users photos as thumbnail photos
 */
-(void)getThumbnailPics {
    
    PFQuery *query = [PFQuery queryWithClassName:kPAPPhotoClassKey];
    query.cachePolicy = kPFCachePolicyNetworkOnly;
   
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
   
    [query whereKey:kPAPPhotoUserKey equalTo:self.user];
    [query orderByDescending:@"createdAt"];
    [query includeKey:kPAPPhotoUserKey];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects,NSError *error){
        
        if (!error) {
           
            self.albumImages = [[NSArray alloc] initWithArray:objects];
            [self.collectionViewPack reloadData];
        }
        
    }];
    
}
-(void)checkFacebookSession {
    if (!FBSession.activeSession.isOpen) {
        NSArray *permission = @[@"user_photos",@"friends_photos",@"read_stream"];
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession openActiveSessionWithReadPermissions:permission
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error) {
                                          if (error) {
                                              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                  message:error.localizedDescription
                                                                                                 delegate:nil
                                                                                        cancelButtonTitle:@"OK"
                                                                                        otherButtonTitles:nil];
                                              [alertView show];
                                          } else if (session.isOpen) {
                                              //[self pickFriendsButtonClick:sender];
                                              [self getAlbumImages];
                                              //                [self getAlbumImages];
                                          }
                                      }];
        
    }
    else {
        NSLog(@"opened");
        [self getAlbumImages];
    }

}
//-(void)viewDidAppear:(BOOL)animated {
//    
//    
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //return _totalImagesData.count;
    return self.albumImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier forIndexPath:indexPath];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    imageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    imageView.layer.borderWidth = 1;
//    UIImageView *imageIndicator = (UIImageView*)[cell viewWithTag:200];
//    imageView.clipsToBounds = YES;
    
    
    
    
    PFObject *object = albumImages[indexPath.item];
    PFFile *file = [object objectForKey:kPAPPhotoThumbnailKey];
    [imageView setImageWithURL:[NSURL URLWithString:file.url]
                   placeholderImage:[UIImage imageNamed:@"thumbline_image_boader"]];
    
//    if ([[object objectForKey:kPAPPhotoBought] boolValue]) {
//        imageIndicator.image = [UIImage imageNamed:@"correct_icon"];
//    }
//    else if ([[object objectForKey:kPAPPhotoBlock] boolValue]) {
//        
//        imageIndicator.image = [UIImage imageNamed:@"lock_icon"];
//    }
//    else  {
//        imageIndicator.image = nil;
//    }

    return cell;
    
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:@"1" forKey:@"SingleObject"];

    
    PAPHomeViewController *home = [[PAPHomeViewController alloc]initWithStyle:UITableViewStylePlain];
    home.selectedObject = albumImages[indexPath.item];
    home.isNeedPullToRefresh = NO;
    [self.navigationController pushViewController:home animated:YES];
    
}



/**
 *  query to get images from facebook
 */
-(void)getAlbumImages {
    
    
    NSString *query = [NSString stringWithFormat:@"SELECT src_small, src_big ,object_id FROM photo WHERE  aid = '%@'",self.albumId];
    
    NSLog(@"query %@",query);
    
    // Set up the query parameter
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    // Make the API request that uses FQL
    [FBRequestConnection startWithGraphPath:@"/fql"
                                 parameters:queryParam
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              if (error) {
                                  
                                  NSLog(@"erro %@",[error localizedDescription]);
                                  
                              } else {
                                  
                                  NSLog(@"result %@",result);
                                  //[self parseAlbums:result];
                                  self.albumImages = [[NSArray alloc] initWithArray:result[@"data"]];
                                  [self.collectionViewPack reloadData];
                                  
                              }
                          }];
}



@end
