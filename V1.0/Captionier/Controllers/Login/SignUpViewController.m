//
//  SignUpViewController.m
//  Pimpstagram
//
//  Created by rahul Sharma on 12/11/13.
//
//

#import "SignUpViewController.h"
#import "WebViewController.h"

@interface SignUpViewController ()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate, WebViewControllerDelegate>
@property (nonatomic, strong) UIImageView *fieldsBackground;
@property (nonatomic, strong) UIImageView *profileImageView;
@property (nonatomic, strong) NSString *eula;
@end

@implementation SignUpViewController
{
    UIView *displayNameView;
    UIView *emailView;
    UIView *userNameView;
    UIView *passwordView;
    UIImageView *displayNameImageView;
    UIImageView *emailImageView;
    UIImageView *userNameImageView;
    UIImageView *passwordImageView;
    UIButton *btnCheckBox;
}

@synthesize fieldsBackground;
@synthesize profileImageView;
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}


/**
 * removing profile picture
 * setting default image when profilepicture remove
 */
-(void)deleteProfilePicture {
    NSURL *cachesDirectoryURL = [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject]; // iOS Caches directory
    
   // NSURL *profilePictureCacheURL = [cachesDirectoryURL URLByAppendingPathComponent:@"FacebookProfilePicture.jpg"];
    NSURL *profilePictureCacheURL = [cachesDirectoryURL URLByAppendingPathComponent:@"FacebookProfilePicture.jpg"];
    //NSData *pictureData = nil;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[profilePictureCacheURL path]]) {
        [[NSFileManager defaultManager] removeItemAtPath:[profilePictureCacheURL path] error:nil];
    }

}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self deleteProfilePicture];
    
   // [self sendRequestForEULA];
    
    //self.view.backgroundColor =  [UIColor blackColor];//UIColorFromRGB(0x523c88);

    self.profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(115+10 ,110 , 80, 80)];
    self.profileImageView.image = [UIImage imageNamed:@"signup_profile_default_image_frame.png"];
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
    self.profileImageView.clipsToBounds = YES;
    [self.profileImageView setClipsToBounds:YES];
    [self.signUpView addSubview:self.profileImageView];
    self.profileImageView.layer.borderColor = [UIColor grayColor].CGColor;
    self.profileImageView.layer.borderWidth = 1;
    self.profileImageView.tag = 123;

    
    
    /*self.profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(115 ,110 , 90, 90)];
    self.profileImageView.image = [UIImage imageNamed:@"default_profile_pic@2x.png"];
    [self.signUpView addSubview:self.profileImageView];
    self.profileImageView.layer.borderColor = [UIColor grayColor].CGColor;
    self.profileImageView.layer.borderWidth = 1;
    self.profileImageView.tag = 123;*/
    
    UIImageView *editProfImg = [[UIImageView alloc] initWithFrame:CGRectMake(176 ,110 , 29, 27)];
    editProfImg.image = [UIImage imageNamed:@"edit_btn_off"];
    //[self.signUpView addSubview:editProfImg];
    if (![Helper isPhone5]) {
        self.profileImageView.frame = CGRectMake(115 ,110 , 90, 90);
    }
    
    UIButton *buttonSelectProfilePic = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSelectProfilePic.frame = self.profileImageView.frame;
    [buttonSelectProfilePic addTarget:self action:@selector(selectPicture) forControlEvents:UIControlEventTouchUpInside];
    [self.signUpView addSubview:buttonSelectProfilePic];
    

    
    if ([UIScreen mainScreen].bounds.size.height > 480.0f) {
        // for the iPhone 5
        self.view.backgroundColor = [UIColor colorWithRed:255/255.0f green:151/255.0f blue:3/255.0f alpha:1.0f];
       // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-568h"]];
    } else {
       self.view.backgroundColor = [UIColor colorWithRed:255/255.0f green:151/255.0f blue:3/255.0f alpha:1.0f];
       // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    }

    

    //[self.view setBackgroundColor:[UIColor whiteColor]];
    [self.signUpView setLogo:nil];
    
    
    displayNameView = [[UIView alloc] initWithFrame:CGRectMake(25, 230, 275, 40)];
    displayNameView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"username_tab"]];
    //[self.signUpView addSubview:displayNameView];
    [self.signUpView insertSubview:displayNameView belowSubview:self.signUpView.additionalField];
    
    displayNameImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 20, 20)];
    [displayNameImageView setImage:[UIImage imageNamed:@"profilescreen_usericon.png"]];
    //[self.view insertSubview:displayNameView aboveSubview:self.signUpView.additionalField];
    [displayNameView addSubview:displayNameImageView];
    
    userNameView = [[UIView alloc] initWithFrame:CGRectMake(25, 271, 275, 40)];
    userNameView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"username_tab"]];
    //[self.signUpView addSubview:userNameView];
    [self.signUpView insertSubview:userNameView belowSubview:self.signUpView.usernameField];
    
    userNameImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 20, 20)];
    [userNameImageView setImage:[UIImage imageNamed:@"profilescreen_usericon.png"]];
    [userNameView addSubview:userNameImageView];
    //[self.view insertSubview:userNameImageView aboveSubview:self.signUpView.usernameField];
    
    emailView = [[UIView alloc] initWithFrame:CGRectMake(25, 312, 275, 40)];
    emailView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"username_tab"]];
    //[self.signUpView addSubview:emailView];
    [self.signUpView insertSubview:emailView belowSubview:self.signUpView.emailField];
    
    emailImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 20, 20)];
    [emailImageView setImage:[UIImage imageNamed:@"profilescreen_mail_icon.png"]];
    [emailView addSubview:emailImageView];
    
    
    
    passwordView = [[UIView alloc] initWithFrame:CGRectMake(25, 353, 275, 40)];
    passwordView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"username_tab"]];
    //[self.view addSubview:passwordView];
    [self.signUpView insertSubview:passwordView belowSubview:self.signUpView.passwordField];
    passwordImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 20, 20)];
    [passwordImageView setImage:[UIImage imageNamed:@"profilescreen_password_icon.png"]];
    [passwordView addSubview:passwordImageView];
    
    
    

   
    
    // Change button apperance
    [self.signUpView.dismissButton setImage:[UIImage imageNamed:@"close_btn@2x.png"] forState:UIControlStateNormal];
    //[self.signUpView.dismissButton setImage:[UIImage imageNamed:@"ExitDown.png"] forState:UIControlStateHighlighted];
    
    [self.signUpView.signUpButton setImage:nil forState:UIControlStateNormal];
    [self.signUpView.signUpButton setImage:nil forState:UIControlStateHighlighted];
    [self.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@"login_tab_off"] forState:UIControlStateNormal];
     [self.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@"login_tab_on"] forState:UIControlStateHighlighted];
    self.signUpView.signUpButton.titleLabel.shadowOffset = CGSizeMake(0, 0);
    [self.signUpView.signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.signUpView.signUpButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];

    //[self.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@"SignUpDown.png"] forState:UIControlStateHighlighted];
    //[self.signUpView.signUpButton setTitle:@"" forState:UIControlStateNormal];
    //[self.signUpView.signUpButton setTitle:@"" forState:UIControlStateHighlighted];
    
    
    // Add background for fields
    //[self setFieldsBackground:[[UIImageView alloc] initWithImage:[ui]]];
    //[self.signUpView insertSubview:fieldsBackground atIndex:1];
    
    // Remove text shadow
    CALayer *layer = self.signUpView.usernameField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.emailField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.passwordField.layer;
    layer.shadowOpacity = 0.0f;
   
    layer = self.signUpView.additionalField.layer;
    layer.shadowOpacity = 0.0f;
    //layer = self.signUpView.additionalField.layer;
    //layer.shadowOpacity = 0.0f;
    
    // Set text color
//    [self.signUpView.usernameField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
//    [self.signUpView.passwordField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
//    [self.signUpView.emailField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
//    [self.signUpView.additionalField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
    
    
    self.signUpView.usernameField.borderStyle = UITextBorderStyleNone;
    self.signUpView.emailField.borderStyle = UITextBorderStyleNone;
    self.signUpView.passwordField.borderStyle = UITextBorderStyleNone;
    
    self.signUpView.additionalField.background = UITextBorderStyleNone;
    
    self.signUpView.usernameField.textColor = [UIColor blackColor];
    self.signUpView.emailField.textColor = [UIColor blackColor];
    self.signUpView.passwordField.textColor = [UIColor blackColor];
    
    self.signUpView.additionalField.textColor = [UIColor blackColor];
    
    self.signUpView.usernameField.textAlignment = NSTextAlignmentLeft;
    self.signUpView.emailField.textAlignment = NSTextAlignmentLeft;
    self.signUpView.passwordField.textAlignment = NSTextAlignmentLeft;
    
    self.signUpView.additionalField.textAlignment = NSTextAlignmentLeft;
    
    //additional textFeild
    self.signUpView.additionalField.placeholder = @"display name";

    self.signUpView.usernameField.delegate = self;
    self.signUpView.emailField.delegate = self;
    self.signUpView.passwordField.delegate = self;
    self.signUpView.additionalField.delegate = self;

    
    [self.signUpView bringSubviewToFront:self.signUpView.additionalField];
    [self.signUpView bringSubviewToFront:self.signUpView.usernameField];
    [self.signUpView bringSubviewToFront:self.signUpView.passwordField];
    [self.signUpView bringSubviewToFront:self.signUpView.emailField];
    
    //set placeholder text color
    UIColor *color = [UIColor blackColor];
    self.signUpView.additionalField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Display name" attributes:@{NSForegroundColorAttributeName: color}];
    self.signUpView.additionalField.textAlignment = NSTextAlignmentCenter;
    self.signUpView.usernameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    self.signUpView.usernameField.textAlignment = NSTextAlignmentCenter;
    self.signUpView.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    self.signUpView.emailField.textAlignment = NSTextAlignmentCenter;
    self.signUpView.passwordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    self.signUpView.passwordField.textAlignment = NSTextAlignmentCenter;

    
    self.signUpView.additionalField.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    self.signUpView.usernameField.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    self.signUpView.emailField.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    self.signUpView.passwordField.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    
    self.signUpView.additionalField.returnKeyType = UIReturnKeyDefault;
    self.signUpView.usernameField.returnKeyType = UIReturnKeyDefault;
    self.signUpView.emailField.returnKeyType = UIReturnKeyDefault;
    self.signUpView.passwordField.returnKeyType = UIReturnKeyGo;

    
    
    btnCheckBox = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCheckBox.tag = 500;
    btnCheckBox.frame = CGRectMake(25, 0, 15, 15);
    [btnCheckBox setBackgroundImage:[UIImage imageNamed:@"profilescreen_check_box_unchecked.png"] forState:UIControlStateNormal];
    [btnCheckBox setBackgroundImage:[UIImage imageNamed:@"profilescreen_check_box_checked.png"] forState:UIControlStateSelected];
    [btnCheckBox addTarget:self action:@selector(checkboxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnCheckBox];
    
    
    UIButton *btnTnC = [UIButton buttonWithType:UIButtonTypeCustom];
    btnTnC.frame = CGRectMake(30 +15 , 0,150, 15);
    //btnTnC.titleLabel.font = [UIFont systemFontOfSize:15];
    //btnTnC.titleLabel.textColor = [UIColor darkGrayColor];

    btnTnC.tag = 600;
    [btnTnC setTitle:@"Terms & Conditions." forState:UIControlStateNormal];
    [Helper setButton:btnTnC Text:@"Terms & Conditions." WithFont:@"HelveticaNeue" FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    [btnTnC setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [btnTnC setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
   
    [btnTnC addTarget:self action:@selector(termsAndConditionClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnTnC];

    [self.signUpView bringSubviewToFront:self.signUpView.signUpButton];
   

}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    
    
    // Move all fields down on smaller screen sizes
    float yOffset = [UIScreen mainScreen].bounds.size.height <= 480.0f ? 40.0 : 0.0f;
    
    [self.signUpView bringSubviewToFront:self.signUpView.dismissButton];
    
    CGRect fieldFrame = self.signUpView.usernameField.frame;
    //fieldFrame.origin.y += 40;
    self.signUpView.usernameField.frame = fieldFrame;
    //fieldFrame.size.height = 35;
    
    [self.signUpView.dismissButton setFrame:CGRectMake(10.0f, 10.0f, 87.5f, 45.5f)];
    [self.signUpView.logo setFrame:CGRectMake(145, 50.0f, 30, 38)];
    
    
    [self.fieldsBackground setFrame:CGRectMake(35.0f, fieldFrame.origin.y + yOffset, 250.0f, 174.0f)];
    

    [self.signUpView.additionalField setFrame:CGRectMake(60-20,
                                                         230,
                                                         240,
                                                         40)];
    
    
    
    

//    UIImageView *hImageView ;
//    CGRect frame ;
    
    [self.signUpView.usernameField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                       fieldFrame.origin.y + yOffset + 83,
                                                       fieldFrame.size.width - 10.0f,
                                                       40)];
    yOffset += fieldFrame.size.height;
    
//    hImageView = (UIImageView*)[self.view viewWithTag:100];
//    frame = hImageView.frame;
//    frame.origin.y = self.signUpView.usernameField.frame.origin.y + 35;
//    hImageView.frame = frame;
    
    
    
    [self.signUpView.emailField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                       fieldFrame.origin.y + yOffset + 82,
                                                       fieldFrame.size.width - 10.0f,
                                                       40)];
    yOffset += fieldFrame.size.height;
    
//    hImageView = (UIImageView*)[self.view viewWithTag:300];
//    frame = hImageView.frame;
//    frame.origin.y = self.signUpView.emailField.frame.origin.y + 35;
//    hImageView.frame = frame;
    
    [self.signUpView.passwordField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                    fieldFrame.origin.y + yOffset+78,
                                                    fieldFrame.size.width - 10.0f,
                                                    40)];
    
    
//    hImageView = (UIImageView*)[self.view viewWithTag:200];
//    frame = hImageView.frame;
//    frame.origin.y = self.signUpView.passwordField.frame.origin.y + 35;
//    hImageView.frame = frame;
//    
//    
//    hImageView = (UIImageView*)[self.view viewWithTag:400];
//    frame = hImageView.frame;
//    frame.origin.y = self.signUpView.additionalField.frame.origin.y + 35;
//    hImageView.frame = frame;
    
    UIButton *btn = (UIButton*)[self.view viewWithTag:500];
    CGRect buttonFrame = btn.frame;
    buttonFrame.origin.x = self.signUpView.passwordField.frame.origin.x + 15;
    buttonFrame.origin.y = self.signUpView.passwordField.frame.origin.y + 50;
    btn.frame = buttonFrame;
    
    btn = (UIButton*)[self.view viewWithTag:600];
    buttonFrame = btn.frame;
    buttonFrame.origin.x = self.signUpView.passwordField.frame.origin.x + 30;
    buttonFrame.origin.y = self.signUpView.passwordField.frame.origin.y + 50;
   // buttonFrame.origin.y = self.signUpView.passwordField.frame.origin.y + 30 +28;
    btn.frame = buttonFrame;
    
    
    
    buttonFrame =  self.signUpView.dismissButton.frame;
    buttonFrame.origin.y = 20;
    buttonFrame.size = CGSizeMake(20, 20);
    self.signUpView.dismissButton.frame = buttonFrame;
    
    
    
    if (![Helper isPhone5]) {
        
        [self.signUpView.signUpButton setFrame:CGRectMake(20, 430.0f, 280, 42)];

        CGRect frame = displayNameView.frame;
        
        CGRect txtFeildframe = self.signUpView.additionalField.frame;
        txtFeildframe.origin.y = frame.origin.y -5;
        self.signUpView.additionalField.frame = txtFeildframe;
        
        frame = userNameView.frame;
        txtFeildframe = self.signUpView.usernameField.frame;
        txtFeildframe.origin.y = frame.origin.y + 1;
        self.signUpView.usernameField.frame = txtFeildframe;
        
        frame = emailView.frame;
        txtFeildframe = self.signUpView.emailField.frame;
        txtFeildframe.origin.y = frame.origin.y + 1;
        self.signUpView.emailField.frame = txtFeildframe;
        
        frame = passwordView.frame;
        txtFeildframe = self.signUpView.passwordField.frame;
        txtFeildframe.origin.y = frame.origin.y + 1;
        self.signUpView.passwordField.frame = txtFeildframe;
    }
    else {
        
        [self.signUpView.signUpButton setFrame:CGRectMake(20+10, 430, 280-10, 42)];
       // [self.signUpView.signUpButton setFrame:CGRectMake(20+10, 400-5, 280-10, 42)];//20, 460.0f, 280, 42

    }
}

/**
 *  listing types to select image for profile image
 */

-(void)selectPicture {
    UIActionSheet *acctionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Library", nil];
    acctionSheet.tag = 200;
    [acctionSheet showInView:self.view];

}
#pragma mark ActionSheetButton Methods

/**
 *  This method will call once press camera is selected to take profile image
 *  it will set to front camera as default camera
 *
 *  @param sender btnPress
 */
-(void)cameraButtonClicked:(id)sender
{
    
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.allowsEditing = YES;
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerCameraDeviceFront;
    [self presentViewController:picker animated:YES completion:nil];
}

/**
 *  This method will select image from image library
 *
 *  @param sender btnPress
 */

-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    // picker.contentSizeForViewInPopover = CGSizeMake(400, 800);
    //    [self presentViewController:picker animated:YES completion:nil];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        
        
        
        //[popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}
#pragma UIImagePickerController Delegate

/**
 *  This method  perform once image is selected
 *
 *  @param picker imagePicker
 *  @param info   image
 */
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage  *albumImg = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    
    profileImageView.image = albumImg;
   // profileImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"FacebookProfilePicture.jpg"];
    
    NSData *data = UIImageJPEGRepresentation(albumImg,1.0);
    [data writeToFile:getImagePath atomically:YES];
   

    
}

/**
 *  This method gives option to take image/video from camera or library
 *
 *  @param actionSheet camera and library
 *  @param buttonIndex 0  and 1
 */

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 200) {
        switch (buttonIndex) {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            default:
                break;
        }
    }
    
}

-(void)checkboxButtonClicked:(UIButton*)sender{
    if (sender.isSelected) {
        [sender setSelected:NO];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"EULAAccepted"];
    }
    else {
        [sender setSelected:YES];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"EULAAccepted"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}


/**
 *  parse query to save data on parse
 */
-(void)sendRequestForEULA {
    PFQuery *query = [PFQuery queryWithClassName:@"EULA"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        if (!error) {
            PFObject *object = [array lastObject];
            self.eula = [object objectForKey:@"eula"];
            [[NSUserDefaults standardUserDefaults] setObject:self.eula forKey:@"EulaURL"];
            
            
        }
    }];
}


/**
 *  This method will open webView for terms and conditions
 *
 *  @param sender btnPress
 */
-(void)termsAndConditionClicked:(id)sender {
    
    //NSString *url = [[NSUserDefaults standardUserDefaults] objectForKey:@"EulaURL"];
    WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
    webView.title = @"Terms of Service";
    //webView.weburl = @"http://108.166.190.172:81/vind/picogramclone/userlice.php";//[[NSUserDefaults standardUserDefaults] objectForKey:@"EulaURL"];;
    webView.weburl = TermsOfService;
    webView.delegate = self;
   // webView.hidesBottomBarWhenPushed = YES;
    //[self.navigationController pushViewController:webView animated:YES];
    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:webView];
    [self presentViewController:navC animated:YES completion:nil];
}
/**
 *  To accept  or reject
 *
 *  @param acceptedOrRejected boolean value
 */
- (void) dismissWithOption:(BOOL)acceptedOrRejected
{
    if (acceptedOrRejected) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"EULAAccepted"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        btnCheckBox.selected = YES;
    }
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    UITextField *nextTF;
    
    if ([textField isEqual:self.signUpView.usernameField]) {
        nextTF = self.signUpView.emailField;
    }
    if ([textField isEqual:self.signUpView.additionalField]) {
        nextTF = self.signUpView.usernameField;
    }
    if ([textField isEqual:self.signUpView.emailField]) {
        nextTF = self.signUpView.passwordField;
    }
    if ([textField isEqual:self.signUpView.passwordField]) {
        nextTF = nil;
    }
    
    if (nextTF) {
        [nextTF becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
        [self.signUpView.signUpButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    
    return YES;



}

@end
