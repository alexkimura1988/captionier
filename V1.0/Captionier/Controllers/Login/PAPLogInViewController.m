//
//  PAPLogInViewController.m
//  Anypic
//
//  Created by Mattieu Gamache-Asselin on 5/17/12.
//

#import "PAPLogInViewController.h"
#import "WebViewController.h"

#define AppLogo @"1login_captioner_logo.png"
#define CaptionerText @"1login_captioner_logo.png"

@implementation PAPLogInViewController
{
    UIView *userNameView;
    UIView *passwordView;
    UIImageView *userNameImageView;
    UIImageView *passwordImageView;
    UIImageView *logoImageView;
}

@synthesize btnCheckBox;

#pragma mark - UIViewControllerz

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:255/255.0f green:151/255.0f blue:3/255.0f alpha:1.0f];
    
    //logoImage
    
    logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_logo.png"]];
    [self.logInView setLogo:logoImageView];
    
    [self.logInView.passwordForgottenButton setImage:nil forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setImage:nil forState:UIControlStateHighlighted];
    [self.logInView.passwordForgottenButton setBackgroundImage:nil forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.logInView.passwordForgottenButton setTitle:@"Forgot your password?" forState:UIControlStateNormal];
    self.logInView.passwordForgottenButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.logInView.passwordForgottenButton setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    
  
    
    userNameView = [[UIView alloc] initWithFrame:CGRectMake(25, 173, 270, 40)];
    userNameView.backgroundColor = [UIColor clearColor];
    UIImageView *unBGimgvw = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270, 40)];
    unBGimgvw.image = [UIImage imageNamed:@"login_tab_off"];
    [userNameView addSubview:unBGimgvw];
    [self.logInView insertSubview:userNameView belowSubview:self.logInView.usernameField];
   // [self.view addSubview:userNameView];
    //userNameImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 20, 20)];
    //[userNameImageView setImage:[UIImage imageNamed:@"profilescreen_usericon.png"]];
    //[userNameView addSubview:userNameImageView];
    
    passwordView = [[UIView alloc] initWithFrame:CGRectMake(25, 230, 270, 40)];
    
    UIImageView *pvBGimgvw = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 270, 40)];
    pvBGimgvw.image = [UIImage imageNamed:@"login_tab_off"];
    [passwordView addSubview:pvBGimgvw];
    [self.logInView insertSubview:passwordView belowSubview:self.logInView.passwordField];
    //[self.view addSubview:passwordView];
    //passwordImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 20, 20)];
    //[passwordImageView setImage:[UIImage imageNamed:@"profilescreen_password_icon.png"]];
    //[passwordView addSubview:passwordImageView];
    
    btnCheckBox = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCheckBox.tag = 500;
    btnCheckBox.frame = CGRectMake(30, 290, 15, 15);
    [btnCheckBox setBackgroundImage:[UIImage imageNamed:@"profilescreen_check_box_unchecked.png"] forState:UIControlStateNormal];
    [btnCheckBox setBackgroundImage:[UIImage imageNamed:@"profilescreen_check_box_checked.png"] forState:UIControlStateSelected];
    [btnCheckBox addTarget:self action:@selector(checkboxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addSubview:btnCheckBox];
    
    
    UILabel *labelRememberMe = [[UILabel alloc] initWithFrame:CGRectMake(30 +20 , 290,60, 15)];
    
    [Helper setToLabel:labelRememberMe Text:@"Or Sign Up Via" WithFont:@"HelveticaNeue" FSize:14 Color:[UIColor whiteColor]];
    //[self.logInView insertSubview:labelRememberMe aboveSubview:self.logInView.passwordField];
    //[self.view addSubview:labelRememberMe];
    
    
    UIButton *btnCheckBox2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCheckBox2.tag = 501;
    btnCheckBox2.frame = CGRectMake(155, 274, 15, 15);
    [btnCheckBox2 setBackgroundImage:[UIImage imageNamed:@"profilescreen_check_box_unchecked.png"] forState:UIControlStateNormal];
    [btnCheckBox2 setBackgroundImage:[UIImage imageNamed:@"profilescreen_check_box_checked.png"] forState:UIControlStateSelected];
    [btnCheckBox2 addTarget:self action:@selector(checkboxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addSubview:btnCheckBox2];
    
    
    UIButton *btnTnC = [UIButton buttonWithType:UIButtonTypeCustom];
    btnTnC.frame = CGRectMake(150+10 , 274,150, 15);
    //btnTnC.titleLabel.font = [UIFont systemFontOfSize:15];
    //btnTnC.titleLabel.textColor = [UIColor darkGrayColor];
    
    btnTnC.tag = 601;
    [btnTnC setTitle:@"Terms & Condition" forState:UIControlStateNormal];
    [Helper setButton:btnTnC Text:@"Terms & Condition" WithFont:@"HelveticaNeue" FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    [btnTnC setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [btnTnC setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    [btnTnC addTarget:self action:@selector(termsAndConditionClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addSubview:btnTnC];
    
    UILabel *labelSignUpVia = [[UILabel alloc] initWithFrame:CGRectMake(60,450,200, 15)];
    labelSignUpVia.text = @"Or Sign Up Via";
    labelSignUpVia.textAlignment = NSTextAlignmentCenter;
    labelSignUpVia.textColor = [UIColor whiteColor];
    [labelSignUpVia setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    
    
   // [Helper setToLabel:labelRememberMe Text:@"Or Sign Up Via" WithFont:@"HelveticaNeue" FSize:13 Color:[UIColor blueColor]];
    // labelSignUpVia.backgroundColor = [UIColor yellowColor];
    // [self.logInView insertSubview:labelSignUpVia aboveSubview:self.logInView.passwordField];
    [self.view addSubview:labelSignUpVia];
    
    
    if (![Helper isPhone5]) {
        
        userNameView.frame = CGRectMake(25, 118, 270, 40);//25, 118, 270, 40
        passwordView.frame = CGRectMake(25, 159, 270, 40);
        btnCheckBox.frame = CGRectMake(25, 209, 15, 15);
        labelRememberMe.frame = CGRectMake(45, 209, 150, 15);
    }
    else {
        userNameView.frame = CGRectMake(25, 173-10, 270, 40);//25, 173, 270, 40
        passwordView.frame = CGRectMake(25, 214, 270, 40);
        btnCheckBox.frame = CGRectMake(25, 274, 15, 15);
        labelRememberMe.frame = CGRectMake(45, 274, 150, 15);
    }
    
    
    
    //set placeholder text color
    UIColor *color = WHITE_COLOR;
    self.logInView.usernameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    self.logInView.passwordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    
    
    // [self.logInView addSubview:textLabel];
    
    
    //    UIImage *fbButtonImage = [UIImage imageNamed:@"lgoin_facebook_btn.png"];
    //    UIButton *btnFB = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [btnFB setBackgroundImage:fbButtonImage forState:UIControlStateNormal];
    //    btnFB.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2 - fbButtonImage.size.width/2, [UIScreen mainScreen].bounds.size.height - 92, fbButtonImage.size.width, fbButtonImage.size.height);
    //    [btnFB addTarget:self action:@selector(fbButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    //[self.logInView addSubview:btnFB];
    
    [self.logInView.facebookButton setImage:nil forState:UIControlStateNormal];
    [self.logInView.facebookButton setImage:nil forState:UIControlStateHighlighted];
    //[self.logInView.facebookButton setBackgroundImage:[UIImage imageNamed:@"lgoin_facebook_btn.png"] forState:UIControlStateHighlighted];
    [self.logInView.facebookButton setBackgroundImage:[UIImage imageNamed:@"login_facebook_btn_off"] forState:UIControlStateNormal];//sign_up_fb_off
    [self.logInView.facebookButton setBackgroundImage:[UIImage imageNamed:@"login_facebook_btn_on"] forState:UIControlStateHighlighted];
    [self.logInView.facebookButton setTitle:@"" forState:UIControlStateNormal];
    [self.logInView.facebookButton setTitle:@"" forState:UIControlStateHighlighted];
    
    
    
    [self.logInView.twitterButton setImage:nil forState:UIControlStateNormal];
    [self.logInView.twitterButton setImage:nil forState:UIControlStateHighlighted];
    //[self.logInView.facebookButton setBackgroundImage:[UIImage imageNamed:@"lgoin_facebook_btn.png"] forState:UIControlStateHighlighted];
    [self.logInView.twitterButton setBackgroundImage:[UIImage imageNamed:@"login_twitter_btn"] forState:UIControlStateNormal];
    [self.logInView.twitterButton setBackgroundImage:[UIImage imageNamed:@"login_twitter_btn"] forState:UIControlStateHighlighted];
    [self.logInView.twitterButton setTitle:@"" forState:UIControlStateNormal];
    [self.logInView.twitterButton setTitle:@"" forState:UIControlStateHighlighted];
    [self.logInView.twitterButton setImage:nil forState:UIControlStateNormal];
    
        CGRect twitterButtonframe = self.logInView.twitterButton.frame;
        twitterButtonframe.size.width = 289;
        twitterButtonframe.size.height = 42;
        twitterButtonframe.origin.y+= 10;
        self.logInView.twitterButton.frame = twitterButtonframe;
    
    
    self.logInView.usernameField.borderStyle = UITextBorderStyleNone;
    self.logInView.usernameField.textAlignment = NSTextAlignmentLeft;
    
    self.logInView.passwordField.borderStyle = UITextBorderStyleNone;
    self.logInView.passwordField.textAlignment = NSTextAlignmentLeft;
    //self.logInView.usernameField.background = [UIImage imageNamed:@"login_btn_login_on@2x"];
    
    
    self.logInView.usernameField.textColor = WHITE_COLOR;
     self.logInView.usernameField.textAlignment = NSTextAlignmentCenter;
    self.logInView.passwordField.textColor = WHITE_COLOR;
    self.logInView.passwordField.textAlignment = NSTextAlignmentCenter;
    
    //to bring the subview to front
    
    [self.logInView bringSubviewToFront:self.logInView.usernameField];
    [self.logInView bringSubviewToFront:self.logInView.passwordField];
    
    [self.logInView.signUpButton setImage:nil forState:UIControlStateNormal];
    [self.logInView.signUpButton setImage:nil forState:UIControlStateHighlighted];
    [self.logInView.signUpButton setBackgroundImage:[UIImage imageNamed:@"login_email_btn"] forState:UIControlStateNormal];
    [self.logInView.signUpButton setBackgroundImage:[UIImage imageNamed:@"login_email_btn"] forState:UIControlStateHighlighted];
    [self.logInView.signUpButton setTitle:@" " forState:UIControlStateNormal];
    [self.logInView.signUpButton setTitle:@" " forState:UIControlStateHighlighted];
    [self.logInView.signUpButton.titleLabel setFont:[UIFont boldSystemFontOfSize:25]];
    
    self.logInView.signUpButton.titleLabel.shadowOffset = CGSizeMake(0, 0);
    
    
    
    [self.logInView.logInButton setImage:nil forState:UIControlStateNormal];
    [self.logInView.logInButton setImage:nil forState:UIControlStateHighlighted];
    [self.logInView.logInButton setBackgroundImage:[UIImage imageNamed:@"login_tab_off"] forState:UIControlStateNormal];
    [self.logInView.logInButton setBackgroundImage:[UIImage imageNamed:@"login_tab_on"] forState:UIControlStateHighlighted];
    //self.logInView.logInButton.backgroundColor = [UIColor whiteColor];
    [self.logInView.logInButton setTitle:@"Login" forState:UIControlStateNormal];
    [self.logInView.logInButton setTitle:@"Login" forState:UIControlStateHighlighted];
    
    
    //    CGRect loginbuttonframe = self.logInView.logInButton.frame;
    //    loginbuttonframe.size.width = 181;
    //    loginbuttonframe.size.height = 31;
    //    self.logInView.logInButton.frame = loginbuttonframe;
    
    self.logInView.logInButton.titleLabel.shadowOffset = CGSizeMake(0, 0);
    
    //self.logInView.signUpLabel.textColor = WHITE_COLOR;
    
    //  self.fields = PFLogInFieldsUsernameAndPassword;
    //  self.logInView.usernameField.placeholder = @"Enter username";
    
    //self.logInView.externalLogInLabel.shadowOffset = CGSizeMake(0, 0);
    self.logInView.externalLogInLabel.hidden = YES;
    self.logInView.signUpLabel.text = @"";
    self.logInView.signUpLabel.textColor = WHITE_COLOR;
    CALayer *layer = self.logInView.usernameField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.logInView.passwordField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.logInView.signUpLabel.layer;
    layer.shadowOpacity = 0.0f;
    
    
//    [self.logInView bringSubviewToFront:self.logInView.logInButton];
//    [self.logInView bringSubviewToFront:self.logInView.signUpButton];
//    [self.logInView bringSubviewToFront:self.logInView.facebookButton];
//    [self.logInView bringSubviewToFront:self.logInView.twitterButton];
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSUserDefaults *userDeafults = [NSUserDefaults standardUserDefaults];
    NSString * userN = [userDeafults objectForKey:@"username"];
    NSString *pswd = [userDeafults objectForKey:@"pswd"];
    
    if ([[userDeafults objectForKey:@"RememberMe"] boolValue]) {
        
        [btnCheckBox setSelected:YES];
        [btnCheckBox setBackgroundImage:[UIImage imageNamed:@"profilescreen_check_box_checked.png"] forState:UIControlStateSelected];
        self.logInView.usernameField.text = userN;
        self.logInView.passwordField.text = pswd;
    }
    else{
        [btnCheckBox setSelected:NO];
        [btnCheckBox setBackgroundImage:[UIImage imageNamed:@"profilescreen_check_box_unchecked.png"] forState:UIControlStateNormal];
    }
}





-(void)sendRequestForEULA {
    PFQuery *query = [PFQuery queryWithClassName:@"EULA"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        if (!error) {
            PFObject *object = [array lastObject];
            PFFile *file = [object objectForKey:@"eula"];
            [[NSUserDefaults standardUserDefaults] setObject:file.url forKey:@"EulaURL"];
            
            
        }
    }];
}

/**
 *  webView of terms and conditions
 *
 *  @param sender btnPress
 */
-(void)termsAndConditionClicked:(id)sender {
    
    //NSString *url = [[NSUserDefaults standardUserDefaults] objectForKey:@"EulaURL"];
    WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
    webView.title = @"Terms of Service";
    webView.weburl = TermsOfService;
    // webView.hidesBottomBarWhenPushed = YES;
    //[self.navigationController pushViewController:webView animated:YES];
    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:webView];
    [self presentViewController:navC animated:YES completion:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return toInterfaceOrientation == UIInterfaceOrientationPortrait;
}

/**
 *  facebook button clicked
 *
 *  @param sender ID type & returning Action
 */
-(void)fbButtonClicked:(id)sender
{
    [self.logInView.facebookButton sendActionsForControlEvents:UIControlEventTouchUpInside];
}


/**
 *  called to adjusting the position of its subviews before the view lays out its subviews.
 */
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // Move all fields down on smaller screen sizes
    float yOffset = [UIScreen mainScreen].bounds.size.height <= 480.0f ? 30.0f : 0.0f;
    
    CGRect fieldFrame = self.logInView.usernameField.frame;
    //fieldFrame.size.height = 35;
    
    
    [self.logInView.logo setFrame:CGRectMake(80, 80.0f, 175, 60)];
    
    //################################
    
    [self.logInView.logInButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.logInView.signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.logInView.logInButton setTitle:@"Login" forState:UIControlStateNormal];
    [self.logInView.logInButton setTitle:@"Login" forState:UIControlStateHighlighted];
    
    
    CGRect facebookButtonframe = self.logInView.facebookButton.frame;
    facebookButtonframe.size.width = 55;//289
    facebookButtonframe.size.height = 55;//42
//    facebookButtonframe.origin.y+= 50;
//    facebookButtonframe.origin.y+= 50;
    //facebookButtonframe.origin.y+ = 50;
    self.logInView.facebookButton.frame = facebookButtonframe;
    
    CGRect twitterButtonframe = self.logInView.twitterButton.frame;
    twitterButtonframe.size.width = 55;
    twitterButtonframe.size.height = 55;
    twitterButtonframe.origin.y+= 10;
    self.logInView.twitterButton.frame = twitterButtonframe;
    
    CGRect loginbuttonframe = self.logInView.logInButton.frame;
    loginbuttonframe.origin.x-= 25;//22
    loginbuttonframe.origin.y-= 40;
    
    loginbuttonframe.size.width = 275-5;
    loginbuttonframe.size.height = 42;
    
    self.logInView.logInButton.frame = loginbuttonframe;
    [self.logInView.logInButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
    //################################
    
    
    [self.logInView.usernameField setFrame:CGRectMake(fieldFrame.origin.x + 15.0f,
                                                      userNameView.frame.origin.y + 3,
                                                      //fieldFrame.origin.y + yOffset-30,
                                                      fieldFrame.size.width - 30.0f,
                                                      35)];
    yOffset += fieldFrame.size.height;
    
    UIImageView *hImageView = (UIImageView*)[self.view viewWithTag:100];
    CGRect frame = hImageView.frame;
    frame.origin.y = self.logInView.usernameField.frame.origin.y + 35;
    hImageView.frame = frame;
    
    [self.logInView.passwordField setFrame:CGRectMake(fieldFrame.origin.x + 15.0f,
                                                      passwordView.frame.origin.y + 3,
                                                      //fieldFrame.origin.y + yOffset-35,
                                                      fieldFrame.size.width - 30.0f,
                                                      35)];
    
    
    
    frame = self.logInView.passwordForgottenButton.frame;
    frame.origin.x = 25;//170
    frame.origin.y = self.logInView.passwordField.frame.origin.y + 105;//55
    frame.size.width = 280;//140
    frame.size.height = 20;
    self.logInView.passwordForgottenButton.frame = frame;

    
    frame = self.logInView.logInButton.frame;
    frame.origin.x = 25;
    self.logInView.logInButton.frame = frame;
    
//    yOffset += fieldFrame.size.height;
//    frame = self.logInView.facebookButton.frame;
//    frame.origin.y = self.logInView.logInButton.frame.origin.y + 225;//70
//    frame.origin.x = self.logInView.logInButton.frame.origin.x+160;
//    [self.logInView.facebookButton setFrame:frame];
    
    /***********************/
    yOffset += fieldFrame.size.height;
    frame = self.logInView.twitterButton.frame;
    frame.origin.y = self.logInView.logInButton.frame.origin.y + 225;//70
    frame.origin.x = self.logInView.logInButton.frame.origin.x+160;
    [self.logInView.twitterButton setFrame:frame];
    
    
    /***********************/
//    CGRect twitterButtonframe1 = self.logInView.twitterButton.frame;
//    twitterButtonframe1.origin.x = self.logInView.logInButton.frame.origin.y + 225;
//    twitterButtonframe1.origin.y = self.logInView.facebookButton.frame.origin.y + 52;
//    self.logInView.twitterButton.frame = twitterButtonframe1;
    
    hImageView = (UIImageView*)[self.view viewWithTag:200];
    frame = hImageView.frame;
    frame.origin.y = self.logInView.passwordField.frame.origin.y + 35;
    hImageView.frame = frame;
    
    frame = self.logInView.signUpButton.frame;
    frame.origin.y -= 3;
    frame.origin.x = self.logInView.logInButton.frame.origin.x+50;
    frame.size.width = 55;
    frame.size.height = 55;
    self.logInView.signUpButton.frame = frame;
    
    
    if (![Helper isPhone5]) {
        
        CGRect tfFrame = self.logInView.usernameField.frame;
        
        [self.logInView.usernameField setFrame:CGRectMake(tfFrame.origin.x,
                                                          userNameView.frame.origin.y + 3,
                                                          tfFrame.size.width,
                                                          tfFrame.size.height)];
        tfFrame = self.logInView.passwordField.frame;
        [self.logInView.passwordField setFrame:CGRectMake(tfFrame.origin.x,
                                                          passwordView.frame.origin.y + 3,
                                                          tfFrame.size.width,
                                                          tfFrame.size.height)];
        
       //[self.logInView.logo setFrame:CGRectMake(320/2-188/2, 40.0f, 188, 116/2)];
        
        CGRect loginbuttonframe = self.logInView.logInButton.frame;
        loginbuttonframe.origin.y += 20;
        self.logInView.logInButton.frame = loginbuttonframe;
        
        loginbuttonframe = self.logInView.facebookButton.frame;
        loginbuttonframe.origin.y += 10;
        self.logInView.facebookButton.frame = loginbuttonframe;
        
        loginbuttonframe = self.logInView.twitterButton.frame;
        loginbuttonframe.origin.y += 10;
        self.logInView.twitterButton.frame = loginbuttonframe;
        
        loginbuttonframe = self.logInView.signUpButton.frame;
        loginbuttonframe.origin.y += 10;
        self.logInView.signUpButton.frame = loginbuttonframe;
        
        self.logInView.logo.frame = CGRectMake(100-10, 40.0f, 110, 50);
        
        self.logInView.signUpButton.frame = CGRectMake(0, self.view.frame.size.height-42, 320, 42);
    }
}


/**
 *  saving the username and password for next login
 *
 *  @param sender checkbox selected
 */
-(void)checkboxButtonClicked:(UIButton*)sender{
    if (sender.isSelected) {
        [sender setSelected:NO];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"pswd"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"username"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RememberMe"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    else {
        [sender setSelected:YES];
        [[NSUserDefaults standardUserDefaults]setValue:self.logInView.passwordField.text forKey:@"pswd"];
        [[NSUserDefaults standardUserDefaults]setValue:self.logInView.usernameField.text forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"RememberMe"];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        NSString * userN = [[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
        NSLog(@"userName:%@",userN);
    }
}


@end
