//
//  PAPWelcomeViewController.m
//  Anypic
//
//  Created by Héctor Ramos on 5/10/12.
//

#import "PAPWelcomeViewController.h"
#import "AppDelegate.h"
#import "PAPHomeViewController.h"
#import "HelpSreenViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "PAPUtility.h"

@implementation PAPWelcomeViewController


#pragma mark - UIViewController
- (void)loadView {
    
    if ([UIScreen mainScreen].bounds.size.height > 480.0f) {
        // for the iPhone 5
        UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
        [backgroundImageView setImage:[UIImage imageNamed:@"default_568h@2x.png"]];
        self.view = backgroundImageView;
    } else {
        UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
        [backgroundImageView setImage:[UIImage imageNamed:@"default.png"]];
        self.view = backgroundImageView;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(moveToNextScreen) userInfo:nil repeats:NO];
}

-(void)moveToNextScreen {
    
    // If not logged in, present login view controller
    if (![PFUser currentUser]) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [(AppDelegate*)[[UIApplication sharedApplication] delegate] presentLoginViewControllerAnimated:NO];
        });
        
        return;
    }
    // Present Anypic UI
    
    
    // Refresh current user with server side data -- checks if user is still valid and so on
    NSLog(@"email %@",[PFUser currentUser].email);
    
    if ([PFUser currentUser].email.length == 0) {
        
        [[PFUser currentUser] fetchInBackgroundWithTarget:self selector:@selector(refreshCurrentUserCallbackWithResult:error:)];
        [(AppDelegate*)[[UIApplication sharedApplication] delegate] presentTabBarController];
        
    }
    else {
        
        [[PFUser currentUser] fetch];
        [(AppDelegate*)[[UIApplication sharedApplication] delegate] presentTabBarController];
    }
}

#pragma mark - UIAlertView Delegate -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
       // PAPHomeViewController *home=[[PAPHomeViewController alloc]init];
       // [home gotoCamera];
    }
}


#pragma mark - ()

- (void)refreshCurrentUserCallbackWithResult:(PFObject *)refreshedObject error:(NSError *)error {
    
    // A kPFErrorObjectNotFound error on currentUser refresh signals a deleted user
    if (error && error.code == kPFErrorObjectNotFound) {
        NSLog(@"User does not exist.");
        [(AppDelegate*)[[UIApplication sharedApplication] delegate] logOut];
        return;
    }

    // Check if user is missing a Facebook ID
    if ([PAPUtility userHasValidFacebookData:[PFUser currentUser]]) {
        // User has Facebook ID.
        
        // refresh Facebook friends on each launch
        [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                if ([[UIApplication sharedApplication].delegate respondsToSelector:@selector(facebookRequestDidLoad:)]) {
                    [[UIApplication sharedApplication].delegate performSelector:@selector(facebookRequestDidLoad:) withObject:result];
                }
            } else {
                if ([[UIApplication sharedApplication].delegate respondsToSelector:@selector(facebookRequestDidFailWithError:)]) {
                    [[UIApplication sharedApplication].delegate performSelector:@selector(facebookRequestDidFailWithError:) withObject:error];
                }
            }
        }];
    } else {
        NSLog(@"Current user is missing their Facebook ID");
        [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                if ([[UIApplication sharedApplication].delegate respondsToSelector:@selector(facebookRequestDidLoad:)]) {
                    [[UIApplication sharedApplication].delegate performSelector:@selector(facebookRequestDidLoad:) withObject:result];
                }
            } else {
                if ([[UIApplication sharedApplication].delegate respondsToSelector:@selector(facebookRequestDidFailWithError:)]) {
                    [[UIApplication sharedApplication].delegate performSelector:@selector(facebookRequestDidFailWithError:) withObject:error];
                }
            }
        }];
    }
}

@end
