//
//  HelpScreens.h
//  FizFeliz
//
//  Created by Surender Rathore on 06/01/14.
//
//

#import <UIKit/UIKit.h>

@interface HelpScreens : UIViewController
{
    int pageNumber;
    
}
- (id)initWithPageNumber:(NSUInteger)page;
@end
