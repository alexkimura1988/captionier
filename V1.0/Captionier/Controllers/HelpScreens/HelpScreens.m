//
//  HelpScreens.m
//  FizFeliz
//
//  Created by Surender Rathore on 06/01/14.
//
//

#import "HelpScreens.h"

@interface HelpScreens ()

@end

@implementation HelpScreens

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
- (id)initWithPageNumber:(NSUInteger)page
{
    if (self = [super initWithNibName:nil bundle:nil])
    {
        pageNumber = page;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSArray *helpScreensArray = @[@"help_home",@"help_capture",@"help_editprofile"];
    UIImageView *helpScreenImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height - 20)];
    helpScreenImgView.image = [UIImage imageNamed:helpScreensArray[pageNumber]];
    [self.view addSubview:helpScreenImgView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
