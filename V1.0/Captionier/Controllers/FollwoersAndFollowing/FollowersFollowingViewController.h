//
//  FollowersFollowingViewController.h
//  MealTrend
//
//  Created by Surender Rathore on 03/12/13.
//
//

#import <Parse/Parse.h>

@interface FollowersFollowingViewController : PFQueryTableViewController
@property(nonatomic,assign)int type;
@property(nonatomic,strong)PFUser *user;
@end
