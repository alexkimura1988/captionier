//
//  FollowersFollowingViewController.m
//  MealTrend
//
//  Created by Surender Rathore on 03/12/13.
//
//

#import "FollowersFollowingViewController.h"
#import "PAPFindFriendsCell.h"
#import "PAPAccountViewController.h"


@interface FollowersFollowingViewController ()<PAPFindFriendsCellDelegate>
@property (nonatomic, strong) NSMutableDictionary *outstandingCountQueries;
@property (nonatomic, strong) NSMutableDictionary *outstandingFollowQueries;
@property (nonatomic, strong) NSArray *_followingUsers;
@end

@implementation FollowersFollowingViewController
@synthesize type;
@synthesize outstandingCountQueries;
@synthesize outstandingFollowQueries;
@synthesize user;




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL) isRootViewController
{
    BOOL isRoot = NO;
    if ([[self.navigationController viewControllers] count] == 1) {
        isRoot = [[[self.navigationController viewControllers] objectAtIndex:0] isEqual:self];
    }
    
    return isRoot;

}


/**
 *  Configures the navigation bar to use a custom button as its left and right buttons.
 */

-(void)customizeNavigationBackButton {
    
    if ([self isRootViewController]) {
        UIButton *menuBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
        [menuBtn addTarget:self.navigationController action:@selector(toggleMenu) forControlEvents:UIControlEventTouchUpInside];
        [menuBtn setFrame:CGRectMake(0.0f,0.0f, 44,44)];
        [Helper setButton:menuBtn Text:@"" WithFont:@"HelveticaNeue" FSize:17 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
        [menuBtn setImage:[UIImage imageNamed:@"menu_icon_off.png"] forState:UIControlStateNormal];
        [menuBtn setImage:[UIImage imageNamed:@"menu_icon_on.png"] forState:UIControlStateHighlighted];
        
        // Create a container bar button
        UIBarButtonItem *containingBarButton1 = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
        
        self.navigationItem.leftBarButtonItem = containingBarButton1;

    }
    else {
        UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn"];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
        [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
        // [backButton setBackgroundImage:[UIImage imageNamed:<#ImageNameOn#>] forState:UIControlStateHighlighted];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    }
    
    
}

/**
 *  go back to previous controller
 *
 *  @param sender backBtn pressed
 */
-(void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self customizeNavigationBackButton];
    
    [self getFollowingUsers];

    UIImageView *bgImgVw = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_screen_bg"]];
    bgImgVw.frame = self.tableView.frame;
    self.tableView.backgroundView = bgImgVw;
    self.tableView.separatorColor = WHITE_COLOR;
}

/**
 *  Parse queries to get list of followers of current user
 */
- (void) getFollowers
{
    PFQuery *query = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
    [query whereKey:kPAPActivityToUserKey equalTo:self.user];
    //[query whereKey:kPAPActivityToAlbumKey equalTo:NULL];
    [query includeKey:kPAPActivityFromUserKey];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        if (!error) {
            __followingUsers = [[NSArray alloc] initWithArray:array];
            [self.tableView reloadData];
        }
    }];
}


/**
 *  Pares queries to get list of users whom current user is folllowing
 */
-(void)getFollowingUsers {
    
    PFQuery *query = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
    [query whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
    //[query whereKey:kPAPActivityToAlbumKey equalTo:NULL];
    [query includeKey:kPAPActivityToUserKey];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        if (!error) {
            __followingUsers = [[NSArray alloc] initWithArray:array];
            [self.tableView reloadData];
        }
    }];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (PFQuery *)queryForTable {
    
    
    PFQuery *query;
    if (type == Following) {
        //following query
        query = [PFQuery queryWithClassName:kPAPActivityClassKey];
        [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
        [query whereKey:kPAPActivityFromUserKey equalTo:self.user];
        [query includeKey:kPAPActivityToUserKey];
        if (self.objects.count == 0) {
            query.cachePolicy = kPFCachePolicyCacheThenNetwork;
        }
        
        [query orderByAscending:@"createdAt"];

    }
    else if(type == FacebookFriends) {
        
        // Use cached facebook friend ids
        NSArray *facebookFriends = [[PAPCache sharedCache] facebookFriends];
        
        // [PFQuery clearAllCachedResults];
        
        // Query for all friends you have on facebook and who are using the app
        PFQuery *friendsQuery = [PFUser query];
        [friendsQuery whereKey:kPAPUserFacebookIDKey containedIn:facebookFriends];
        
        // Query for all auto-follow accounts
        NSMutableArray *autoFollowAccountFacebookIds = [[NSMutableArray alloc] initWithArray:kPAPAutoFollowAccountFacebookIds];
        [autoFollowAccountFacebookIds removeObject:[[PFUser currentUser] objectForKey:kPAPUserFacebookIDKey]];
        PFQuery *parseEmployeeQuery = [PFUser query];
        [parseEmployeeQuery whereKey:kPAPUserFacebookIDKey containedIn:autoFollowAccountFacebookIds];
        
        
        
        
        query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:friendsQuery, parseEmployeeQuery, nil]];
        query.cachePolicy = kPFCachePolicyNetworkOnly;
        
        if (self.objects.count == 0) {
            query.cachePolicy = kPFCachePolicyCacheThenNetwork;
        }
        
        [query orderByAscending:kPAPUserDisplayNameKey];

        
    }
    else {
        
        //follower query
        query = [PFQuery queryWithClassName:kPAPActivityClassKey];
        [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
        [query whereKey:kPAPActivityToUserKey equalTo:self.user];
        //[query wh]
        //[query whereKey:kPAPActivityToAlbumKey equalTo:@""];
        [query includeKey:kPAPActivityFromUserKey];
        if (self.objects.count == 0) {
            query.cachePolicy = kPFCachePolicyCacheThenNetwork;
        }
        
        [query orderByAscending:@"createdAt"];

//        [query orderByAscending:kPAPUserDisplayNameKey];
    }
    
    
    return query;
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    

    if (NSClassFromString(@"UIRefreshControl")) {
        [self.refreshControl endRefreshing];
    }
    
    
    
 }

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *FriendCellIdentifier = @"FriendCell";
    
    PAPFindFriendsCell *cell = [tableView dequeueReusableCellWithIdentifier:FriendCellIdentifier];
    if (cell == nil) {
        cell = [[PAPFindFriendsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:FriendCellIdentifier];
        [cell setDelegate:self];
        //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    }
    
   // NSLog(@"object  %@",object);
    
    PFUser *pfuser;
    if (type == Following) {
        pfuser = [object objectForKey:kPAPActivityToUserKey];
        cell.followButton.selected = NO;
    }
    else if(type == Followers) {
        pfuser = [object objectForKey:kPAPActivityFromUserKey];
        cell.followButton.selected = NO;
    }
    else {
        pfuser = (PFUser*)object;
    }
    //PFUser *user = [object objectForKey:kPAPActivityFromUserKey];
    [cell setUser:pfuser];
    [cell.photoLabel setText:@"0 posts"];
    
    cell.followButton.hidden = NO;

    if ([pfuser.objectId isEqualToString:[PFUser currentUser].objectId]) {
        cell.followButton.hidden = YES;
    }
    
    NSDictionary *attributes = [[PAPCache sharedCache] attributesForUser:(PFUser *)object];
    
    if (attributes) {
        // set them now
        NSString *pluralizedPhoto;
        NSNumber *number = [[PAPCache sharedCache] photoCountForUser:(PFUser *)object];
        if ([number intValue] == 1) {
            pluralizedPhoto = @"post";
        } else {
            pluralizedPhoto = @"posts";
        }
        [cell.photoLabel setText:[NSString stringWithFormat:@"%@ %@", number, pluralizedPhoto]];
    } else {
        @synchronized(self) {
            NSNumber *outstandingCountQueryStatus = [self.outstandingCountQueries objectForKey:indexPath];
            if (!outstandingCountQueryStatus && pfuser) {
                [self.outstandingCountQueries setObject:[NSNumber numberWithBool:YES] forKey:indexPath];
                PFQuery *photoNumQuery = [PFQuery queryWithClassName:kPAPFeedClassKey];
                [photoNumQuery whereKey:kPAPFeedUserKey equalTo:pfuser];
                [photoNumQuery setCachePolicy:kPFCachePolicyCacheThenNetwork];
                [photoNumQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
                    @synchronized(self) {
                        [[PAPCache sharedCache] setPhotoCount:[NSNumber numberWithInt:number] user:(PFUser *)object];
                        [self.outstandingCountQueries removeObjectForKey:indexPath];
                    }
                    PAPFindFriendsCell *actualCell = (PAPFindFriendsCell*)[tableView cellForRowAtIndexPath:indexPath];
                    NSString *pluralizedPhoto;
                    if (number == 1) {
                        pluralizedPhoto = @"post";
                    } else {
                        pluralizedPhoto = @"posts";
                    }
                    [actualCell.photoLabel setText:[NSString stringWithFormat:@"%d %@", number, pluralizedPhoto]];
                    
                }];
            };
        }
    }
    
    
    cell.tag = indexPath.row;
    
    for(PFObject *obj in __followingUsers) {
        PFUser *followingUser = [obj objectForKey:kPAPActivityToUserKey];
        if ([followingUser.objectId isEqualToString:pfuser.objectId]) {
            cell.followButton.selected = YES;
            break;
        }
    }
    
    return cell;
}
#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.objects.count) {
        return [PAPFindFriendsCell heightForCell];
    } else {
        return 44.0f;
    }
}

#pragma mark - PAPFindFriendsCellDelegate


/**
 *  This delegata method call whenever userName is tapped
 *  It open the tapped user accounts
 *
 *  @param cellView
 *  @param aUser    tappedUser
 */
- (void)cell:(PAPFindFriendsCell *)cellView didTapUserButton:(PFUser *)aUser {
    // Push account view controller
    PAPAccountViewController *accountViewController = [[PAPAccountViewController alloc] initWithStyle:UITableViewStylePlain];
    accountViewController.showBackButton = YES;
    [accountViewController setUser:aUser];
    [self.navigationController pushViewController:accountViewController animated:YES];

}


/**
 *  This method calls the toggel method to change button action
 *
 *  @param cellView
 *  @param aUser    user
 */
- (void)cell:(PAPFindFriendsCell *)cellView didTapFollowButton:(PFUser *)aUser {
    [self shouldToggleFollowFriendForCell:cellView];
    
}


/**
 *  This method change button action from  follow to following or vise-versa
 *
 *  @param cell
 */
- (void)shouldToggleFollowFriendForCell:(PAPFindFriendsCell*)cell {
    PFUser *cellUser = cell.user;
//    if ([cellUser.username isEqualToString:@"XtremeBR"]) {
//        return;
//    }
    if ([cell.followButton isSelected]) {
        // Unfollow
        cell.followButton.selected = NO;
        [PAPUtility unfollowUserEventually:cellUser For:type];
       // [[NSNotificationCenter defaultCenter] postNotificationName:PAPUtilityUserFollowingChangedNotification object:nil];
        Helper *helper = [Helper sharedInstance];
        helper.didCurrentUserFollowedSomebody = 100;
        
        
    } else {
        // Follow
        
        
        cell.followButton.selected = YES;
        [PAPUtility followUserEventually:cellUser block:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [[NSNotificationCenter defaultCenter] postNotificationName:PAPUtilityUserFollowingChangedNotification object:nil];
                Helper *helper = [Helper sharedInstance];
                helper.didCurrentUserFollowedSomebody = 100;
                
                [self sendPushToFollowingUser:cellUser];

            } else {
                cell.followButton.selected = NO;
            }
        }];
    }
    
}



/**
 *  This method send push notifications to users whom current user start following
 *
 *  @param toUser is whom current user is following
 */
-(void)sendPushToFollowingUser:(PFUser*)toUser{
    
    //NSString *message = [NSString stringWithFormat:@"%@ started Following you",[[PFUser currentUser] objectForKey:kPAPUserDisplayNameKey]];
    NSString *message = [NSString stringWithFormat:@"%@ started Following you",[[PFUser currentUser] username]];

    
    NSDictionary *payload =
    [NSDictionary dictionaryWithObjectsAndKeys:
     message, kAPNSAlertKey,
     @"Increment",kAPNSBadgeKey,
     kPAPPushPayloadActivityFollowKey, kPAPPushPayloadActivityTypeKey,
     @"sms-received.wav",kAPNSSoundKey,
     nil];
    
    // Send the push
    PFPush *push = [[PFPush alloc] init];
    [push setChannel:[toUser objectForKey:kPAPUserPrivateChannelKey]];
    [push setData:payload];
    [push sendPushInBackground];
}

@end
