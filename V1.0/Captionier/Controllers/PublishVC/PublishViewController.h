//
//  PublishViewController.h
//  Anypic
//
//  Created by rahul Sharma on 05/08/13.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import <MediaPlayer/MediaPlayer.h>

#import "OptionsViewController.h"
//#import <Parse/Parse.h>




typedef enum {
    FACEBOOK_SHARING= 10,
    TWITTER_SHARING = 11,
    THUMBLR_SHARING = 12,
    FLICKER_SHARING = 13,
    FOURSQUARE_SHARING = 14,
    EMAIL_SHARING = 15
    
}SocialSharing;

typedef enum {
    
    VIDEO_MEDIA = 0,
    IMAGE_MEDIA,
    AUDIO_MEDIA
    
}MediaType;

@interface PublishViewController : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    
	
}



@property(nonatomic,strong)     IBOutlet UIImageView *publishImageView;
@property(nonatomic,strong)     IBOutlet UITextView *commentTextField;
@property(nonatomic,weak)       IBOutlet UIView *shareView;
@property(nonatomic,weak)       IBOutlet UIView *commentView;
@property(nonatomic,weak)       IBOutlet UILabel *lableShareWith;
@property(nonatomic,weak)       IBOutlet UILabel *lableAddChannel;
@property(nonatomic,weak)       IBOutlet UIImageView *imgMenuAddChannel;
@property(nonatomic,weak)       IBOutlet UIImageView *imgArrowAddChannel;

@property(nonatomic,weak)       IBOutlet UILabel *labelPlaceHolder;
@property(nonatomic,weak)       IBOutlet UIButton *btnDone;
@property(nonatomic,weak)       IBOutlet UIButton *btnPlay;
@property(nonatomic,strong)     NSString *videofilePath;
@property(nonatomic,strong)     UIImage *publishImage;
@property(nonatomic)            MediaType mediatype;



-(IBAction)btnClicked:(id)sender;
- (IBAction)doneButtonAction:(id)sender;
-(IBAction)btnPlayClicked:(id)sender;

-(void) publishBtnAction:(id)sender;

@end
