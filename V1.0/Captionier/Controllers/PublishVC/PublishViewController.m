//
//  PublishViewController.m
//  Anypic
//
//  Created by rahul Sharma on 05/08/13.
//
//

#import "PublishViewController.h"
#import "UIImage+ResizeAdditions.h"
#import  <Social/Social.h>

#import "AppDelegate.h"

#import "ProgressView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "VideoPlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UploadProgress.h"
#import "FollowingList.h"
#import <FacebookSDK/FacebookSDK.h>


NSString *kFetchRequestTokenStep = @"kFetchRequestTokenStep";
NSString *kGetUserInfoStep = @"kGetUserInfoStep";
NSString *kSetImagePropertiesStep = @"kSetImagePropertiesStep";
NSString *kUploadImageStep = @"kUploadImageStep";

@interface PublishViewController ()<OptionsViewControllerDelegate>
@property (nonatomic, strong) PFFile *photoFile;
@property (nonatomic, strong) PFFile *thumbnailFile;
@property (nonatomic, strong) PFFile *videoFile;
@property (nonatomic, strong) PFFile *audioFile;
@property (nonatomic, assign) UIBackgroundTaskIdentifier fileUploadBackgroundTaskId;
@property (nonatomic, assign) UIBackgroundTaskIdentifier photoPostBackgroundTaskId;
@property (nonatomic,strong) VideoPlayerViewController *player;
@property (nonatomic,strong) MPMoviePlayerController *moviePlayer;
@property (nonatomic,strong) NSString *btncategorytitle;
@property (nonatomic,strong)   NSMutableArray *followers;
@property (nonatomic,strong)   NSMutableArray *taggedUsers;
@property (nonatomic,strong)   FollowingList *followinglist;
@end

@implementation PublishViewController
@synthesize publishImageView;
@synthesize commentTextField;
@synthesize publishImage;
@synthesize photoFile;
@synthesize audioFile;
@synthesize thumbnailFile;
@synthesize fileUploadBackgroundTaskId;
@synthesize photoPostBackgroundTaskId;
@synthesize videofilePath;
@synthesize mediatype;
@synthesize labelPlaceHolder;
@synthesize lableShareWith;
@synthesize commentView;
@synthesize shareView;
@synthesize btnDone;
@synthesize btnPlay;

@synthesize player;
@synthesize moviePlayer;
@synthesize btncategorytitle;
@synthesize followinglist;
@synthesize lableAddChannel;
@synthesize imgArrowAddChannel;
@synthesize imgMenuAddChannel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Share";
        
    }
    return self;
}
-(void)makeUIChanges
{
    
    //[self.view setBackgroundColor:WHITE_COLOR];
    [self.view setBackgroundColor:UIColorFromRGB(0xe2e2db)];
    commentView.layer.cornerRadius = 2;
    
    shareView.layer.cornerRadius = 2;
    
    [Helper setToLabel:lableShareWith Text:@"Share with" WithFont:HELVETICA_BOLD FSize:14 Color:[UIColor colorWithRed:122.0/255.0 green:122.0/255.0 blue:122.0/255.0 alpha:1.0]];
    
    [Helper setToLabel:labelPlaceHolder Text:@"Add description" WithFont:HELVETICA FSize:12 Color:[UIColor colorWithRed:147.0/255.0 green:148.0/255.0 blue:148.0/255.0 alpha:1.0]];
    
    
    
//    [(UIButton*)[shareView viewWithTag:FACEBOOK_SHARING] setBackgroundImage:[UIImage imageNamed:@"facebook.png"] forState:UIControlStateNormal];
//    [(UIButton*)[shareView viewWithTag:TWITTER_SHARING] setBackgroundImage:[UIImage imageNamed:@"twitter.png"] forState:UIControlStateNormal];
//    [(UIButton*)[shareView viewWithTag:THUMBLR_SHARING] setBackgroundImage:[UIImage imageNamed:@"sms.png"] forState:UIControlStateNormal];
//    [(UIButton*)[shareView viewWithTag:FOURSQUARE_SHARING] setBackgroundImage:[UIImage imageNamed:@"foursquare_icon.png"] forState:UIControlStateNormal];
//    [(UIButton*)[shareView viewWithTag:EMAIL_SHARING] setBackgroundImage:[UIImage imageNamed:@"mail.png"] forState:UIControlStateNormal];
//    [(UIButton*)[shareView viewWithTag:FLICKER_SHARING] setBackgroundImage:[UIImage imageNamed:@"flicker.png"] forState:UIControlStateNormal];
    
//    CGRect viewFrame = self.view.frame;
//    viewFrame.origin.y = 66;
//    self.view.frame = viewFrame;
    
    
    CGRect frame;
    frame = self.commentView.frame;
    frame.origin.y = 10;
    self.commentView.frame = frame;
    self.commentView.backgroundColor = WHITE_COLOR;
    
    
    
    
}




- (void)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBarHidden =YES;
    
}

- (void)customizeNavigationBarButtons {
	UIImage *imgButton = [UIImage imageNamed:@"cahrt_btn.png"];
	
	UIButton *rightBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[rightBarButton setBackgroundImage:imgButton forState:UIControlStateNormal];
    //[rightBarButton setBackgroundImage:[UIImage imageNamed:@"settings_btn_on.png"] forState:UIControlStateHighlighted];
	[rightBarButton setFrame:CGRectMake(0, 0, 60, 30)];
    [rightBarButton setTitle:@"Publish" forState:UIControlStateNormal];
	
	rightBarButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
	rightBarButton.titleLabel.textColor = [UIColor whiteColor];
	rightBarButton.titleLabel.shadowOffset = CGSizeMake(0,-1);
	rightBarButton.titleLabel.shadowColor = [UIColor darkGrayColor];
    
	[rightBarButton addTarget:self action:@selector(publishBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButton];
	
	
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   
    [self customizeNavigationBarButtons];
    // self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Publish" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonAction:)];
    
    
    [self makeUIChanges];
    
    publishImageView.image = publishImage;
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn_on.png"] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.fileUploadBackgroundTaskId = UIBackgroundTaskInvalid;
    self.photoPostBackgroundTaskId = UIBackgroundTaskInvalid;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadImageOnFlicker) name:SnapAndRunShouldUpdateAuthInfoNotification object:nil];
    

    
   
    
    NSLog(@"url %@",self.videofilePath);
   
    if (mediatype == VIDEO_MEDIA) {
        //[NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(getVideoFrame) userInfo:Nil repeats:NO];
        [self getVideoFrame];
        [(UIButton*)[self.view viewWithTag:THUMBLR_SHARING] setEnabled:NO];
        [(UIButton*)[self.view viewWithTag:FOURSQUARE_SHARING] setEnabled:NO];
        [(UIButton*)[self.view viewWithTag:FLICKER_SHARING] setEnabled:NO];
        [(UIButton*)[self.view viewWithTag:TWITTER_SHARING] setEnabled:NO];
    }
    else if(mediatype == IMAGE_MEDIA)
    {
        [self shouldUploadImage:publishImage];
    }
    [Helper setButton:btnDone Text:@"Add Channel" WithFont:@"HelveticaNeue" FSize:13 TitleColor:UIColorFromRGB(0x666666) ShadowColor:nil];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
     if ([Helper isPhone5]) {
         //[self.commentView setFrame:CGRectMake(0, 10, 320, 300)];
        // [commentTextField setFrame:CGRectMake(0, 315, 320, 40)];
         
        [lableAddChannel setFrame:CGRectMake(0, 375, 320, 40)];
         [imgMenuAddChannel setFrame:CGRectMake(5, 383, 15, 15)];
         [imgArrowAddChannel setFrame:CGRectMake(295, 385, 15, 15)];
         [btnDone setFrame:CGRectMake(30, 375, 280, 30)];
        [shareView setFrame:CGRectMake(10, 425, 306, 70)];
         
     }else{
         [lableAddChannel setFrame:CGRectMake(0, 290, 320, 40)];
         
         [imgMenuAddChannel setFrame:CGRectMake(0, 303, 15, 15)];
         [imgArrowAddChannel setFrame:CGRectMake(295, 300, 15, 15)];
         [btnDone setFrame:CGRectMake(20, 295, 280, 30)];
         [shareView setFrame:CGRectMake(10, 335, 306, 70)];
    
     }
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidUnload
{
    self.commentTextField = nil;
    self.publishImageView = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)itemDidFinishPlaying {
    [player.player pause];
    [btnPlay setHidden:NO];
}
- (void)moviePlayerFinishedPlaying {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [moviePlayer stop];
    [moviePlayer.view removeFromSuperview];
    moviePlayer = nil;
    [commentView setBackgroundColor:[UIColor colorWithPatternImage:publishImage]];
    [btnPlay setHidden:NO];
    //[btnPlay bringSubviewToFront:moviePlayer.view];
    
    
    
    
}

-(void)getVideoFrame
{
    
    
    NSLog(@"%@",self.videofilePath);
    NSURL *url = [NSURL fileURLWithPath:videofilePath];
	
	MPMoviePlayerController *Player = [[MPMoviePlayerController alloc] initWithContentURL:url];
    
    
    UIImage *singleFrameImage = [Player thumbnailImageAtTime:1
                                                  timeOption:MPMovieTimeOptionExact];
    [Player stop];
    publishImageView.image = singleFrameImage;
    
    publishImage = singleFrameImage;
    [commentView setBackgroundColor:[UIColor colorWithPatternImage:publishImage]];
    
    //[self shouldUploadVideo:publishImage];
    

}
#pragma mark - TextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
  
    
    
    if(textView.text.length >1 || text.length != 0)
    {
        [labelPlaceHolder setHidden:YES];
        NSLog(@"string %@ location %d",text,range.length);
        if ([text isEqualToString:@"@"]) {
            [self getUserWhomIamFollowing];
        }
        
        if ([text isEqualToString:@" "] || (range.length == 1 && range.location == 0)) {
            [self removeFollowingListView];
        }
    }
    else
    {
        [labelPlaceHolder setHidden:NO];
        
        return YES;
    }
    if([text isEqualToString:@"\n"])
    {
        [self removeFollowingListView];
        [self moveViewDown];
        [textView resignFirstResponder];
        return NO;
    }
    return YES;

}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self moveViewUp];
    if (textView.text.length == 0) {
        [labelPlaceHolder setHidden:NO];
    }
    else
    {
        [labelPlaceHolder setHidden:YES];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self moveViewDown];
    if (textView.text.length == 0) {
        [labelPlaceHolder setHidden:NO];
    }
    else
    {
        [labelPlaceHolder setHidden:YES];
    }
}

-(void)moveViewUp {
    
    CGRect frame = commentView.frame;
    if ([Helper isPhone5]) {
          frame.origin.y -= 85;
    }
    else {
          frame.origin.y -= 80;
    }
  
    
    [UIView animateWithDuration:.1 animations:^(void){
        commentView.frame = frame;
    }];
    
}
-(void)moveViewDown {
    CGRect frame = commentView.frame;
    frame.origin.y = 10;
    
    [UIView animateWithDuration:.1 animations:^(void){
        commentView.frame = frame;
    }];
}

-(void)handleSingleTapGesture:(UIGestureRecognizer*)gestureRecognizer
{
    NSLog(@"TapGesture");
    [self moveViewDown];
    [commentTextField resignFirstResponder];
    
}

#pragma mark - Tagging User
-(void)getUserWhomIamFollowing{
    
    if (_followers.count > 0) {
        [self displayFollowingList];
    }
    else {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view withMessage:@""];
        PFQuery  *query = [PFQuery queryWithClassName:kPAPActivityClassKey];
        [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
        [query whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
        [query includeKey:kPAPActivityToUserKey];
        [query orderByAscending:kPAPUserDisplayNameKey];
        [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
            
            if (!error) {
                _followers = [[NSMutableArray alloc] initWithArray:array];
                [self displayFollowingList];
                [pi hideProgressIndicator];
            }
            
        }];
    }
    
    
}
-(void)displayFollowingList {
    
    if (!followinglist) {
        followinglist = [[FollowingList alloc] initWithFrame:CGRectMake(0,64, 320, 240)];
        followinglist.tag = 100;
        __weak typeof(self) weakSelf = self;
        followinglist.callback = ^(PFUser *taggedUser){
            [weakSelf addDisplayName:taggedUser];
        };
        followinglist.followingList = _followers;
        [self.view addSubview:followinglist];
        [followinglist refresh];
    }
    else {
        [self.view addSubview:followinglist];
        [followinglist refresh];
    }
    
    
}
-(void)removeFollowingListView{
    //FollowingList *fl =  (FollowingList*)[self.view viewWithTag:100];
    if (followinglist) {
        [followinglist removeFromSuperview];
    }
    
    // followinglist = Nil;
}
-(void)addDisplayName:(PFUser*)taggedUser{
    
    if (!_taggedUsers) {
        _taggedUsers = [[NSMutableArray alloc] init];
    }
    [_taggedUsers addObject:taggedUser];
    
    NSString *combinedtext = [NSString stringWithFormat:@"%@%@",commentTextField.text,taggedUser.username];
    commentTextField.text = combinedtext;
    [self removeFollowingListView];
}


#pragma mark - ButtonActions
-(IBAction)btnPlayClicked:(id)sender{
    
   // [btnPlay setHidden:YES];
   // [player.player play];
    moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:videofilePath]];
    [moviePlayer setControlStyle:MPMovieControlStyleNone];
    if ([Helper isPhone5]) {
        moviePlayer.view.frame = CGRectMake(0, 0, 320, 300);
    }
    else {
        moviePlayer.view.frame = CGRectMake(0, 0, 320, 277);       //xib 4
    }
    [moviePlayer setScalingMode:MPMovieScalingModeAspectFill];
    
    
    [commentView addSubview: moviePlayer.view];
    [moviePlayer play];
    [btnPlay setHidden:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerFinishedPlaying)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

-(IBAction)btnClicked:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    switch (btn.tag) {
        case FACEBOOK_SHARING:
        {
            
            [Flurry logEvent:@"FacebookSharingClicked"];
            [self shareOnFB];
            break;
        }
        case TWITTER_SHARING:
        {
            [Flurry logEvent:@"TwitterSharingClicked"];
            [self shareOnTwitter];
            break;
        }
        case FLICKER_SHARING:
        {
            [Flurry logEvent:@"FlickerSharingClicked"];
           // [self authorizeAction];
            break;
        }
        case THUMBLR_SHARING:
        {
            [Flurry logEvent:@"SmsSharingClicked"];
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            if (![ud objectForKey:kPAPSaveOriginalPhoto]) {
                UIImageWriteToSavedPhotosAlbum(publishImage, nil, nil, nil);
            }
            [self sendSMS];
            break;
        }
        case FOURSQUARE_SHARING:
        {
            [Flurry logEvent:@"FoursquareShairngClicked"];
            //[self checkFoursquareStatus];
            break;
        }
        case EMAIL_SHARING:
        {
            [Flurry logEvent:@"EmailSharingClicked"];
            [self sendEmail];
            break;
        }
        default:
            break;
    }
}

////######################
//#pragma mark - OPtionViewControllerDelegate
//-(void)optionSelected:(NSString *)option{
//    
//    [btnDone setTitle:option forState:UIControlStateNormal];
//}
////#####################

-(void) doneButtonAction:(id)sender
{
    
    OptionsViewController *optionVC = [[OptionsViewController alloc] init];
    //optionVC.filterOptions = @[@"Dates",@"Work",@"Hobby",@"Day to day",@"Food and Drink",@"Home",@"Personal",@"Other"];
    //optionVC.filterOptions =  @[@"Travel",@"Education",@"World",@"Urban",@"Sports",@"Special",@"Politics",@"Nature",@"Music",@"Health",@"Food",@"Beauty",@"Family",@"Dog",@"Cat"];
    
    optionVC.filterOptions =   @[@"Travel",@"Education",@"World",@"Urban",@"Sports",@"Special",@"Politics",@"Nature",@"Music",@"Health",@"Food",@"Beauty",@"Family",@"Dog",@"Cat"];
    // optionVC.catagoryTitle = @"Select Channel";
    optionVC.delegate = self;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:optionVC];
    //[self presentModalViewController:navController animated:YES];
    [self.navigationController presentViewController:navController animated:YES completion:nil];
    

}
- (void)publishBtnAction:(id)sender {
    
    [Flurry logEvent:@"PublishButtonClicked"];
    
    //check if category is selected
    if ([btnDone.titleLabel.text isEqualToString:@"Add Channel"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please select a Channel for video" delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:Nil, nil];
        [alert show];
        return;
    }
    
    if (mediatype == IMAGE_MEDIA) {
        if ([self shouldUploadImage:publishImage]) {
            if (!self.photoFile || !self.thumbnailFile) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your photo" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
                return;
            }
            else {
                [self startUploadingImageFile];
            }
            
        }
        else {
            
            if (!self.photoFile || !self.thumbnailFile) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your photo" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
                return;
            }
        }
        
    }
    else if(mediatype == VIDEO_MEDIA)
    {
        if ([self shouldUploadVideo:publishImage]) {
            
            if (!self.videoFile ||  !self.photoFile || !self.thumbnailFile) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your video" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
                return;
            }
            else {
                [self startUploadingVideoFile];
            }
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your video" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
            [alert show];
            return;
        }
        
        
    }
    else if(mediatype == AUDIO_MEDIA) {
        
        if ([self shouldUploadAudio:publishImage]) {
            
            if (!self.audioFile ||  !self.photoFile || !self.thumbnailFile) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your audio" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
                return;
            }
            else {
                [self startUploadingAudioFile];
            }
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your audio" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
            [alert show];
            return;
        }
    }
    
    
    // both files have finished uploading
    
 
    // create a photo object
    NSArray *mUserArray = @[[[PFUser currentUser]username]];
    NSArray *mArray = @[commentTextField.text];
    PFObject *photo = [PFObject objectWithClassName:kPAPPhotoClassKey];
    [photo setObject:[PFUser currentUser] forKey:kPAPPhotoUserKey];
    [photo setObject:self.photoFile forKey:kPAPPhotoPictureKey];
    //[photo setObject:commentTextField.text forKey:kPAPPhotoDescriptionTextKey];
    [photo setObject:self.thumbnailFile forKey:kPAPPhotoThumbnailKey];
    //[photo setObject:[NSNumber numberWithInt:0] forKey:kPAPPhotoCommentsCount];
    [photo setObject:[NSNumber numberWithInt:0] forKey:kPAPPhotoLikeCountKey];
    [photo setObject:btnDone.titleLabel.text forKey:kPAPPhotoCategory];
    if (commentTextField.text.length > 0 ) {
        [photo setObject:mArray forKey:kPAPPhotoCommentArrayKey];
        [photo setObject:mUserArray forKey:@"users"];
        [photo setObject:[NSNumber numberWithInt:1] forKey:kPAPPhotoCommentsCount];
    }
    else {
        [photo setObject:[NSNumber numberWithInt:0] forKey:kPAPPhotoCommentsCount];
    }
    
    if (mediatype == VIDEO_MEDIA) {
        [photo setObject:self.videoFile forKey:kPAPPhotoVidoKey];
        [photo setObject:kPAPPhotoMediaVideoKey forKey:kPAPPhotoMediaTypeKey];
    }
    else if (mediatype == IMAGE_MEDIA) {
        
        [photo setObject:kPAPPhotoMediaPhotoKey forKey:kPAPPhotoMediaTypeKey];
    }
    else {
        
        [photo setObject:kPAPPhotoMediaAudioKey forKey:kPAPPhotoMediaTypeKey];
        [photo setObject:self.audioFile forKey:kPAPPhotoAudioKey];
    }
    
    
    
    // photos are public, but may only be modified by the user who uploaded them
    PFACL *photoACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [photoACL setPublicReadAccess:YES];
    [photoACL setPublicWriteAccess:YES];
    
    photo.ACL = photoACL;
    
    
    Helper *helpr = [Helper sharedInstance];
    helpr.isAddedNewPhoto = YES;
    
    // Request a background execution task to allow us to finish uploading the photo even if the app is backgrounded
    self.photoPostBackgroundTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:self.photoPostBackgroundTaskId];
    }];
    
    // save
    [photo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            //NSLog(@"Photo uploaded");
            
            [self sendPushToTaggedUsersForObject:photo];
            
            [self makeAdditionalTextAsFirstComment:photo];
            
            [Flurry logEvent:@"PhotoPublishedSuccessfully"];
            
            [[PAPCache sharedCache] setAttributesForPhoto:photo likers:[NSArray array] commenters:[NSArray array] likedByCurrentUser:NO];
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:PAPTabBarControllerDidFinishEditingPhotoNotification object:photo];
        } else {
            NSLog(@"Photo failed to save: %@", error);
            [Flurry logEvent:@"PhotoUploadFailed"];
            if (mediatype == VIDEO_MEDIA) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your Video" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your photo" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            }
            
        }
        [[UIApplication sharedApplication] endBackgroundTask:self.photoPostBackgroundTaskId];
    }];
    
   // self.tabBarController.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:NO];
}
-(void)makeAdditionalTextAsFirstComment:(PFObject*)photo {
    
    PFObject *comment = [PFObject objectWithClassName:kPAPActivityClassKey];
    [comment setObject:commentTextField.text forKey:kPAPActivityContentKey]; // Set comment text
    [comment setObject:[PFUser currentUser] forKey:kPAPActivityToUserKey]; // Set toUser
    [comment setObject:[PFUser currentUser] forKey:kPAPActivityFromUserKey]; // Set fromUser
    [comment setObject:kPAPActivityTypeComment forKey:kPAPActivityTypeKey];
    [comment setObject:photo forKey:kPAPActivityPhotoKey];
    
    PFACL *ACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [ACL setPublicReadAccess:YES];
    [ACL setPublicWriteAccess:YES];
    [ACL setWriteAccess:YES forUser:[photo objectForKey:kPAPPhotoUserKey]];
    comment.ACL = ACL;
    
    [[PAPCache sharedCache] incrementCommentCountForPhoto:photo];
    
    // Show HUD view
    //[MBProgressHUD showHUDAddedTo:self.view.superview animated:YES];
    
    // If more than 5 seconds pass since we post a comment, stop waiting for the server to respond
    // NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(handleCommentTimeout:) userInfo:@{@"comment": comment} repeats:NO];
    NSMutableArray *taggedUsers = [[NSMutableArray alloc] init];
    [taggedUsers addObject:comment];
    
    if (_taggedUsers.count > 0) {
        //NSMutableArray *channels = [[NSMutableArray alloc] init];
        
        for(PFUser *usr in _taggedUsers) {
            
            if ([commentTextField.text rangeOfString:usr.username].location == NSNotFound) {
                //do not send push notification to that user
            }
            else {
                
                if (![usr.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    PFObject *commentTagActivity = [PFObject objectWithClassName:kPAPActivityClassKey];
                    [commentTagActivity setObject:[PFUser currentUser] forKey:kPAPActivityFromUserKey];
                    [commentTagActivity setObject:@"commentTag" forKey:kPAPActivityTypeKey];
                    [commentTagActivity setObject:usr forKey:kPAPActivityToUserKey];
                    [commentTagActivity setObject:commentTextField.text forKey:kPAPActivityContentKey];
                    [commentTagActivity setObject:photo forKey:kPAPActivityPhotoKey];
                    
                    PFACL *likeACL = [PFACL ACLWithUser:[PFUser currentUser]];
                    [likeACL setPublicReadAccess:YES];
                    [likeACL setPublicWriteAccess:YES];
                    [likeACL setWriteAccess:YES forUser:[photo objectForKey:kPAPPhotoUserKey]];
                    commentTagActivity.ACL = likeACL;
                    
                    [taggedUsers addObject:commentTagActivity];
                }
                
            }
            
            
        }
        
        
    }
    
    
    
    
    
    [PFObject saveAllInBackground:taggedUsers block:^(BOOL successed , NSError *error){
        if (error) {
            //[self makeEntryForTaggedusersInbackground];
        }
        else {
            NSLog(@"saved all tagged usrs");
        }
    }];
    
    NSMutableArray *hashtags = [NSMutableArray arrayWithArray:[self getHashtag:commentTextField.text]];
    NSMutableArray *marray = [[NSMutableArray alloc] init];
    
    PFQuery *query = [PFQuery queryWithClassName:@"HashTag"];
    [query whereKey:@"hashtag" containedIn:hashtags];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError*error){
        NSLog(@" arry %@",array);
        
        //update count for previous added hashtags
        for(PFObject *obj in array){
            
            NSString *tag = [obj objectForKey:@"hashtag"];
            if ([hashtags containsObject:tag]) {
                [obj incrementKey:@"hashCount"];
                [hashtags removeObject:tag];
            }
            
            
            [marray addObject:obj];
        }
        
        //add newly created hashtags
        for(NSString *tags in hashtags){
            PFObject *object = [PFObject objectWithClassName:@"HashTag"];
            [object setObject:tags forKey:@"hashtag"];
            [object incrementKey:@"hashCount"];
            
            
            PFACL *ACL = [PFACL ACLWithUser:[PFUser currentUser]];
            [ACL setPublicReadAccess:YES];
            [ACL setPublicWriteAccess:YES];
            object.ACL = ACL;
            
            
            [marray addObject:object];
        }
        
        [PFObject saveAllInBackground:marray block:^(BOOL success, NSError*error){
            NSLog(@"error %@",error);
            
        }];
    }];
    
}
-(NSArray*)getHashtag:(NSString *)_str
{
    NSString * aString = _str;
    NSMutableArray *substrings = [NSMutableArray new];
    NSScanner *scanner = [NSScanner scannerWithString:aString];
    [scanner scanUpToString:@"#" intoString:nil]; // Scan all characters before #
    while(![scanner isAtEnd]) {
        NSString *substring = nil;
        [scanner scanString:@"#" intoString:nil]; // Scan the # character
        if([scanner scanUpToString:@" " intoString:&substring]) {
            // If the space immediately followed the #, this will be skipped
            [substrings addObject:[NSString stringWithFormat:@"#%@",substring]];
        }
        [scanner scanUpToString:@"#" intoString:nil]; // Scan all characters before next #
    }
    // do something with substrings
    return substrings;
    
    
}

- (BOOL)shouldUploadImage:(UIImage *)anImage {
    
    UIImage *resizedImage = [anImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(620.0f, 620.0f) interpolationQuality:kCGInterpolationHigh];
    UIImage *thumbnailImage = [anImage thumbnailImage:95.0f transparentBorder:0.0f cornerRadius:0.0f interpolationQuality:kCGInterpolationDefault];
    
    // JPEG to decrease file size and enable faster uploads & downloads
    NSData *imageData = UIImageJPEGRepresentation(resizedImage, 0.8f);
    NSData *thumbnailImageData = UIImagePNGRepresentation(thumbnailImage);
    
    if (!imageData || !thumbnailImageData) {
        return NO;
    }
    
    self.photoFile = [PFFile fileWithData:imageData];
    self.thumbnailFile = [PFFile fileWithData:thumbnailImageData];
    
    return YES;
    
}

-(void)sendPushToTaggedUsersForObject:(PFObject *)object
{
    if (_taggedUsers.count > 0) {
        NSMutableArray *channels = [[NSMutableArray alloc] init];
        
        for(PFUser *usr in _taggedUsers) {
            
            if ([commentTextField.text rangeOfString:usr.username].location == NSNotFound) {
                //do not send push notification to that user
            }
            else {
                [channels addObject:[usr objectForKey:kPAPUserPrivateChannelKey]];
            }
            
            
        }
        
        NSString *message;
        if (mediatype == VIDEO_MEDIA) {
            message = [NSString stringWithFormat:@"%@ tagged you in video",[PFUser currentUser].username];
        }
        else if(mediatype == IMAGE_MEDIA) {
            message = [NSString stringWithFormat:@"%@ tagged you in photo",[PFUser currentUser].username];
        }
        else if(mediatype == AUDIO_MEDIA) {
            message = [NSString stringWithFormat:@"%@ tagged you in audio",[PFUser currentUser].username];
        }
        
        NSDictionary *payload =
        [NSDictionary dictionaryWithObjectsAndKeys:
         message, kAPNSAlertKey,
         @"Increment",kAPNSBadgeKey,
         kPAPPushPayloadPayloadTypeActivityKey, kPAPPushPayloadPayloadTypeKey,
         kPAPPushPayloadActivityTaggedInPhotoKey, kPAPPushPayloadActivityTypeKey,
         [[PFUser currentUser] objectId], kPAPPushPayloadFromUserObjectIdKey,
         [object objectId], kPAPPushPayloadPhotoObjectIdKey,
         @"sms-received.wav",kAPNSSoundKey,
         nil];
        
        PFPush *push = [[PFPush alloc] init];
        
        // Be sure to use the plural 'setChannels'.
        [push setChannels:channels];
        [push setData:payload];
        //[push setMessage:[NSString stringWithFormat:@"%@ tagged you in photo",[PFUser currentUser].username]];
        [push sendPushInBackground];
    }
    
}

#pragma mark - Video upload

-(BOOL)shouldUploadVideo:(UIImage*)anImage
{
    UIImage *resizedImage = [anImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(560.0f, 560.0f) interpolationQuality:kCGInterpolationHigh];
    UIImage *thumbnailImage = [anImage thumbnailImage:86.0f transparentBorder:0.0f cornerRadius:10.0f interpolationQuality:kCGInterpolationDefault];
    
    // JPEG to decrease file size and enable faster uploads & downloads
    NSData *imageData = UIImageJPEGRepresentation(resizedImage, 0.8f);
    NSData *thumbnailImageData = UIImagePNGRepresentation(thumbnailImage);
    NSData *videoData = [NSData dataWithContentsOfFile:videofilePath];
    
   // NSLog(@"%@ ,%@",imageData,thumbnailImageData);
    
    if (!imageData || !thumbnailImageData) {
        return NO;
    }
    
    self.photoFile = [PFFile fileWithName:@"image.png" data:imageData];
    self.thumbnailFile = [PFFile fileWithName:@"thumbnail.png" data:thumbnailImageData];
    self.videoFile = [PFFile fileWithName:@"video.mp4" data:videoData];

    
//    ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
//    [assetLibrary assetForURL:videofilePath resultBlock:^(ALAsset *asset){
//        NSLog(@"called");
//        ALAssetRepresentation *rep = [asset defaultRepresentation];
//        Byte *buffer = (Byte*)malloc(rep.size);
//        NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
//        NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
//        NSLog(@"data length %d",data.length);
//        
//        self.photoFile = [PFFile fileWithData:imageData];
//        self.thumbnailFile = [PFFile fileWithData:thumbnailImageData];
//        self.videoFile = [PFFile fileWithData:data];
//    }
//    failureBlock:^(NSError *err){
//        
//    }];
//    self.fileUploadBackgroundTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
//        [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
//    }];
// ///////////////////////
//    [self.videoFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
//        
//        if (succeeded) {
//            [self.photoFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//                if (succeeded) {
//                    NSLog(@"Photo uploaded successfully");
//                    [self.thumbnailFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//                        if (succeeded) {
//                            NSLog(@"Thumbnail uploaded successfully");
//                        }
//                        [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
//                    }];
//                } else {
//                    [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
//                }
//            }];
//        }
//        else
//        {
//            NSLog(@"Video Upload Faild");
//        }
//    }];
 ///////////////////////
    
    return YES;
    
}

-(void)startUploadingVideoFile {
    
    //start progress view
    UploadProgress *up = [UploadProgress sharedInstance];
    
    // Request a background execution task to allow us to finish uploading the photo even if the app is backgrounded
    self.fileUploadBackgroundTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
    }];
    
    NSLog(@"Requested background expiration task with id %d for Anypic photo upload", self.fileUploadBackgroundTaskId);
    
    [self.videoFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
        
        if (succeeded) {
            
            [self startUploadingVideoThumbnailFile];
        }
        else
        {
            [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
            NSLog(@"Video Upload Faild");
            [up setMessage:@"failed to upload"];
            [up hide];
        }
    }progressBlock:^(int percentDone) {
        // Update your progress spinner here. percentDone will be between 0 and 100.
        float process = (float)percentDone/100;
        [up updateProgress:process];
    }];
}

-(void)startUploadingVideoThumbnailFile{
    
    //start progress view
    UploadProgress *up = [UploadProgress sharedInstance];
    
    [self.photoFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            
            NSLog(@"Photo uploaded successfully");
            [self.thumbnailFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                
                [up hide];
                if (succeeded) {
                    
                }
                [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
            }];
        }
        else{
            [up hide];
            [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
        }
    }progressBlock:^(int percentDone) {
        // Update your progress spinner here. percentDone will be between 0 and 100.
        float process = (float)percentDone/100;
        up.progressBar.progress = 0;
        [up setMessage:@"Finishing up"];
        [up updateProgress:process];
    }];
}

#pragma mark - Image Upload

-(void)startUploadingImageFile{
    
    //start progress view
    UploadProgress *up = [UploadProgress sharedInstance];
    
    // Request a background execution task to allow us to finish uploading the photo even if the app is backgrounded
    self.fileUploadBackgroundTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
    }];
    
    [self.photoFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            
            NSLog(@"Photo uploaded successfully");
            [self.thumbnailFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                
                [up hide];
                if (succeeded) {
                    
                }
                [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
                
            }progressBlock:^(int percentDone) {
                // Update your progress spinner here. percentDone will be between 0 and 100.
                float process = (float)percentDone/100;
                up.progressBar.progress = 0;
                [up setMessage:@"Finishing up"];
                [up updateProgress:process];
            }];
        }
        else{
            [up setMessage:@"failed to upload"];
            [up hide];
            [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
        }
    }progressBlock:^(int percentDone) {
        // Update your progress spinner here. percentDone will be between 0 and 100.
        float process = (float)percentDone/100;
        up.progressBar.progress = 0;
        [up setMessage:@"uploading.."];
        [up updateProgress:process];
    }];
}

#pragma mark - Audio upload

-(BOOL)shouldUploadAudio:(UIImage*)anImage{
    
//    UIImage *resizedImage = [anImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(560.0f, 560.0f) interpolationQuality:kCGInterpolationHigh];
//    UIImage *thumbnailImage = [anImage thumbnailImage:86.0f transparentBorder:0.0f cornerRadius:10.0f interpolationQuality:kCGInterpolationDefault];
//    
//    // JPEG to decrease file size and enable faster uploads & downloads
//    NSData *imageData = UIImageJPEGRepresentation(resizedImage, 0.8f);
//    NSData *thumbnailImageData = UIImagePNGRepresentation(thumbnailImage);
//    NSData *audioData = [NSData dataWithContentsOfURL:audioURL];
//    
//    
//    if (!imageData || !thumbnailImageData || !audioData) {
//        return NO;
//    }
//    
//    
//    
//    self.audioFile = [PFFile fileWithData:audioData];
//    self.photoFile = [PFFile fileWithData:imageData];
//    self.thumbnailFile = [PFFile fileWithData:thumbnailImageData];
//    
//    return YES;
    
}

-(void)startUploadingAudioFile {
    
    //start progress view
    UploadProgress *up = [UploadProgress sharedInstance];
    
    // Request a background execution task to allow us to finish uploading the photo even if the app is backgrounded
    self.fileUploadBackgroundTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
    }];
    
    NSLog(@"Requested background expiration task with id %d for Anypic photo upload", self.fileUploadBackgroundTaskId);
    
    [self.audioFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
        
        if (succeeded) {
            
            [self startUploadingVideoThumbnailFile];
        }
        else
        {
            [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
            NSLog(@"Video Upload Faild");
            [up setMessage:@"failed to upload"];
            [up hide];
        }
    }progressBlock:^(int percentDone) {
        // Update your progress spinner here. percentDone will be between 0 and 100.
        float process = (float)percentDone/100;
        [up updateProgress:process];
    }];
}
//######################
#pragma mark - OPtionViewControllerDelegate
-(void)optionSelected:(NSString *)option{
    
    [btnDone setTitle:option forState:UIControlStateNormal];
}


#pragma mark  Facebook Sharing
-(void)shareOnFB
{
    ProgressView *pView = [ProgressView sharedInstance];
    if ([[FBSession activeSession] isOpen]) {
        // session opened
        if (mediatype == VIDEO_MEDIA) {
            [pView setMessage:@"Posting video.."];
            [self uploadVideo];
        }
        else
        {
            [pView setMessage:@"Posting image.."];
            [self shareImage];
        }
        
    }
    else {
        
        [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES completionHandler:^(FBSession *session,
                                                                                                                                                                       FBSessionState status,
                                                                                                                                                                       NSError *error){
            
            if (mediatype == VIDEO_MEDIA) {
                [pView setMessage:@"Posting video.."];
                [self uploadVideo];
            }
            else
            {
                [pView setMessage:@"Posting image.."];
                [self shareImage];
            }

         }];
        

    }
}
-(void)shareImage
{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   commentTextField.text,@"message",
                                   self.publishImage, @"picture",
                                   nil];
    
    
    [FBRequestConnection startWithGraphPath:@"me/photos" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        
        
        if (result!= nil) {
            [Flurry logEvent:@"PhotoSharedOnFacebook"];
            ProgressView *pView = [ProgressView sharedInstance];
            [pView setMessage:@"Posted"];
            [pView hide];
            
            
            
        }
        
        
    }];
}
-(void)uploadVideo
{
          
       // NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
       // NSString *documentsDirectory = [paths objectAtIndex:0];
       // NSString *moviePath = [documentsDirectory stringByAppendingPathComponent:videofilePath];
    
    NSLog(@"the videofilePath is %@",videofilePath);
    ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
    [assetLibrary assetForURL:[NSURL URLWithString:videofilePath] resultBlock:^(ALAsset *asset){
        NSLog(@"called");
        ALAssetRepresentation *rep = [asset defaultRepresentation];
        Byte *buffer = (Byte*)malloc(rep.size);
        NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
        NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
        
        NSData * data1=[NSData dataWithContentsOfFile:videofilePath];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       data1, @"video.mov",
                                       @"video/quicktime", @"contentType",
                                       @"VIND", @"title",
                                       @"Created from VIND", @"description",
                                       nil];
        
        [FBRequestConnection startWithGraphPath:@"me/videos" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            
            if (result!= nil) {
                [Flurry logEvent:@"VideoPostedOnFacebook"];
                ProgressView *pView = [ProgressView sharedInstance];
                [pView setMessage:@"Posted"];
                [pView hide];
                
                
            }
            else {
                ProgressView *pView = [ProgressView sharedInstance];
                [pView setMessage:@"Posted"];
                [pView hide];
            }
        }];
        
    } failureBlock:^(NSError *err){
    }];
    
    
  
}
#pragma mark  Twitter Sharing
-(void)shareOnTwitter
{
   
    [Flurry logEvent:@"SharedOnTwitter"];
    
    SLComposeViewController *tweetSheet = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeTwitter];
   
   // NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
   
    
    [tweetSheet addImage:publishImage];
    [tweetSheet setInitialText:commentTextField.text];
    
    [self presentViewController:tweetSheet animated:YES completion:nil];
}
#pragma mark - Sms sharing
-(void)sendSMS
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.persistent = YES;
    pasteboard.image = [UIImage imageNamed:@"refresh_btn_on.png"];
    
    NSString *phoneToCall = @"sms:";
    NSString *phoneToCallEncoded = [phoneToCall stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:phoneToCallEncoded];
    [[UIApplication sharedApplication] openURL:url];
    if([MFMessageComposeViewController canSendText])
    {
          controller.body = commentTextField.text;
        //  controller.recipients = [NSArray arrayWithObjects:@"1(234)567-8910", nil];
        controller.messageComposeDelegate = self;
        
        NSMutableDictionary* navBarTitleAttributes = [[UINavigationBar appearance] titleTextAttributes].mutableCopy;
        UIFont *navBarTitleFont = navBarTitleAttributes[UITextAttributeFont];
        navBarTitleAttributes[UITextAttributeFont] = [UIFont systemFontOfSize:navBarTitleFont.pointSize];
        [[UINavigationBar appearance] setTitleTextAttributes:navBarTitleAttributes];
        
        [self presentViewController:controller animated:YES completion:^{ navBarTitleAttributes[UITextAttributeFont] = navBarTitleFont;
            [[UINavigationBar appearance] setTitleTextAttributes:navBarTitleAttributes];
    }];
        
        
        
    }
}
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    
    if (result == MessageComposeResultCancelled)
              
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Email
-(void)sendEmail
{
    ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
    [assetLibrary assetForURL:[NSURL URLWithString:videofilePath] resultBlock:^(ALAsset *asset){
        NSLog(@"calledvideofilePath is %@",videofilePath);
        ALAssetRepresentation *rep = [asset defaultRepresentation];
        Byte *buffer = (Byte*)malloc(rep.size);
        NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
        NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
        
        if ([MFMailComposeViewController canSendMail])
        {
            
            MFMailComposeViewController *  mailComposecontroller=[[MFMailComposeViewController alloc]init];
            mailComposecontroller.mailComposeDelegate=self;
            
            
            [mailComposecontroller setToRecipients:[NSArray arrayWithObjects:@" ", nil]];
            if (commentTextField.text.length == 0) {
                [mailComposecontroller setMessageBody:@"Uploaded using #VIND" isHTML:NO];
            }
            else {
                [mailComposecontroller setMessageBody:commentTextField.text isHTML:NO];
            }
            
            [mailComposecontroller setSubject:@"A message from VIND"];
            
            if (mediatype == IMAGE_MEDIA) {
                NSData * dataImage=UIImagePNGRepresentation(publishImage);
                [mailComposecontroller addAttachmentData:dataImage mimeType:@"image/jpg" fileName:@"photo.jpg"];
            }
            else
            {
                NSData * data=[NSData dataWithContentsOfFile:videofilePath];
                [mailComposecontroller addAttachmentData:data mimeType:@"video/quicktime" fileName:@"video.mov"];
            }
            
            
            [self presentViewController:mailComposecontroller animated:YES completion:^{
            }];
            
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Your device is not currently connected to an email account."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
        }
        
    } failureBlock:^(NSError *err){
    }];
    
  
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //  ////NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            //  ////NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            //NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            // ////NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    // UINavigationController *navigationController = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).navigationcontroller;
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark - 
#pragma mark - Foursquare Sharing






@end
