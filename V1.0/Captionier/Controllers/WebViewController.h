//
//  WebViewController.h
//  Restaurant
//
//  Created by 3Embed on 25/09/12.
//
//

#import <UIKit/UIKit.h>


@protocol WebViewControllerDelegate <NSObject>

- (void) dismissWithOption:(BOOL)acceptedOrRejected;

@end

@interface WebViewController : UIViewController
{
    
}
@property(nonatomic,strong)IBOutlet UIWebView *webview;
@property(nonatomic,strong)NSString *weburl;
@property (nonatomic, weak) id <WebViewControllerDelegate> delegate;
@end

