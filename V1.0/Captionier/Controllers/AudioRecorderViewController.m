//
//  AudioRecorderViewController.m
//  AudioRecorder
//
//  Created by rahul Sharma on 03/01/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "AudioRecorderViewController.h"
#import "PublishViewController.h"

@interface AudioRecorderViewController ()
@property(nonatomic,strong)AVAudioSession *session;
@property(nonatomic,strong)NSTimer *timer;
@property(nonatomic,assign) int countSeconds;

@end

@implementation AudioRecorderViewController
//@synthesize stopButton;
//@synthesize playButton;
//@synthesize recordPauseButton;
@synthesize session;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   // [self.navigationController.navigationBar setHidden:NO];
    
   
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonclicked:)];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
    _playButton.enabled = NO;
    _stopButton.enabled = NO;
    
    NSArray *dirPaths;
    NSString *docsDir;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(
                                                   NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    NSString *soundFilePath = [docsDir
                               stringByAppendingPathComponent:@"sound.caf"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:AVAudioQualityMin],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 2],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    NSError *error = nil;
    
    self.session = [AVAudioSession sharedInstance];
    [self.session setCategory:AVAudioSessionCategoryPlayAndRecord
                        error:nil];
    [self.session setActive:YES error:Nil];
    
    _audioRecorder = [[AVAudioRecorder alloc]
                      initWithURL:soundFileURL
                      settings:recordSettings
                      error:&error];
    [_audioRecorder recordForDuration:15];
    _audioRecorder.delegate = self;
    
    if (error)
    {
        NSLog(@"error: %@", [error localizedDescription]);
    } else {
        [_audioRecorder prepareToRecord];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)recordPauseTapped:(id)sender
{
    // Stop the audio player before recording
    
    if (![_recordPauseButton isSelected])
    {
        _countSeconds = 0;
        _playButton.enabled = NO;
        _stopButton.enabled = YES;
        //if ([_audioRecorder recordForDuration:15]) {
            [_audioRecorder record];
            _progressView.progress = 0.0;
        [self handleTimer];
            _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(handleTimer) userInfo:nil repeats:YES];
       // }
        //[_recordPauseButton setTitle:@"recording.." forState:UIControlStateNormal];
        [_recordPauseButton setSelected:YES];
        
    }
   
    
  
}

- (IBAction)stopTapped:(id)sender
{
    [_recordPauseButton setSelected:NO];
    _playButton.enabled = YES;
    _recordPauseButton.enabled = YES;
    [_timer invalidate];
    if (_audioRecorder.recording)
    {
        [_audioRecorder stop];
    }
    else if (_audioPlayer.playing) {
        [_audioPlayer stop];
    }
}

- (IBAction)playTapped:(id)sender
{
    if (!_audioRecorder.recording)
    {
        _stopButton.enabled = YES;
        _recordPauseButton.enabled = NO;
        
        NSError *error;
        
        _audioPlayer = [[AVAudioPlayer alloc]
                        initWithContentsOfURL:_audioRecorder.url
                        error:&error];
        
        _audioPlayer.delegate = self;
        
        if (error)
            NSLog(@"Error: %@",
                  [error localizedDescription]);
        else
            [_audioPlayer play];
    }
}
-(void)doneButtonclicked:(id)sender {
    
    [self.session setActive:NO error:Nil];
    
    PublishViewController *viewController;
    
    viewController = [[PublishViewController alloc] initWithNibName:@"PublishViewController-ip5" bundle:nil];
    //viewController.audioURL = _audioRecorder.url;
    [viewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    viewController.hidesBottomBarWhenPushed = YES;
    viewController.publishImage = [UIImage imageNamed:@"speaker_icon"];
   // [viewController setMediatype:AUDIO_MEDIA];
    [self.navigationController pushViewController:viewController animated:NO];

}
- (void) handleTimer
{
    _countSeconds++;
	_progressView.progress += .066;
    _timerlabel.text = [NSString stringWithFormat:@"%d",_countSeconds];
	if(_progressView.progress == 1.0)
	{
		//[_timer invalidate];
        [self stopTapped:nil];
		
	}
}

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    //[_recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    
    [_stopButton setEnabled:NO];
    [_playButton setEnabled:YES];
}
- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error{
    NSLog(@"audioRecorderEncodeErrorDidOccur :%@ ",[error localizedDescription]);
}
- (void)audioRecorderBeginInterruption:(AVAudioRecorder *)recorder{
    NSLog(@"audioRecorderBeginInterruption");
}
- (void)audioRecorderEndInterruption:(AVAudioRecorder *)recorder withOptions:(NSUInteger)flags{
    NSLog(@"audioRecorderEndInterruption");
}
- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Done"
                                                    message: @"Finish playing the recording!"
                                                   delegate: nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


@end
