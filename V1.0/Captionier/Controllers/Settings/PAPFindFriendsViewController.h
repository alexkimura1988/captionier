//
//  PAPFindFriendsViewController.h
//  Anypic
//
//  Created by Mattieu Gamache-Asselin on 5/9/12.
//

#import <AddressBookUI/AddressBookUI.h>
#import <MessageUI/MessageUI.h>
#import "PAPFindFriendsCell.h"
typedef enum {
    
    PROFILE_SCREEN = 0,
    INVITE_SCREEN = 1,
    
}ViewLoadedFrom;
@interface PAPFindFriendsViewController : PFQueryTableViewController <PAPFindFriendsCellDelegate, ABPeoplePickerNavigationControllerDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, UIActionSheetDelegate>

-(void)showHeader:(BOOL)_bool;
@end
