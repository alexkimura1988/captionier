//
//  PAPFindFriendsViewController.m
//  Anypic
//
//  Created by Mattieu Gamache-Asselin on 5/9/12.
//

#import "PAPFindFriendsViewController.h"
#import "PAPProfileImageView.h"
#import "AppDelegate.h"
#import "PAPLoadMoreCell.h"
#import "PAPAccountViewController.h"
#import "MBProgressHUD.h"
#import "OGProtocols.h"

typedef enum {
    PAPFindFriendsFollowingNone = 0,    // User isn't following anybody in Friends list
    PAPFindFriendsFollowingAll,         // User is following all Friends
    PAPFindFriendsFollowingSome         // User is following some of their Friends
} PAPFindFriendsFollowStatus;

@interface PAPFindFriendsViewController ()
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, assign) PAPFindFriendsFollowStatus followStatus;
@property (nonatomic, strong) NSString *selectedEmailAddress;
@property (nonatomic, strong) NSMutableDictionary *outstandingFollowQueries;
@property (nonatomic, strong) NSMutableDictionary *outstandingCountQueries;
@property (nonatomic, assign) BOOL showHeader;
@property (readwrite, nonatomic, copy) NSString *fbidSelection;

@property (nonatomic, strong) ABPeoplePickerNavigationController *addressBook;

@end

static NSUInteger const kPAPCellFollowTag = 2;
static NSUInteger const kPAPCellNameLabelTag = 3;
static NSUInteger const kPAPCellAvatarTag = 4;
static NSUInteger const kPAPCellPhotoNumLabelTag = 5;

@implementation PAPFindFriendsViewController
@synthesize headerView;
@synthesize followStatus;
@synthesize selectedEmailAddress;
@synthesize outstandingFollowQueries;
@synthesize outstandingCountQueries;
@synthesize showHeader;
@synthesize addressBook;

#pragma mark - Initialization
-(void)showHeader:(BOOL)_bool
{
    self.showHeader = _bool;
}
- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        
        self.outstandingFollowQueries = [NSMutableDictionary dictionary];
        self.outstandingCountQueries = [NSMutableDictionary dictionary];
        
        self.selectedEmailAddress = @"";

        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pull-to-refresh is enabled
        if (NSClassFromString(@"UIRefreshControl")) {
            self.pullToRefreshEnabled = NO;
        } else {
            self.pullToRefreshEnabled = YES;
        }
        
        // The number of objects to show per page
        self.objectsPerPage = 15;
        
        // Used to determine Follow/Unfollow All button status
        self.followStatus = PAPFindFriendsFollowingSome;
        
        [self.tableView setSeparatorColor:[UIColor colorWithRed:210.0f/255.0f green:203.0f/255.0f blue:182.0f/255.0f alpha:1.0]];
    }
    return self;
}


#pragma mark - UIViewController
- (void)customizeNavigationBaUnfollowAllButton {
	UIImage *imgButton = [UIImage imageNamed:@"ButtonNavigationBar.png"];
	
	UIButton *rightBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
	//[rightBarButton setBackgroundImage:imgButton forState:UIControlStateNormal];
    //[rightBarButton setBackgroundImage:[UIImage imageNamed:@"settings_btn_on.png"] forState:UIControlStateHighlighted];
	[rightBarButton setFrame:CGRectMake(0, 0, 95, 20)];
    [rightBarButton setTitle:@"Unfollow All" forState:UIControlStateNormal];
	
	rightBarButton.titleLabel.font = [UIFont fontWithName:Lato size:17];
    [rightBarButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBarButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
	//rightBarButton.titleLabel.shadowOffset = CGSizeMake(0,-1);
	//rightBarButton.titleLabel.shadowColor = [UIColor darkGrayColor];
	[rightBarButton addTarget:self action:@selector(unfollowAllFriendsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButton];
	
	
}
- (void)customizeNavigationBafollowAllButton {
	UIImage *imgButton = [UIImage imageNamed:@"ButtonNavigationBar.png"];
	
	UIButton *rightBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
	//[rightBarButton setBackgroundImage:imgButton forState:UIControlStateNormal];
    //[rightBarButton setBackgroundImage:[UIImage imageNamed:@"settings_btn_on.png"] forState:UIControlStateHighlighted];
	[rightBarButton setFrame:CGRectMake(0, 0, 80, 20)];
    [rightBarButton setTitle:@"Follow All" forState:UIControlStateNormal];
	
	rightBarButton.titleLabel.font = [UIFont fontWithName:Lato size:17];
    [rightBarButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBarButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
	//rightBarButton.titleLabel.shadowOffset = CGSizeMake(0,-1);
	//rightBarButton.titleLabel.shadowColor = [UIColor darkGrayColor];
	[rightBarButton addTarget:self action:@selector(followAllFriendsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButton];
	
	
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    
    UIView *texturedBackgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
    [texturedBackgroundView setBackgroundColor:WHITE_COLOR];
    self.tableView.backgroundView = texturedBackgroundView;
    
    self.tableView.backgroundColor = CLEAR_COLOR;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //self.title = @"Find Friends";
        
   // self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TitleFindFriends.png"]];
    
    //AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
   // [appDelegate addBackButtonTo:self.navigationItem];
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    //    backButton.titleLabel.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
    //    backButton.titleEdgeInsets = UIEdgeInsetsMake( 0.0f, 5.0f, 0.0f, 0.0f);
    //    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    //    [backButton setTitleColor:[UIColor colorWithRed:214.0f/255.0f green:210.0f/255.0f blue:197.0f/255.0f alpha:1.0] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
  //  [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn_on.png"] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
 
    
    Helper *helper = [Helper sharedInstance];
    
    if (helper.showHeaderINInviteScreen == 1) {
        
        if ([MFMailComposeViewController canSendMail] || [MFMessageComposeViewController canSendText]) {
            
            
            self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 98)];
            [self.headerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"invite_screen_facebook_contact_button"]]];
            
//            //contact BG
//            UIView *myContactsBG = [[UIView alloc] init];
//            myContactsBG.backgroundColor = DARKTABBAR_COLOR;
//            myContactsBG.frame = CGRectMake(0, 0, 320, 56);
//            [self.headerView addSubview:myContactsBG];
            
            //mycontact icon
            UIImageView *mycontactsIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"invite_screen_contact_icon"]];
            mycontactsIcon.frame = CGRectMake(10, 56/2 - 36/2, 35, 36);
            [self.headerView addSubview:mycontactsIcon];
            
            NSString *inviteString = @"Invite friends";
           // CGSize inviteStringSize = [inviteString sizeWithFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:16] constrainedToSize:CGSizeMake(310, CGFLOAT_MAX) lineBreakMode:NSLineBreakByTruncatingTail];
            UILabel *inviteLabel = [[UILabel alloc] initWithFrame:CGRectMake(56, 56/2 - 36.5/2, 250, 36.5)];
            [inviteLabel setText:inviteString];
            [inviteLabel setFont:[UIFont fontWithName:Lato size:15]];
            [inviteLabel setTextColor:WHITE_COLOR];
            [inviteLabel setBackgroundColor:[UIColor clearColor]];
            [self.headerView addSubview:inviteLabel];
            
            UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [clearButton setBackgroundColor:[UIColor clearColor]];
            [clearButton addTarget:self action:@selector(inviteFriendsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [clearButton setFrame:CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, self.headerView.frame.size.height/2)];
            [self.headerView addSubview:clearButton];
            
            
            //separtor
            UIImageView *seperator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line.png"]];
            seperator.frame = CGRectMake(0, 0, 320, .5);
            [self.headerView addSubview:seperator];
            
          
            
//            // facebook BG
//            UIView *facebookBG = [[UIView alloc] init];
//            facebookBG.backgroundColor = DARKTABBAR_COLOR;
//            facebookBG.frame = CGRectMake(0, 56.5, 320, 56);
//            [self.headerView addSubview:facebookBG];
            
            //facebook icon
            UIImageView *facebookIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"invite_screen_facebook_icon"]];
            facebookIcon.frame = CGRectMake(10, 56, 35.5, 36.5);
            [self.headerView addSubview:facebookIcon];


            /*Invite With facebook button*/
            UIButton *inviteWithFB = [UIButton buttonWithType:UIButtonTypeCustom];
            [inviteWithFB setBackgroundColor:[UIColor clearColor]];
            [inviteWithFB addTarget:self action:@selector(inviteFriendsWithFacebook:) forControlEvents:UIControlEventTouchUpInside];
            [inviteWithFB setFrame:CGRectMake(50, 56.5, 250, 36.5)];
            [inviteWithFB setTitle:@"Invite With Facebook" forState:UIControlStateNormal];
            inviteWithFB.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            inviteWithFB.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
            [inviteWithFB setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
            inviteWithFB.titleLabel.font = [UIFont fontWithName:Lato size:15];
            [self.headerView addSubview:inviteWithFB];
            
            
            
            UIImageView *separatorImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SeparatorTimeline.png"]];
            [separatorImage setFrame:CGRectMake(0, self.headerView.frame.size.height-2, 320, 2)];
            [self.headerView addSubview:separatorImage];
            
            
            [self.tableView setTableHeaderView:self.headerView];
        }

    }
 //   [self objectsDidLoad:nil];
//    if (NSClassFromString(@"UIRefreshControl")) {
//        // Use the new iOS 6 refresh control.
//        UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
//        self.refreshControl = refreshControl;
//        self.refreshControl.tintColor = BACKGROUND_COLOR;
//        [self.refreshControl addTarget:self action:@selector(refreshControlValueChanged:) forControlEvents:UIControlEventValueChanged];
//        self.pullToRefreshEnabled = NO;
//    }
}

//-(void)viewWillAppear:(BOOL)animated {
//    Helper *helper = [Helper sharedInstance];
//     if (helper.followersClicked == 1) { // followers
//         
//         PFQuery *queryFollowerCount = [PFQuery queryWithClassName:kPAPActivityClassKey];
//         [queryFollowerCount whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
//         [queryFollowerCount whereKey:kPAPActivityToUserKey equalTo:[PFUser currentUser]];
//         [queryFollowerCount setCachePolicy:kPFCachePolicyCacheThenNetwork];
//         [queryFollowerCount findObjectsInBackgroundWithBlock:^(NSArray *objects , NSError *error){
//             
//             NSLog(@"follower objects %@",objects);
//             
//             
//             
//             
//         }];
//     }
//     else {
//         PFQuery *queryFollowingCount = [PFQuery queryWithClassName:kPAPActivityClassKey];
//         [queryFollowingCount whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
//         [queryFollowingCount whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
//         [queryFollowingCount setCachePolicy:kPFCachePolicyCacheThenNetwork];
//         
//         [queryFollowingCount setCachePolicy:kPFCachePolicyCacheThenNetwork];
//         [queryFollowingCount findObjectsInBackgroundWithBlock:^(NSArray *objects , NSError *error){
//             
//             
//             NSLog(@"following objects %@",objects);
//             
//         }];
//     }
//    
//    
//}
#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.objects.count) {
        return [PAPFindFriendsCell heightForCell];
    } else {
        return 44.0f;
    }
}


#pragma mark - PFQueryTableViewController

- (PFQuery *)queryForTable {
    // Use cached facebook friend ids
    NSArray *facebookFriends = [[PAPCache sharedCache] facebookFriends];
    
   // [PFQuery clearAllCachedResults];
    
    // Query for all friends you have on facebook and who are using the app
    PFQuery *friendsQuery = [PFUser query];
    [friendsQuery whereKey:kPAPUserFacebookIDKey containedIn:facebookFriends];
    
    // Query for all auto-follow accounts
    NSMutableArray *autoFollowAccountFacebookIds = [[NSMutableArray alloc] initWithArray:kPAPAutoFollowAccountFacebookIds];
    [autoFollowAccountFacebookIds removeObject:[[PFUser currentUser] objectForKey:kPAPUserFacebookIDKey]];
    PFQuery *parseEmployeeQuery = [PFUser query];
    [parseEmployeeQuery whereKey:kPAPUserFacebookIDKey containedIn:autoFollowAccountFacebookIds];
    
   

    
    PFQuery *query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:friendsQuery, parseEmployeeQuery, nil]];
    query.cachePolicy = kPFCachePolicyNetworkOnly;
    
    if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByAscending:kPAPUserDisplayNameKey];

    
    //following query
//    PFQuery *queryFollowingCount = [PFQuery queryWithClassName:kPAPActivityClassKey];
//    [queryFollowingCount whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
//    [queryFollowingCount whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
//    [queryFollowingCount setCachePolicy:kPFCachePolicyCacheThenNetwork];
   
//
//
    //follower query
//    PFQuery *queryFollowerCount = [PFQuery queryWithClassName:kPAPActivityClassKey];
//    [queryFollowerCount whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
//    [queryFollowerCount whereKey:kPAPActivityToUserKey equalTo:[PFUser currentUser]];
//    [queryFollowerCount setCachePolicy:kPFCachePolicyCacheThenNetwork];

    
    return query;
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    if (NSClassFromString(@"UIRefreshControl")) {
        [self.refreshControl endRefreshing];
    }
}
//
//    Helper *helper = [Helper sharedInstance];
//    
//    
//    [PFQuery clearAllCachedResults];
//    PFQuery *isFollowingQuery = [PFQuery queryWithClassName:kPAPActivityClassKey];
//    
//    if (helper.followersClicked == 1) { // followers
////        [queryFollowerCount whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
////        [queryFollowerCount whereKey:kPAPActivityToUserKey equalTo:self.user];
//        
//        
//
//        
//        [isFollowingQuery whereKey:kPAPActivityToUserKey equalTo:[PFUser currentUser]];
//        [isFollowingQuery whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
//        //[isFollowingQuery whereKey:kPAPActivityToUserKey containedIn:self.objects];
//    }
//    else { // following
//        [isFollowingQuery whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
//        [isFollowingQuery whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
//        //[isFollowingQuery whereKey:kPAPActivityToUserKey containedIn:self.objects];
//    }
//    
//    
//    [isFollowingQuery setCachePolicy:kPFCachePolicyNetworkOnly];
//    
//    [isFollowingQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
//        if (!error) {
//            NSLog(@"number %d",number);
//            if (number == self.objects.count) {
//                self.followStatus = PAPFindFriendsFollowingAll;
//               // [self configureUnfollowAllButton];
//                for (PFUser *user in self.objects) {
//                    [[PAPCache sharedCache] setFollowStatus:YES user:user];
//                }
//            } else if (number == 0) {
//                
//                self.followStatus = PAPFindFriendsFollowingNone;
//                //[self configureFollowAllButton];
//                for (PFUser *user in self.objects) {
//                    [[PAPCache sharedCache] setFollowStatus:NO user:user];
//                }
//            } else {
//                self.followStatus = PAPFindFriendsFollowingSome;
//               // [self configureFollowAllButton];
//            }
//        }
//        
////        if (self.objects.count == 0) {
////            self.navigationItem.rightBarButtonItem = nil;
////            if (kPAPShowAlertOnce == 6) {
////                kPAPShowAlertOnce = 7;
////                self.navigationItem.rightBarButtonItem = nil;
////                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Currently none of  your facebook friends  using wrapapp. Invite them to checkout wrapapp." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
////                [alert show];
////            }
////            
////        }
//    }];
//    
//    if (self.objects.count == 0) {
//        self.navigationItem.rightBarButtonItem = nil;
//    }
//}
//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *FriendCellIdentifier = @"FriendCell";
    
    PAPFindFriendsCell *cell = [tableView dequeueReusableCellWithIdentifier:FriendCellIdentifier];
    if (cell == nil) {
        cell = [[PAPFindFriendsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:FriendCellIdentifier];
        [cell setDelegate:self];
         self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return cell;
    NSLog(@"object  %@",object);
    
   // PFUser *user = [object objectForKey:(PFUser*)object];
    
    
    
    [cell setUser:(PFUser*)object];

    [cell.photoLabel setText:@"0 photos"];
    
    NSDictionary *attributes = [[PAPCache sharedCache] attributesForUser:(PFUser *)object];
    
    if (attributes) {
        // set them now
        NSString *pluralizedPhoto;
        NSNumber *number = [[PAPCache sharedCache] photoCountForUser:(PFUser *)object];
        if ([number intValue] == 1) {
            pluralizedPhoto = @"photo";
        } else {
            pluralizedPhoto = @"photos";
        }
        [cell.photoLabel setText:[NSString stringWithFormat:@"%@ %@", number, pluralizedPhoto]];
    } else {
        @synchronized(self) {
            NSNumber *outstandingCountQueryStatus = [self.outstandingCountQueries objectForKey:indexPath];
            if (!outstandingCountQueryStatus) {
                [self.outstandingCountQueries setObject:[NSNumber numberWithBool:YES] forKey:indexPath];
                PFQuery *photoNumQuery = [PFQuery queryWithClassName:kPAPPhotoClassKey];
                [photoNumQuery whereKey:kPAPPhotoUserKey equalTo:object];
                [photoNumQuery setCachePolicy:kPFCachePolicyCacheThenNetwork];
                [photoNumQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
                    @synchronized(self) {
                        [[PAPCache sharedCache] setPhotoCount:[NSNumber numberWithInt:number] user:(PFUser *)object];
                        [self.outstandingCountQueries removeObjectForKey:indexPath];
                    }
                    PAPFindFriendsCell *actualCell = (PAPFindFriendsCell*)[tableView cellForRowAtIndexPath:indexPath];
                    NSString *pluralizedPhoto;
                    if (number == 1) {
                        pluralizedPhoto = @"gift";
                    } else {
                        pluralizedPhoto = @"gifts";
                    }
                    [actualCell.photoLabel setText:[NSString stringWithFormat:@"%d %@", number, pluralizedPhoto]];
                    
                }];
            };
        }
    }

    cell.followButton.selected = NO;
    cell.tag = indexPath.row;
    
    if (self.followStatus == PAPFindFriendsFollowingSome) {
//        if (attributes) {
//            [cell.followButton setSelected:[[PAPCache sharedCache] followStatusForUser:(PFUser *)object]];
//        } else {
            @synchronized(self) {
                NSNumber *outstandingQuery = [self.outstandingFollowQueries objectForKey:indexPath];
                if (!outstandingQuery) {
                    
                    Helper *helper = [Helper sharedInstance];
                    
                    if (helper.followersClicked == 1) {
                        [self.outstandingFollowQueries setObject:[NSNumber numberWithBool:YES] forKey:indexPath];
                        PFQuery *isFollowingQuery = [PFQuery queryWithClassName:kPAPActivityClassKey];
                        [isFollowingQuery whereKey:kPAPActivityToUserKey equalTo:[PFUser currentUser]];
                        [isFollowingQuery whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
                        [isFollowingQuery setCachePolicy:kPFCachePolicyCacheThenNetwork];
                        
                        [isFollowingQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
                            @synchronized(self) {
                                [self.outstandingFollowQueries removeObjectForKey:indexPath];
                                [[PAPCache sharedCache] setFollowStatus:(!error && number > 0) user:(PFUser *)object];
                            }
                            if (cell.tag == indexPath.row) {
                                [cell.followButton setSelected:(!error && number > 0)];
                            }
                        }];
                    }
                    else {
                        [self.outstandingFollowQueries setObject:[NSNumber numberWithBool:YES] forKey:indexPath];
                        PFQuery *isFollowingQuery = [PFQuery queryWithClassName:kPAPActivityClassKey];
                        [isFollowingQuery whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
                        [isFollowingQuery whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
                        [isFollowingQuery setCachePolicy:kPFCachePolicyCacheThenNetwork];
                        
                        [isFollowingQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
                            @synchronized(self) {
                                [self.outstandingFollowQueries removeObjectForKey:indexPath];
                                [[PAPCache sharedCache] setFollowStatus:(!error && number > 0) user:(PFUser *)object];
                            }
                            if (cell.tag == indexPath.row) {
                                [cell.followButton setSelected:(!error && number > 0)];
                            }
                        }];
                    }
                    
                    
                }
            }
        
    } else {
        [cell.followButton setSelected:(self.followStatus == PAPFindFriendsFollowingAll)];
    }
    
    return cell;
}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString *NextPageCellIdentifier = @"NextPageCell";
//    
//    PAPLoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:NextPageCellIdentifier];
//    
//    if (cell == nil) {
//        cell = [[PAPLoadMoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NextPageCellIdentifier];
//        [cell.mainView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundFindFriendsCell.png"]]];
//        cell.hideSeparatorBottom = YES;
//        cell.hideSeparatorTop = YES;
//    }
//    
//    cell.selectionStyle = UITableViewCellSelectionStyleGray;
//    
//    return cell;
//}


#pragma mark - PAPFindFriendsCellDelegate

- (void)cell:(PAPFindFriendsCell *)cellView didTapUserButton:(PFUser *)aUser {
    // Push account view controller
    PAPAccountViewController *accountViewController = [[PAPAccountViewController alloc] initWithStyle:UITableViewStylePlain];
    [accountViewController setUser:aUser];
    [self.navigationController pushViewController:accountViewController animated:YES];
}

- (void)cell:(PAPFindFriendsCell *)cellView didTapFollowButton:(PFUser *)aUser {
    [self shouldToggleFollowFriendForCell:cellView];
}


#pragma mark - ABPeoplePickerDelegate

/* Called when the user cancels the address book view controller. We simply dismiss it. */
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    [self dismissModalViewControllerAnimated:YES];
}

/* Called when a member of the address book is selected, we return YES to display the member's details. */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    return YES;
}

/* Called when the user selects a property of a person in their address book (ex. phone, email, location,...)
   This method will allow them to send a text or email inviting them to Anypic.  */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {

    if (property == kABPersonEmailProperty) {

        ABMultiValueRef emailProperty = ABRecordCopyValue(person,property);
        NSString *email = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emailProperty,identifier);
        self.selectedEmailAddress = email;

        if ([MFMailComposeViewController canSendMail] && [MFMessageComposeViewController canSendText]) {
            // ask user
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Invite %@",@""] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Email", @"iMessage", nil];
            //[actionSheet showFromTabBar:self.tabBarController.tabBar];
            [actionSheet showInView:addressBook.view];
        } else if ([MFMailComposeViewController canSendMail]) {
            // go directly to mail
            [self presentMailComposeViewController:email];
        } else if ([MFMessageComposeViewController canSendText]) {
            // go directly to iMessage
            [self presentMessageComposeViewController:email];
        }

    } else if (property == kABPersonPhoneProperty) {
        ABMultiValueRef phoneProperty = ABRecordCopyValue(person,property);
        NSString *phone = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(phoneProperty,identifier);
        
        if ([MFMessageComposeViewController canSendText]) {
            [self presentMessageComposeViewController:phone];
        }
    }
    
    return NO;
}

#pragma mark - MFMailComposeDelegate

/* Simply dismiss the MFMailComposeViewController when the user sends an email or cancels */
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissModalViewControllerAnimated:YES];  
}


#pragma mark - MFMessageComposeDelegate

/* Simply dismiss the MFMessageComposeViewController when the user sends a text or cancels */
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissModalViewControllerAnimated:YES];
}


#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }

    if (buttonIndex == 0) {
        [self presentMailComposeViewController:self.selectedEmailAddress];
    } else if (buttonIndex == 1) {
        [self presentMessageComposeViewController:self.selectedEmailAddress];
    }
}

#pragma mark - ()

- (void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)inviteFriendsButtonAction:(id)sender {
    
    
    
    addressBook = [[ABPeoplePickerNavigationController alloc] init];
    addressBook.peoplePickerDelegate = self;
    
    if ([MFMailComposeViewController canSendMail] && [MFMessageComposeViewController canSendText]) {
        addressBook.displayedProperties = [NSArray arrayWithObjects:[NSNumber numberWithInt:kABPersonEmailProperty], [NSNumber numberWithInt:kABPersonPhoneProperty], nil];
    } else if ([MFMailComposeViewController canSendMail]) {
        addressBook.displayedProperties = [NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonEmailProperty]];
    } else if ([MFMessageComposeViewController canSendText]) {
        addressBook.displayedProperties = [NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonPhoneProperty]];
    }

    [self presentModalViewController:addressBook animated:YES];
}
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}
-(void)inviteFriendsWithFacebook:(id)sender
{
   // DLog(@"inviteFriendsWithFacebook");
//    [self clickInviteFriends:Nil];
    
//    if ([FBSession activeSession].isOpen) {
//        [FBWebDialogs
//         presentRequestsDialogModallyWithSession:nil
//         message:@"Learn how to make your iOS apps social."
//         title:nil
//         parameters:nil
//         handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//             if (error) {
//                 // Error launching the dialog or sending the request.
//                 NSLog(@"Error sending request.");
//             } else {
//                 if (result == FBWebDialogResultDialogNotCompleted) {
//                     // User clicked the "x" icon
//                     NSLog(@"User canceled request.");
//                 } else {
//                     // Handle the send request callback
//                     NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
//                     if (![urlParams valueForKey:@"request"]) {
//                         // User clicked the Cancel button
//                         NSLog(@"User canceled request.");
//                     } else {
//                         // User clicked the Send button
//                         NSString *requestID = [urlParams valueForKey:@"request"];
//                         NSLog(@"Request ID: %@", requestID);
//                     }
//                 }
//             }
//         }];
    
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"https://itunes.apple.com/us/app/crgle/id858548785?ls=1&mt=8", @"link",
                                       
                                       NSLocalizedString(@"Crgle", nil), @"caption",
                                       NSLocalizedString(@"Hey, You have to check out this awesome app called Crgle! \nTake or upload photos.\nDownload it from the Apple app store here https://itunes.apple.com/us/app/crgle/id858548785?ls=1&mt=8 ", nil), @"description",
                                       nil];
        
        
        
        [FBRequestConnection startWithGraphPath:@"me/feed" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            
            
            if (error) {
                NSLog(@"error : %@", [error localizedDescription]);
            }
            else {
                [Flurry logEvent:@"InvitedWithFacebookSuccessfully"];
                ProgressIndicator *pi =[ProgressIndicator sharedInstance];
                [pi showMessage:@"Posted on Facebook" On:self.view];
            }
            
            
            //[self showAlert:message result:result error:error];
        }];
}

   

#pragma mark - FBFriendPickerDelegate implementation

// FBSample logic
// The following two methods implement the FBFriendPickerDelegate protocol. This shows an example of two
// interesting SDK features: 1) filtering support in the friend picker, 2) the "installed" field field when
// fetching me/friends. Filtering lets you choose whether or not to display each friend, based on application
// determined criteria; the installed field is present (and true) if the friend is also a user of the calling
// application.

- (void)friendPickerViewControllerSelectionDidChange:(FBFriendPickerViewController *)friendPicker {
    //self.activityTextView.text = @"";
    if (friendPicker.selection.count) {
        [self updateActivityForID:[[friendPicker.selection objectAtIndex:0] id]];
    } else {
        self.fbidSelection = nil;
    }
}

- (BOOL)friendPickerViewController:(FBFriendPickerViewController *)friendPicker
                 shouldIncludeUser:(id <FBGraphUser>)user {
    return [[user objectForKey:@"installed"] boolValue];
}

#pragma mark - private methods

// FBSample logic
// This is the workhorse method of this view. It updates the textView with the activity of a given user. It
// accomplishes this by fetching the "throw" actions for the selected user.
- (void)updateActivityForID:(NSString *)fbid {
    
    // keep track of the selction
    self.fbidSelection = fbid;
    
    // create a request for the "throw" activity
    FBRequest *playActivity = [FBRequest requestForGraphPath:[NSString stringWithFormat:@"%@/fb_sample_rps:throw", fbid]];
    [playActivity.parameters setObject:@"U" forKey:@"date_format"];
    
    // this block is the one that does the real handling work for the requests
    void (^handleBlock)(id) = ^(id<RPSGraphActionList> result) {
        if (result) {
            for (id<RPSGraphPublishedThrowAction> entry in result.data) {
                // we  translate the date into something useful for sorting and displaying
                entry.publish_date = [NSDate dateWithTimeIntervalSince1970:entry.publish_time.intValue];
            }
        }
        
        // sort the array by date
        NSMutableArray *activity = [NSMutableArray arrayWithArray:result.data];
        [activity sortUsingComparator:^NSComparisonResult(id<RPSGraphPublishedThrowAction> obj1,
                                                          id<RPSGraphPublishedThrowAction> obj2) {
            if (obj1.publish_date && obj2.publish_date) {
                return [obj2.publish_date compare:obj1.publish_date];
            }
            return NSOrderedSame;
        }];
        
        NSMutableString *output = [NSMutableString string];
        for (id<RPSGraphPublishedThrowAction> entry in activity) {
            NSDateComponents *c = [[NSCalendar currentCalendar]
                                   components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit
                                   fromDate:entry.publish_date];
            [output appendFormat:@"%02d/%02d/%02d - %@ %@ %@\n",
             c.month,
             c.day,
             c.year,
             entry.data.gesture.title,
             @"vs",
             entry.data.opposing_gesture.title];
        }
     //   self.activityTextView.text = output;
    };
    
    // this is an example of a batch request using FBRequestConnection; we accomplish this by adding
    // two request objects to the connection, and then calling start; note that each request handles its
    // own response, despite the fact that the SDK is serializing them into a single request to the server
    FBRequestConnection *connection = [[FBRequestConnection alloc] init];
    [connection addRequest:playActivity
         completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
             handleBlock(result);
         }];
    // start the actual request
    [connection start];
}

- (IBAction)clickInviteFriends:(id)sender {
    // if there is a selected user, seed the dialog with that user
    NSDictionary *parameters = self.fbidSelection ? @{@"to":self.fbidSelection} : nil;
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                  message:@"Please come play RPS with me!"
                                                    title:@"Invite a Friend"
                                               parameters:parameters
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (result == FBWebDialogResultDialogCompleted) {
                                                          NSLog(@"Web dialog complete: %@", resultURL);
                                                      } else {
                                                          NSLog(@"Web dialog not complete, error: %@", error.description);
                                                      }
                                                  }
                                              friendCache:Nil];
}


- (void)followAllFriendsButtonAction:(id)sender {
    
    [Flurry logEvent:@"FollowAllFriendsButtonClicked"];
    
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];

    self.followStatus = PAPFindFriendsFollowingAll;
    [self configureUnfollowAllButton];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Unfollow All" style:UIBarButtonItemStyleBordered target:self action:@selector(unfollowAllFriendsButtonAction:)];
        [self customizeNavigationBaUnfollowAllButton];

        NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:self.objects.count];
        for (int r = 0; r < self.objects.count; r++) {
            PFObject *user = [self.objects objectAtIndex:r];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:r inSection:0];
            PAPFindFriendsCell *cell = (PAPFindFriendsCell *)[self tableView:self.tableView cellForRowAtIndexPath:indexPath object:user];
            cell.followButton.selected = YES;
            [indexPaths addObject:indexPath];
        }
        
        [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        [MBProgressHUD hideAllHUDsForView:[UIApplication sharedApplication].keyWindow animated:YES];
        
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(followUsersTimerFired:) userInfo:nil repeats:NO];
        [PAPUtility followUsersEventually:self.objects block:^(BOOL succeeded, NSError *error) {
            // note -- this block is called once for every user that is followed successfully. We use a timer to only execute the completion block once no more saveEventually blocks have been called in 2 seconds
            [timer setFireDate:[NSDate dateWithTimeIntervalSinceNow:2.0f]];
        }];

    });
}

- (void)unfollowAllFriendsButtonAction:(id)sender {
    
    [Flurry logEvent:@"UnFollowAllFriendsButtonClicked"];
    
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];

    self.followStatus = PAPFindFriendsFollowingNone;
    [self configureFollowAllButton];

    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Follow All" style:UIBarButtonItemStyleBordered target:self action:@selector(followAllFriendsButtonAction:)];
        [self customizeNavigationBafollowAllButton];
        
        
        
        
        

        NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:self.objects.count];
        for (int r = 0; r < self.objects.count; r++) {
            PFObject *user = [self.objects objectAtIndex:r];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:r inSection:0];
            PAPFindFriendsCell *cell = (PAPFindFriendsCell *)[self tableView:self.tableView cellForRowAtIndexPath:indexPath object:user];
            cell.followButton.selected = NO;
            [indexPaths addObject:indexPath];
        }
        
        [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        [MBProgressHUD hideAllHUDsForView:[UIApplication sharedApplication].keyWindow animated:YES];

        [PAPUtility unfollowUsersEventually:self.objects];

        [[NSNotificationCenter defaultCenter] postNotificationName:PAPUtilityUserFollowingChangedNotification object:nil];
    });

}

- (void)shouldToggleFollowFriendForCell:(PAPFindFriendsCell*)cell {
    PFUser *cellUser = cell.user;
    NSLog(@"cell User %@",cellUser);
    if ([cell.followButton isSelected]) {
        // Unfollow
        cell.followButton.selected = NO;
        [PAPUtility unfollowUserEventually:cellUser];
        [[NSNotificationCenter defaultCenter] postNotificationName:PAPUtilityUserFollowingChangedNotification object:nil];
    } else {
        // Follow
        cell.followButton.selected = YES;
        [PAPUtility followUserEventually:cellUser block:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [[NSNotificationCenter defaultCenter] postNotificationName:PAPUtilityUserFollowingChangedNotification object:nil];
            } else {
                cell.followButton.selected = NO;
            }
        }];
    }
}

- (void)configureUnfollowAllButton {
  //  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Unfollow All" style:UIBarButtonItemStyleBordered target:self action:@selector(unfollowAllFriendsButtonAction:)];
    [self customizeNavigationBaUnfollowAllButton];
}

- (void)configureFollowAllButton {
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Follow All" style:UIBarButtonItemStyleBordered target:self action:@selector(followAllFriendsButtonAction:)];
    [self customizeNavigationBafollowAllButton];
}

- (void)presentMailComposeViewController:(NSString *)recipient {
    // Create the compose email view controller
    MFMailComposeViewController *composeEmailViewController = [[MFMailComposeViewController alloc] init];
    
    // Set the recipient to the selected email and a default text
    [composeEmailViewController setMailComposeDelegate:self];
    [composeEmailViewController setSubject:@"Join me on Crgle"];
    [composeEmailViewController setToRecipients:[NSArray arrayWithObjects:recipient, nil]];
    [composeEmailViewController setMessageBody:@"<h2>Share your pictures, share your story.</h2><p><a href=\"https://itunes.apple.com/us/app/crgle/id858548785?ls=1&mt=8\">FizFeliz</a> is the easiest way to share photos with your friends. Get the app and share your fun photos with the world.</p><p><a href=\"http://crgle.com/\">Crgle</a> is fully powered by <a href=\"http://parse.com\">Parse</a>.</p>" isHTML:YES];
    
    // Dismiss the current modal view controller and display the compose email one.
    // Note that we do not animate them. Doing so would require us to present the compose
    // mail one only *after* the address book is dismissed.
    [self dismissModalViewControllerAnimated:NO];
    [self presentModalViewController:composeEmailViewController animated:NO];
}

- (void)presentMessageComposeViewController:(NSString *)recipient {
    // Create the compose text message view controller
    MFMessageComposeViewController *composeTextViewController = [[MFMessageComposeViewController alloc] init];
    
    // Send the destination phone number and a default text
    [composeTextViewController setMessageComposeDelegate:self];
    [composeTextViewController setRecipients:[NSArray arrayWithObjects:recipient, nil]];
    [composeTextViewController setBody:@"Check out Crgel! https://itunes.apple.com/us/app/crgle/id858548785?ls=1&mt=8"];
    
    // Dismiss the current modal view controller and display the compose text one.
    // See previous use for reason why these are not animated.
    [self dismissModalViewControllerAnimated:NO];
    [self presentModalViewController:composeTextViewController animated:NO];
}

- (void)followUsersTimerFired:(NSTimer *)timer {
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:PAPUtilityUserFollowingChangedNotification object:nil];
}

- (void)refreshControlValueChanged:(UIRefreshControl *)refreshControl {
    [self loadObjects];
}

@end
