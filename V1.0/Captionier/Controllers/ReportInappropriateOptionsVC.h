//
//  ReportInappropriateOptionsVC.h
//  Mogram
//
//  Created by rahul Sharma on 06/10/13.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ReportInappropriateOptionsVC : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
@property(nonatomic,strong)PFObject *object;
@property(nonatomic,assign)int mediaType;
@end
