//
//  AVCameraCaptureManager.h
//  SlideShowMaker
//
//  Created by rahul Sharma on 19/07/13.
//  Copyright (c) 2013 3Embed. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

//@class AVCamRecorder;
@protocol AVCamCaptureManagerDelegate;

@interface AVCameraCaptureManager : NSObject{
   
}

@property (nonatomic,retain) AVCaptureSession *session;
@property (nonatomic,assign) AVCaptureVideoOrientation orientation;
@property (nonatomic,assign) UIImage *capturedImage;
//@property (nonatomic,retain) AVCaptureDeviceInput *videoInput;
//@property (nonatomic,retain) AVCaptureDeviceInput *audioInput;
@property (nonatomic,retain) AVCaptureDeviceInput *cameraInput;
@property (nonatomic,retain) AVCaptureStillImageOutput *stillImageOutput;
//@property (nonatomic,retain) AVCamRecorder *recorder;
@property (nonatomic,assign) id deviceConnectedObserver;
@property (nonatomic,assign) id deviceDisconnectedObserver;
@property (nonatomic,assign) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic,assign) id <AVCamCaptureManagerDelegate> delegate;

- (BOOL) setupSession;
//- (void) ButtonCamera;
//- (void) stopRecording;
- (void) captureStillImage;
- (BOOL) toggleCamera;
- (NSUInteger) cameraCount;
- (NSUInteger) micCount;
- (void) autoFocusAtPoint:(CGPoint)point;
- (void) continuousFocusAtPoint:(CGPoint)point;
- (AVCaptureDevice *) backCamera;

@end

// These delegate methods can be called on any arbitrary thread. If the delegate does something with the UI when called, make sure to send it to the main thread.
@protocol AVCamCaptureManagerDelegate <NSObject>
@optional
- (void) captureManager:(AVCameraCaptureManager *)captureManager didFailWithError:(NSError *)error;
- (void) captureManagerRecordingBegan:(AVCameraCaptureManager *)captureManager;
- (void) captureManagerRecordingFinished:(AVCameraCaptureManager *)captureManager;
- (void) captureManagerStillImageCaptured:(AVCameraCaptureManager *)captureManager;
- (void) captureManagerDeviceConfigurationChanged:(AVCameraCaptureManager *)captureManager;
- (void) captureManagerCapturedImage:(UIImage*)image Counter:(int)count;
@end
