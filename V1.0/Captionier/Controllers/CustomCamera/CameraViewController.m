//
//  CameraViewController.m
//  SlideShowMaker
//
//  Created by rahul Sharma on 19/07/13.
//  Copyright (c) 2013 3Embed. All rights reserved.
//

#import "CameraViewController.h"
#import "AVCameraCaptureManager.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import <QuartzCore/QuartzCore.h>





#import "AudioRecorderViewController.h"



#import "AppDelegate.h"
#import "UIImage+Crop.h"
#import "PublishViewController.h"
//#import "ProgressView.h"

#import "iRate.h"

@interface CameraViewController (AVCameraCaptureManagerDelegate) <AVCamCaptureManagerDelegate,UIActionSheetDelegate>

@property (nonatomic,strong) UINavigationController *navController;
@property (nonatomic, strong) ALAssetsLibrary * assetLibrary;
@property (nonatomic, strong) NSMutableArray * sessions;
@property (nonatomic, strong) UIPopoverController * popover;

@end



@implementation CameraViewController
@synthesize captureManager;
@synthesize captureVideoPreviewLayer;
@synthesize cameraPreview = _cameraPreview;
@synthesize imagePreview;
@synthesize lblImageCounter;
@synthesize cameraTopView;
@synthesize bottomView;
@synthesize btnCancel;
@synthesize btnFlash;
@synthesize btnChangeCamera;
@synthesize bottomImageView;
@synthesize superView;
@synthesize topView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Controller LifeCycle Mehods
-(void)makeUIChanges
{
    
    
    CGRect rect;
    
    //topView
    self.topView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navigation_bar_ios7@2x"]];
    
    
    rect = self.topView.frame;
    //camera Preview view
    UIImage *topImage = [UIImage imageNamed:@"camera_screen_camera_bg"];
    self.cameraPreview.frame = CGRectMake(0, CGRectGetHeight(rect), topImage.size.width, topImage.size.height);
    self.cameraTopView.frame = CGRectMake(0, 0, topImage.size.width, topImage.size.height);
    [self.cameraTopView setImage:topImage];
    
    rect = self.cameraPreview.frame;
    NSLog(@"height  %f",CGRectGetHeight(rect));
    UIImage *bottomViewImage = [UIImage imageNamed:@"camera_screen_tab_bar_background"];
    self.bottomView.backgroundColor = [UIColor colorWithPatternImage:bottomViewImage];
   
    
    //bottom View
    if ([Helper isPhone5]) {
        //shodow
        
         self.bottomView.frame = CGRectMake(0,CGRectGetMinY(rect)+ CGRectGetHeight(rect), 320, bottomViewImage.size.height);
       
    }
    else {
        
         self.bottomView.frame = CGRectMake(0,CGRectGetMinY(rect)+ CGRectGetHeight(rect) -10
                                            , 320, bottomViewImage.size.height);
    }
   

    
    
    //camerabutton
    UIImage *cameraButtonImage = [UIImage imageNamed:@"camera_screen_camera_button_off"];
    UIButton *cameraButton = (UIButton*)[bottomView viewWithTag:SSCaptureButtonTag];
    [cameraButton setImage:cameraButtonImage forState:UIControlStateNormal];
    [cameraButton setImage:[UIImage imageNamed:@"camera_screen_camera_button_on"] forState:UIControlStateHighlighted];
    cameraButton.frame = CGRectMake(320/2 - cameraButtonImage.size.width/2, self.bottomView.frame.size.height/2 - cameraButtonImage.size.height/2 , cameraButtonImage.size.width, cameraButtonImage.size.height);

    
    //library Button
    UIImage *libraryButtonImage = [UIImage imageNamed:@"camera_screen_gallery_button_off"];
    UIButton *libraryButton = (UIButton*)[bottomView viewWithTag:SSCLibraryButtonTag];
    [libraryButton setBackgroundImage:libraryButtonImage forState:UIControlStateNormal];
    [libraryButton setBackgroundImage:[UIImage imageNamed:@"camera_screen_gallery_button_on"] forState:UIControlStateHighlighted];
    libraryButton.frame = CGRectMake(20, self.bottomView.frame.size.height/2 - libraryButtonImage.size.height/2 , libraryButtonImage.size.width, libraryButtonImage.size.height);
    
    
    //flip camera Button
    UIImage *videoImage = [UIImage imageNamed:@"video_icon"];
    UIButton *videoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [videoButton setBackgroundImage:videoImage forState:UIControlStateNormal];
    videoButton.tag = SSVideoButtonTag;
    [videoButton setBackgroundImage:[UIImage imageNamed:@"video_icon"] forState:UIControlStateHighlighted];
    videoButton.frame = CGRectMake(300 - videoImage.size.width, self.bottomView.frame.size.height/2 - libraryButtonImage.size.height/2 + 25, videoImage.size.width, videoImage.size.height);
    [videoButton addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:videoButton];
    
    
   
    //cancel button
    [Helper setButton:btnCancel Text:@"Cancel" WithFont:Lato FSize:18 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    UIImage *cancelImage = [UIImage imageNamed:@"camera_cancel_btn.png"];
   // [btnCancel setBackgroundImage:cancelImage forState:UIControlStateNormal];

    
    
    //cangeCamera
    UIImage *changeCamreaImage = [UIImage imageNamed:@"Camera_filp_camera"];
    [btnChangeCamera setBackgroundImage:changeCamreaImage forState:UIControlStateNormal];
//    
//    
    //flash button
    UIImage *flashImage = [UIImage imageNamed:@"camera_screen_flashauto_"];
    [btnFlash setBackgroundImage:flashImage forState:UIControlStateNormal];
    if ([Helper isPhone5]) {
        
        btnFlash.frame = CGRectMake(300 - flashImage.size.width, 20, flashImage.size.width, flashImage.size.height);
        btnCancel.frame = CGRectMake(20, 30, 60, 20);
        btnChangeCamera.frame  = CGRectMake(160 - changeCamreaImage.size.width/2, 30, changeCamreaImage.size.width, changeCamreaImage.size.height);
        
    }
    else
    {
//        if (SYSTEM_VERSION_GREATER_THAN(@"7")) {
//            btnFlash.frame = CGRectMake(300 - flashImage.size.width, 12, flashImage.size.width-5, flashImage.size.height-5);
//           
//            btnCancel.frame = CGRectMake(20, 23, 60, 20);
//
//        }
//        else {
            btnFlash.frame = CGRectMake(300 - flashImage.size.width, 3, flashImage.size.width, flashImage.size.height);
            
            btnCancel.frame = CGRectMake(20, 15, 60, 20);

      //  }
               //[btnCancel setBackgroundImage:cancelImage forState:UIControlStateNormal];
    }
    
//    self.bottomView.backgroundColor = UIColorFromRGB(cCameraBottomViewColor);
////    UIImage *bgImage = [UIImage imageNamed:@"capture_bottom_bar.png"];
////  //  DLogSize(bgImage.size);
////    CGRect frame = bottomView.frame;
////    frame.size.height = bgImage.size.height;
////    frame.size.width = bgImage.size.width;
////    
////    bottomView.frame = frame;
////    
////    bottomImageView.frame = CGRectMake(0, 0, bgImage.size.width, bgImage.size.height);
////    bottomImageView.image = bgImage;
    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
//        superView.frame = CGRectMake(0, 20, superView.frame.size.width, superView.frame.size.height);
//    }
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //[self.navigationController setNavigationBarHidden:YES];
    
    [self makeUIChanges];
  
    
    
    if ([self captureManager] == nil) {
		AVCameraCaptureManager *manager = [[AVCameraCaptureManager alloc] init];
		[self setCaptureManager:manager];
		
		
		[[self captureManager] setDelegate:self];
        
		if ([[self captureManager] setupSession]) {
            // Create video preview layer and add it to the UI
			AVCaptureVideoPreviewLayer *newCaptureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:[[self captureManager] session]];
			UIView *view = [self cameraPreview];
			CALayer *viewLayer = [view layer];
			[viewLayer setMasksToBounds:YES];
			
			CGRect bounds = [view bounds];
			[newCaptureVideoPreviewLayer setFrame:bounds];
			
			//if ([newCaptureVideoPreviewLayer isOrientationSupported]) {
			//	[newCaptureVideoPreviewLayer setOrientation:AVCaptureVideoOrientationPortrait];
			//}
			
			[newCaptureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
			
			[viewLayer insertSublayer:newCaptureVideoPreviewLayer below:[[viewLayer sublayers] objectAtIndex:0]];
			
			[self setCaptureVideoPreviewLayer:newCaptureVideoPreviewLayer];
            
			
            
        }

    }
    
   


}


 
-(void)viewWillAppear:(BOOL)animated
{
    
  [self.navigationController setNavigationBarHidden:YES];
  [self hideStatusBar:YES];
    
    Helper *helper = [Helper sharedInstance];
    
    if (helper.videoControllerPoped == 1) {
        helper.videoControllerPoped = 0;
        [self hideStatusBar:NO];
        [self.tabBarController setSelectedIndex:0];
    }
    else
    {
        // Start the session. This is done asychronously since -startRunning doesn't return until the session is running.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[[self captureManager] session] startRunning];
        });
    }
    
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
}

-(void)viewWillDisappear:(BOOL)animated
{
    //[self.navigationController setNavigationBarHidden:NO];
    
    
   
    // stop the session. This is done asychronously since -stopRunning doesn't return until the session is stopped.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         [[[self captureManager]session] stopRunning];
    });
    
}
-(void)hideStatusBar:(BOOL)hide {
    
    [[UIApplication sharedApplication] setStatusBarHidden:hide withAnimation:UIStatusBarAnimationSlide];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIImage *)captureView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
    
    
  
}
-(UIImage *) screenshot
{
    
//    CGRect rect;
//    rect=CGRectMake(0, 0, 320, 333);
//    UIGraphicsBeginImageContext(rect.size);
//    
//    CGContextRef context=UIGraphicsGetCurrentContext();
//    [self.captureVideoPreviewLayer renderInContext:context];
//    
//    UIImage *image=UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    return image;
    
    
    
    UIGraphicsBeginImageContext(self.captureVideoPreviewLayer.bounds.size);
    [self.captureVideoPreviewLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark - ButtonActions

-(IBAction)btnClicked:(id)sender
{
    UIButton *btn  = (UIButton*)sender;
    
    switch (btn.tag) {
        case SSCaptureButtonTag:
        {
            
            
//            PublishViewController *viewController;
//           // if ([Helper isPhone5]) {
//                viewController = [[PublishViewController alloc] initWithNibName:@"PublishViewController-ip5" bundle:nil];
//           
//            
//            
//            [viewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//            
//            [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//            viewController.hidesBottomBarWhenPushed = YES;
//            //viewController.publishImage = croppedImage;
//            [viewController setMediatype:IMAGE_MEDIA];
//            [self.navigationController pushViewController:viewController animated:NO];
//
//            return;
            
            //[[iRate sharedInstance] logEvent:NO];
            // Capture a still image
            //[btn setEnabled:NO];
            
            HUD = [[MBProgressHUD alloc] initWithView:self.view];
            [self.view addSubview:HUD];
            
            // Register for HUD callbacks so we can remove it from the window at the right time
            HUD.delegate = self;
            
            // Show the HUD while the provided method executes in a new thread
            [HUD show:YES];
            
          

            
            [[self captureManager] captureStillImage];
            
            // Flash the screen white and fade it out to give UI feedback that a still image was taken
            UIView *flashView = [[UIView alloc] initWithFrame:[[self cameraPreview] frame]];
            [flashView setBackgroundColor:[UIColor whiteColor]];
            [[[self view] window] addSubview:flashView];
            
            [UIView animateWithDuration:.2f
                             animations:^{
                                 [flashView setAlpha:0.f];
                             }
                             completion:^(BOOL finished){
                                 [flashView removeFromSuperview];
                               
//                                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                                     
//                                     if ([[self backFacingCamera] flashMode] != AVCaptureFlashModeOn){
//                                         [[[self captureManager]session] stopRunning];
//                                     }
//
//                                 });
                               
                                 
                             }
             ];
            break;
        }
        case SSCLibraryButtonTag:
        {
           // [[iRate sharedInstance] logEvent:NO];
            [self loadPhotoLibrary];
           
            
            break;
        }
        case SSFlashButtonTag:
        {
            AVCaptureDevice *backCamera = [[self captureManager] backCamera];
            
            if ([backCamera flashMode] == AVCaptureFlashModeAuto) { // currently Auto -> On
                
                if ([backCamera lockForConfiguration:nil]) {
                    if ([backCamera isFlashModeSupported:AVCaptureFlashModeOn]) {
                        [backCamera setFlashMode:AVCaptureFlashModeOn];
                        
                        //[btn setTitle:@"On" forState:UIControlStateNormal];
                        [btn setBackgroundImage:[UIImage imageNamed:@"camera_screen_flash_"] forState:UIControlStateNormal];
                    }
                    [backCamera unlockForConfiguration];
                }
               
            }
            else  if ([backCamera flashMode] == AVCaptureFlashModeOn) { // currently ON - > Off
                
                if ([backCamera lockForConfiguration:nil]) {
                    if ([backCamera isFlashModeSupported:AVCaptureFlashModeOff]) {
                        [backCamera setFlashMode:AVCaptureFlashModeOff];
                        
                        //[btn setTitle:@"Off" forState:UIControlStateNormal];
                        [btn setBackgroundImage:[UIImage imageNamed:@"camera_screen_flashoff_"] forState:UIControlStateNormal];
                    }
                    [backCamera unlockForConfiguration];
                }
                
            }
            else  if ([backCamera flashMode] == AVCaptureFlashModeOff) { // currently OFF - > Auto
                
                if ([backCamera lockForConfiguration:nil]) {
                    if ([backCamera isFlashModeSupported:AVCaptureFlashModeAuto]) {
                        [backCamera setFlashMode:AVCaptureFlashModeAuto];
                        
                        //[btn setTitle:@"Auto" forState:UIControlStateNormal];
                        [btn setBackgroundImage:[UIImage imageNamed:@"camera_screen_flashauto_"] forState:UIControlStateNormal];
                    }
                    [backCamera unlockForConfiguration];
                }
                
            }
            

            break;
        }
        case SSToggleCameraButtonTag:
        {
            
            // Toggle between cameras when there is more than one
            [[self captureManager] toggleCamera];
            
            // Do an initial focus
            [[self captureManager] continuousFocusAtPoint:CGPointMake(.5f, .5f)];
            
//            VideoViewController *video;
//            if ([Helper isPhone5]) {
//                video = [[VideoViewController alloc] initWithNibName:@"VideoViewController-ip5" bundle:nil];
//            }
//            else {
//                video = [[VideoViewController alloc] initWithNibName:@"VideoViewController" bundle:nil];
//            }
//            
//            video.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:video animated:YES];
            
            break;
        }
        case SSVideoButtonTag:
        {
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Record with" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Video",@"Audio", nil];
            [actionSheet showInView:self.view];
            
//            VideoViewController *video;
//            if ([Helper isPhone5]) {
//                    video = [[VideoViewController alloc] initWithNibName:@"VideoViewController-ip5" bundle:nil];
//            }
//            else {
//                    video = [[VideoViewController alloc] initWithNibName:@"VideoViewController" bundle:nil];
//            }
//            
//            video.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:video animated:YES];
//            [self hideStatusBar:NO];
//            AudioRecorderViewController *voiceRecordVC = [[AudioRecorderViewController alloc] init];
//            [self.navigationController pushViewController:voiceRecordVC animated:YES];
            
            
            break;
        }
        case SSCancelButtonTag:
        {
            [self hideStatusBar:NO];
            [self.tabBarController setSelectedIndex:0];
        }
       
            
        default:
            break;
    }
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [self hideStatusBar:NO];
    if (buttonIndex != actionSheet.cancelButtonIndex) {
        if (buttonIndex == 0) {
//            VideoViewController *video;
//            if ([Helper isPhone5]) {
//                video = [[VideoViewController alloc] initWithNibName:@"VideoViewController-ip5" bundle:nil];
//            }
//            else {
//                video = [[VideoViewController alloc] initWithNibName:@"VideoViewController" bundle:nil];
//            }
//            
//            video.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:video animated:YES];
            
        }
        else{
            AudioRecorderViewController *voiceRecordVC = [[AudioRecorderViewController alloc] init];
            [self.navigationController pushViewController:voiceRecordVC animated:YES];
        }
    }
   
}

#pragma mark - AVCameraCaptureManagerDelegate

- (void) captureManagerCapturedImage:(UIImage*)image Counter:(int)count
{
    
     
    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
        //[[self stillButton] setEnabled:YES];
    });
   
    
    /*Crop the image in square form wXh = 720X1280*/
    UIImage *cimage = [self squareimage:image];
    //resize image in 640X640 ratio
    UIImage *croppedImage = [self getcroppedImage:cimage];
    
   // [self launchPhotoEditorWithImage:croppedImage highResolutionImage:nil];
    

}
- (void) captureManager:(AVCameraCaptureManager *)captureManager didFailWithError:(NSError *)error
{
    //[Helper showAlertWithTitle:@"Error" Message:[NSString stringWithFormat:@"%@. Please try again.",[error localizedDescription]]];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@. Please try again.",[error localizedDescription]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.tag = 100;
    [alert show];
    if (HUD) {
        [HUD hide:YES];
    }
    
}
-(UIImage*)squareimage:(UIImage*)image {
    
    CGSize imageSize = image.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    if (width != height) {
        CGFloat newDimension = MIN(width, height);
        CGFloat widthOffset = (width - newDimension) / 2;
        CGFloat heightOffset = (height - newDimension) / 2;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDimension, newDimension), NO, 0.);
        [image drawAtPoint:CGPointMake(-widthOffset, -heightOffset)
                 blendMode:kCGBlendModeCopy
                     alpha:1.];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return image;
}
#pragma mark - ImageResize

-(UIImage*)getcroppedImage:(UIImage*)oldImage
{
    CGSize size;
    size.height = 640;
    size.width = 640;
    
    UIGraphicsBeginImageContext(size);
    [oldImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage2;
    
}

#pragma mark - UIImagePikcerviewControllerDeleage
-(void)loadPhotoLibrary
{
    UIImagePickerController *imagepicker=[[UIImagePickerController alloc]init];
    imagepicker.delegate=self;
    imagepicker.allowsEditing = YES;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagepicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
   
        
        
        
        [self presentViewController:imagepicker
                           animated:YES completion:nil];
        
        
        
   
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
	
	[self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
     [self dismissViewControllerAnimated:YES completion:^{
         UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
         
//         PublishViewController *viewController;
//         //if ([Helper isPhone5]) {
//         viewController = [[PublishViewController alloc] initWithNibName:@"PublishViewController-ip5" bundle:nil];
//         //    }
//         //    else {
//         //        viewController = [[PublishViewController alloc] initWithNibName:@"PublishViewController" bundle:nil];
//         //    }
//         
//         
//         [viewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//         
//         [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//         viewController.hidesBottomBarWhenPushed = YES;
//         viewController.publishImage = image;
//         [viewController setMediatype:IMAGE_MEDIA];
//         [self.navigationController pushViewController:viewController animated:NO];
        // DLogSize(image.size);
        // [self launchPhotoEditorWithImage:image highResolutionImage:nil];
     }];
    
   
    //UIImage *croppedImage = [self getcroppedImage:image];
    
}


#pragma mark - ALAssets Helper Methods

- (UIImage *)editingResImageForAsset:(ALAsset*)asset
{
    CGImageRef image = [[asset defaultRepresentation] fullScreenImage];
    
    return [UIImage imageWithCGImage:image scale:1.0 orientation:UIImageOrientationUp];
}

- (UIImage *)highResImageForAsset:(ALAsset*)asset
{
    ALAssetRepresentation * representation = [asset defaultRepresentation];
    
    CGImageRef image = [representation fullResolutionImage];
    // UIImageOrientation orientation = [representation orientation];
    CGFloat scale = [representation scale];
    
    return [UIImage imageWithCGImage:image scale:scale orientation:UIImageOrientationUp];
}

#pragma mark - Private Helper Methods

- (BOOL) hasValidAPIKey
{
    NSString * key = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Aviary-API-Key"];
    if ([key isEqualToString:@"Your-Aviary-API-Key"]) {
        [[[UIAlertView alloc] initWithTitle:@"Oops!" message:@"You forgot to add your aviary API key!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return NO;
    }
    return YES;
}
#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD hides
    [HUD removeFromSuperview];
	HUD = nil;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [[[self captureManager] session] startRunning];
        });
    }
}
- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == position) {
            return device;
        }
    }
    return nil;
}
// Find a front facing camera, returning nil if one is not found
- (AVCaptureDevice *) frontFacingCamera
{
    return [self cameraWithPosition:AVCaptureDevicePositionFront];
}

// Find a back facing camera, returning nil if one is not found
- (AVCaptureDevice *) backFacingCamera
{
    return [self cameraWithPosition:AVCaptureDevicePositionBack];
}

@end
