//
//  CameraViewController.h
//  SlideShowMaker
//
//  Created by rahul Sharma on 19/07/13.
//  Copyright (c) 2013 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

typedef enum {
    SSCaptureButtonTag = 10,
    SSCLibraryButtonTag = 11,
    SSVideoButtonTag = 12,
    SSFlashButtonTag = 13,
    SSToggleCameraButtonTag = 14,
    SSCancelButtonTag = 15
    
}SSCameraViewControllerButtonTags;

@class AVCameraCaptureManager, AVCamPreviewView, AVCaptureVideoPreviewLayer;
@interface CameraViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,MBProgressHUDDelegate>
{
     MBProgressHUD *HUD;
}
@property(nonatomic,strong) IBOutlet UIView *cameraPreview;
@property (nonatomic,retain) AVCameraCaptureManager *captureManager;
@property (nonatomic,strong) IBOutlet UIImageView *imagePreview;
@property (nonatomic,strong) IBOutlet UILabel *lblImageCounter;
@property (nonatomic,weak)   IBOutlet UIImageView *cameraTopView;
@property (nonatomic,weak)   IBOutlet UIImageView *bottomImageView;
@property (nonatomic,weak)   IBOutlet UIButton *btnCancel;
@property (nonatomic,weak)   IBOutlet UIButton *btnFlash;
@property (nonatomic,weak)   IBOutlet UIButton *btnChangeCamera;
@property (nonatomic,weak)   IBOutlet UIView *bottomView;
@property (nonatomic,weak)   IBOutlet UIView *topView;
@property (nonatomic,retain) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (nonatomic,weak) IBOutlet UIView *superView;
-(IBAction)btnClicked:(id)sender;
@end
