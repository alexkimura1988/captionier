//
//  ReportInappropriateOptionsVC.m
//  Mogram
//
//  Created by rahul Sharma on 06/10/13.
//
//

#import "ReportInappropriateOptionsVC.h"

@interface ReportInappropriateOptionsVC (){
    
    
}
@property(nonatomic,strong)NSArray *reportOptions;
@property(nonatomic,strong)NSArray *videoReportOptions;
@property (nonatomic,strong)IBOutlet UITableView *tbleView;
@property(nonatomic,strong)PFUser *photoUser;
@end

@implementation ReportInappropriateOptionsVC
@synthesize object;
@synthesize tbleView;
@synthesize photoUser;
@synthesize mediaType;
#define IMAGE_MEDIA 0
#define VIDEO_MEDIA 1

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:WHITE_COLOR];
    self.tbleView.backgroundColor = [UIColor whiteColor];
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    //[backButton setBackgroundImage:[UIImage imageNamed:@"back_btn_on.png"] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    
    self.title = @"Report";
    
    _reportOptions = @[@"I don't like this Video/photo",@"Video/photo is spam or a scam",@"Video/photo contains inappropriate content",@"Video/photo contains Graphic Violence"];
    _videoReportOptions = @[@"I don't like this Video/photo",@"Video/photo is spam or a scam",@"Video/photo contains inappropriate content",@"Video/photo contains Graphic Violence"];
    photoUser = [self.object objectForKey:kPAPPhotoUserKey];
    
    // mediaType = IMAGE_MEDIA;
    
//    if ([[self.object objectForKey:kPAPPhotoMediaTypeKey] boolValue] == NO && [self.object objectForKey:kPAPPhotoVidoKey] != nil) {  // video
//         mediaType = VIDEO_MEDIA;
//     }
//     else {
//         mediaType = IMAGE_MEDIA;
//         
//     }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark TableView DataSourse Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return _reportOptions.count;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont fontWithName:LATO_LIGHT size:18];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.textColor = TIMESTAMP_COLOR;
//        cell.textLabel.shadowOffset = CGSizeMake(0, 1);
//        cell.textLabel.shadowColor = BLACK_COLOR;
        cell.textLabel.backgroundColor = CLEAR_COLOR;
        
        
//        UIImageView *imageSeprator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
//        
//        
//        UIImage *bgImage = [UIImage imageNamed:@"cell.png"];
//        UIImageView *bg = [[UIImageView alloc] initWithImage:bgImage];
//        cell.backgroundView = bg;
//        
//        UIImage *selectionBackground = [UIImage imageNamed:@"cell_selected.png"];
//        UIImageView *bgview=[[UIImageView alloc] initWithImage:selectionBackground];
//        cell.selectedBackgroundView=bgview;
        
//        UIImage *arrowImage = [UIImage imageNamed:@"arrow.png"];
//        UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(300 - arrowImage.size.width ,56/2 - arrowImage.size.height/2, arrowImage.size.width, arrowImage.size.height)];
//        arrowImageView.image = arrowImage;
//        arrowImageView.tag = 100;
//        [cell.contentView addSubview:arrowImageView];
        
        
        UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height +10, 300, 1)];
        line.image = [UIImage imageNamed:@"profile_screen_filter_line-568h"];
        [cell.contentView addSubview:line];
    }
    
    if (mediaType == IMAGE_MEDIA) {
         cell.textLabel.text = _reportOptions[indexPath.row];
        
    }
    else {
         cell.textLabel.text =  _videoReportOptions[indexPath.row];
    }
   
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
	[tableViewCell setSelected:NO animated:YES];
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.frame = CGRectMake(0, 0, 24, 24);
    
    tableViewCell.accessoryView = spinner;
    [spinner startAnimating];
    
   // NSString *message = [[NSString alloc] init] ;
    

    
    
    PFObject *report = [PFObject objectWithClassName:@"Reports"];
    [report setObject:photoUser forKey:@"reportedAgainst"];
    [report setObject:[PFUser currentUser] forKey:@"reportBy"];
    [report setObject:object forKey:@"reportedFor"];
    if (mediaType == IMAGE_MEDIA) {
        
        [report setObject:_reportOptions[indexPath.row] forKey:@"reason"];
        [report setObject:@"imageMedia" forKey:@"mediaType"];
        
    }
    else {
        
        [report setObject:_videoReportOptions[indexPath.row] forKey:@"reason"];
         [report setObject:@"videoMedia" forKey:@"mediaType"];
    }
    PFACL *photoACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [photoACL setPublicReadAccess:YES];
    [photoACL setPublicWriteAccess:YES];
    report.ACL = photoACL;
    [report saveInBackgroundWithBlock:^(BOOL successed , NSError *error){
        if (successed) {
            [spinner stopAnimating];
            if (mediaType == IMAGE_MEDIA) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Thankyou for your report. We will remove this photo if it violates our Community Guidelines." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil ];
                [alert show ];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Thankyou for your report. We will remove this video if it violates our Community Guidelines." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil ];
                [alert show ];
            }
           
        }
    }];
}

//-(void)openEmailWithMessge:(NSString*)message{
//    
//    if ([MFMailComposeViewController canSendMail])
//    {
//        
//        MFMailComposeViewController *  mailComposecontroller=[[MFMailComposeViewController alloc]init];
//        mailComposecontroller.mailComposeDelegate=self;
//        
//        [mailComposecontroller setToRecipients:[NSArray arrayWithObjects:@"info@3embed.com", nil]];
//        [mailComposecontroller setMessageBody:message isHTML:NO];
//        [mailComposecontroller setSubject:@"Report Inappropriate"];
//        
//        if (mediaType == IMAGE_MEDIA) {
//            PFFile *imageFile = [self.object objectForKey:kPAPPhotoPictureKey];
//            NSData * dataImage=  [imageFile getData];//UIImagePNGRepresentation();
//            [mailComposecontroller addAttachmentData:dataImage mimeType:@"image/jpg" fileName:@"photo.jpg"];
//        }
//        else
//        {
//            PFFile *videofile = [self.object objectForKey:kPAPPhotoPictureKey];
//            NSData * dataImage=  [videofile getData];
//           // NSData *myData = [NSData dataWithContentsOfFile:videofilePath];
//            [mailComposecontroller addAttachmentData:dataImage mimeType:@"video/quicktime" fileName:@"video.mov"];
//        }
//        
//        
//        [self presentViewController:mailComposecontroller animated:YES completion:^{
//        }];
//        
//        
//        
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
//                                                        message:@"Your device is not currently connected to an email account."
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles: nil];
//        [alert show];
//    }
//
//}
//- (void)mailComposeController:(MFMailComposeViewController*)controller
//          didFinishWithResult:(MFMailComposeResult)result
//                        error:(NSError*)error {
//    switch (result)
//    {
//        case MFMailComposeResultCancelled:
//            //  ////NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
//            break;
//        case MFMailComposeResultSaved:
//            //  ////NSLog(@"Mail saved: you saved the email message in the drafts folder.");
//            break;
//        case MFMailComposeResultSent:
//        {
//            if (mediaType == IMAGE_MEDIA) {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Sent" message:@"Thankyou for your report. We will remove this photo if it violates our Community Guidelines." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil ];
//                [alert show ];
//            }
//            else {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Sent" message:@"Thankyou for your report. We will remove this video if it violates our Community Guidelines." delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil ];
//                [alert show ];
//            }
//          
//        
//            break;
//        }
//        case MFMailComposeResultFailed:
//            //NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
//            break;
//        default:
//            // ////NSLog(@"Mail not sent.");
//            break;
//    }
//    // Remove the mail view
//    // UINavigationController *navigationController = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).navigationcontroller;
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//    
//}
//-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
//    
//}

@end
