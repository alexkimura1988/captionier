//
//  InviteFriendsViewController.m
//  InsightApp
//
//  Created by Surender Rathore on 14/01/14.
//
//

#import "InviteFriendsViewController.h"

@interface InviteFriendsViewController ()
@property(nonatomic,strong)UIView *headerView;
@property (nonatomic, strong) ABPeoplePickerNavigationController *addressBook;
@property (nonatomic, strong) NSString *selectedEmailAddress;
@end

@implementation InviteFriendsViewController
@synthesize headerView;
@synthesize addressBook;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, 320, 58)];
    [self.headerView setBackgroundColor:[UIColor whiteColor]];
    
    //            //contact BG
    //            UIView *myContactsBG = [[UIView alloc] init];
    //            myContactsBG.backgroundColor = DARKTABBAR_COLOR;
    //            myContactsBG.frame = CGRectMake(0, 0, 320, 56);
    //            [self.headerView addSubview:myContactsBG];
    
    //mycontact icon
    UIImageView *mycontactsIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"invite_screen_contact_icon"]];
    mycontactsIcon.frame = CGRectMake(10, 56/2 - 36/2, 35, 36);
    [self.headerView addSubview:mycontactsIcon];
    
    NSString *inviteString = @"Invite friends";
    // CGSize inviteStringSize = [inviteString sizeWithFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:16] constrainedToSize:CGSizeMake(310, CGFLOAT_MAX) lineBreakMode:NSLineBreakByTruncatingTail];
    UILabel *inviteLabel = [[UILabel alloc] initWithFrame:CGRectMake(56, 56/2 - 36.5/2, 250, 36.5)];
    [inviteLabel setText:inviteString];
    [inviteLabel setFont:[UIFont fontWithName:Lato size:15]];
    [inviteLabel setTextColor:WHITE_COLOR];
    [inviteLabel setBackgroundColor:[UIColor clearColor]];
    [self.headerView addSubview:inviteLabel];
    
    UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [clearButton setBackgroundColor:[UIColor clearColor]];
    [clearButton addTarget:self action:@selector(inviteFriendsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [clearButton setFrame:CGRectMake(self.headerView.frame.origin.x, 0, self.headerView.frame.size.width, self.headerView.frame.size.height/2)];
    [self.headerView addSubview:clearButton];
    
    
    //separtor
    UIImageView *seperator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line.png"]];
    seperator.frame = CGRectMake(0, 0, 320, .5);
    [self.headerView addSubview:seperator];
    
    
    
    //            // facebook BG
    //            UIView *facebookBG = [[UIView alloc] init];
    //            facebookBG.backgroundColor = DARKTABBAR_COLOR;
    //            facebookBG.frame = CGRectMake(0, 56.5, 320, 56);
    //            [self.headerView addSubview:facebookBG];
    
    //facebook icon
    UIImageView *facebookIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"invite_screen_facebook_icon"]];
    facebookIcon.frame = CGRectMake(10, 56, 35.5, 36.5);
    [self.headerView addSubview:facebookIcon];
    
    
    /*Invite With facebook button*/
    UIButton *inviteWithFB = [UIButton buttonWithType:UIButtonTypeCustom];
    [inviteWithFB setBackgroundColor:[UIColor clearColor]];
    [inviteWithFB addTarget:self action:@selector(inviteFriendsWithFacebook:) forControlEvents:UIControlEventTouchUpInside];
    [inviteWithFB setFrame:CGRectMake(50, 56.5, 250, 36.5)];
    [inviteWithFB setTitle:@"Invite With Facebook" forState:UIControlStateNormal];
    inviteWithFB.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    inviteWithFB.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [inviteWithFB setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    inviteWithFB.titleLabel.font = [UIFont fontWithName:Lato size:15];
    [self.headerView addSubview:inviteWithFB];
    
    
    
    UIImageView *separatorImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SeparatorTimeline.png"]];
    [separatorImage setFrame:CGRectMake(0, self.headerView.frame.size.height-2, 320, 2)];
    [self.headerView addSubview:separatorImage];
    
    self.headerView.backgroundColor = [UIColor redColor];
    
    [self.view addSubview:self.headerView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)inviteFriendsButtonAction:(id)sender {
    
    
    
    addressBook = [[ABPeoplePickerNavigationController alloc] init];
    addressBook.peoplePickerDelegate = self;
    
    if ([MFMailComposeViewController canSendMail] && [MFMessageComposeViewController canSendText]) {
        addressBook.displayedProperties = [NSArray arrayWithObjects:[NSNumber numberWithInt:kABPersonEmailProperty], [NSNumber numberWithInt:kABPersonPhoneProperty], nil];
    } else if ([MFMailComposeViewController canSendMail]) {
        addressBook.displayedProperties = [NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonEmailProperty]];
    } else if ([MFMessageComposeViewController canSendText]) {
        addressBook.displayedProperties = [NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonPhoneProperty]];
    }
    
    [self presentModalViewController:addressBook animated:YES];
}

#pragma mark - ABPeoplePickerDelegate

/* Called when the user cancels the address book view controller. We simply dismiss it. */
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/* Called when a member of the address book is selected, we return YES to display the member's details. */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    return YES;
}

/* Called when the user selects a property of a person in their address book (ex. phone, email, location,...)
 This method will allow them to send a text or email inviting them to Anypic.  */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    
    if (property == kABPersonEmailProperty) {
        
        ABMultiValueRef emailProperty = ABRecordCopyValue(person,property);
        NSString *email = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emailProperty,identifier);
        self.selectedEmailAddress = email;
        
        if ([MFMailComposeViewController canSendMail] && [MFMessageComposeViewController canSendText]) {
            // ask user
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Invite %@",@""] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Email", @"iMessage", nil];
            //[actionSheet showFromTabBar:self.tabBarController.tabBar];
            [actionSheet showInView:addressBook.view];
        } else if ([MFMailComposeViewController canSendMail]) {
            // go directly to mail
            [self presentMailComposeViewController:email];
        } else if ([MFMessageComposeViewController canSendText]) {
            // go directly to iMessage
            [self presentMessageComposeViewController:email];
        }
        
    } else if (property == kABPersonPhoneProperty) {
        ABMultiValueRef phoneProperty = ABRecordCopyValue(person,property);
        NSString *phone = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(phoneProperty,identifier);
        
        if ([MFMessageComposeViewController canSendText]) {
            [self presentMessageComposeViewController:phone];
        }
    }
    
    return NO;
}

#pragma mark - MFMailComposeDelegate

/* Simply dismiss the MFMailComposeViewController when the user sends an email or cancels */
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissModalViewControllerAnimated:YES];
}


#pragma mark - MFMessageComposeDelegate

/* Simply dismiss the MFMessageComposeViewController when the user sends a text or cancels */
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissModalViewControllerAnimated:YES];
}


#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    if (buttonIndex == 0) {
        [self presentMailComposeViewController:self.selectedEmailAddress];
    } else if (buttonIndex == 1) {
        [self presentMessageComposeViewController:self.selectedEmailAddress];
    }
}

- (void)presentMailComposeViewController:(NSString *)recipient {
    // Create the compose email view controller
    MFMailComposeViewController *composeEmailViewController = [[MFMailComposeViewController alloc] init];
    
    // Set the recipient to the selected email and a default text
    [composeEmailViewController setMailComposeDelegate:self];
    [composeEmailViewController setSubject:@"Join me on Crgle"];
    [composeEmailViewController setToRecipients:[NSArray arrayWithObjects:recipient, nil]];
    [composeEmailViewController setMessageBody:@"<h2>Share your pictures, share your story.</h2><p><a href=\"https://itunes.apple.com/us/app/crgle/id858548785?ls=1&mt=8\">Crgle</a> is the easiest way to share photos with your friends. Get the app and share your fun photos with the world.</p><p><a href=\"http://crgle.com\">Crgle</a> is fully powered by <a href=\"http://parse.com\">Parse</a>.</p>" isHTML:YES];
    
    // Dismiss the current modal view controller and display the compose email one.
    // Note that we do not animate them. Doing so would require us to present the compose
    // mail one only *after* the address book is dismissed.
    [self dismissModalViewControllerAnimated:NO];
    [self presentModalViewController:composeEmailViewController animated:NO];
}

- (void)presentMessageComposeViewController:(NSString *)recipient {
    // Create the compose text message view controller
    MFMessageComposeViewController *composeTextViewController = [[MFMessageComposeViewController alloc] init];
    
    // Send the destination phone number and a default text
    [composeTextViewController setMessageComposeDelegate:self];
    [composeTextViewController setRecipients:[NSArray arrayWithObjects:recipient, nil]];
    [composeTextViewController setBody:@"Check out Crgle! https://itunes.apple.com/us/app/crgle/id858548785?ls=1&mt=8"];
    
    // Dismiss the current modal view controller and display the compose text one.
    // See previous use for reason why these are not animated.
    [self dismissModalViewControllerAnimated:NO];
    [self presentModalViewController:composeTextViewController animated:NO];
}


@end
