//
//  SearchList.h
//
//
//  Created by Surender Rathore on 06/01/14.
//
//

#import <UIKit/UIKit.h>

typedef void (^SearchListCallback)(PFObject *pickedObject);

@interface SearchList : UIView <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) NSArray *searchList;
@property(nonatomic,strong) UITableView *tbleView;
@property(nonatomic,copy) SearchListCallback callback;

-(void)refresh;

@end
