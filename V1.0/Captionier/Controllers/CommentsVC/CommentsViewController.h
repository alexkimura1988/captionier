//
//  CommentsViewController.h
//  MealTrend
//
//  Created by rahul Sharma on 31/10/13.
//
//

#import <UIKit/UIKit.h>

@interface CommentsViewController : UIViewController
@property (nonatomic, strong) PFObject *photo;
@property (nonatomic, strong) PFUser *user;

@end
