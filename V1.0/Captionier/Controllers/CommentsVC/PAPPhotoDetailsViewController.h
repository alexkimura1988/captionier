//
//  PAPPhotoDetailViewController.h
//  Anypic
//
//  Created by Mattieu Gamache-Asselin on 5/15/12.
//

#import "PAPPhotoDetailsHeaderView.h"
#import "PAPBaseTextCell.h"
@protocol PAPPhotoDetailsViewDelegate;
@interface PAPPhotoDetailsViewController : PFQueryTableViewController <UITextFieldDelegate, UIActionSheetDelegate, PAPPhotoDetailsHeaderViewDelegate, PAPBaseTextCellDelegate>

@property (nonatomic, strong) PFObject *photo;
@property (nonatomic, strong)id <PAPPhotoDetailsViewDelegate> delegate;
- (id)initWithPhoto:(PFObject*)aPhoto;
-(void)commentOnPhoto:(NSString*)comment;

@end

@protocol PAPPhotoDetailsViewDelegate <NSObject>

-(void)cell:(PAPBaseTextCell*)cell navigateToUser:(PFUser*)user;

-(void)cell:(PAPBaseTextCell*)cell navigateToHashTag:(NSString*)hashtag;
-(void)cell:(PAPBaseTextCell*)cell editCellText:(NSString*)text;
@end