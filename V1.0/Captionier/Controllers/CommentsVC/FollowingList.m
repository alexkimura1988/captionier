//
//  FollowingList.m
//  FizFeliz
//
//  Created by Surender Rathore on 06/01/14.
//
//

#import "FollowingList.h"
@interface FollowingList ()

@end
@implementation FollowingList
@synthesize followingList;
@synthesize tbleView;
@synthesize callback;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.tbleView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, frame.size.height)];
        self.tbleView.delegate = self;
        self.tbleView.dataSource = self;
        [self addSubview:self.tbleView];
    }
    return self;
}
-(void)refresh {
    
    [self.tbleView reloadData];
}
#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return followingList.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        
    }
    PFUser *user = followingList[indexPath.row];
   // PFUser *user = [object objectForKey:kPAPActivityToUserKey];
    //    NSString *displayname = [[[user objectForKey:kPAPUserDisplayNameKey] componentsSeparatedByString:@" "] lastObject];
    cell.textLabel.text = user.username; //[user objectForKey:kPAPUserDisplayNameKey];
    cell.detailTextLabel.text = [user objectForKey:kPAPUserDisplayNameKey];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"didSelectRowAtIndexPath");
    UITableViewCell* tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
	[tableViewCell setSelected:NO animated:YES];
    PFUser *object = followingList[indexPath.row];
   // PFUser *user = [object objectForKey:kPAPActivityToUserKey];
   // NSString *displayname = user.username; //[[user objectForKey:kPAPUserDisplayNameKey] componentsSeparatedByString:@" "][0];    NSLog(@"displayname");
    if (self.callback) {
        self.callback(object);
    }
}
-(NSString*)removeSpaces:(NSString*)str {
    NSMutableString *string = [[NSMutableString alloc] initWithString:str] ;
	[string replaceOccurrencesOfString:@" " withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0,[string length])];
 	return string;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
