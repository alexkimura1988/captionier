//
//  FollowingList.h
//  FizFeliz
//
//  Created by Surender Rathore on 06/01/14.
//
//

#import <UIKit/UIKit.h>
typedef void (^FollowingListCallback)(PFUser *taggedUser);
@interface FollowingList : UIView <UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)NSArray *followingList;
@property(nonatomic,strong)UITableView *tbleView;
@property(nonatomic,copy) FollowingListCallback callback;
-(void)refresh;
@end
