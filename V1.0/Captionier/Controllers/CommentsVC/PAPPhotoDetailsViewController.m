//
//  PAPPhotoDetailViewController.m
//  Anypic
//
//  Created by Mattieu Gamache-Asselin on 5/15/12.
//

#import "PAPPhotoDetailsViewController.h"
#import "PAPBaseTextCell.h"
#import "PAPActivityCell.h"
#import "PAPPhotoDetailsFooterView.h"
#import "PAPConstants.h"
#import "PAPAccountViewController.h"
#import "PAPLoadMoreCell.h"
#import "PAPUtility.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"




enum ActionSheetTags {
    MainActionSheetTag = 0,
    ConfirmDeleteActionSheetTag = 1
};

@interface PAPPhotoDetailsViewController ()<UITextFieldDelegate,UIActionSheetDelegate>
{
    NSNumber *previous ;
    PFObject *feedObject;
}
@property (nonatomic, strong) UITextField *commentTextField;
@property (nonatomic, strong) PAPPhotoDetailsHeaderView *headerView;
@property (nonatomic, assign) BOOL likersQueryInProgress;
@property (nonatomic, assign) BOOL isCommentedByUser;
@property (nonatomic,strong) UIView *commentView;
@property (nonatomic,strong) UITextField *commentField;
@property (nonatomic,strong) UIButton *buttonSend;
@property (nonatomic,strong) PFObject *selectedObeject;
@property (nonatomic,assign) NSInteger deletedCommentIndex;
@property (nonatomic, assign) BOOL isCommentEditing;
@property (nonatomic, assign) BOOL needToRefreshComments;
@end

static const CGFloat kPAPCellInsetWidth = 0.0f;

@implementation PAPPhotoDetailsViewController

@synthesize commentTextField;
@synthesize photo, headerView;
@synthesize commentView;
@synthesize commentField;
@synthesize buttonSend;
@synthesize delegate;

#pragma mark - Initialization

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PAPUtilityUserLikedUnlikedPhotoCallbackFinishedNotification object:self.photo];
}

- (id)initWithPhoto:(PFObject *)aPhoto {
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        // The className to query on
        self.parseClassName = kPAPActivityClassKey;

        // Whether the built-in pull-to-refresh is enabled
        if (NSClassFromString(@"UIRefreshControl")) {
            self.pullToRefreshEnabled = NO;
        } else {
            self.pullToRefreshEnabled = YES;
        }

        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of comments to show per page
        self.objectsPerPage = 30;
        
        self.photo = aPhoto;
        
        self.likersQueryInProgress = NO;
    }
    return self;
}


/**
 *  go to previous controller
 *
 *  @param sender back btn pressed
 */
- (void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UIViewController

- (void)viewDidLoad {
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    [super viewDidLoad];
    
    self.navigationItem.title = @"COMMENTS";
    self.navigationItem.hidesBackButton = YES;

    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    //    backButton.titleLabel.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
    //    backButton.titleEdgeInsets = UIEdgeInsetsMake( 0.0f, 5.0f, 0.0f, 0.0f);
    //    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    //    [backButton setTitleColor:[UIColor colorWithRed:214.0f/255.0f green:210.0f/255.0f blue:197.0f/255.0f alpha:1.0] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    //[backButton setBackgroundImage:[UIImage imageNamed:@"back_btn_on.png"] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    // Set table view properties
   
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    
    

    if (NSClassFromString(@"UIActivityViewController")) {
        // Use UIActivityViewController if it is available (iOS 6 +)
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(activityButtonAction:)];
    } else if ([self currentUserOwnsPhoto]) {
        // Else we only want to show an action button if the user owns the photo and has permission to delete it.
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionButtonAction:)];
    }
    
    if (NSClassFromString(@"UIRefreshControl")) {
        // Use the new iOS 6 refresh control.
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
       
        self.refreshControl = refreshControl;
        //self.refreshControl.tintColor = [UIColor colorWithRed:73.0f/255.0f green:55.0f/255.0f blue:35.0f/255.0f alpha:1.0f];
        self.refreshControl.tintColor = [UIColor blackColor];
        [self.refreshControl addTarget:self action:@selector(refreshControlValueChanged:) forControlEvents:UIControlEventValueChanged];
        self.pullToRefreshEnabled = NO;
    }
    
    // Register to be notified when the keyboard will be shown to scroll the view
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLikedOrUnlikedPhoto:) name:PAPUtilityUserLikedUnlikedPhotoCallbackFinishedNotification object:self.photo];
    
    UIImageView *bgImgVw = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_screen_bg"]];
    bgImgVw.frame = self.tableView.frame;
    self.tableView.backgroundView = bgImgVw;

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];


    
}
-(void)viewWillDisappear:(BOOL)animated{
    
    if (_isCommentedByUser) {
        
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        NSArray *viewControllers = appDelegate.navController.viewControllers;
        if (viewControllers.count > 1 && [viewControllers objectAtIndex:viewControllers.count-2] == self) {
            // View is disappearing because a new view controller was pushed onto the stack
            NSLog(@"New view controller was pushed");
        } else if ([viewControllers indexOfObject:self] == NSNotFound) {
            // View is disappearing because it was popped from the stack
            NSLog(@"View controller was popped");
            [[NSNotificationCenter defaultCenter] postNotificationName:PAPPhotoDetailsViewControllerUserCommentedOnPhotoNotification object:self.photo userInfo:@{@"comments": @(self.objects.count + 1)}];
        }
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.objects.count) { // A comment row
        PFObject *object = [self.objects objectAtIndex:indexPath.row];
        
        if (object) {
            NSString *commentString = [self.objects[indexPath.row] objectForKey:kPAPActivityContentKey];
            
            PFUser *commentAuthor = (PFUser *)[object objectForKey:kPAPActivityFromUserKey];
            
            NSString *nameString = @"";
            if (commentAuthor) {
                nameString = [commentAuthor objectForKey:kPAPUserDisplayNameKey];
            }
           
            return [PAPBaseTextCell heightForCellWithName:nameString contentString:commentString cellInsetWidth:kPAPCellInsetWidth];
        }
    }
    
    // The pagination row
    return 44.0f;
}


#pragma mark - PFQueryTableViewController

- (PFQuery *)queryForTable {
    PFQuery *query;
     if ([[photo parseClassName] isEqualToString:kPAPPrivateFeedClassKey])
     {
         query = [PFQuery queryWithClassName:kPAPPrivateGroupActivityClassKey];
         
     }
     else{
    query = [PFQuery queryWithClassName:self.parseClassName];
     }
    [query whereKey:kPAPActivityPhotoKey equalTo:self.photo];
    [query includeKey:kPAPActivityFromUserKey];
    [query includeKey:kPAPActivityUpvoteCommentsUsersArrayKey];
    [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeComment];
    //[query orderByAscending:@"createdAt"];
    [query orderByDescending:kPAPActivityUpvoteCommentsCount];

    [query setCachePolicy:kPFCachePolicyNetworkOnly];

    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    //
    // If there is no network connection, we will hit the cache first.
    if (self.objects.count == 0 || ![[UIApplication sharedApplication].delegate performSelector:@selector(isParseReachable)]) {
        [query setCachePolicy:kPFCachePolicyCacheThenNetwork];
    }
    
    return query;
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];

    if (NSClassFromString(@"UIRefreshControl")) {
        [self.refreshControl endRefreshing];
    }

    if (_needToRefreshComments) {
        _needToRefreshComments = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self refreshComments];
        });
        
    }
    //[self.headerView reloadLikeBar];
    //[self loadLikers];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *cellID = @"CommentCell";


    // Try to dequeue a cell and create one if necessary
    PAPBaseTextCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[PAPBaseTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
        cell.delegate = self;
    }
    NSLog(@"comment %@",[object objectForKey:kPAPActivityContentKey]);
    
    
    [cell setUser:[object objectForKey:kPAPActivityFromUserKey]];
    [cell setContentText:[object objectForKey:kPAPActivityContentKey]];
    
    
    NSLog(@"UPVOTECOUNT:%@",[object objectForKey:kPAPActivityUpvoteCommentsCount]);
    NSNumber *upvoteNumber = [object objectForKey:kPAPActivityUpvoteCommentsCount];
    
    NSString *upvoteNumberInString = [upvoteNumber stringValue];
    cell.upvoteBtnLbl.text = upvoteNumberInString;
    [cell setDate:[object createdAt]];
    
    [cell setPhoto:object];
    
    NSArray *array =  [object objectForKey:kPAPActivityUpvoteCommentsUsersArrayKey];
    
    if ([array containsObject:[PFUser currentUser].objectId]) {
        [cell.upvoteBtn setSelected:YES];
    }
    else {
        [cell.upvoteBtn setSelected:NO];
    }
    
    
   

    return cell;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"NextPage";
    
    PAPLoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[PAPLoadMoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.cellInsetWidth = kPAPCellInsetWidth;
        cell.hideSeparatorTop = YES;
        cell.mainView.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.objects.count > indexPath.row) {
        
        if ([[(PFUser*)[photo objectForKey:kPAPPhotoUserKey] objectId] isEqualToString:[[PFUser currentUser] objectId]]){
            return YES;
        }
        else {
            
            PFObject *object = self.objects[indexPath.row];
            if ([[(PFUser*)[object objectForKey:kPAPActivityFromUserKey] objectId] isEqualToString:[PFUser currentUser].objectId]) {
                return YES;
            }
            return NO;
        }
        
        
    }
    
    return NO;
    
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"More";
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _selectedObeject = self.objects[indexPath.row];
    _deletedCommentIndex = indexPath.row;
    
    NSString *currentUserObjectId = [PFUser currentUser].objectId;
    //NSString *commentUserobjectId;
    if ([[(PFUser*)[photo objectForKey:kPAPPhotoUserKey] objectId] isEqualToString:currentUserObjectId]) { //photo is uploaded by current user
        
        //commented by current user
        PFObject *object = self.objects[indexPath.row];
        if ([[(PFUser*)[object objectForKey:kPAPActivityFromUserKey] objectId] isEqualToString:[PFUser currentUser].objectId]) {
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Delete",@"Edit", nil];
            //actionSheet.tag = indexPath.row;
            AppDelegate *appledegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [actionSheet showFromTabBar:appledegate.tabBarController.tabBar];
            
        }
        else {
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Delete", nil];
            //actionSheet.tag = indexPath.row;
            AppDelegate *appledegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [actionSheet showFromTabBar:appledegate.tabBarController.tabBar];
        }
        
        
        
    }
    else {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Options" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Delete",@"Edit", nil];
        //actionSheet.tag = indexPath.row;
        AppDelegate *appledegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [actionSheet showFromTabBar:appledegate.tabBarController.tabBar];
        
        
    }
    
    
    
    
}

#pragma mark - ActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.cancelButtonIndex == buttonIndex) {
        //
    }
    else if (buttonIndex == 0){
        
        
        [_selectedObeject deleteInBackgroundWithBlock:^(BOOL succeeded , NSError *error){
            if (succeeded) {
                
                _needToRefreshComments = YES;
                [self loadObjects];
                
                
                //[self refreshComments];
                
                
            }
            else{
                [Helper showAlertWithTitle:@"Error" Message:[error localizedDescription]];
            }
            
        }];
    }
    else if (buttonIndex == 1){
        //        [_selectedObeject deleteInBackgroundWithBlock:^(BOOL succeeded , NSError *error){
        //            if (succeeded) {
        //                [self loadObjects];
        //            }
        //
        //        }];
        _isCommentEditing = YES;
        [delegate cell:nil editCellText:[_selectedObeject objectForKey:kPAPActivityContentKey]];
        
    }
}


/**
 *  This method refresh and update the comment count
 */
-(void)refreshComments{
    
    
    //comments array contains your comments
    if (_deletedCommentIndex > (self.objects.count - 4)) {
        
        
        
        //NSArray *array = [self.photo objectForKey:kPAPFeedCommentsArrayKey];
        //NSArray *userArray = [self.photo objectForKey:kPAPFeedCommentersArrayKey];
        
        int capacity =    self.objects.count >= 3 ? 3 : self.objects.count;
        NSMutableArray *mArray = [[NSMutableArray alloc] initWithCapacity:capacity];
        NSMutableArray *mUserArray = [[NSMutableArray alloc] initWithCapacity:capacity];
        
         __block int count = 1;
        [self.objects enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            
            if (count >= 3) {
                *stop = YES ;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self saveComments:mArray users:mUserArray];
                });
            }
            
            PFObject *object = obj;
            PFUser *user = [object objectForKey:kPAPActivityFromUserKey];
            [mArray insertObject:[object objectForKey:kPAPActivityContentKey] atIndex:0];
            [mUserArray insertObject:[user objectForKey:@"username"] atIndex:0];
            
            
            count++;
            
        }];
        
       
        
        //[mArray addObject:commentstring];
       
        
    }
    else{
        [self.photo incrementKey:kPAPPhotoCommentsCount byAmount:@(-1)];
        [self.photo saveInBackground];
    }
}


/**
 *  This method will update comment count of post on parse
 *
 *  @param comments list of comment
 *  @param users
 */

-(void)saveComments:(NSArray*)comments users:(NSArray*)users{
    
 //   int previousCount = [[self.photo objectForKey:kPAPFeedCommentsCount] integerValue];
//    [self.photo setObject:[NSNumber numberWithInt:(previousCount-1)] forKey:kPAPPhotoCommentsCount];
    [self.photo incrementKey:kPAPPhotoCommentsCount byAmount:@(-1)];
    [self.photo setObject:comments forKey:kPAPPhotoCommentArrayKey];
    [self.photo setObject:users forKey:kPAPFeedCommentersArrayKey];
    [self.photo saveInBackground];
}

#pragma mark - UITextFieldDelegate

/**
 *  This method invoke when comment is posting on media 
 *  Once it successfully posted it will send notification to user
 *
 *
 *  @param commentstring latest comment
 */
-(void)commentOnPhoto:(NSString*)commentstring {
    
    NSString *trimmedComment = [commentstring stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (trimmedComment.length != 0 && [self.photo objectForKey:kPAPFeedUserKey]) {
        PFObject *comment;
        
        if ([[photo parseClassName] isEqualToString:kPAPPrivateFeedClassKey])
        {
            comment = [PFObject objectWithClassName:kPAPPrivateGroupActivityClassKey];
            NSLog(@"Testing Group Comment");
        }
        else
        {
            comment = [PFObject objectWithClassName:kPAPActivityClassKey];
        }
        [comment setObject:trimmedComment forKey:kPAPActivityContentKey]; // Set comment text
        [comment setObject:[self.photo objectForKey:kPAPFeedUserKey] forKey:kPAPActivityToUserKey]; // Set toUser
        [comment setObject:[PFUser currentUser] forKey:kPAPActivityFromUserKey]; // Set fromUser
        [comment setObject:kPAPActivityTypeComment forKey:kPAPActivityTypeKey];
        [comment setObject:[NSNumber numberWithInt:0] forKey:kPAPActivityUpvoteCommentsCount];
        [comment setObject:self.photo forKey:kPAPActivityPhotoKey];
        
        PFACL *ACL = [PFACL ACLWithUser:[PFUser currentUser]];
        [ACL setPublicReadAccess:YES];
                [ACL setPublicWriteAccess:YES];
        [ACL setWriteAccess:YES forUser:[self.photo objectForKey:kPAPPhotoUserKey]];
        comment.ACL = ACL;
        
        [[PAPCache sharedCache] incrementCommentCountForPhoto:self.photo];
        
        // Show HUD view
        [MBProgressHUD showHUDAddedTo:self.view.superview animated:YES];
        
        // If more than 5 seconds pass since we post a comment, stop waiting for the server to respond
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(handleCommentTimeout:) userInfo:@{@"comment": comment} repeats:NO];
        
        [comment saveEventually:^(BOOL succeeded, NSError *error) {
            [timer invalidate];
            

            
            _isCommentedByUser = YES;
            
            NSString *username = [[PFUser currentUser] username]; //[(PFUser*)[self.photo objectForKey:kPAPFeedUserKey] objectForKey:@"username"];
            PFQuery *query;
            if ([[photo parseClassName] isEqualToString:kPAPPrivateFeedClassKey])
            {
                query = [PFQuery queryWithClassName:kPAPPrivateFeedClassKey];
            }
            else{
            query = [PFQuery queryWithClassName:kPAPFeedClassKey];
            }
            
            // Retrieve the object by id
            [query getObjectInBackgroundWithId:self.photo.objectId block:^(PFObject *photoCount, NSError *error) {
                
                int previousCount = [[photoCount objectForKey:kPAPFeedCommentsCount] integerValue];
                NSArray *array = [photoCount objectForKey:kPAPFeedCommentsArrayKey];
                NSArray *userArray = [photoCount objectForKey:kPAPFeedCommentersArrayKey];
                
                NSMutableArray *mArray = [[NSMutableArray alloc] initWithCapacity:3];
                NSMutableArray *mUserArray = [[NSMutableArray alloc] initWithCapacity:3];
                switch (array.count) {
                    case 0:
                    {
                        [mArray addObject:trimmedComment];
                        
                        [mUserArray addObject:username];
                        break;
                    }
                    case 1:
                    {
                        [mArray addObject:array[0]];
                        [mArray addObject:trimmedComment];
                        
                        [mUserArray addObject:userArray[0]];
                        [mUserArray addObject:username];
                        break;
                    }
                    case 2:
                    {
                        [mArray addObjectsFromArray:array];
                        [mArray addObject:commentstring];
                        
                        [mUserArray addObjectsFromArray:userArray];
                        [mUserArray addObject:username];
                        
                        break;
                    }
                    case 3:
                    {
                        [mArray addObject:array[1]];
                        [mArray addObject:array[2]];
                        [mArray addObject:trimmedComment];
                        
                        
                        [mUserArray addObject:userArray[1]];
                        [mUserArray addObject:userArray[2]];
                        [mUserArray addObject:username];
                        break;
                    }
                    default:
                        break;
                }
                
                //[mArray addObject:commentstring];
                [photoCount setObject:[NSNumber numberWithInt:(previousCount+1)] forKey:kPAPPhotoCommentsCount];
                [photoCount setObject:mArray forKey:kPAPPhotoCommentArrayKey];
                [photoCount setObject:mUserArray forKey:kPAPFeedCommentersArrayKey];
                [photoCount saveInBackground];
                
            }];
            
            if (error && error.code == kPFErrorObjectNotFound) {
                
                [[PAPCache sharedCache] decrementCommentCountForPhoto:self.photo];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Could not post comment", nil) message:NSLocalizedString(@"This photo is no longer available", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            

            
            [[NSNotificationCenter defaultCenter] postNotificationName:PAPPhotoDetailsViewControllerUserCommentedOnPhotoNotification object:self.photo userInfo:@{@"comments": @(self.objects.count + 1)}];
            
            [MBProgressHUD hideHUDForView:self.view.superview animated:YES];
            [self loadObjects];

            
            if (![[(PFUser*)[self.photo objectForKey:kPAPFeedUserKey] objectId] isEqualToString:[PFUser currentUser].objectId]) {
                
                
                // Explicitly add the photographer in case he didn't comment
                
                if ([[self.photo objectForKey:kPAPFeedUserKey] objectForKey:kPAPUserPrivateChannelKey]) {
                    NSMutableSet *channelSet = [NSMutableSet setWithCapacity:1];
                    
                    
                    [channelSet addObject:[[self.photo objectForKey:kPAPFeedUserKey] objectForKey:kPAPUserPrivateChannelKey]];
                    
                    
                    // Create push payload
                    if (channelSet.count > 0) {
                        
                        // Create notification message
                        //NSString *userFirstName = [PAPUtility firstNameForDisplayName:[[PFUser currentUser] objectForKey:kPAPUserDisplayNameKey]];
                        NSString *userFirstName = [PAPUtility firstNameForDisplayName:[[PFUser currentUser] username]];
                        NSString *message = [NSString stringWithFormat:@"%@ said: %@", userFirstName, trimmedComment];
                        
                        // Truncate message if necessary and ensure we have enough space
                        // for the rest of the payload
                        if (message.length > 100) {
                            message = [message substringToIndex:99];
                            message = [message stringByAppendingString:@"..."];
                        }
                        
                        NSDictionary *payload =
                        [NSDictionary dictionaryWithObjectsAndKeys:
                         message, kAPNSAlertKey,
                         @"Increment",kAPNSBadgeKey,
                         kPAPPushPayloadPayloadTypeActivityKey, kPAPPushPayloadPayloadTypeKey,
                         kPAPPushPayloadActivityCommentKey, kPAPPushPayloadActivityTypeKey,
                         [[PFUser currentUser] objectId], kPAPPushPayloadFromUserObjectIdKey,
                         [self.photo objectId], kPAPPushPayloadPhotoObjectIdKey,
                         @"sms-received.wav",kAPNSSoundKey,
                         nil];
                        
                        // Send the push
                        PFPush *push = [[PFPush alloc] init];
                        [push setChannels:[channelSet allObjects]];
                        [push setData:payload];
                        [push sendPushInBackground];
                    }

                }
                
                
            }
            [self updateHashTags:trimmedComment];

        }];
    }

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSString *trimmedComment = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (trimmedComment.length != 0 && [self.photo objectForKey:kPAPPhotoUserKey]) {
        
        PFObject *comment = [PFObject objectWithClassName:kPAPActivityClassKey];
        [comment setObject:trimmedComment forKey:kPAPActivityContentKey]; // Set comment text
        [comment setObject:[self.photo objectForKey:kPAPPhotoUserKey] forKey:kPAPActivityToUserKey]; // Set toUser
        [comment setObject:[PFUser currentUser] forKey:kPAPActivityFromUserKey]; // Set fromUser
        [comment setObject:kPAPActivityTypeComment forKey:kPAPActivityTypeKey];
        [comment setObject:self.photo forKey:kPAPActivityPhotoKey];
        
        PFACL *ACL = [PFACL ACLWithUser:[PFUser currentUser]];
        [ACL setPublicReadAccess:YES];
        [ACL setPublicWriteAccess:YES];
        [ACL setWriteAccess:YES forUser:[self.photo objectForKey:kPAPPhotoUserKey]]; 
        comment.ACL = ACL;

        [[PAPCache sharedCache] incrementCommentCountForPhoto:self.photo];
        
        // Show HUD view
        [MBProgressHUD showHUDAddedTo:self.view.superview animated:YES];
        
        // If more than 5 seconds pass since we post a comment, stop waiting for the server to respond
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(handleCommentTimeout:) userInfo:@{@"comment": comment} repeats:NO];

        [comment saveEventually:^(BOOL succeeded, NSError *error) {
            [timer invalidate];
            
            PFQuery *query = [PFQuery queryWithClassName:kPAPPhotoClassKey];
            
            // Retrieve the object by id
            [query getObjectInBackgroundWithId:self.photo.objectId block:^(PFObject *photoCount, NSError *error) {
                
                // Now let's update it with some new data. In this case, only cheatMode and score
                // will get sent to the cloud. playerName hasn't changed.
                int previousCount = [[self.photo objectForKey:kPAPPhotoCommentsCount] integerValue];
                [photoCount setObject:[NSNumber numberWithInt:(previousCount+1)] forKey:kPAPPhotoCommentsCount];
                [photoCount saveInBackground];
                
            }];
            
            if (error && error.code == kPFErrorObjectNotFound) {
                [[PAPCache sharedCache] decrementCommentCountForPhoto:self.photo];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Could not post comment", nil) message:NSLocalizedString(@"This photo is no longer available", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:PAPPhotoDetailsViewControllerUserCommentedOnPhotoNotification object:self.photo userInfo:@{@"comments": @(self.objects.count + 1)}];
            
            [MBProgressHUD hideHUDForView:self.view.superview animated:YES];
            [self loadObjects];
            
            
            if (![[(PFUser*)[self.photo objectForKey:kPAPPhotoUserKey] objectId] isEqualToString:[PFUser currentUser].objectId]) {
                
                
                NSMutableSet *channelSet = [NSMutableSet setWithCapacity:1];
                // Explicitly add the photographer in case he didn't comment
                [channelSet addObject:[[self.photo objectForKey:kPAPPhotoUserKey] objectForKey:kPAPUserPrivateChannelKey]];
                
                
                // Create push payload
                if (channelSet.count > 0) {
                    
                    // Create notification message
                    NSString *userFirstName = [PAPUtility firstNameForDisplayName:[[PFUser currentUser]username]];
                    NSString *message = [NSString stringWithFormat:@"%@ said: %@", userFirstName, trimmedComment];
                    
                    // Truncate message if necessary and ensure we have enough space
                    // for the rest of the payload
                    if (message.length > 100) {
                        message = [message substringToIndex:99];
                        message = [message stringByAppendingString:@"..."];
                    }
                    
                    NSDictionary *payload =
                    [NSDictionary dictionaryWithObjectsAndKeys:
                     message, kAPNSAlertKey,
                     @"Increment",kAPNSBadgeKey,
                     kPAPPushPayloadActivityFollowKey, kPAPPushPayloadActivityTypeKey,
                     @"sms-received.wav",kAPNSSoundKey,
                     nil];
                    
                    // Send the push
                    PFPush *push = [[PFPush alloc] init];
                    [push setChannels:[channelSet allObjects]];
                    [push setData:payload];
                    [push sendPushInBackground];
                }
            }
        }];
    }
    
    [textField setText:@""];
    return [textField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self keyboardShow];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self keyboardHide];
}

-(void)keyboardShow{
    CGRect rectFild = self.commentView.frame;
    rectFild.origin.y -= 215;
    [UIView animateWithDuration:0.25f
                     animations:^{
                         [self.commentView setFrame:rectFild];
                        
                     }
     ];
}

-(void)keyboardHide{
//    CGRect rectFild = self.commentView.frame;
//    rectFild.origin.y += 215;
//    
//    [UIView animateWithDuration:0.25f
//                     animations:^{
//                         [self.commentView setFrame:rectFild];
//                                              }
//     ];
}
-(void)updateHashTags:(NSString*)comment{
    
    NSMutableArray *hashtags = [NSMutableArray arrayWithArray:[self getHashtag:comment]];
    NSMutableArray *marray = [[NSMutableArray alloc] init];
    
    PFQuery *query = [PFQuery queryWithClassName:@"HashTag"];
    [query whereKey:@"hashtag" containedIn:hashtags];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError*error){
        //NSLog(@" arry %@",array);
        
        //update count for previous added hashtags
        for(PFObject *obj in array){
            
            NSString *tag = [obj objectForKey:@"hashtag"];
            if ([hashtags containsObject:tag]) {
                [obj incrementKey:@"hashCount"];
                [hashtags removeObject:tag];
            }
            
            
            [marray addObject:obj];
        }
        
        //add newly created hashtags
        for(NSString *tags in hashtags){
            PFObject *object = [PFObject objectWithClassName:@"HashTag"];
            [object setObject:tags forKey:@"hashtag"];
            [object incrementKey:@"hashCount"];
            [marray addObject:object];
        }
        
        [PFObject saveAllInBackground:marray block:^(BOOL success, NSError*error){
            NSLog(@"error %@",error);
            
        }];
    }];
    
}
-(NSArray*)getHashtag:(NSString *)_str
{
    NSString * aString = _str;
    NSMutableArray *substrings = [NSMutableArray new];
    NSScanner *scanner = [NSScanner scannerWithString:aString];
    [scanner scanUpToString:@"#" intoString:nil]; // Scan all characters before #
    while(![scanner isAtEnd]) {
        NSString *substring = nil;
        [scanner scanString:@"#" intoString:nil]; // Scan the # character
        if([scanner scanUpToString:@" " intoString:&substring]) {
            // If the space immediately followed the #, this will be skipped
            [substrings addObject:[NSString stringWithFormat:@"#%@",substring]];
        }
        [scanner scanUpToString:@"#" intoString:nil]; // Scan all characters before next #
    }
    // do something with substrings
    return substrings;
    
    
}

#pragma mark - UIActionSheetDelegate

//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
//    if (actionSheet.tag == MainActionSheetTag) {
//        if ([actionSheet destructiveButtonIndex] == buttonIndex) {
//            // prompt to delete
//            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Are you sure you want to delete this photo?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:NSLocalizedString(@"Yes, delete photo", nil) otherButtonTitles:nil];
//            actionSheet.tag = ConfirmDeleteActionSheetTag;
//            [actionSheet showFromTabBar:self.tabBarController.tabBar];
//        } else {
//            [self activityButtonAction:actionSheet];
//        }
//    } else if (actionSheet.tag == ConfirmDeleteActionSheetTag) {
//        if ([actionSheet destructiveButtonIndex] == buttonIndex) {
//            
//            [self shouldDeletePhoto];
//        }
//    }
//}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [commentTextField resignFirstResponder];
}


#pragma mark - PAPBaseTextCellDelegate



- (void)cell:(PAPBaseTextCell *)cellView didTapUserButton:(PFUser *)aUser {
    //[self shouldPresentAccountViewForUser:aUser];
    if (delegate && [delegate respondsToSelector:@selector(cell:navigateToUser:)]) {
        [delegate cell:cellView navigateToUser:aUser];
    }
}



//-(void)cell:(PAPBaseTextCell *)cellView didTapPvoteButton:(PFUser *)aUser

-(void)cell:(PAPBaseTextCell *)cellView didTapUPvoteButton:(PFUser *)aUser :(PFObject *)aPhoto
{
    NSString *userid = [[PFUser currentUser] objectId];
    NSMutableArray * userArry;
    if ([aPhoto objectForKey:kPAPActivityUpvoteCommentsUsersArrayKey]) {
        userArry= [[NSMutableArray alloc] initWithArray:[aPhoto objectForKey:kPAPActivityUpvoteCommentsUsersArrayKey]];
    }
    else{
       userArry= [[NSMutableArray alloc] init];
    }
    
   // NSMutableArray * userArry= [[NSMutableArray alloc] initWithArray:[aPhoto objectForKey:kPAPActivityUpvoteCommentsUsersArrayKey]];
    //NSString *user = [PFUser currentUser];
    if ([userArry containsObject:userid]) {
        return;
    }
   
    int previousCount = [[aPhoto objectForKey:kPAPActivityUpvoteCommentsCount] integerValue];
    cellView.upvoteBtnLbl.text = [NSString stringWithFormat:@"%d",(previousCount+1)];
    [cellView.upvoteBtn setSelected:YES];
    
    
    [userArry addObject:userid];
    [aPhoto setObject:userArry forKey:kPAPActivityUpvoteCommentsUsersArrayKey];
    [aPhoto incrementKey:kPAPActivityUpvoteCommentsCount];
    
    [aPhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (error) {
           
            cellView.upvoteBtnLbl.text = [NSString stringWithFormat:@"%d",(previousCount)];
            [cellView.upvoteBtn setSelected:NO];
            
           
        }
        if (succeeded) {
            

               NSString *userFirstName = [PAPUtility firstNameForDisplayName:[[PFUser currentUser]username]];
                NSString *message = [NSString stringWithFormat:@"%@ Upvoted on your comment:%@", userFirstName,[aPhoto objectForKey:@"content"]];
                if (message.length > 100) {
                    message = [message substringToIndex:99];
                    message = [message stringByAppendingString:@"..."];
                }
                
                NSDictionary *payload =[NSDictionary dictionaryWithObjectsAndKeys:
             message, kAPNSAlertKey,
             @"Increment",kAPNSBadgeKey,
             kPAPPushPayloadActivityUpVoteKey, kPAPPushPayloadActivityTypeKey,
             @"sms-received.wav",kAPNSSoundKey,
             nil];
                
                // Send the push
                PFPush *push = [[PFPush alloc] init];
                if ([aUser objectForKey:kPAPUserPrivateChannelKey])
                    {
                        [push setChannel:[aUser objectForKey:kPAPUserPrivateChannelKey]];
                        [push setData:payload];
                        [push sendPushInBackground];
            
                    }

            NSMutableArray *mArrayNew = [[NSMutableArray alloc] init];
            NSMutableArray *mUserArrayNew = [[NSMutableArray alloc]init];
            PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
            [query whereKey:kPAPActivityPhotoKey equalTo:self.photo];
            [query orderByDescending:kPAPActivityUpvoteCommentsCount];
            [query includeKey:kPAPActivityPhotoKey];
            [query includeKey:kPAPActivityFromUserKey];
            [query includeKey:kPAPActivityToUserKey];
            
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                
                if (!error) {
                    NSLog(@"messages :%@",objects);
                    int i = 0;
                    
                    for (PFObject *activity in objects){
                        if (i > 2) {
                            break;
                        }
                        else{
                            PFUser *user = [activity objectForKey:kPAPActivityFromUserKey];
                            // NSLog(@"user name %@",user.username);
                            feedObject = [activity objectForKey:kPAPActivityPhotoKey];
                            [mArrayNew insertObject:[activity objectForKey:kPAPActivityContentKey] atIndex:i];
                            NSLog(@"upvoted Max cmmts:%@",mArrayNew);
                            [mUserArrayNew insertObject:user.username atIndex:i];
                            NSLog(@"upvoted Max cmmts:%@",mUserArrayNew);
                            i++;
                            
                        }
                        
                    }
                    NSLog(@"upvoted Max cmmts:%@",mArrayNew);
                    [feedObject setObject:mArrayNew forKey:kPAPPhotoCommentArrayKey];
                    [feedObject setObject:mUserArrayNew forKey:kPAPFeedCommentersArrayKey];
                    [feedObject saveInBackground];
                }
                
            }];

            
            
        
        
            }
     
        
     
     
     
     
    }];
    
    // updating comments and commenters according to highest upvotes on feed
    
//    [aPhoto setObject:mArrayNew forKey:kPAPPhotoCommentArrayKey];
//    [aPhoto setObject:mUserArrayNew forKey:kPAPFeedCommentersArrayKey];
    
    
    
    /*
    PFQuery *query = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [query whereKey:kPAPActivityFromUserKey equalTo:[aPhoto objectForKey:kPAPActivityFromUserKey]];
    [query whereKey:kPAPActivityToUserKey equalTo:[aPhoto objectForKey:kPAPActivityToUserKey]];
    [query whereKey:kPAPActivityContentKey equalTo:[aPhoto objectForKey:@"content"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSString *chk = @"0";
            NSLog(@"Found Data:%@",objects);
            NSMutableArray *upvoteUsers =  [[NSMutableArray alloc]init];
            if (![aPhoto objectForKey:kPAPActivityUpvoteCommentsUsersArrayKey]) {
                [upvoteUsers addObject:user];
                int previousCount = [[aPhoto objectForKey:kPAPActivityUpvoteCommentsCount] integerValue];
                [aPhoto incrementKey:kPAPActivityUpvoteCommentsCount];
                
                [aPhoto setObject:upvoteUsers forKey:kPAPActivityUpvoteCommentsUsersArrayKey];
                [aPhoto saveInBackground];
                cellView.upvoteBtnLbl.text = [NSString stringWithFormat:@"%d",(previousCount+1)];
                [cellView.upvoteBtn setEnabled:NO];

                
            }
            else
            {
            upvoteUsers = [aPhoto objectForKey:kPAPActivityUpvoteCommentsUsersArrayKey];
                for (int i =0 ;i< upvoteUsers.count;i++) {
                    if ([upvoteUsers[i] isEqualToString: user]) {
                        chk = @"1";
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert!", nil) message:NSLocalizedString(@"Already voted.", nil)  delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
                        [alert show];
                        
                        break;
                    }
                }
                if ([chk isEqualToString:@"0"]) {
                    [cellView.upvoteBtn setEnabled:NO];

                    [upvoteUsers addObject:user];
                    int previousCount = [[aPhoto objectForKey:kPAPActivityUpvoteCommentsCount] integerValue];
                    [aPhoto incrementKey:kPAPActivityUpvoteCommentsCount];
                    
                    [aPhoto setObject:upvoteUsers forKey:kPAPActivityUpvoteCommentsUsersArrayKey];
                    [aPhoto saveInBackground];
                    cellView.upvoteBtnLbl.text = [NSString stringWithFormat:@"%d",(previousCount+1)];
                    NSLog(@"Previous upvote count:%d",previousCount);
                    
 
                    
                }
                
            }
            
            
            
//            [upvoteUsers addObject:user];
//            int previousCount = [[aPhoto objectForKey:kPAPActivityUpvoteCommentsCount] integerValue];
//            [aPhoto incrementKey:kPAPActivityUpvoteCommentsCount];
//           
//            [aPhoto setObject:upvoteUsers forKey:kPAPActivityUpvoteCommentsUsersArrayKey];
//            
//            //[aPhoto setObject:[aPhoto objectForKey:kPAPActivityClassKey] forKey:kPAPActivityUpvoteCommentsUsersArrayKey];
//            [aPhoto saveInBackground];
            
            
            
            
            
        }
    }];
    
  */
    
}



/**
 *  This delegate method call when user tap on taggeduser name string
 *  find all tagged user posts
 *  @param cellView 
 *  @param user     taggedUser
 */
- (void)cell:(PAPBaseTextCell *)cellView didTapTaggedUser:(NSString *)user{
    
   // NSLog(@"user %@",user);
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" containsString:user];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        if (!error) {
           // NSLog(@"array %@",array);
            if (array.count > 0) {
                PFUser *user = [array lastObject];
                // [self shouldPresentAccountViewForUser:user];
                if (delegate && [delegate respondsToSelector:@selector(cell:navigateToUser:)]) {
                    [delegate cell:cellView navigateToUser:user];
                }
            }
           
        }
    }];
}


/**
 *  This delegate method invoke navigateTohashTag method
 *
 *  @param cellView <#cellView description#>
 *  @param hashTag  hashTaggedUser
 */
-(void)cell:(PAPBaseTextCell *)cellView didTapHashTag:(NSString *)hashTag{
    if (delegate && [delegate respondsToSelector:@selector(cell:navigateToHashTag:)]) {
        [delegate cell:cellView navigateToHashTag:hashTag];
    }
}


#pragma mark - PAPPhotoDetailsHeaderViewDelegate

-(void)photoDetailsHeaderView:(PAPPhotoDetailsHeaderView *)headerView didTapUserButton:(UIButton *)button user:(PFUser *)user {
    [self shouldPresentAccountViewForUser:user];
}


/**
 *  This method will open action sheet for share and delete photo
 *
 *  @param sender btn pressed
 */
- (void)actionButtonAction:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
    actionSheet.delegate = self;
    actionSheet.tag = MainActionSheetTag;
    actionSheet.destructiveButtonIndex = [actionSheet addButtonWithTitle:NSLocalizedString(@"Delete Photo", nil)];
    if (NSClassFromString(@"UIActivityViewController")) {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Share Photo", nil)];
    }
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)activityButtonAction:(id)sender {
    if (NSClassFromString(@"UIActivityViewController")) {
        // TODO: Need to do something when the photo hasn't finished downloading!
        if ([[self.photo objectForKey:kPAPPhotoPictureKey] isDataAvailable]) {
            [self showShareSheet];
        } else {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[self.photo objectForKey:kPAPPhotoPictureKey] getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if (!error) {
                    [self showShareSheet];
                }
            }];
        }
    }
}


#pragma mark - ()

- (void)showShareSheet {
    [[self.photo objectForKey:kPAPPhotoPictureKey] getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error) {
            NSMutableArray *activityItems = [NSMutableArray arrayWithCapacity:3];
                        
            // Prefill caption if this is the original poster of the photo, and then only if they added a caption initially.
            if ([[[PFUser currentUser] objectId] isEqualToString:[[self.photo objectForKey:kPAPPhotoUserKey] objectId]] && [self.objects count] > 0) {
                PFObject *firstActivity = self.objects[0];
                if ([[[firstActivity objectForKey:kPAPActivityFromUserKey] objectId] isEqualToString:[[self.photo objectForKey:kPAPPhotoUserKey] objectId]]) {
                    NSString *commentString = [firstActivity objectForKey:kPAPActivityContentKey];
                    [activityItems addObject:commentString];
                }
            }
            
            [activityItems addObject:[UIImage imageWithData:data]];
            [activityItems addObject:[NSURL URLWithString:[NSString stringWithFormat:@"https://anypic.org/#pic/%@", self.photo.objectId]]];
            
            UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
            [self.navigationController presentViewController:activityViewController animated:YES completion:nil];
        }
    }];
}

/**
 *  This method is to handle comment posting timeout
 *
 *  @param aTimer definfed time limit
 */
- (void)handleCommentTimeout:(NSTimer *)aTimer {
    [MBProgressHUD hideHUDForView:self.view.superview animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"New Comment", nil) message:NSLocalizedString(@"Your comment will be posted next time there is an Internet connection.", nil)  delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Dismiss", nil), nil];
    [alert show];
}

- (void)shouldPresentAccountViewForUser:(PFUser *)user {
//    
//    if (delegate && [delegate respondsToSelector:@selector(cell:navigateToUser:)]) {
//        [delegate cell:<#(PAPBaseTextCell *)#> navigateToUser:<#(PFUser *)#>]
//    }
//    ;    PAPAccountViewController *accountViewController = [[PAPAccountViewController alloc] initWithStyle:UITableViewStylePlain];
//    [accountViewController setUser:user];
//    [appdelegate.navController pushViewController:accountViewController animated:YES];
}




- (void)userLikedOrUnlikedPhoto:(NSNotification *)note {
    [self.headerView reloadLikeBar];
}

- (void)keyboardWillShow:(NSNotification*)note {
    // Scroll the view to the comment text box
//    NSDictionary* info = [note userInfo];
//    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    [self.tableView setContentOffset:CGPointMake(0.0f, self.tableView.contentSize.height-kbSize.height) animated:YES];
}

- (void)loadLikers {
    if (self.likersQueryInProgress) {
        return;
    }

    self.likersQueryInProgress = YES;
    PFQuery *query = [PAPUtility queryForActivitiesOnPhoto:photo cachePolicy:kPFCachePolicyNetworkOnly];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        self.likersQueryInProgress = NO;
        if (error) {
            [self.headerView reloadLikeBar];
            return;
        }
        
        NSMutableArray *likers = [NSMutableArray array];
        NSMutableArray *commenters = [NSMutableArray array];
        
        BOOL isLikedByCurrentUser = NO;
        
        for (PFObject *activity in objects) {
            if ([[activity objectForKey:kPAPActivityTypeKey] isEqualToString:kPAPActivityTypeLike] && [activity objectForKey:kPAPActivityFromUserKey]) {
                [likers addObject:[activity objectForKey:kPAPActivityFromUserKey]];
            } else if ([[activity objectForKey:kPAPActivityTypeKey] isEqualToString:kPAPActivityTypeComment] && [activity objectForKey:kPAPActivityFromUserKey]) {
                [commenters addObject:[activity objectForKey:kPAPActivityFromUserKey]];
            }
            
            if ([[[activity objectForKey:kPAPActivityFromUserKey] objectId] isEqualToString:[[PFUser currentUser] objectId]]) {
                if ([[activity objectForKey:kPAPActivityTypeKey] isEqualToString:kPAPActivityTypeLike]) {
                    isLikedByCurrentUser = YES;
                }
            }
        }
        
        [[PAPCache sharedCache] setAttributesForPhoto:photo likers:likers commenters:commenters likedByCurrentUser:isLikedByCurrentUser];
        [self.headerView reloadLikeBar];
    }];
}


/**
 *  This method will load the latest updated data
 *
 *  @param refreshControl
 */
- (void)refreshControlValueChanged:(UIRefreshControl *)refreshControl {
    [self loadObjects];
}


/**
 *  This method is to check post belongs to current user or not
 *
 *  @return boolan value
 */
- (BOOL)currentUserOwnsPhoto {
    return [[[self.photo objectForKey:kPAPPhotoUserKey] objectId] isEqualToString:[[PFUser currentUser] objectId]];
}


/**
 *  This method delete the photo if its belong to current user
 */
- (void)shouldDeletePhoto {
    // Delete all activites related to this photo
    PFQuery *query = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [query whereKey:kPAPActivityPhotoKey equalTo:self.photo];
    [query findObjectsInBackgroundWithBlock:^(NSArray *activities, NSError *error) {
        if (!error) {
            for (PFObject *activity in activities) {
                [activity deleteEventually];
            }
        }
        
        // Delete photo
        [self.photo deleteEventually];
    }];
    [[NSNotificationCenter defaultCenter] postNotificationName:PAPPhotoDetailsViewControllerUserDeletedPhotoNotification object:[self.photo objectId]];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
