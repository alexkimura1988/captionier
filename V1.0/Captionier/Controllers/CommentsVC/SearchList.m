//
//  FollowingList.m
//  
//
//  Created by Surender Rathore on 06/01/14.
//
//

#import "SearchList.h"

@interface SearchList ()

@end

@implementation SearchList

@synthesize searchList;
@synthesize tbleView;
@synthesize callback;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.tbleView = [[UITableView alloc] initWithFrame:self.bounds];
        self.tbleView.delegate = self;
        self.tbleView.dataSource = self;
        [self addSubview:self.tbleView];
    }
    return self;
}

-(void)refresh {
    
    [self.tbleView reloadData];
}

#pragma mark - UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.tbleView.frame = self.bounds;

    return searchList.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        
    }
    PFObject *object = searchList[indexPath.row];
    if ([object isKindOfClass:[PFUser class]]) {
        PFUser *user = (PFUser*)object;
        cell.textLabel.text = user.username; //[user objectForKey:kPAPUserDisplayNameKey];
        cell.detailTextLabel.text = [user objectForKey:kPAPUserDisplayNameKey];

    }
    else if ([[object parseClassName] isEqualToString:@"HashTag"]) {
        cell.textLabel.text = [object objectForKey:@"hashtag"];
        cell.detailTextLabel.text = @"";
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"didSelectRowAtIndexPath");
    UITableViewCell* tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
	[tableViewCell setSelected:NO animated:YES];
    PFObject *object = searchList[indexPath.row];
    PFObject *selectedObject = nil;
    if ([object isKindOfClass:[PFUser class]]) {
        selectedObject = object;
    }
    else if ([[object parseClassName] isEqualToString:@"HashTag"]) {
        selectedObject = object;
    }
    if (self.callback) {
        self.callback(selectedObject);
    }
}
-(NSString*)removeSpaces:(NSString*)str {
    NSMutableString *string = [[NSMutableString alloc] initWithString:str] ;
	[string replaceOccurrencesOfString:@" " withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0,[string length])];
 	return string;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
