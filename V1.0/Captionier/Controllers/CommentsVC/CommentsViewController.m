//
//  CommentsViewController.m
//  MealTrend
//
//  Created by rahul Sharma on 31/10/13.
//
//

#import "CommentsViewController.h"
#import "CommetTableViewController.h"
#import "PAPPhotoDetailsViewController.h"
#import "FollowingList.h"
#import "PAPAccountViewController.h"
#import "PAPHomeViewController.h"


@interface CommentsViewController ()<UITextFieldDelegate,PAPPhotoDetailsViewDelegate>
@property (nonatomic,strong) UIView *commentView;
@property (nonatomic,strong) UITextField *commentField;
@property (nonatomic,strong) UIButton *buttonSend;
@property (nonatomic,strong) PAPPhotoDetailsViewController *commentsTable;
@property (nonatomic,strong) NSMutableArray *followers;
@property (nonatomic,strong)     FollowingList *followinglist;
@property (nonatomic,strong)   NSMutableArray *taggedUsers;
@property (nonatomic,assign) float keyboardHeight;
@property (nonatomic, assign) NSRange searchRange;
@property (nonatomic,strong)   NSMutableArray *searchResults;
@property (nonatomic,strong) PFQuery *lastQuery;
@property (nonatomic,strong)   FollowingList *searchList;
@property (nonatomic,strong) NSString *searchText;
-(IBAction)buttonSendClicked:(id)sender;
@end


@implementation CommentsViewController
@synthesize commentField;
@synthesize buttonSend;
@synthesize commentView;
@synthesize photo;
@synthesize commentsTable;
@synthesize followinglist;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //comment box;
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.commentsTable = [[PAPPhotoDetailsViewController alloc] initWithPhoto:self.photo];
    self.commentsTable.delegate = self;
    //self.commentsTable.view.backgroundColor = [UIColor colorWithRed:0.264 green:0.945 blue:0.234 alpha:1.000];
    [self.view addSubview:commentsTable.view];
    
    NSLog(@"table : %@ , view %@",NSStringFromCGRect(self.commentsTable.tableView.frame),NSStringFromCGRect(self.commentsTable.view.frame));
    
    //self.commentsTable.tableView.backgroundColor = [UIColor yellowColor];
    
    //self.view.backgroundColor = [UIColor grayColor];
    
    self.commentView = [[UIView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 64 -40, 320, 40)];
    
    
    if ([Helper isPhone5]) {
        self.commentsTable.view.frame = CGRectMake(0, 0, 320,[UIScreen mainScreen].bounds.size.height -40-64 -20);
    }
    else{
        self.commentsTable.view.frame = CGRectMake(0, 0, 320,[UIScreen mainScreen].bounds.size.height -40);
    }
    
    
    
   
    NSLog(@"table : %f view :%f",CGRectGetHeight(self.commentsTable.view.frame),[UIScreen mainScreen].bounds.size.height);
   
    self.commentView.backgroundColor = [UIColor colorWithWhite:0.737 alpha:1.000];
    //self.commentView.backgroundColor = [UIColor colorWithRed:76/255.0 green:37/255.0 blue:136/255.0 alpha:1.0];
    
    [self.view addSubview:self.commentView];
    
    UIImageView *commentBox = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"comment_box"] resizableImageWithCapInsets:UIEdgeInsetsMake(5.0f, 10.0f, 5.0f, 10.0f)]];
    commentBox.frame = CGRectMake(5.0f, 2.5f, 240.0f, 35.0f);
    [self.commentView addSubview:commentBox];
    
    commentField = [[UITextField alloc] initWithFrame:CGRectMake( 10.0f, 4.0f, 230.0f, 31.0f)];
    commentField.font = [UIFont fontWithName:robo size:14.0f];//systemFontOfSize:14.0f
    
    commentField.placeholder = @"Add a comment";
    commentField.returnKeyType = UIReturnKeySend;
    commentField.delegate = self;
    commentField.keyboardType = UIKeyboardTypeTwitter;
    commentField.textColor = [UIColor colorWithRed:67.0f/255.0f green:74.0f/255.0f blue:84.0f/255.0f alpha:1.0f];//67,74,84
    commentField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [commentField setValue:[UIColor colorWithRed:67.0f/255.0f green:74.0f/255.0f blue:84.0f/255.0f alpha:1.0f] forKeyPath:@"_placeholderLabel.textColor"]; // Are we allowed to
    [self.commentView addSubview:commentField];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameDidChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
  
    self.buttonSend = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.buttonSend setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.buttonSend setTitleColor:[UIColor colorWithRed:0.380 green:0.832 blue:0.984 alpha:1.000] forState:UIControlStateHighlighted];
    //[self.buttonSend setTitleColor:UIColorFromRGB(cLikeButtonTitleSelector) forState:UIControlStateHighlighted];
    self.buttonSend.frame = CGRectMake(320 - 50, 8, 30, 25);//320 - 70, 5, 65, 30.5
    [self.buttonSend setTitle:@"" forState:UIControlStateNormal];
    self.buttonSend.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [self.buttonSend setBackgroundImage:[UIImage imageNamed:@"send_btn"] forState:UIControlStateNormal];
    //[self.buttonSend setBackgroundImage:[UIImage imageNamed:@"cahrt_btn_on.png"] forState:UIControlStateHighlighted];
    self.buttonSend.titleLabel.font = [UIFont fontWithName:PROXIMANOVA_LIGHT size:12];
    [self.buttonSend addTarget:self action:@selector(buttonSendClicked:) forControlEvents:UIControlEventTouchUpInside];
    //self.buttonSend.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0);
    [self.commentView addSubview:self.buttonSend];
    
    
    
    self.navigationItem.title = @"Comments";
    self.navigationItem.hidesBackButton = YES;
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    //    backButton.titleLabel.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
    //    backButton.titleEdgeInsets = UIEdgeInsetsMake( 0.0f, 5.0f, 0.0f, 0.0f);
    //    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    //    [backButton setTitleColor:[UIColor colorWithRed:214.0f/255.0f green:210.0f/255.0f blue:197.0f/255.0f alpha:1.0] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
  //  [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn_on.png"] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
   

    
}
-(void)viewWillAppear:(BOOL)animated{
    //[self.commentsTable.tableView reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ButtonMethod


/**
 *  This method send new comment
 *  send push notification to particular user
 *
 *  @param sender sendBtn
 */
-(IBAction)buttonSendClicked:(id)sender{
        [self removeFollowingListView];
    
    if (commentField.text.length != 0) {
        
        [self.commentsTable commentOnPhoto:commentField.text];
        [self sendPushToTaggedUsersForObject:self.photo];
        [self.commentField resignFirstResponder];
        [self keyboardHide];
        self.commentField.text = @"";
        
    }
}


/**
 *  go back to previous controller
 *
 *  @param sender backBtn pressed
 */
- (void)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    _searchRange = NSMakeRange(0, 0);
    [self keyboardShow];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    //_searchResults = nil;
    //NSLog(@"string %@ location %d",string,range.length);
    if ([string isEqualToString:@" "] || (range.length == 1 && range.location == 0)) {
        [self removeFollowingListView];
    }
    else if ([string isEqualToString:@"@"]) {
        _searchRange = NSMakeRange(range.location, 1);
        [self getUserWhomIamFollowingForCommentTagging:[textField.text stringByReplacingCharactersInRange:range withString:string]];
    }
    else if (_searchRange.length) {
        NSInteger l = textField.text.length;
        NSInteger nl = [textField.text stringByReplacingCharactersInRange:range withString:string].length;
        _searchRange = NSMakeRange(_searchRange.location, _searchRange.length + nl - l);
        
        [self getUserWhomIamFollowingForCommentTagging:[textField.text stringByReplacingCharactersInRange:range withString:string]];
        
    }
    
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    _searchRange = NSMakeRange(0, 0);
    //[self keyboardHide];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self removeFollowingListView];
    if (textField.text.length != 0) {
         [self.commentsTable commentOnPhoto:commentField.text];
         [self sendPushToTaggedUsersForObject:self.photo];
    }
    [textField resignFirstResponder];
    [self keyboardHide];
    
    return YES;
}

-(void)keyboardShow{
//    CGRect rectFild = self.commentView.frame;
//    rectFild.origin.y -= 215;
//    
//    CGRect tableFrame = self.commentsTable.view.frame;
//    tableFrame.size.height = rectFild.origin.y;
//    
//    [UIView animateWithDuration:0.25f
//                     animations:^{
//                         [self.commentView setFrame:rectFild];
//                         [self.commentsTable.view setFrame:tableFrame];
//                         
//                     }
//     ];
}

-(void)keyboardHide{
//    CGRect rectFild = self.commentView.frame;
//    rectFild.origin.y += 215;
//    
//    CGRect tableFrame = self.commentsTable.view.frame;
//    tableFrame.size.height = [UIScreen mainScreen].bounds.size.height-64-40;//[self getHeightForTableView];
//
//    
//    [UIView animateWithDuration:0.25f
//                     animations:^{
//                         [self.commentField setText:@""];
//                         [self.commentView setFrame:rectFild];
//                         [self.commentsTable.view setFrame:tableFrame];
//                     }
//     ];
}

-(float)getHeightForTableView {
    if (SYSTEM_VERSION_GREATER_THAN(@"7")) {
        
        
        if ([Helper isPhone5]) {
            return 460;
        }
        else {
            return 370;
        }
        
    }
    else {
        if ([Helper isPhone5]) {
            return 430;
        }
        else {
            return 330;
        }
        
    }
}


/**
 *  This method is invoke when user enter "@"
 *  list all the user list whom current user is following
 *  @param _name string
 */
-(void)getUserWhomIamFollowing:(NSString*)_name {
    
    if (!_name.length) {
        _searchResults = nil;
        [_searchList removeFromSuperview];
        return;
    }
    
    if (_searchResults.count > 0) {
        [self displayFollowingList];
    }
    else {
        //ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        //[pi showPIOnView:self.view withMessage:@""];
        PFQuery  *query = [PFQuery queryWithClassName:kPAPActivityClassKey];
        [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
        [query whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
        [query includeKey:kPAPActivityToUserKey];
        [query orderByAscending:kPAPUserDisplayNameKey];
       
        
        NSString *string = [[_name componentsSeparatedByString:@" "] lastObject];
        PFQuery *queryUsername = [PFUser query];
        [queryUsername whereKey:@"username" matchesRegex:string modifiers:@"i"];
        
        PFQuery *querydisplayname = [PFUser query];
        [querydisplayname whereKey:kPAPUserDisplayNameKey matchesRegex:string modifiers:@"i"];
        
        
        NSLog(@"searching for : %@", string);
        
        __block NSMutableArray *results;
        
        
        PFQuery *queryName = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:queryUsername,querydisplayname, nil]];
        
        [query whereKey:kPAPActivityToUserKey matchesQuery:queryName];
        
        if (_lastQuery) {
            [_lastQuery cancel];
        }
        _lastQuery = query;
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects , NSError *error) {
            if (!error) {
                if (!results) {
                    results = [NSMutableArray new];
                }
                
                for (PFObject *obj in objects) {
                    if ([obj.parseClassName isEqualToString:kPAPActivityClassKey]) {
                        [results addObject:[obj objectForKey:kPAPActivityToUserKey]];
                    }
                }
                //results = [objects mutableCopy];
                if ([[PFUser currentUser].username rangeOfString:string options:NSCaseInsensitiveSearch].length == string.length)
                {
                    [results insertObject:[PFUser currentUser] atIndex:0];
                }
                
                _searchResults = results;
                [self displayFollowingList];
            }
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
        }];
        
    }
    
    
}


/**
 *  This method is invoke every time user enter alphabet preceeding by "@"
 *
 *  @param _name string
 */
-(void)getUserWhomIamFollowingForCommentTagging:(NSString*)_name {
    
    if (!_name.length) {
        _searchResults = nil;
        _searchText = nil;
        [_searchList removeFromSuperview];
        return;
    }
    
    NSArray *arr = [self getUsername:_name];
    
    if (!arr.count && ![_name isEqualToString:@"@"]) {
        _searchText = nil;
        return;
    }
    
    if (_searchResults.count > 0) {
        [self displayFollowingList];
    }
    else {
        //ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        //[pi showPIOnView:self.view withMessage:@""];
        
        NSString *string = [arr lastObject];
        
        PFQuery  *query = [PFQuery queryWithClassName:kPAPActivityClassKey];
        [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
        [query whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
        [query includeKey:kPAPActivityToUserKey];
        [query orderByAscending:kPAPUserDisplayNameKey];
        
        PFQuery *queryUsername = [PFUser query];
        PFQuery *querydisplayname = [PFUser query];
        
        if (!string) {
            _searchText = @"@";
        }
        else {
            [queryUsername whereKey:@"username" matchesRegex:[string substringFromIndex:1] modifiers:@"i"];
            [querydisplayname whereKey:kPAPUserDisplayNameKey matchesRegex:[string substringFromIndex:1] modifiers:@"i"];
            _searchText = string;
            
        }
        
        
        
        NSLog(@"searching for : %@", string);
        
        __block NSMutableArray *results;
        
        
        PFQuery *queryName = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:queryUsername,querydisplayname, nil]];
        
        [query whereKey:kPAPActivityToUserKey matchesQuery:queryName];
        
        if (_lastQuery) {
            [_lastQuery cancel];
        }
        _lastQuery = query;
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects , NSError *error) {
            if (!error) {
                if (!results) {
                    results = [NSMutableArray new];
                }
                
                for (PFObject *obj in objects) {
                    if ([obj.parseClassName isEqualToString:kPAPActivityClassKey]) {
                        [results addObject:[obj objectForKey:kPAPActivityToUserKey]];
                    }
                }
//                //results = [objects mutableCopy];
//                if ([[PFUser currentUser].username rangeOfString:[string substringFromIndex:1] options:NSCaseInsensitiveSearch].length == [string substringFromIndex:1].length)
//                {
//                    [results insertObject:[PFUser currentUser] atIndex:0];
//                }
                
                _searchResults = results;
                [self displayFollowingList];
            }
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
        }];
        
    }
    
    
}


-(NSArray*)getUsername:(NSString *)_str
{
    NSString * aString = _str;
    NSMutableArray *substrings = [NSMutableArray new];
    NSScanner *scanner = [NSScanner scannerWithString:aString];
    [scanner scanUpToString:@"@" intoString:nil]; // Scan all characters before #
    while(![scanner isAtEnd]) {
        NSString *substring = nil;
        [scanner scanString:@"@" intoString:nil]; // Scan the # character
        if([scanner scanUpToString:@" " intoString:&substring]) {
            // If the space immediately followed the #, this will be skipped
            [substrings addObject:[NSString stringWithFormat:@"@%@",substring]];
        }
        [scanner scanUpToString:@"@" intoString:nil]; // Scan all characters before next #
    }
    // do something with substrings
    return substrings;
    
    
}

/**
 *  This method will present the view containg list of user whom current user following
 */
-(void)displayFollowingList {
    
    if (_searchResults.count == 0) {
        if (followinglist) {
            [followinglist removeFromSuperview];
        }
        return;
    }

    
    if (!followinglist) {
        
        followinglist = [[FollowingList alloc] initWithFrame:CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height - 40 - _keyboardHeight - 75)];
        
        followinglist.tag = 100;
        __weak typeof(self) weakSelf = self;
        followinglist.callback = ^(PFUser *taggeduser){
            
            [weakSelf addDisplayName:taggeduser];
            
        };
        followinglist.followingList = _searchResults;
        [self.view addSubview:followinglist];
        [followinglist refresh];
    }
    else {
        
        if (![self.view.subviews containsObject:followinglist]) {
          [self.view addSubview:followinglist];
        }
        //
        followinglist.followingList = _searchResults;
        [followinglist refresh];
    }
    
    
}

/**
 *  This method invoke once current user select user from following list
 */
-(void)removeFollowingListView{
    //FollowingList *fl =  (FollowingList*)[self.view viewWithTag:100];
    if (followinglist) {
        [followinglist removeFromSuperview];
    }
    
   // followinglist = Nil;
}


/**
 *  This method add "@" before tagged user name
 *
 *  @param taggedUser username
 */
-(void)addDisplayName:(PFUser*)taggedUser{
    
    
    [_searchList removeFromSuperview];
    
    if (!_taggedUsers) {
        _taggedUsers = [[NSMutableArray alloc] init];
    }
    [_taggedUsers addObject:taggedUser];
    
    NSString *combinedtext = [NSString stringWithFormat:@"%@%@ ", @"@", taggedUser.username];
    commentField.text = [commentField.text stringByReplacingCharactersInRange:_searchRange withString:combinedtext];
    //commentField.text = combinedtext;
    [self removeFollowingListView];
    _searchRange = NSMakeRange(0, 0);
}
#pragma PAPDetailViewController Delegate
-(void)cell:(PAPBaseTextCell *)cell navigateToUser:(PFUser *)user{
    
        PAPAccountViewController *accountViewController = [[PAPAccountViewController alloc] initWithStyle:UITableViewStylePlain];
        [accountViewController setUser:user];
    accountViewController.showBackButton = YES;
        [self.navigationController pushViewController:accountViewController animated:YES];
}


-(void)cell:(PAPBaseTextCell *)cell navigateToHashTag:(NSString *)hashTag{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:hashTag forKey:@"HashTag"];
    [ud synchronize];
    PAPHomeViewController *home = [[PAPHomeViewController alloc] initWithStyle:UITableViewStylePlain];
    home.hashTagString = hashTag;
    [home setHashTag:YES];
    [self.navigationController pushViewController:home animated:YES];
}
-(void)cell:(PAPBaseTextCell *)cell editCellText:(NSString *)text{
    [self.commentField setText:text];
    [self.commentField becomeFirstResponder];
}

#pragma mark - SendPushToTaggedUsers


/**
 *  This method will send push notification to tagged user
 *
 *  @param object post details
 */

-(void)sendPushToTaggedUsersForObject:(PFObject *)object
{
    NSString *comment = commentField.text;
    
    NSMutableArray *taggedUsers = [[NSMutableArray alloc] init];
    
    if (_taggedUsers.count > 0) {
        NSMutableArray *channels = [[NSMutableArray alloc] init];
        
        for(PFUser *usr in _taggedUsers) {
            
            if ([comment rangeOfString:usr.username].location == NSNotFound) {
                //do not send push notification to that user
            }
            else {
                [channels addObject:[usr objectForKey:kPAPUserPrivateChannelKey]];
                
                if (![usr.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    PFObject *commentTagActivity = [PFObject objectWithClassName:kPAPActivityClassKey];
                    [commentTagActivity setObject:[PFUser currentUser] forKey:kPAPActivityFromUserKey];
                    [commentTagActivity setObject:kPAPActivityTypeCommentTag forKey:kPAPActivityTypeKey];
                    [commentTagActivity setObject:usr forKey:kPAPActivityToUserKey];
                    [commentTagActivity setObject:comment forKey:kPAPActivityContentKey];
                    [commentTagActivity setObject:photo forKey:kPAPActivityPhotoKey];
                    
                    
                    PFACL *likeACL = [PFACL ACLWithUser:[PFUser currentUser]];
                    [likeACL setPublicReadAccess:YES];
                    [likeACL setWriteAccess:YES forUser:[photo objectForKey:kPAPPhotoUserKey]];
                    commentTagActivity.ACL = likeACL;
                    
                    [taggedUsers addObject:commentTagActivity];
                }
            }
            
            
        }
        
        [PFObject saveAllInBackground:taggedUsers block:^(BOOL successed , NSError *error){
            if (error) {
                //[self makeEntryForTaggedusersInbackground];
            }
            else {
                NSLog(@"saved all tagged usrs");
            }
        }];
        
        
        NSString *message  = [NSString stringWithFormat:@"%@ mentioned you in comment: %@",[PFUser currentUser].username,comment];
        NSLog(@"Message:%@",message);
        
        NSDictionary *payload =
        [NSDictionary dictionaryWithObjectsAndKeys:
         message, kAPNSAlertKey,
         @"Increment",kAPNSBadgeKey,
         kPAPPushPayloadPayloadTypeActivityKey, kPAPPushPayloadPayloadTypeKey,
         kPAPPushPayloadActivityTaggedInCommentKey, kPAPPushPayloadActivityTypeKey,
         [[PFUser currentUser] objectId], kPAPPushPayloadFromUserObjectIdKey,
         [object objectId], kPAPPushPayloadPhotoObjectIdKey,
         @"sms-received.wav",kAPNSSoundKey,
         nil];
        
        PFPush *push = [[PFPush alloc] init];
        
        // Be sure to use the plural 'setChannels'.
        [push setChannels:channels];
        [push setData:payload];
        [push sendPushInBackground];
    }
    
}

- (void)keyboardFrameDidChange:(NSNotification *)notification
{
    CGRect keyboardEndFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardBeginFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    
    
    NSLog(@"end : %@ , begin : %@",NSStringFromCGRect(keyboardEndFrame),NSStringFromCGRect(keyboardBeginFrame));
    //NSValue* keyboardFrameBegin = [notification valueForKey:UIKeyboardFrameEndUserInfoKey];

    _keyboardHeight = keyboardEndFrame.size.height;
    ;
    CGRect rectFild = self.commentView.frame;
    
    
    
    if (rectFild.origin.y >  keyboardEndFrame.origin.y) {
        
        rectFild.origin.y = rectFild.origin.y - keyboardEndFrame.size.height;
    }
    else{
        rectFild.origin.y = rectFild.origin.y -  (keyboardBeginFrame.origin.y - keyboardEndFrame.origin.y);
    }
    
   
    
    CGRect tableFrame = self.commentsTable.view.frame;
    tableFrame.size.height = rectFild.origin.y;
    
    [UIView animateWithDuration:0.25f
                     animations:^{
                         [self.commentView setFrame:rectFild];
                         [self.commentsTable.view setFrame:tableFrame];
                         
                     }
     ];
    

}

@end
