//
//  SearchOptionsViewController.h
//  Pimpstagram
//
//  Created by rahul Sharma on 12/11/13.
//
//

#import <UIKit/UIKit.h>
#import <Pinterest/Pinterest.h>

@interface SearchOptionsViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,UISearchBarDelegate>
{
    UICollectionView *_collectionView;
}
@property(nonatomic,strong)NSArray *filterOptions;
@property(nonatomic,strong)NSArray *filterImage;
@property(nonatomic,strong)UITextField *searchText;

@property(nonatomic,assign) int count;
@end
