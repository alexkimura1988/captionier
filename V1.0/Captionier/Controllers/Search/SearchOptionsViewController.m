//
//  SearchOptionsViewController.m
//  Pimpstagram
//
//  Created by rahul Sharma on 12/11/13.
//
//

#import "SearchOptionsViewController.h"
#import "OptionsViewController.h"
#import "SearchUserCell.h"
#import "UIImageView+WebCache.h"
//#import "ui"
#import "PAPHomeViewController.h"
#import "PAPAccountViewController.h"

#import "NavigationViewController.h"
#import "CameraViewController.h"
#import "PBJViewController.h"
#import "ExploreViewController.h"
#import "CustomCameraViewController.h"
#import "UILabel+FormattedText.h"



@interface SearchOptionsViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)NSArray *searchOption;
@property(nonatomic,weak)IBOutlet UISegmentedControl *segmentControl;
@property(nonatomic,strong)IBOutlet UISearchBar *srchBar;
@property(nonatomic,weak) IBOutlet UITableView *tbleView;
@property(nonatomic,strong) NSMutableArray *searchUsers;
@property(nonatomic,strong) NSMutableArray *searchHashTag;
@property(nonatomic,strong) NSMutableArray *likedPictures;
-(IBAction)segmentControlClicked:(id)sender;
@end

@implementation SearchOptionsViewController
@synthesize searchUsers;
@synthesize searchHashTag;
@synthesize srchBar;
@synthesize searchText;

#define Users 100
#define HashTag 101
#define Default 102

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)customizeNavigationBackButton {
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn_off"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn_off"] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn_on"] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
}
-(void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//- (void)viewWillLayoutSubviews
//{
//    [super viewWillLayoutSubviews];
//    NavigationViewController *navigationController = (NavigationViewController *)self.navigationController;
//    [navigationController.menu setNeedsLayout];
//}
- (void)viewDidLoad {
    
    //[self customizeNavigationBackButton];


    _count =0;
    
    [super viewDidLoad];
    
    if (IS_IOS7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.view.backgroundColor = [UIColor whiteColor];
  
    _tbleView.backgroundColor = [UIColor whiteColor];
  
    //UIBarButtonItem *menu = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(toggleMenu)];
   // [menu setImage:[UIImage imageNamed:@"channel_explore_btn.png"]];
    //self.navigationItem.leftBarButtonItem = menu;
    
  
    
  
    
    _filterOptions = (NSMutableArray *) @[@"General",@"Stunts",@"Motorcycle",@"Automotive",@"Adventure",@"Air",@"Sports",@"Style",@"Music",@"Animal",@"Dance",@"Water",@"Crazy",@"Scary",@"Art", @"Snow"];
    
    _filterImage = (NSMutableArray *) @[@"channel_general_on.png",@"channel_stunts_on.png",@"channel_motorcycle_on.png",@"channel_automotive_on.png",@"channel_adventure_on.png",@"channel_air_on.png",@"channel_sports_on.png",@"channel_style_on.png",@"channel_music_on.png",@"channel_animal_on.png",@"channel_dance_on.png",@"channel_water_on.png",@"channel_crazy_on.png",@"channel_scary_on.png",@"channel_art_on.png", @"channel_snow_on.png"];
    
   // _filterOptions = (NSMutableArray *)    @[@"Birthday",@"Comedy",@"Family",@"Fashion",@"Food",@"Hot",@"Messages",@"Music",@"Nature",@"News",@"Omgwtf",@"Places",@"Pets",@"Pictures",@"Science",@"Sports",@"Treats",@"Tricks",@"Weekened",@"Horoscopes",@"Travels"];
    

    
    
    //_filterImage = (NSMutableArray *) @[@"birthday_channel_off.png",@"comedy_channel_off.png",@"family_channel_off.png",@"fashion_channel_off.png",@"food_channel_off.png",@"hot_channel_off.png",@"messages_channel_off.png",@"music_channel_off.png",@"nature_channel_off.png",@"news_channel_off.png",@"omgwtf_channel_off.png",@"palaces_channel_off.png",@"pets_channel_off.png",@"pictures_channel_off.png",@"science_channel_off.png",@"sports_channel_off.png",@"treats_channel_off.png",@"tricks_channel_off.png",@"weekend_channel_off.png",@"horoscopes_channel_off.png",@"travels_channel_off.png"];
    

    
    
    
    _searchOption = @[@"Search Photos and Videos",@"Search Users"];
    
    self.segmentControl.hidden = YES;
    
    
    self.tbleView.tag = Users;
    [self.view setBackgroundColor:WHITE_COLOR];
    self.navigationItem.title = @"Search";
    
    
    
    

    
    //inilize collectionView for explore opitons
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
  CGSize screensize = [[UIScreen mainScreen] bounds].size;;
    if (screensize.height == 480) {
         _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 44, 320, 320) collectionViewLayout:layout];
    }else{
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 44, 320, 410) collectionViewLayout:layout];
    }
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
   // [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView registerNib:[UINib nibWithNibName:@"ExploreCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_collectionView];
    
    //_collectionView.hidden = YES;
    
//    searchText = [[UITextField alloc] initWithFrame:CGRectMake( 10.0f, 4.0f, 300.0f, 31.0f)];
//    searchText.font = [UIFont fontWithName:robo size:14.0f];
//    searchText.backgroundColor = WHITE_COLOR;
//    [self.view addSubview:searchText];
    
    
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [self getMostLikedPictures];
}

-(void)getMostLikedPictures{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [NSDateComponents new];
    comps.day = -30;
    NSDate *sevenDays = [calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
    
    PFQuery *query = [PFQuery queryWithClassName:kPAPFeedClassKey];
    [query whereKey:@"createdAt" greaterThan:sevenDays];
    [query whereKey:kPAPFeedCommentsCount greaterThan:[NSNumber numberWithInt:0]];
    
    PFQuery *likes = [PFQuery queryWithClassName:kPAPFeedClassKey];
    [likes whereKey:kPAPFeedLikeCountKey greaterThan:[NSNumber numberWithInt:0]];
    [likes whereKey:@"createdAt" greaterThan:sevenDays];
    
    PFQuery *combined = [PFQuery orQueryWithSubqueries:@[query,likes]];
    
    [combined orderByDescending:@"createdAt"];
    [combined findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        if (!error) {
            _likedPictures = [[NSMutableArray alloc] initWithArray:array];
            [_collectionView reloadData];
        }
    }];
}


/**
 *  This method will open camera screen
 */
-(void) gotoCamera
{
    NavigationViewController *navigationController = (NavigationViewController *)self.navigationController;
    navigationController.menu.isOpen = YES;
    [navigationController toggleMenu];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if (IS_IPHONE_5) {
        CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController" bundle:nil];
        
        [self.navigationController pushViewController:obj animated:YES];
    }
    else {
        CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController_ip4" bundle:nil];
        
        [self.navigationController pushViewController:obj animated:YES];
    }
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(IBAction)segmentControlClicked:(id)sender{
    
    if (_segmentControl.selectedSegmentIndex == 0) {
        NSLog(@"users");
        
        
        self.tbleView.tag = Users;
        self.srchBar.placeholder = @"search users";
        
    }
    else {
        NSLog(@"hashtag");
        self.tbleView.tag = HashTag;
        self.srchBar.placeholder = @"search with hashtag";
    }
    [self.tbleView reloadData];
}

#pragma Mark- UITextFieldDelegate


- (BOOL)textFieldShouldClear:(UITextField *)textField {
    //if we only try and resignFirstResponder on textField or searchBar,
    //the keyboard will not dissapear (at least not on iPad)!
    [self performSelector:@selector(searchBarCancelButtonClicked:) withObject:srchBar afterDelay: 0.1];
    return YES;
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    self.segmentControl.hidden = NO;
    
        
   // [self segmentControlClicked:nil];
    _collectionView.hidden = YES;
    

    CGRect frame = self.tbleView.frame;
    frame.size.height = frame.size.height - 216 + 49;
    self.tbleView.frame = frame;
    


    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    CGRect frame = self.tbleView.frame;
    frame.size.height = frame.size.height + 216 - 49;
    self.tbleView.frame = frame;
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
//    for (UIView *searchBarSubview in [searchBar subviews])
//    {
////        if ([searchBarSubview conformsToProtocol:@protocol(UITextInputTraits)])
////        {
//    
//            [[UISearchBar appearance] setSearchFieldBackgroundImage:[UIImage imageNamed:@"background"]forState:UIControlStateNormal];
//            [(UITextField *)searchBarSubview setBorderStyle:UITextBorderStyleNone];
//            
//            [[UISearchBar appearance] setBackgroundImage:[UIImage imageNamed:@"search"]];
//            [[UISearchBar appearance] setTintColor:[UIColor clearColor]];
////        }
////        
//    }
    
    
    

    
    if(searchBar.text.length != 0) {
        if (_segmentControl.selectedSegmentIndex == 0) {
            
            [self findUsersWithName:searchBar.text];
        }
        else if(_segmentControl.selectedSegmentIndex == 1)
        {
            [self findHashTag:searchBar.text];
        }
//        else
//        {
//            
//        }
    }
    
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(searchBar.text.length != 0) {
        if (_segmentControl.selectedSegmentIndex == 0) {
            
            [self findUsersWithName:[searchBar.text stringByReplacingCharactersInRange:range withString:text]];
        }
        else {
            [self findHashTag:[searchBar.text stringByReplacingCharactersInRange:range withString:text]];
        }
        
    }
    
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    
    CGRect frame = self.tbleView.frame;
    frame.size.height = frame.size.height + 216 - 49;
    self.tbleView.frame = frame;
    
    _collectionView.hidden = NO;
    self.segmentControl.hidden = YES;
    searchBar.text = @"";
    
    if (self.tbleView.tag == Users) {
        [searchUsers removeAllObjects];
    }
    else if(self.tbleView.tag == HashTag){
        [self.searchHashTag removeAllObjects];
    }
    else if(self.tbleView.tag == Default){
        self.filterOptions = nil;
    }
    //self.tbleView.tag = Default;
    
    
//    _count = 0;
//    CGRect frame = self.tbleView.frame;
//  
//    frame.origin.y = self.tbleView.frame.origin.y - 30;
//    self.tbleView.frame = frame;
    [self.tbleView reloadData];
    
    //[self.navigationController setNavigationBarHidden:NO animated:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
}
-(void)findUsersWithName:(NSString*)name {
    
    PFQuery *queryUsername = [PFUser query];
    [queryUsername whereKey:@"username" matchesRegex:name modifiers:@"i"];
    
    PFQuery *querydisplayname = [PFUser query];
    [querydisplayname whereKey:kPAPUserDisplayNameKey matchesRegex:name modifiers:@"i"];
    
    PFQuery *query = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:queryUsername,querydisplayname, nil]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects , NSError *error) {
        if (!error) {
            
            self.tbleView.tag = Users;
            
            if (!searchUsers) {
                searchUsers = [[NSMutableArray alloc] init];
            }
            
            [searchUsers removeAllObjects];
            searchUsers = (NSMutableArray*)objects;
            

            
            [self.tbleView reloadData];
        }
        NSLog(@"error %@",[error localizedDescription]);
    }];
    
}
-(void)findHashTag:(NSString*)hashtag {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    //[pi showPIOnView:self.view withMessage:@""];
    PFQuery *query = [PFQuery queryWithClassName:@"HashTag"];
    [query whereKey:@"hashtag" matchesRegex:hashtag modifiers:@"i"];
    [query addDescendingOrder:@"hashCount"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        if (!error) {
            
            if (!searchHashTag) {
                searchHashTag = [[NSMutableArray alloc] init];
            }
            [searchHashTag removeAllObjects];
            
            for(PFObject *obj in array){
                [searchHashTag addObject:[obj objectForKey:@"hashtag"]];
            }
            
            
            [pi hideProgressIndicator];
            self.tbleView.tag = HashTag;
            [self.tbleView reloadData];
        }
    }];
    
}
-(void)hashTagInString:(NSString *)_str Searched:(NSString*)sHashTag
{
    //NSString * aString = @"This is the #substring1 and #subString2 I want";
    //NSMutableArray *substrings = [NSMutableArray new];
   
    if (!searchHashTag) {
        searchHashTag = [[NSMutableArray alloc] init];
    }
    NSScanner *scanner = [NSScanner scannerWithString:_str];
    [scanner scanUpToString:@"#" intoString:nil]; // Scan all characters before #
    while(![scanner isAtEnd]) {
        NSString *substring = nil;
        [scanner scanString:@"#" intoString:nil]; // Scan the # character
        if([scanner scanUpToString:@" " intoString:&substring]) {
            // If the space immediately followed the #, this will be skipped
   
            if ([substring rangeOfString:sHashTag].location != NSNotFound) {
                if ([searchHashTag indexOfObject:substring] == NSNotFound) {
                    [searchHashTag addObject:substring];
                    //break;
                }
            }
          
        }
        [scanner scanUpToString:@"#" intoString:nil]; // Scan all characters before next #
    }
    // do something with substrings
   
//    CGRect frame = self.tbleView.frame;
//    frame.origin.y = self.tbleView.frame.origin.y + 15;
//    
//    self.tbleView.frame = frame;
    
    [self.tbleView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == Users) {
        return searchUsers.count;
    }
    else if(tableView.tag == HashTag){
        return self.searchHashTag.count;
    }
    else if(tableView.tag == Default){
        return self.filterOptions.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *FriendCellIdentifier = @"FriendCell";
    static NSString *FCellIdentifier = @"HashTagCell";
    
    if (tableView.tag == Users)
    {
        SearchUserCell *cell = (SearchUserCell*)[tableView dequeueReusableCellWithIdentifier:FriendCellIdentifier];
        if (cell == nil)
        {
            cell = [[SearchUserCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:FriendCellIdentifier];
            
            //UIImageView *imageSeprator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
            
            
            UIImage *bgImage = [UIImage imageNamed:@"cell.png"];
            UIImageView *bg = [[UIImageView alloc] initWithImage:bgImage];
            cell.backgroundView = bg;
            
            
            UIImage *selectionBackground = [UIImage imageNamed:@"cell_selected.png"];
            UIImageView *bgview=[[UIImageView alloc] initWithImage:selectionBackground];
            cell.selectedBackgroundView=bgview;
            
            UIImage *arrowImage = [UIImage imageNamed:@"arrow.png"];
            UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(300 - arrowImage.size.width ,56/2 - arrowImage.size.height/2, arrowImage.size.width, arrowImage.size.height)];
            arrowImageView.image = arrowImage;
            arrowImageView.tag = 100;
            [cell.contentView addSubview:arrowImageView];
        }
        

        
        PFUser *chatusers = searchUsers[indexPath.row];
      
        cell.lblUserName.text = [chatusers objectForKey:kPAPUserDisplayNameKey];
        PFFile *displayPic = [chatusers objectForKey:kPAPUserProfilePicSmallKey];
        [cell.userIcon setImageWithURL:[NSURL URLWithString:displayPic.url]
                      placeholderImage:[UIImage imageNamed:@"Icon-72.png"]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                                 //[cell.activityIndicatior stopAnimating];
                                 //cell.activityIndicatior.hidden = YES;
                             }];
        cell.backgroundColor = [UIColor whiteColor];
        return cell;
    }
   // else if(tableView.tag == HashTag)
     else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:FCellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:FCellIdentifier];
            cell.textLabel.textColor = BLACK_COLOR;
            cell.textLabel.font = [UIFont fontWithName:robo_bold size:16];
           cell.backgroundColor = [UIColor whiteColor];
            //UIImageView *imageSeprator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
            
            
            UIImage *bgImage = [UIImage imageNamed:@"cell.png"];
            UIImageView *bg = [[UIImageView alloc] initWithImage:bgImage];
            cell.backgroundView = bg;
            
            UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(10, 43, 300, 1)];
            line.image = [UIImage imageNamed:@"profile_screen_filter_line-568h"];
            [cell.contentView addSubview:line];

            
            
            UIImage *selectionBackground = [UIImage imageNamed:@"cell_selected.png"];
            UIImageView *bgview=[[UIImageView alloc] initWithImage:selectionBackground];
            cell.selectedBackgroundView=bgview;
            
            UIImage *arrowImage = [UIImage imageNamed:@"arrow.png"];
            UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(300 - arrowImage.size.width ,56/2 - arrowImage.size.height/2, arrowImage.size.width, arrowImage.size.height)];
            arrowImageView.image = arrowImage;
            arrowImageView.tag = 100;
            [cell.contentView addSubview:arrowImageView];
        }
        //NSString *hashtext = searchHashTag[indexPath.row];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@",searchHashTag[indexPath.row]];
        
        //[cell.textLabel setTextColor:[UIColor colorWithRed:0.137 green:0.780 blue:0.349 alpha:1.000] range:NSMakeRange(0, 1)];
        
        
        return cell;
    }
//    else
//    {
//       
//        static NSString *CellIdentifier = @"Cell";
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//        if (cell == nil) {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//        }
//        
//        // Configure the cell...
//        cell.textLabel.text = _filterOptions[indexPath.row];
//        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:28];
//        cell.textLabel.textColor =  UIColorFromRGB(0x000000);
//        
////        return cell;
//        return cell;
//    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == Users) {
        if (indexPath.row < self.searchUsers.count) {
            return 65;
        } else {
            return 44.0f;
        }
    }
    return 44;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == Users) {
        
        CGRect frame = self.tbleView.frame;
        frame.size.height = frame.size.height + 216 - 49;
        self.tbleView.frame = frame;
        
        [self.srchBar resignFirstResponder];
        
        PFObject *object = searchUsers[indexPath.row];
        PAPAccountViewController *accountViewController = [[PAPAccountViewController alloc] initWithStyle:UITableViewStylePlain];
        accountViewController.showBackButton = YES;
        [accountViewController setUser:(PFUser*)object];
        [self.navigationController pushViewController:accountViewController animated:YES];
        
       
        
        

        
    }
    else if(tableView.tag == HashTag) {
        
        CGRect frame = self.tbleView.frame;
        frame.size.height = frame.size.height + 216 - 49;
        self.tbleView.frame = frame;
        
        [self.srchBar resignFirstResponder];
        
        [self loadViewWithHashTag:[NSString stringWithFormat:@"%@",searchHashTag[indexPath.row]]];//#
    }
    else if(tableView.tag == Default)
    {
        NSLog(@"%@",_filterOptions[indexPath.row]);
        
//        ExploreEachCatagoryViewController *obj =[[ExploreEachCatagoryViewController alloc] initWithNibName:@"ExploreEachCatagoryViewController" bundle:nil];
//        obj.catagoty =_filterOptions[indexPath.row];
//        
//        [self.navigationController pushViewController:obj animated:YES];
    }
    
}
-(void)loadViewWithHashTag:(NSString*)hashTag
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:hashTag forKey:@"HashTag"];
    [ud synchronize];
    PAPHomeViewController *home = [[PAPHomeViewController alloc] initWithStyle:UITableViewStylePlain];
    home.hashTagString = hashTag;
    [home setHashTag:YES];
    [self.navigationController pushViewController:home animated:YES];
}


#pragma mark - UICollectionGridViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _likedPictures.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
       
    UIImageView *cellImageView = (UIImageView*)[cell viewWithTag:100];
    
    cellImageView.image = nil;
    
    PFObject *object = _likedPictures[indexPath.row];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
   
    NSURL *mediaURL = [NSURL URLWithString:[object objectForKey:kPAPFeedMediaThumbnailLikKey]];
    
    [manager downloadWithURL:mediaURL
                     options:0
                    progress:^(NSUInteger receivedSize , long long expectesSize){
                        
                                            }
                   completed:^(UIImage *image , NSError *error , SDImageCacheType cacheType ,BOOL finished){
                       
                     
                       cellImageView.image = image;
                       
                   }];
    
    
    //[cellImageView setImage:[UIImage imageNamed:_filterImage[indexPath.item]]];
   
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(106, 106);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    PFObject *activity = _likedPictures[indexPath.row];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:@"1" forKey:@"SingleObject"];
    
    
    PAPHomeViewController *home = [[PAPHomeViewController alloc]initWithStyle:UITableViewStylePlain];
    home.selectedObject = activity;
    home.isNeedPullToRefresh = NO;
    [self.navigationController pushViewController:home animated:YES];
  
}

//#pragma mark-customNavigation button
//
//-(void)customizeNavigationBar
//{
////    [[UIApplication sharedApplication] setStatusBarHidden:NO];
////    UIView *navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
////    navView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navigation_bar.png"]];
////    [self.view addSubview:navView];
////    
////    UILabel *navTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 20, 270, 40)];
////    [Helper setToLabel:navTitleLabel Text:_quizTitle WithFont:@"HelveticaNeue-Semibold" FSize:16 Color:[UIColor whiteColor]];
////    navTitleLabel.numberOfLines = 0;
////    navTitleLabel.textAlignment = NSTextAlignmentCenter;
////    [navView addSubview:navTitleLabel];
//    
//    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(5, 20, 40, 40)];
//    //[Helper setButton:backBtn Text:@"Back" WithFont:@"HelveticaNeue" FSize:16 TitleColor:[UIColor whiteColor] ShadowColor:nil];
//    [backBtn setImage:[UIImage imageNamed:@"back_btn_off@2x.png"] forState:UIControlStateNormal];
//    [backBtn addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
//    //[navView addSubview:backBtn];
//    
//}
//
//- (void)backBtnClicked: (UIButton *)btn
//{
//    [self.navigationController popViewControllerAnimated:YES];
//    
//}





@end
