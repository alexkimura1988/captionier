//
//  SearchUserCell.h
//  InsightApp
//
//  Created by Surender Rathore on 07/01/14.
//
//

#import <UIKit/UIKit.h>

@class OHAttributedLabel;


@interface SearchUserCell : UITableViewCell<UICollectionViewDataSource>
@property(nonatomic,strong)UIImageView *userIcon;
@property(nonatomic,strong)UILabel *lblUserName;
@end
