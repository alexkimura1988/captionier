//
//  SearchUserCell.m
//  InsightApp
//
//  Created by Surender Rathore on 07/01/14.
//
//

#import "SearchUserCell.h"
#import "NSAttributedString+Attributes.h"
#import "OHASBasicMarkupParser.h"
#import "OHAttributedLabel.h"


@implementation SearchUserCell

@synthesize lblUserName;
@synthesize userIcon;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        if (!userIcon) {
            userIcon = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2.5, 60, 60)];
           userIcon.layer.cornerRadius = userIcon.frame.size.width/2;
            self.userIcon.clipsToBounds = YES;
            [self.userIcon setClipsToBounds:YES];
            
            [self.contentView addSubview:userIcon];
            
        }
        if (!lblUserName) {
            lblUserName = [[UILabel alloc] initWithFrame:CGRectMake(70, 65/2-25/2, 280, 25)];
            [Helper setToLabel:lblUserName Text:@"" WithFont:robo_bold FSize:18 Color:[UIColor blackColor]];//BLACK_COLOR
            lblUserName.backgroundColor = [UIColor clearColor];
           
            
//            lblUserName = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(70, 65/2-25/2, 280, 25)];
//            lblUserName.numberOfLines = 0;
//            lblUserName.linkColor = BLACK_COLOR;
//            lblUserName.linkUnderlineStyle = kCTUnderlineStyleNone;
//            [Helper setToLabel:lblUserName Text:@"" WithFont:robo_bold FSize:18 Color:[UIColor blackColor]];
//            //lblUserName.font = [UIFont systemFontOfSize:13];
//            lblUserName.lineBreakMode = NSLineBreakByWordWrapping;
//            //lblUserName.delegate = self;
//            //labelCommentOne.backgroundColor = GREEN_COLOR;
//            //[commentview addSubview:labelCommentOne];
//           [lblUserName sizeToFit];
          [self.contentView addSubview:lblUserName];
            
        }
    }
    return self;
}







- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
