//
//  MediaShareViewController.h
//  VIND
//
//  Created by Vinay Raja on 27/08/14.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface MediaShareViewController : UIViewController<MFMessageComposeViewControllerDelegate>

@property (nonatomic, assign) BOOL isMediaTypeImage;
@property (nonatomic, strong) NSString *mediaPath;
@property (strong, nonatomic) IBOutlet UILabel *sharelbl;
@property (strong, nonatomic) IBOutlet UIScrollView *shareScrollView;
@property (strong, nonatomic) IBOutlet UIButton *smsShare;

- (IBAction)shareOnSms:(id)sender;
- (IBAction)shareOnPinterest:(id)sender;

@end
