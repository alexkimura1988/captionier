//
//  MediaShareViewController.m
//  VIND
//
//  Created by Vinay Raja on 27/08/14.
//
//

#import "MediaShareViewController.h"


#import <MessageUI/MessageUI.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import  <Social/Social.h>
#import "FollowingList.h"
#import "OptionsViewController.h"
#import "UploadProgress.h"
#import "UIImage+ResizeAdditions.h"
#import <Pinterest/Pinterest.h>
#import "PlacesViewController.h"
#import "AmazonTransfer.h"
#import <CoreLocation/CoreLocation.h>
#import <FacebookSDK/FacebookSDK.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>

#define Bucket @"captionierapp"

@interface MediaShareViewController () <UITextFieldDelegate, MFMailComposeViewControllerDelegate, UITextViewDelegate, UIDocumentInteractionControllerDelegate, OptionsViewControllerDelegate,CLLocationManagerDelegate>
{
    UIDocumentInteractionController *dic;
    
    BOOL shareOnFB;
    BOOL shareOnTwitter;
    
    PFObject *feedObject;
}

@property (weak, nonatomic) IBOutlet UITextView *captionTextView;
@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (weak, nonatomic) IBOutlet UIView *addCaptionView;
@property (weak, nonatomic) IBOutlet UIView *addLocationView;
@property (weak, nonatomic) IBOutlet UIView *addChannelView;
@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet UITextField *locationTextField;
@property (weak, nonatomic) IBOutlet UITextField *channelTextField;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnTwitter;
@property (weak, nonatomic) IBOutlet UIButton *btnInstagram;
@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;

@property (strong, nonatomic) NSString *uploadedMediaLink;
@property (strong, nonatomic) NSString *uploadedThumbnailMediaLink;

@property (strong, nonatomic) NSString *socialMedia;
@property (nonatomic,strong) NSString *shortUrl;

@property (nonatomic,strong)   NSMutableArray *followers;
@property (nonatomic,strong)   NSMutableArray *taggedUsers;
@property (nonatomic,strong)   FollowingList *followinglist;

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) CLLocation *currentLocation;
@property (nonatomic,strong) CLLocation *taggedLocation;

@end

@implementation MediaShareViewController
@synthesize sharelbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIButton *menuBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn addTarget:self.navigationController action:@selector(toggleMenu) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setFrame:CGRectMake(0.0f,0.0f, 44,44)];
    [Helper setButton:menuBtn Text:@"" WithFont:@"HelveticaNeue" FSize:17 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [menuBtn setImage:[UIImage imageNamed:@"menu_icon_off.png"] forState:UIControlStateNormal];
    [menuBtn setImage:[UIImage imageNamed:@"menu_icon_on.png"] forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingBarButton1 = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    
    //self.navigationItem.leftBarButtonItem = containingBarButton1;
    
    UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
    [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
    // [backButton setBackgroundImage:[UIImage imageNamed:<#ImageNameOn#>] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground:) name:UIApplicationDidEnterBackgroundNotification object:nil];

    [self setTitle:@"Share"];
  
    //_shareScrollView.contentSize =CGSizeMake(320,1000-500);//320,1000
    _addCaptionView.layer.cornerRadius = 8;
    _shareView.layer.cornerRadius = 8;
    _addLocationView.layer.cornerRadius = 8;
    _addChannelView.layer.cornerRadius = 8;
    _shareScrollView.scrollEnabled = NO;
    
    [Helper setToLabel:sharelbl Text:@"Share with" WithFont:robo_light FSize:18 Color:[UIColor colorWithRed:101/255.0f green:109/255.0f blue:120/255.0f alpha:1.0f]];
    
    _captionTextView.delegate = self;
    _captionTextView.text = @" Add Description";
    _captionTextView.textColor = [UIColor lightGrayColor];
    
    
    if (!_isMediaTypeImage) {
        [_btnEmail setEnabled:NO];
        [_btnFacebook setEnabled:NO];
        [_btnInstagram setEnabled:NO];
        [_btnMessage setEnabled:NO];
        [_btnTwitter setEnabled:NO];
        
    }
   
    if ([CLLocationManager locationServicesEnabled ]) {
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        [self.locationManager startUpdatingLocation];
    }
    
    shareOnFB = NO;
    shareOnTwitter = NO;
    
}

- (void) didEnterBackground:(NSNotification*)notif
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:_mediaPath forKey:@"workingMedia"];
    [ud synchronize];
}

- (void) willEnterForeground:(NSNotification*)notif
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"workingMedia"]) {
        _mediaPath = [ud objectForKey:@"workingMedia"];
    }
    
    if (_isMediaTypeImage) {
        _postImageView.image = [UIImage imageWithContentsOfFile:_mediaPath];
    }
    else {
        _postImageView.image = [self firstFrameOfVideo];
        
    }
}


/**
 *  it will make application to go back to previous controller
 *
 *  @param sender btn pressed
 */

- (void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


/**
 *  This method will open the option controller to select the channel for post
 *
 *  @param sender channelBtn pressed
 */
-(IBAction) selectChannel:(id)sender
{
    
    OptionsViewController *optionVC = [[OptionsViewController alloc] init];
    //optionVC.filterOptions = @[@"Dates",@"Work",@"Hobby",@"Day to day",@"Food and Drink",@"Home",@"Personal",@"Other"];
    //optionVC.filterOptions =  @[@"Travel",@"Education",@"World",@"Urban",@"Sports",@"Special",@"Politics",@"Nature",@"Music",@"Health",@"Food",@"Beauty",@"Family",@"Dog",@"Cat"];
    
    optionVC.filterOptions =   @[@"General",@"Stunts",@"Motorcycle",@"Automotive",@"Adventure",@"Air",@"Sports",@"Style",@"Music",@"Animal",@"Dance",@"Water",@"Crazy",@"Scary",@"Art", @"Snow"];
    // optionVC.catagoryTitle = @"Select Channel";
    optionVC.delegate = self;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:optionVC];
    //[self presentModalViewController:navController animated:YES];
    [self.navigationController presentViewController:navController animated:YES completion:nil];
    
    
}

#pragma mark - OPtionViewControllerDelegate

/**
 *  This method will be invoke once user select the channel from option controller
 *
 *  @param option channelname String
 */
-(void)optionSelected:(NSString *)option{
    
    [_channelTextField setText:option];
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    _shareScrollView.contentSize =CGSizeMake(320,1000-500);

    if (_isMediaTypeImage) {
        _postImageView.image = [UIImage imageWithContentsOfFile:_mediaPath];
    }
    else {
        _postImageView.image = [self firstFrameOfVideo];

    }
}


- (void)viewWillDisappear:(BOOL)animated {
    
    [self deregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
    
}



/**
 *  This method will open place controller to add location of post
 *  it return location  name string
 *
 *  @param sender
 */
- (IBAction)addLocation:(id)sender
{
   // [_locationTextField becomeFirstResponder];
    PlacesViewController *places = [[PlacesViewController alloc] initWithNibName:@"PlacesViewController" bundle:nil];
    places.currentLocation = self.currentLocation;
    
    places.callback = ^(NSString *name , CLLocation *locaiton){
        _locationTextField.text = name;
        _taggedLocation = locaiton;
    };
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:places];
    
    [self presentViewController:navigationController animated:YES completion:nil];
}


/**
 *  This method will set boolean value for fb sharing on or off
 *
 *  @param sender fbButton selected
 */
- (IBAction)shareOnFB:(UIButton*)sender
{
    if (sender.selected) {
        sender.selected = NO;
        shareOnFB = NO;
        return;
    }
    sender.selected = YES;
    //_socialMedia = @"Facebook";
    
    shareOnFB = YES;
    // [self shareOnFacebook];
    
}


/**
 *  This method will set boolean value for twitter sharing on or off
 *
 *  @param sender twitter button selected
 */
- (IBAction)shareOnTwitter:(UIButton*)sender
{
    if (sender.selected) {
        sender.selected = NO;
        shareOnTwitter = NO;
        
        return;
    }
    
   
    shareOnTwitter = YES;
    
    sender.selected = YES;
   
    

    
}


/**
 *  This method will share post on instagram
 *
 *  @param sender button pressed
 */
- (IBAction)shareOnInstagram:(UIButton*)sender
{
    if (sender.selected) {
                sender.selected = NO;
        return;
    }
    
    if (!_isMediaTypeImage) {
        [Helper showAlertWithTitle:@"Message" Message:@"Videos can not be directly shared to Instagram.\nThis video has been saved to Photo Album."];
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(_mediaPath))
        {
            UISaveVideoAtPathToSavedPhotosAlbum(_mediaPath, nil, nil, nil);
        }
        sender.selected = YES;

        return;
    }
    

    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        //imageToUpload is a file path with .ig file extension
        
        NSString *savePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/sharing.igo"];
        
        NSFileManager *fm = [NSFileManager defaultManager];
        if ([fm fileExistsAtPath:savePath])
        {
            [fm removeItemAtPath:savePath error:nil];
        }
        
        [fm copyItemAtPath:_mediaPath toPath:savePath error:nil];
        
        dic = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
        dic.UTI = @"com.instagram.exclusivegram";
        dic.delegate = nil;
        
        dic.annotation = [NSDictionary dictionaryWithObject:@"Shared via Picogram" forKey:@"InstagramCaption"];
        [dic presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
        
        sender.selected = YES;

    }
    else
    {
        [Helper showAlertWithTitle:@"Message" Message:@"You don't have Instagram installed. Download instagram app to get more functionality."];

    }
}


/**
 *  This method will let the media link generate in main thread
 *
 *  @param feed media path
 */
- (void) postToSocialMedia:(PFObject*)feed
{
    if (shareOnTwitter) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self postOnTwitter:feed];
        });
        
    }
    else if (shareOnFB) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self postOnFacebook:feed];
        });
    }
}



- (IBAction)postFeed:(id)sender
{
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Posting Feed..."];
    
    _socialMedia = @"Parse";
    
    if (_uploadedMediaLink.length) {
        
        [self postToParse:_uploadedMediaLink];
    }
    else {
        [self uploadMediaToS3];
    }
}


/**
 *  This method will post the media link on parse
 *
 *  @param urlString image
 */
- (void) postToParse:(NSString*)urlString
{
    PFUser *user = [PFUser currentUser];
    PFACL *photoACL = [PFACL ACLWithUser:user];
    [photoACL setPublicReadAccess:YES];
    [photoACL setPublicWriteAccess:YES];

    PFObject *feed = [PFObject objectWithClassName:kPAPFeedClassKey];
    [feed setObject:[PFUser currentUser] forKey:kPAPFeedUserKey];
    feed.ACL = photoACL;

    if (_isMediaTypeImage) {
        [feed setObject:kPAPFeedMediaTypeImageKey forKey:kPAPFeedMediaTypeKey];
    }
    else {
        [feed setObject:kPAPFeedMediaTypeVideoKey forKey:kPAPFeedMediaTypeKey];
    }
    
    [feed setObject:_uploadedMediaLink forKey:kPAPFeedMediaLinkKey];
    [feed setObject:_uploadedThumbnailMediaLink forKey:kPAPFeedMediaThumbnailLikKey];
    [feed setObject:[NSNumber numberWithInt:0] forKey:kPAPFeedLikeCountKey];
    //[feed setObject:@"no" forKey:@"isPrivateGroup"];

    
   
    if (self.taggedLocation != nil) {
        
        PFGeoPoint *point = [PFGeoPoint geoPointWithLatitude:self.taggedLocation.coordinate.latitude longitude:self.taggedLocation.coordinate.longitude];
        [feed setObject:point forKey:kPAPFeedPhotoTaggedLocation];
        
        [feed setObject:_locationTextField.text forKey:kPAPFeedLocationKey];
    }
    else if (self.currentLocation != nil){
        
        PFGeoPoint *point = [PFGeoPoint geoPointWithLatitude:self.currentLocation.coordinate.latitude longitude:self.currentLocation.coordinate.longitude];
        [feed setObject:point forKey:kPAPFeedPhotoTaggedLocation];
    }

    
    if (_locationTextField.text.length > 0 ) {
        [feed setObject:_locationTextField.text forKey:kPAPFeedLocationKey];
    }
    
    if (![_captionTextView.text isEqualToString:@" Add Description"] ) {
        
        [feed setObject:@[[PFUser currentUser].username] forKey:kPAPFeedCommentersArrayKey];
        [feed setObject:@[_captionTextView.text] forKey:kPAPFeedCommentsArrayKey];
        [feed setObject:[NSNumber numberWithInt:1] forKey:kPAPFeedCommentsCount];
        

    }
    else {
        [feed setObject:[NSNumber numberWithInt:0] forKey:kPAPFeedCommentsCount];
    }
    
    NSString *channel = @"General";
    if (_channelTextField.text) {
        channel = _channelTextField.text;
    }
    
    [feed setObject:channel forKey:kPAPFeedChannelKey];
    

    [feed saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        
        UploadProgress *up = [UploadProgress sharedInstance];
        [up hide];
        
        if (succeeded) {
            
            [self postToSocialMedia:feed];
            
            [self sendPushToTaggedUsersForObject:feed];
            
            if (![_captionTextView.text isEqualToString:@" Add Description"] ) {
                [self makeAdditionalTextAsFirstComment:feed];
            }

           [[Helper sharedInstance] setNeedToInilizeCamera:1];
            
            self.tabBarController.selectedIndex = 0;
            
            [self.navigationController popToRootViewControllerAnimated:NO];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                  [[NSNotificationCenter defaultCenter] postNotificationName:PAPTabBarControllerDidFinishEditingPhotoNotification object:feed];
            });
          
            
        }
    }];
    
    
}


- (void) getHashtags:(NSString*)hashTag
{
    NSArray *arr = [self getHashtag:hashTag];
}

-(NSArray*)getHashtag:(NSString *)_str
{
    NSString * aString = _str;
    NSMutableArray *substrings = [NSMutableArray new];
    NSScanner *scanner = [NSScanner scannerWithString:aString];
    [scanner scanUpToString:@"#" intoString:nil]; // Scan all characters before #
    while(![scanner isAtEnd]) {
        NSString *substring = nil;
        [scanner scanString:@"#" intoString:nil]; // Scan the # character
        if([scanner scanUpToString:@" " intoString:&substring]) {
            // If the space immediately followed the #, this will be skipped
            [substrings addObject:[NSString stringWithFormat:@"#%@",substring]];
        }
        [scanner scanUpToString:@"#" intoString:nil]; // Scan all characters before next #
    }
    // do something with substrings
    return substrings;
    
    
}


/**
 *  This method will send push notification
 *
 *  @param toUser tagged user of the post
 */

-(void)sendPushToTaggedUsersForObject:(PFObject *)object
{
    if (_taggedUsers.count > 0) {
        NSMutableArray *channels = [[NSMutableArray alloc] init];
        
        for(PFUser *usr in _taggedUsers) {
            
            if ([_captionTextView.text rangeOfString:usr.username].location == NSNotFound) {
                //do not send push notification to that user
            }
            else {
                [channels addObject:[usr objectForKey:kPAPUserPrivateChannelKey]];
            }
            
            
        }
        
        NSString *message;
        if (!_isMediaTypeImage) {
            message = [NSString stringWithFormat:@"%@ tagged you in video",[PFUser currentUser].username];//username
        }
        else {
            message = [NSString stringWithFormat:@"%@ tagged you in photo",[PFUser currentUser].username];
        }
        
        NSDictionary *payload =
        [NSDictionary dictionaryWithObjectsAndKeys:
         message, kAPNSAlertKey,
         @"Increment",kAPNSBadgeKey,
         kPAPPushPayloadPayloadTypeActivityKey, kPAPPushPayloadPayloadTypeKey,
         kPAPPushPayloadActivityTaggedInPhotoKey, kPAPPushPayloadActivityTypeKey,
         [[PFUser currentUser] objectId], kPAPPushPayloadFromUserObjectIdKey,
         [object objectId], kPAPPushPayloadPhotoObjectIdKey,
         @"sms-received.wav",kAPNSSoundKey,
         nil];
        
        PFPush *push = [[PFPush alloc] init];
        
        // Be sure to use the plural 'setChannels'.
        [push setChannels:channels];
        [push setData:payload];
        //[push setMessage:[NSString stringWithFormat:@"%@ tagged you in photo",[PFUser currentUser].username]];
        [push sendPushInBackground];
    }
    
}


-(void)makeAdditionalTextAsFirstComment:(PFObject*)photo {
    
    PFObject *comment = [PFObject objectWithClassName:kPAPActivityClassKey];
    [comment setObject:_captionTextView.text forKey:kPAPActivityContentKey]; // Set comment text
    [comment setObject:[PFUser currentUser] forKey:kPAPActivityToUserKey]; // Set toUser
    [comment setObject:[PFUser currentUser] forKey:kPAPActivityFromUserKey]; // Set fromUser
    [comment setObject:kPAPActivityTypeComment forKey:kPAPActivityTypeKey];
    [comment setObject:photo forKey:kPAPActivityPhotoKey];
    
    PFACL *ACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [ACL setPublicReadAccess:YES];
    [ACL setWriteAccess:YES forUser:[photo objectForKey:kPAPPhotoUserKey]];
    comment.ACL = ACL;
    
    [[PAPCache sharedCache] incrementCommentCountForPhoto:photo];
    
   
    NSMutableArray *taggedUsers = [[NSMutableArray alloc] init];
    [taggedUsers addObject:comment];
    
    if (_taggedUsers.count > 0) {
        //NSMutableArray *channels = [[NSMutableArray alloc] init];
        
        for(PFUser *usr in _taggedUsers) {
            
            if ([_captionTextView.text rangeOfString:usr.username].location == NSNotFound) {
                //do not send push notification to that user
            }
            else {
                
                if (![usr.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    PFObject *commentTagActivity = [PFObject objectWithClassName:kPAPActivityClassKey];
                    [commentTagActivity setObject:[PFUser currentUser] forKey:kPAPActivityFromUserKey];
                    [commentTagActivity setObject:kPAPActivityTypeCommentTag forKey:kPAPActivityTypeKey];
                    [commentTagActivity setObject:usr forKey:kPAPActivityToUserKey];
                    [commentTagActivity setObject:_captionTextView.text forKey:kPAPActivityContentKey];
                    [commentTagActivity setObject:photo forKey:kPAPActivityPhotoKey];
                    
                    PFACL *likeACL = [PFACL ACLWithUser:[PFUser currentUser]];
                    [likeACL setPublicReadAccess:YES];
                    [likeACL setWriteAccess:YES forUser:[photo objectForKey:kPAPPhotoUserKey]];
                    commentTagActivity.ACL = likeACL;
                    
                    [taggedUsers addObject:commentTagActivity];
                }
                
            }
         
        }
        
        
    }
    
    
    
    
    
    [PFObject saveAllInBackground:taggedUsers block:^(BOOL successed , NSError *error){
        if (error) {
            //[self makeEntryForTaggedusersInbackground];
        }
        else {
            NSLog(@"saved all tagged usrs");
        }
    }];
    
    NSMutableArray *hashtags = [NSMutableArray arrayWithArray:[self getHashtag:_captionTextView.text]];
    NSMutableArray *marray = [[NSMutableArray alloc] init];
    
    PFQuery *query = [PFQuery queryWithClassName:@"HashTag"];
    [query whereKey:@"hashtag" containedIn:hashtags];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError*error){
        NSLog(@" arry %@",array);
        
        //update count for previous added hashtags
        for(PFObject *obj in array){
            
            NSString *tag = [obj objectForKey:@"hashtag"];
            if ([hashtags containsObject:tag]) {
                [obj incrementKey:@"hashCount"];
                [hashtags removeObject:tag];
            }
            
            
            [marray addObject:obj];
        }
        
        //add newly created hashtags
        for(NSString *tags in hashtags){
            PFObject *object = [PFObject objectWithClassName:@"HashTag"];
            [object setObject:tags forKey:@"hashtag"];
            [object incrementKey:@"hashCount"];
            
            
            PFACL *ACL = [PFACL ACLWithUser:[PFUser currentUser]];
            [ACL setPublicReadAccess:YES];
            [ACL setPublicWriteAccess:YES];
            object.ACL = ACL;
            
            
            [marray addObject:object];
        }
        
        [PFObject saveAllInBackground:marray block:^(BOOL success, NSError*error){
            NSLog(@"error %@",error);
            
        }];
    }];
    
}

/**
 *  This method will send image or video to amazon
 */
- (void) uploadMediaToS3
{
    PFUser *cUser = [PFUser currentUser];
    
    if (_isMediaTypeImage) {
        
        UIImage *image = [UIImage imageWithContentsOfFile:_mediaPath];
        
        [self uploadFullImageOnAmezon:image];
        
//        NSString *mediaName = [NSString stringWithFormat:@"%@/image%@.%@", cUser.username, [self getCurrentTime],[_mediaPath pathExtension]];
//        NSData *data = [NSData dataWithContentsOfFile:_mediaPath];
//        
//        [[AmazonAndPubnubWrapper sharedInstance] imageUploadFromNSData:data uploadType:Delegate inBucket:BUCKET withName:mediaName withDelegate:self];
        
    }
    else {
        
        [self uplaodVideoOnAmezon];
        
//        NSString *mediaName = [NSString stringWithFormat:@"%@/video%@.%@", cUser.username, [self getCurrentTime],[_mediaPath pathExtension]];
//        
//        [[AmazonAndPubnubWrapper sharedInstance] videoUpload:[NSURL fileURLWithPath:_mediaPath] uploadType:Delegate inBucket:BUCKET withName:mediaName withDelegate:self];
        
    }
}

- (void) showPlacePickerController:(NSString*)place
{
    if (!place.length) {
        return;
    }
    
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"email",
                            nil];
    [FBSession openActiveSessionWithReadPermissions:permissions
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState state,
                                                      NSError *error) {
                                      
                                      if (state == FBSessionStateOpen) {
                                          // Initialize the place picker
                                          FBPlacePickerViewController *placePickerController = [[FBPlacePickerViewController alloc] init];
                                          // Set the place picker title
                                          placePickerController.title = @"Pick Location";
                                          // Hard code current location to Menlo Park, CA
                                          //    placePickerController.locationCoordinate = CLLocationCoordinate2DMake(37.453827, -122.182187);
                                          //    // Configure the additional search parameters
                                          //    placePickerController.radiusInMeters = 1000;
                                          placePickerController.resultsLimit = 50;
                                          placePickerController.searchText = place;
                                          
                                          // TODO: Set up the place picker delegate to handle
                                          // picker callbacks, ex: Done/Cancel button
                                          
                                          // Load the place data
                                          [placePickerController loadData];
                                          // Show the place picker modally
                                          
                                          [self.navigationController pushViewController:placePickerController animated:YES];
                                      
                                      }
                                  }];
    

}


- (UIImage*)firstFrameOfVideo
{
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:_mediaPath]];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMake(1, 1);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:UIImageOrientationUp];
    
    return thumbnail;

}

#pragma mark  Facebook Sharing

/*
-(void)shareOnFacebook
{
    
    if ([[FBSession activeSession] isOpen]) {
        // session opened
        [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Posting..."];

        if (!_isMediaTypeImage) {
            [self uploadVideo];
        }
        else
        {
            [self shareImage];
        }
        
    }
    else {
        
        [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES completionHandler:^(FBSession *session,
                                                                                                                                                                       FBSessionState status,
                                                                                                                                                                       NSError *error){
            
            if (!error) {
                [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Posting..."];
                if (!_isMediaTypeImage) {
                    [self uploadVideo];
                }
                else
                {
                    [self shareImage];
                }

            }
            else {
                [[[UIAlertView alloc] initWithTitle:@"Facebook" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];

            }
            
            
        }];
        
        
    }
}
*/


/**
 *  This method will convert the media link into short link
 *
 *  @param feed - media and its details
 *
 *  @return shortlink
 */
- (NSString*)getWebLinkForFeed:(PFObject*)feed
{
    if (_shortUrl == nil || _shortUrl.length == 0) {
        NSString *link = [NSString stringWithFormat:@"http://postmenu.cloudapp.net/picogram/fbgallerys1.php?id=%@&uid=%@",feed.objectId, [PFUser currentUser].objectId];
        NSString *apiEndpoint = [NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@",link];
        _shortUrl = [NSString stringWithContentsOfURL:[NSURL URLWithString:apiEndpoint]
                                             encoding:NSASCIIStringEncoding
                                                error:nil];
    }
    return _shortUrl;

}


/**
 *  This method will post the media on facebook with its descriptions
 *
 *  @param params mediatype,medialink,caption
 */
- (void) makeFBPostWithParams:(NSDictionary*)params
{
    if ([[PFFacebookUtils session] isOpen]) {
        
        [FBRequestConnection startWithGraphPath:@"/me/feed/" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            
            
            // [[ProgressIndicator sharedInstance] hideProgressIndicator];
            
            NSString *message = @"Posted Successfully";
            if (result!= nil) {
                [Flurry logEvent:@"PhotoSharedOnFacebook"];
                
            }
           
            [self donePosting];
            
            
        }];
    }
    else {
        
        [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES completionHandler:^(FBSession *session,
                                                                                                                                                                       FBSessionState status,
                                                                                                                                                                       NSError *error){
            
            if (!error) {
                [FBRequestConnection startWithGraphPath:@"/me/feed/" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                    
                    
                    //  [[ProgressIndicator sharedInstance] hideProgressIndicator];
                    
                    NSString *message = @"Posted Successfully";
                    if (result!= nil) {
                        [Flurry logEvent:@"PhotoSharedOnFacebook"];
                        
                    }
                   
                    [self donePosting];
                    
                }];
            }
            else {
                [[[UIAlertView alloc] initWithTitle:@"Facebook" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                
            }
            
            
        }];
        
        
    }
}

- (void) postOnFacebookWithSecondaryLink:(NSString*)secondaryLink
{
    NSString *mediaLink = [self getWebLinkForFeed:feedObject];
    
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   secondaryLink, @"picture",
                                   @"Created from Captionier", @"caption",
                                   _captionTextView.text, @"description",
                                   mediaLink, @"link",
                                   nil];
    
    [self makeFBPostWithParams:params];
}

/**
 *  This method will add media descrition to post on Facebook
 *
 *  @param feed media with its details
 */
- (void) postOnFacebook:(PFObject*)feed
{
    feedObject = feed;
    
    NSString *mediaLink = [self getWebLinkForFeed:feed];
    
    NSString *caption = @"Checkout this cool app";
    if (![_captionTextView.text isEqualToString:@" Add Description"]) {
        caption = _captionTextView.text;
    }
    
    NSString *picturelink = [feed objectForKey:kPAPFeedMediaLinkKey];
    if (!_isMediaTypeImage) {
        picturelink = [feed objectForKey:kPAPFeedMediaThumbnailLikKey];
    }
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   picturelink, @"picture",
                                   @"Created from Captionier", @"caption",
                                   caption, @"description",
                                   mediaLink, @"link",
                                   nil];
    
    [self makeFBPostWithParams:params];
}


/**
 *  This method is call when media post successfully
 */
- (void) donePosting
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [self.navigationController popToRootViewControllerAnimated:YES];
}





-(void)shareImage
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   _captionTextView.text,@"message",
                                   _postImageView.image, @"picture",
                                   nil];
    
    
    [FBRequestConnection startWithGraphPath:@"me/photos" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        
        [[ProgressIndicator sharedInstance] hideProgressIndicator];

        NSString *message = @"Posted Successfully";
        if (result!= nil) {
            [Flurry logEvent:@"PhotoSharedOnFacebook"];

        }
        if (error) {
            message = [error localizedDescription];
        }
        [[[UIAlertView alloc] initWithTitle:@"Facebook" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        
    }];
}

-(void)uploadVideo
{
    
    
    NSLog(@"the videofilePath is %@",_mediaPath);

        
        NSData * data1=[NSData dataWithContentsOfFile:_mediaPath];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       data1, @"video.mov",
                                       @"video/quicktime", @"contentType",
                                       @"Created from Captionier", @"caption",
                                       _captionTextView.text, @"description",
                                       nil];
        
        [FBRequestConnection startWithGraphPath:@"me/videos" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            
            NSString *message = @"Posted Successfully";
            if (result!= nil) {
                [Flurry logEvent:@"VideoPostedOnFacebook"];
                
            }
            if (error) {
                message = [error localizedDescription];
            }
            [[[UIAlertView alloc] initWithTitle:@"Facebook" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            
        }];
        
//    } failureBlock:^(NSError *err){
//    }];
    
    
    
}

#pragma mark  Twitter Sharing

/**
 *  This method will post media link on configured twitter account
 *
 *  @param feed mediaLink
 */
-(void)postOnTwitter:(PFObject*)feed
{
    NSString *mediaLink = [self getWebLinkForFeed:feed];
   
    
    
    // Create an account store object.
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    
    // Create an account type that ensures Twitter accounts are retrieved.
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    // Request access from the user to use their Twitter accounts.
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        if(granted) {
            // Get the list of Twitter accounts.
            NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
            
            if ([accountsArray count] > 0) {
                // Grab the initial Twitter account to tweet from.
                ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                SLRequest *postRequest = nil;
                
                
                /*************A***********/
                //ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                NSLog(@"Twitter Login User:%@",twitterAccount.username);
                NSLog(@"Twitter AccountType:%@",twitterAccount.accountType);
                /****************************/
                
                // Post Text
                NSString *caption = @"";
                if (_captionTextView.text) {
                    caption = _captionTextView.text;
                }
                
                NSString *posttext = [NSString stringWithFormat:@"@Captionier %@",mediaLink];
                NSDictionary *message = @{@"status": mediaLink, @"wrap_links": @"true"};
                
                // URL
                NSURL *requestURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
                
                // Request
                postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:requestURL parameters:message];
                
                // Set Account
                postRequest.account = twitterAccount;
                
                // Post
                [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    NSLog(@"Twitter HTTP response: %li", (long)[urlResponse statusCode]);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (shareOnFB) {
                            [self postOnFacebook:feed];
                        }
                        else {
                            [self donePosting];
                        }
                    });
                    
                }];
                
            }
            else {
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (shareOnFB) {
                        [self postOnFacebook:feed];
                    }
                    else {
                        //                        UIAlertView *tweet = [[UIAlertView alloc]initWithTitle:@"No Twitter Accounts" message:@"There are no Twitter accounts configured" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        //                        [tweet show];
                        [self donePosting];
                    }
                });
            }
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (shareOnFB) {
                    [self postOnFacebook:feed];
                }
                else {
                    
                    [self donePosting];
                }
            });
        }
    }];
    
}



#pragma mark - TextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    if(textView.text.length >1 || text.length != 0)
    {
        //NSLog(@"string %@ location %d",text,range.length);
        if ([text isEqualToString:@"@"]) {
            [self getUserWhomIamFollowing];
        }
//        else if ([text isEqualToString:@"#"]) {
//            [self getHashtags:[textView.text stringByReplacingCharactersInRange:range withString:text]];
//        }
        
        if ([text isEqualToString:@" "] || (range.length == 1 && range.location == 0)) {
            [self removeFollowingListView];
        }
    }
    else
    {
        
        return YES;
    }
    if([text isEqualToString:@"\n"])
    {
        [self removeFollowingListView];
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    //_shareScrollView.contentSize = CGSizeMake(0,-40);
    
    //_shareScrollView.contentOffset = CGPointMake(0, 120);
    
    if ([_captionTextView.text isEqualToString:@" Add Description"]) {
        _captionTextView.text = @"";
        _captionTextView.textColor = [UIColor blackColor]; //optional
    }
   // [_captionTextView becomeFirstResponder];
}
    

- (void)textViewDidEndEditing:(UITextView *)textView
{
 // _shareScrollView.contentOffset = CGPointMake(0, 0);
    if ([_captionTextView.text isEqualToString:@""]) {
        _captionTextView.text = @" Add Description";
        _captionTextView.textColor = [UIColor lightGrayColor]; //optional
    }
    //[_captionTextView resignFirstResponder];
}

#pragma mark - Tagging User


/**
 *  This method will get list of all users whom current user is following from parse
 */
-(void)getUserWhomIamFollowing{
    
    if (_followers.count > 0) {
        [self displayFollowingList];
    }
    else {
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view withMessage:@""];
        PFQuery  *query = [PFQuery queryWithClassName:kPAPActivityClassKey];
        [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
        [query whereKey:kPAPActivityFromUserKey equalTo:[PFUser currentUser]];
        [query includeKey:kPAPActivityToUserKey];
        [query orderByAscending:kPAPUserDisplayNameKey];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
            [pi hideProgressIndicator];

            if (!error) {
                _followers = [[NSMutableArray alloc] init];
                
                for (PFObject *obj in array) {
                    if ([obj.parseClassName isEqualToString:kPAPActivityClassKey]) {
                        [_followers addObject:[obj objectForKey:kPAPActivityToUserKey]];
                    }
                }

                [self displayFollowingList];
            }
            
        }];
    }
    
    
}

/**
 *  This method will present the list of users whom current user is following
 */
-(void)displayFollowingList {
    
    if (!_followinglist) {
        _followinglist = [[FollowingList alloc] initWithFrame:CGRectMake(0,0, 320, 240)];
        _followinglist.tag = 100;
        __weak typeof(self) weakSelf = self;
        _followinglist.callback = ^(PFUser *taggedUser){
            [weakSelf addDisplayName:taggedUser];
        };
        _followinglist.followingList = _followers;
        [self.view addSubview:_followinglist];
        [_followinglist refresh];
    }
    else {
        [self.view addSubview:_followinglist];
        [_followinglist refresh];
    }
    
    
}
/**
 *  This method will remove the presented following listview
 */
-(void)removeFollowingListView{
    //FollowingList *fl =  (FollowingList*)[self.view viewWithTag:100];
    if (_followinglist) {
        [_followinglist removeFromSuperview];
    }
    
    // followinglist = Nil;
}

/**
 *  This method will combine tagged string and caption string as caption string
 *
 *  @param taggedUser
 */
-(void)addDisplayName:(PFUser*)taggedUser{
    
    if (!_taggedUsers) {
        _taggedUsers = [[NSMutableArray alloc] init];
    }
    [_taggedUsers addObject:taggedUser];
    
    NSString *combinedtext = [NSString stringWithFormat:@"%@%@", _captionTextView.text, taggedUser.username];
    _captionTextView.text = combinedtext;
    [self removeFollowingListView];
}



#pragma mark - Email

/**
 *  This method will open email composer to share media by email
 *
 *  @param sender Emailbutton
 */
-(IBAction)sendEmail:(id)sender
{

        if ([MFMailComposeViewController canSendMail])
        {
            
            MFMailComposeViewController *  mailComposecontroller=[[MFMailComposeViewController alloc]init];
            mailComposecontroller.mailComposeDelegate=self;
            
            
            [mailComposecontroller setToRecipients:[NSArray arrayWithObjects:@" ", nil]];
            if (_captionTextView.text.length == 0) {
                [mailComposecontroller setMessageBody:@"Shared via Captionier" isHTML:NO];
            }
            else {
                
                if (![_captionTextView.text isEqualToString:@" Add Description"]) {
                   // caption = _captionTextView.text;
                    [mailComposecontroller setMessageBody:_captionTextView.text isHTML:NO];
                }
                else{
                [mailComposecontroller setMessageBody:@"" isHTML:NO];
                }
            }
            
            [mailComposecontroller setSubject:@"A message from Captionier"];
            
            if (_isMediaTypeImage) {
                NSData * dataImage=UIImageJPEGRepresentation(_postImageView.image, 1);
                [mailComposecontroller addAttachmentData:dataImage mimeType:@"image/jpg" fileName:@"photo.jpg"];
            }
            else
            {
                NSData * data=[NSData dataWithContentsOfFile:_mediaPath];
                [mailComposecontroller addAttachmentData:data mimeType:@"video/quicktime" fileName:@"video.mov"];
            }
            
            [self presentViewController:mailComposecontroller animated:NO completion:^{
                
            }];
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Your device is not currently connected to an email account."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
        }
 
}


- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //  ////NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            //  ////NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            //NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            // ////NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    // UINavigationController *navigationController = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).navigationcontroller;
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


/**
 *  This method formate date and time
 *
 *  @return date & time as a string
 */
-(NSString*)getCurrentTime
{
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    
    [dateFormatter setDateFormat:@"EEEMMddyyyyHHmmss"];
    
    // Get the date time in NSString
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    
    //  //AlphaLog(@"%@", dateInStringFormated);
    return dateInStringFormated;
    // Release the dateFormatter
    
    //[dateFormatter release];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField isEqual:_locationTextField]) {
        //[self showPlacePickerController:textField.text];
    }
    
    return YES;
}




#pragma mark- Sms

/**
 *  This method will share image through sms
 *
 *  @param sender smsBtn clicked
 */
- (IBAction)shareOnSms:(id)sender
{
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
//    NSArray *recipents = @[@""];
//    NSString *message = [NSString stringWithFormat:@"Just sent the %@ file to your email. Please check!", @""];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    
    NSData *imgData = [NSData dataWithContentsOfFile:@"blablabla"];
    BOOL didAttachImage = [messageController addAttachmentData:imgData typeIdentifier:@"public.data" filename:@"photo.jpg"];
    
    if (didAttachImage)
    {
        // Present message view controller on screen
        [self presentViewController:messageController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                               message:@"Failed to attach image"
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
}


/**
 *  This method will share media on Pinterest
 *
 *  @param sender PinterestBtn Pressed
 */
- (IBAction)shareOnPinterest:(id)sender {
    
    Pinterest *pinterest = [[Pinterest alloc] initWithClientId:@"1442572"];
    
    //NSString *caption = ;
    
    NSURL *url1 = [NSURL URLWithString:@"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8caf4x7GCjRlAKJt3cdvZCLSYeUEMYr8seiHvE_tVMm11QwZ_Wa79Izg"];
    NSURL *url2 = [NSURL URLWithString:@"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8caf4x7GCjRlAKJt3cdvZCLSYeUEMYr8seiHvE_tVMm11QwZ_Wa79Izg"];
    
    [pinterest createPinWithImageURL:url1
                            sourceURL:url2
                          description:@"Pinning from Pin It Demo"];
    
}





- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Amazon Delegate
/**
 *  This method is invoked when uploading is done successfully
 *
 *  @param urlString mediaLink url
 */
- (void)mediaUploadSuccessful:(NSString*)urlString
{
    _uploadedMediaLink = urlString;
    NSLog(@"media uploaded : %@", urlString);
    
    if ([_socialMedia isEqualToString:@"Twitter"]) {
        [self postOnTwitter:urlString];
    }
    else if ([_socialMedia isEqualToString:@"Parse"]) {
        [self postToParse:urlString];
    }
    
}


/**
 *  This method is invoked when uploading is unsuccessful
 *
 *  @param error failour message
 */
- (void)mediaUploadFailure:(NSError*)error
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
    NSLog(@"media upload fialed : %@", error);
    [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
}

#pragma mark - Upload image With Amezon cdn
/**
 *  This method will upload image on amazon
 *
 *  @param image actual image path
 */
-(void)uploadFullImageOnAmezon:(UIImage*)image{
    
    
    if (_uploadedMediaLink != nil) {
         [self uploadThumbnailImageOnAmezon:image];
    }
    else{
        
        //configure image
        
        PFUser *user =   [PFUser currentUser];
        
        //start progress view
        UploadProgress *up = [UploadProgress sharedInstance];
        [up setMessage:@"Uploading.."];
        
        
        NSString *name = [NSString stringWithFormat:@"%@%@.png",@"image",[self getCurrentTime]];
        
        
        
        
        
        NSString *fullImageName = [NSString stringWithFormat:@"%@/Photos/%@",user.username,name];
        
        
        
        [AmazonTransfer upload:_mediaPath
                       fileKey:fullImageName
                      toBucket:Bucket
                      mimeType:@"image/jpeg"
                 progressBlock:^(NSInteger progressSize, NSInteger expectedSize){
                     CGFloat progess = ((float)progressSize/(float)expectedSize);
                     
                     [up updateProgress:progess];
                 }
               completionBlock:^(BOOL success, id result, NSError *error) {
                   
                   if (success) {
                       // send message
                       _uploadedMediaLink = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/%@", Bucket,fullImageName];
                       
                       //_uploadedMediaLink = result;
                       [self uploadThumbnailImageOnAmezon:image];
                       
                   }
                   else {
                       
                       [[UploadProgress sharedInstance] hide];
                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                       [Helper showAlertWithTitle:@"Messgae" Message:@"Failed to upload"];
                   }

                   
               }];
        

    }
    
    
    
    
    
}



/**
 *  This method will upload video on amazon
 */
-(void)uplaodVideoOnAmezon{
    
    
    if (_uploadedMediaLink != nil) {
        
         [self uploadThumbnailImageOnAmezon:_postImageView.image];
    }
    else{
        //start progress view
        UploadProgress *up = [UploadProgress sharedInstance];
        [up setMessage:@"Uploading.."];
        
        NSString *name = [NSString stringWithFormat:@"%@%@.mp4",@"video",[self getCurrentTime]];
        
        
        
        PFUser *user =   [PFUser currentUser];
        
        NSString *fullImageName = [NSString stringWithFormat:@"%@/Videos/%@",user.username,name];
        
        
        
        //NSData *fullImageData = [NSData dataWithContentsOfFile:_mediaPath];
        
        [AmazonTransfer upload:_mediaPath
                       fileKey:fullImageName
                      toBucket:Bucket
                      mimeType:@"video/mp4"
                 progressBlock:^(NSInteger progressSize, NSInteger expectedSize){
                     CGFloat progess = ((float)progressSize/(float)expectedSize);
                     
                    [up updateProgress:progess];
                 }
               completionBlock:^(BOOL success, id result, NSError *error) {
                   if (success) {
                       // send message
                       
                        _uploadedMediaLink = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/%@", Bucket,fullImageName];
                      // _uploadedMediaLink = result;
                       
                       //UIImage *image = [self getFrameImageFrom:[NSURL URLWithString:_mediaPath]];
                       
                       [self uploadThumbnailImageOnAmezon:_postImageView.image];
                       
                   }
                   else {
                       
                       [[UploadProgress sharedInstance] hide];
                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                       [Helper showAlertWithTitle:@"Message" Message:@"Failed to upload"];
                   }

               }];

        
    }
    
    
}




-(void)uploadThumbnailImageOnAmezon:(UIImage*)image{
    
    
    
    if (_uploadedThumbnailMediaLink != nil) {
        
       [self postToParse:_uploadedMediaLink];
    }
    else{
        //configure image
        UIImage *thumbnailImage = [self getThumbnailImage:image];
        
        //NSData *thumbnailData = UIImagePNGRepresentation(thumbnailImage);
        
        
        NSString *name = [NSString stringWithFormat:@"%@%@.png",@"image",[self getCurrentTime]];
        
        
        
        
        PFUser *user =   [PFUser currentUser];
        NSString *thumbnail = [NSString stringWithFormat:@"%@/Thumbnail/%@",user.username,name];
        
        
        
        // NSData *data = UIImagePNGRepresentation(image);
        
        
        UploadProgress *up = [UploadProgress sharedInstance];
        [up setMessage:@"Finishing Up.."];
        up.progressBar.progress = 0;
        
        
        NSString *finalMediaPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"thumbnail.jpg"];
        NSData *imgData = UIImageJPEGRepresentation(thumbnailImage, 1);
        [imgData writeToFile:finalMediaPath atomically:YES];

        
        
        
        [AmazonTransfer upload:finalMediaPath
                       fileKey:thumbnail
                      toBucket:Bucket
                      mimeType:@"image/jpeg"
                 progressBlock:^(NSInteger progressSize, NSInteger expectedSize){
                     CGFloat progess = ((float)progressSize/(float)expectedSize);
                     
                     [up updateProgress:progess];
                 }
               completionBlock:^(BOOL success, id result, NSError *error) {
                   
                   if (success) {
                       // send message
                       _uploadedThumbnailMediaLink = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/%@", Bucket,thumbnail];
                       //_uploadedThumbnailMediaLink = result;
                       
                       [self postToParse:_uploadedMediaLink];
                       
                       
                   }
                   else {
                       
                       [[UploadProgress sharedInstance] hide];
                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                       [Helper showAlertWithTitle:@"Messgae" Message:@"Failed to upload"];
                   }

               }];
        
        
        
    }
    
   
    
    
    
    
    
}
-(UIImage*)getFrameImageFrom:(NSURL*)path {
    
    AVAsset *asset = [AVAsset assetWithURL:path];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMake(1, 1);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:UIImageOrientationUp];
    
    return thumbnail;
    
}
-(UIImage*)getThumbnailImage:(UIImage*)image{
    
    UIImage *thumbnailImage = [image thumbnailImage:160 transparentBorder:0 cornerRadius:0 interpolationQuality:kCGInterpolationLow];
    return thumbnailImage;
}
-(UIImage*)getVideoThumbnail:(UIImage*)image{
    
    UIImage *thumbnailImage = [image thumbnailImage:50 transparentBorder:0 cornerRadius:0 interpolationQuality:kCGInterpolationLow];
    return thumbnailImage;
}
- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

- (void)keyboardWasShown:(NSNotification *)notification {
    
    
    
    if ([self.captionTextView isFirstResponder]) {
        NSDictionary* info = [notification userInfo];
        
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        CGPoint textfieldOrigin = self.captionTextView.frame.origin;
        
        CGFloat textfieldHeight = self.captionTextView.frame.size.height;
        
        CGRect visibleRect = self.view.frame;
        
        visibleRect.size.height -= keyboardSize.height;
        
        // if (!CGRectContainsPoint(visibleRect, textfieldOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, textfieldOrigin.y - visibleRect.size.height + textfieldHeight);
        
        [_shareScrollView setContentOffset:scrollPoint animated:YES];
        
    }
    
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    [_shareScrollView setContentOffset:CGPointZero animated:YES];
    
}






- (IBAction)smsShare:(id)sender {
}

#pragma mark - CLLocationManager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", [locations lastObject]);
    [self.locationManager stopUpdatingLocation];
    
    self.currentLocation = [locations lastObject];
    
    
    
}

@end
