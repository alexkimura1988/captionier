//
//  MediaPreviewViewController.m
//  VIND
//
//  Created by Vinay Raja on 27/08/14.
//
//

#import "MediaPreviewViewController.h"
#import "SCVideoPlayerView.h"
#import "SCFilterSwitcherView.h"

@interface MediaPreviewViewController () <SCPlayerDelegate>
{
    SCPlayer *_player;

}

@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;
@property (weak, nonatomic) IBOutlet SCFilterSwitcherView *filterSwitcherView;

@end

@implementation MediaPreviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIButton *menuBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn addTarget:self.navigationController action:@selector(toggleMenu) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setFrame:CGRectMake(0.0f,0.0f, 44,44)];
    [Helper setButton:menuBtn Text:@"" WithFont:@"HelveticaNeue" FSize:17 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [menuBtn setImage:[UIImage imageNamed:@"menu_icon_off.png"] forState:UIControlStateNormal];
    [menuBtn setImage:[UIImage imageNamed:@"menu_icon_on.png"] forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingBarButton1 = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    
    self.navigationItem.leftBarButtonItem = containingBarButton1;

}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    [self.navigationController setNavigationBarHidden:YES];
    
    if (_isMediaTypeImage) {
        _previewImageView.hidden = NO;
        _filterSwitcherView.hidden = YES;

        
        _previewImageView.image = [UIImage imageWithContentsOfFile:_mediaPath];
        [_previewImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    }
    else {
        _filterSwitcherView.hidden = NO;
        _previewImageView.hidden = YES;
        
        self.filterSwitcherView.disabled = NO;;
        _player = [SCPlayer player];
        self.filterSwitcherView.player = _player;
        
        _player.shouldLoop = YES;
        
        [_player play];
        
        [_player setItemByStringPath:_mediaPath];

        
    }
}



- (IBAction)goBack:(id)sender
{
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sgoToShareView:(id)sender
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
