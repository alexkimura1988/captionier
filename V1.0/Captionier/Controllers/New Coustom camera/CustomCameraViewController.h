//
//  CustomCameraViewController.h
//  VIND
//
//  Created by Vinay Raja on 23/08/14.
//
//

#import <UIKit/UIKit.h>

@interface CustomCameraViewController : UIViewController
@property(nonatomic,assign)BOOL isEmergency;
@property(nonatomic,assign)BOOL isPrivateGrp;
@property(nonatomic,strong)NSString *groupName;
@property (nonatomic, strong) PFObject *group;

@property(nonatomic,assign)NSString *fromController;
@end
