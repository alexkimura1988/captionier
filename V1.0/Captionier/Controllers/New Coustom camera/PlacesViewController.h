//
//  PlacesViewController.h
//  Picogram
//
//  Created by 3Embed on 19/01/15.
//
//

#import <UIKit/UIKit.h>

typedef void(^PlacesCallback)(NSString *name , CLLocation *location);
@interface PlacesViewController : UIViewController
@property(nonatomic,strong)CLLocation *currentLocation;
@property(nonatomic,strong) PlacesCallback callback;
@end
