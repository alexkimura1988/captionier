//
//  PlacesViewController.m
//  Picogram
//
//  Created by 3Embed on 19/01/15.
//
//

#import "PlacesViewController.h"

@interface PlacesViewController ()<UISearchBarDelegate>
@property(nonatomic,weak)IBOutlet UITableView *tbleView;
@property(nonatomic,weak)IBOutlet UISearchBar *searchbar;
@property(nonatomic,strong)NSArray *searchResults;
@property(nonatomic,strong)NSArray *firstResut;
@end

@implementation PlacesViewController



#define kGOOGLE_API_KEY @"AIzaSyCizq7QvPED3UkztXhCs1BTqyyFoRWRYWI"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Choose Location";
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelButtonClicked:)];
    [self getNearbyPlaces];
    
    
}
-(void)cancelButtonClicked:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)getNearbyPlaces{
   // https://maps.googleapis.com/maps/api/place/search/json?location=-33.8670522,151.1957362&radius=500&sensor=true&key=AIzaSyA7WH1h7WtWLqQuK6o0FGQDjJby6Aw_Pow
    
     NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?location=%f,%f&radius=500&sensor=true&key=AIzaSyA7WH1h7WtWLqQuK6o0FGQDjJby6Aw_Pow",_currentLocation.coordinate.latitude,_currentLocation.coordinate.longitude];
    
    //Formulate the string as URL object.
    NSURL *googleRequestURL=[NSURL URLWithString:strUrl];
    
    // Retrieve the results of the URL.
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL: googleRequestURL];
        [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
    });
}

-(void) queryGooglePlaces:(NSString *) _searchString
{
    
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/textsearch/json?query=%@&sensor=true&key=AIzaSyA7WH1h7WtWLqQuK6o0FGQDjJby6Aw_Pow&location=%f,%f&radius=500",_searchString,_currentLocation.coordinate.latitude,_currentLocation.coordinate.longitude];
    
    //Formulate the string as URL object.
    NSURL *googleRequestURL=[NSURL URLWithString:strUrl];
    
    // Retrieve the results of the URL.
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL: googleRequestURL];
        [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
    });
}

- (void)fetchedData:(NSData *)responseData {
    
    if (responseData == nil) {
        return;
    }
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          
                          options:kNilOptions
                          error:&error];
    
    //The results from Google will be an array obtained from the NSDictionary object with the key "results".
    
    
    _searchResults = [json objectForKey:@"results"];
    if (_firstResut.count == 0) {
        _firstResut = _searchResults;
    }
    
    //Write out the data to the console.
    [self.tbleView reloadData];
    
    //Plot the data in the places array onto the map with the plotPostions method.
   // [self plotPositions:places];
    
    
}
#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.searchResults.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
       
        
    }
    
    NSDictionary *dict = _searchResults[indexPath.row];
    cell.textLabel.text = dict[@"name"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
    [tableViewCell setSelected:NO animated:YES];
    
     NSDictionary *dict = _searchResults[indexPath.row];
    float lat = [dict[@"geometry"][@"location"][@"lat"] floatValue];
    float lng = [dict[@"geometry"][@"location"][@"lng"] floatValue];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
    
    [self dismissViewControllerAnimated:YES completion:^(){
        
        if (self.callback) {
            self.callback(dict[@"name"],location);
        }
        
    }];
    
}


#pragma UIsearchbardelegate
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
     [searchBar setShowsCancelButton:NO animated:YES];
    _searchResults = _firstResut;
    [self.tbleView reloadData];
    
    [searchBar resignFirstResponder];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:NO animated:YES];
    [self queryGooglePlaces:searchBar.text];
    [searchBar resignFirstResponder];
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    [self queryGooglePlaces:searchBar.text];
    return YES;
}

@end
