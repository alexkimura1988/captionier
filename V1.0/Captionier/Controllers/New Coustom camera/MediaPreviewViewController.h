//
//  MediaPreviewViewController.h
//  VIND
//
//  Created by Vinay Raja on 27/08/14.
//
//

#import <UIKit/UIKit.h>

@interface MediaPreviewViewController : UIViewController

@property (nonatomic, assign) BOOL isMediaTypeImage;
@property (nonatomic, strong) NSString *mediaPath;

@end
