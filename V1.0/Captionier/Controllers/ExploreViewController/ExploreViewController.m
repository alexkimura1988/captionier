//
//  ExploreViewController.m
//  VIND
//
//  Created by Surender Rathore on 23/02/14.
//
//

#import "ExploreViewController.h"
#import "NavigationViewController.h"
#import "CustomCameraViewController.h"

@interface ExploreViewController ()

@end

@implementation ExploreViewController
@synthesize category;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self customizeNavigationBackButton];
}

- (BOOL) isRootViewController
{
    BOOL isRoot = NO;
    if ([[self.navigationController viewControllers] count] == 1) {
        isRoot = [[[self.navigationController viewControllers] objectAtIndex:0] isEqual:self];
    }
    
    return isRoot;
    
}


/**
 *  Configures the navigation bar left button as custome button
 *
 */


-(void)customizeNavigationBackButton {
    
    if ([self isRootViewController]) {
        UIButton *menuBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
        [menuBtn addTarget:self.navigationController action:@selector(toggleMenu) forControlEvents:UIControlEventTouchUpInside];
        [menuBtn setFrame:CGRectMake(0.0f,0.0f, 44,44)];
        [Helper setButton:menuBtn Text:@"" WithFont:@"HelveticaNeue" FSize:17 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
        [menuBtn setImage:[UIImage imageNamed:@"menu_icon_off.png"] forState:UIControlStateNormal];
        [menuBtn setImage:[UIImage imageNamed:@"menu_icon_on.png"] forState:UIControlStateHighlighted];
        
        // Create a container bar button
        UIBarButtonItem *containingBarButton1 = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
        
        self.navigationItem.leftBarButtonItem = containingBarButton1;
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake( 0.0f, 0.0f,35, 35);
        [backButton addTarget:self action:@selector(cameraButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [backButton setBackgroundImage:[UIImage imageNamed:@"camera_nav_bar_icon_off"] forState:UIControlStateNormal];
        //[backButton setBackgroundImage:[UIImage imageNamed:@"camera_btn_on"] forState:UIControlStateHighlighted];
        //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
    }
    else {
        UIImage *backbuttonImage = [UIImage imageNamed:@"back_btn"];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake( 0.0f, 0.0f,backbuttonImage.size.width, backbuttonImage.size.height);
        [backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
        // [backButton setBackgroundImage:[UIImage imageNamed:<#ImageNameOn#>] forState:UIControlStateHighlighted];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        
    }
    
    [self.navigationItem setTitle:self.category];
    
}

/**
 *  This method will call once camera button is pressed to open camera
 *
 *  @param sender btnPress
 */

-(void)cameraButtonAction:(id)sender {
    NavigationViewController *navigationController = (NavigationViewController *)self.navigationController;
    navigationController.menu.isOpen = YES;
    [navigationController toggleMenu];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if (IS_IPHONE_5) {
        CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController" bundle:nil];
        [self.navigationController pushViewController:obj animated:YES];
    }
    else {
        CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController_ip4" bundle:nil];
        
        [self.navigationController pushViewController:obj animated:YES];
    }
}


/**
 *  go back to previous controller
 *
 *  @param sender backBtn pressed
 */
-(void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
  
    
}


/**
 *  Parse query to get all activitys which belongs to current user
 *
 *  @return query
 */

- (PFQuery *)queryForTable {
    
    if (![PFUser currentUser]) {
        PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
        [query setLimit:0];
        return query;
    }
    
    
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    query.cachePolicy = kPFCachePolicyNetworkOnly;
    if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    [query whereKey:@"channel" equalTo:self.category];
    [query whereKey:kPAPFeedUserKey notEqualTo:[PFUser currentUser]];
    [query orderByDescending:@"createdAt"];
    [query includeKey:kPAPFeedUserKey];
    return query;
}

@end
