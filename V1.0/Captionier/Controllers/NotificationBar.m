//
//  NotificationBar.m
//  FizFeliz
//
//  Created by Surender Rathore on 19/12/13.
//
//

#import "NotificationBar.h"
#import "LikersViewController.h"
#import "CommentsViewController.h"
#import "AppDelegate.h"

@implementation NotificationBar
@synthesize buttonMessage;
@synthesize dictionary;
#define _height  50

static NotificationBar *nb;

+ (id)sharedInstance {
	if (!nb) {
		nb  = [[self alloc] init];
	}
	
	return nb;
}

-(id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        
        
        [self deafultInit];
    }
    return self;
}
-(void)setMessage:(NSString*)message withUserInfo:(NSDictionary*)userInfo {

    timer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(hide) userInfo:nil repeats:NO];
    
    message = userInfo[@"aps"][@"alert"];
    [buttonMessage setTitle:message forState:UIControlStateNormal];
    
    dictionary = userInfo;
}

-(void)deafultInit
{
    
    
    buttonMessage = [[UIButton alloc] initWithFrame:CGRectMake(0,10, 320, 40)];
    [buttonMessage setTitle:@"" forState:UIControlStateNormal];
    buttonMessage.titleLabel.font = [UIFont fontWithName:Lato size:15];
    [buttonMessage setTitleColor:[UIColor colorWithRed:0.380 green:0.832 blue:0.984 alpha:1.000] forState:UIControlStateNormal];
    [buttonMessage setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [buttonMessage addTarget:self action:@selector(openViewController:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:buttonMessage];
    
//    UIButton *btnCross = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnCross.frame = CGRectMake(320-18, 3, 15, 15);
//    [btnCross setImage:[UIImage imageNamed:@"close_btn.png"] forState:UIControlStateNormal];
//    [btnCross addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
//    [self addSubview:btnCross];
    
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    self.backgroundColor = [UIColor blackColor];
   // CGSize size = [[UIScreen mainScreen] bounds].size;
    self.frame = CGRectMake(0,-50, 320, _height);
    [window addSubview:self];
    
    
    CGRect frame = self.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:.4f
                     animations:^{
                         
                         self.frame = frame;
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
    
    
}
-(void)openViewController:(id)sender {
    
    if (timer) {
        [timer invalidate];
    }
    
    [self hide];
    
    NSString *photoObjectId = [dictionary objectForKey:kPAPPushPayloadPhotoObjectIdKey];
    PFObject *object = [PFObject objectWithoutDataWithClassName:kPAPPhotoClassKey objectId:photoObjectId];
    
    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UINavigationController *homeNavigationController = [[appdelegate.tabBarController viewControllers] objectAtIndex:PAPHomeTabBarItemIndex];
    [appdelegate.tabBarController setSelectedViewController:homeNavigationController];
    //like
    if ([[dictionary objectForKey:kPAPPushPayloadActivityTypeKey] isEqualToString:kPAPPushPayloadActivityLikeKey]) {
        LikersViewController *likeVC = [[LikersViewController alloc] initWithNibName:@"LikersViewController" bundle:Nil];
        likeVC.photo = object;
        [homeNavigationController pushViewController:likeVC animated:YES];

    }
    else {
        CommentsViewController *commentVC = [[CommentsViewController alloc] initWithNibName:@"CommentsViewController" bundle:nil];
        commentVC.photo = object;
        [homeNavigationController pushViewController:commentVC animated:YES];
        
    }
}
-(void)hide
{
    
    //  CGSize size = [[UIScreen mainScreen] bounds].size;
    CGRect frame = self.frame;
    frame.origin.y = -50;
    
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.frame = frame;
                         
                         
                     }
                     completion:^(BOOL finished){
                         
                         [self removeFromSuperview];
                         nb = nil;
                         
                         
                     }];
    
    
}
@end
