//
//  PAPLocationViewController.m
//  InstaChurch
//
//  Created by 3Embed on 04/02/15.
//
//

#import "PAPLocationViewController.h"
#import "MapAnnotationView.h"
#import <MapKit/MapKit.h>

@interface PAPLocationViewController ()

@end
#define METERS_PER_MILE 1609.344
@implementation PAPLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.title = [self.object objectForKey:kPAPFeedLocationKey];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    
    MKMapView *map=[[MKMapView alloc]initWithFrame:CGRectMake(0,0,320,150)];
    [headerView addSubview:map];
    
    
    PFGeoPoint *point = [self.object objectForKey:kPAPFeedPhotoTaggedLocation];
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [point latitude];
    zoomLocation.longitude= [point longitude];
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 10*METERS_PER_MILE, 10*METERS_PER_MILE);
    
    // 3
    [map setRegion:viewRegion animated:YES];
    
    //add annotation
    MapAnnotationView *annotation = [[MapAnnotationView alloc] initWithTitle:[self.object objectForKey:kPAPFeedLocationKey] AndCoordinate:zoomLocation];
    
    [map addAnnotation:annotation];
    
    self.tableView.tableHeaderView = headerView;
   


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - PFQueryTableViewController

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
   // self.tableView.tableHeaderView = self.headerView;
    //[self.tableView reloadData];
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
}

- (PFQuery *)queryForTable {
    
    PFQuery *query;
    
    query = [PFQuery queryWithClassName:@"Feed"];
    [query whereKey:kPAPFeedLocationKey containsString:[self.object objectForKey:kPAPFeedLocationKey]];
    [query orderByDescending:@"createdAt"];
    [query includeKey:kPAPFeedUserKey];
    
    return query;
}


@end
