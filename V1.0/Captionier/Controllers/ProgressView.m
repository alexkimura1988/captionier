//
//  ProgressView.m
//  Anypic
//
//  Created by rahul Sharma on 14/08/13.
//
//

#import "ProgressView.h"


@implementation ProgressView
@synthesize lblMessage;
@synthesize activityIndicator;


#define DeafultText @"Please Wait..."

#define _height  50

static ProgressView *pv;

+ (id)sharedInstance {
	if (!pv) {
		pv  = [[self alloc] init];
	}
	
	return pv;
}

-(id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        
      
       [self deafultInit];
    }
    return self;
}


-(void)deafultInit
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(35, _height/2-10, 20, 20)];
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityIndicator startAnimating];
    [self addSubview:activityIndicator];
    
    lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(55, _height/2-25/2, 200, 25)];
    lblMessage.textAlignment = NSTextAlignmentCenter;
    lblMessage.textColor = [UIColor blackColor];
    lblMessage.backgroundColor = [UIColor clearColor];
    lblMessage.font = [UIFont boldSystemFontOfSize:14];
    lblMessage.text = DeafultText;
    [self addSubview:lblMessage];
    
    UIButton *btnCross = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCross.frame = CGRectMake(320-18, 3, 15, 15);
    [btnCross setImage:[UIImage imageNamed:@"close_btn.png"] forState:UIControlStateNormal];
    [btnCross addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnCross];
    
  
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    self.backgroundColor = [UIColor grayColor];
    CGSize size = [[UIScreen mainScreen] bounds].size;
    self.frame = CGRectMake(0, size.height, 320, _height);
    [window addSubview:self];
    
    
    CGRect frame = self.frame;
    frame.origin.y = ( size.height -_height);
    
    [UIView animateWithDuration:.4f
                     animations:^{
                        
                         self.frame = frame;
                     }
                     completion:^(BOOL finished){
                         
                    }];
    
     
    
}
-(void)setMessage:(NSString*)message
{
    lblMessage.text = message;
}
-(void)hide
{
  
   
    
  
  //  CGSize size = [[UIScreen mainScreen] bounds].size;
    CGRect frame = self.frame;
    frame.origin.y = ( frame.origin.y + _height);
    
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.frame = frame;
                          
                          
                     }
                     completion:^(BOOL finished){
                         
                         [self removeFromSuperview];
                         pv = nil;
                         
                         
                     }];
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
