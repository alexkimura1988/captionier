//
//  NotificationBar.h
//  FizFeliz
//
//  Created by Surender Rathore on 19/12/13.
//
//

#import <UIKit/UIKit.h>

@interface NotificationBar : UIView {
    NSTimer *timer;
}
@property(nonatomic,strong)UIButton *buttonMessage;
@property(nonatomic,strong)NSDictionary *dictionary;
-(void)setMessage:(NSString*)message withUserInfo:(NSDictionary*)userInfo;
+ (id)sharedInstance;
@end
