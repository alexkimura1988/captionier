//
//  PrivateGroupListViewController.m
//  Captionier
//
//  Created by Rahul Sharma on 21/10/15.
//
//

#import "PrivateGroupListViewController.h"
#import "PrivateDetailGroupViewController.h"
#import "PrivateDetailGrpViewController.h"
#import "CreatGroupViewController.h"
#import "GroupTableViewCell.h"
#import "MBProgressHUD.h"

@interface PrivateGroupListViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

//@property(nonatomic,strong)NSArray *groupsList;
@property (nonatomic, assign) BOOL needToRefreshComments;
@property (nonatomic, strong) NSDate *lastRefresh;
@property (nonatomic, strong) UIView *blankTimelineView;
@property (nonatomic, strong)UIRefreshControl *refreshControl;
@property (nonatomic, strong) MBProgressHUD *hud;
@property(nonatomic,strong)NSMutableArray *groupsList;
@end

@implementation PrivateGroupListViewController
@synthesize refreshControl;
@synthesize createBtn;
@synthesize hud;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Private Group";
    _groupTblView.separatorColor = WHITE_COLOR;
    _groupTblView.delegate = self;
    _groupTblView.dataSource = self;
    _groupTblView.userInteractionEnabled = YES;
    _groupTblView.scrollEnabled = YES;
    _groupTblView.backgroundColor = [UIColor whiteColor];
    [self createBtnView];
    [self getPrivateGroupLists];
//    CGFloat customRefreshControlHeight = 50.0f;
//    CGFloat customRefreshControlWidth = 320.0f;
//    CGRect customRefreshControlFrame = CGRectMake(0.0f,
//                                                  -customRefreshControlHeight,
//                                                  customRefreshControlWidth,
//                                                  customRefreshControlHeight);
//    refreshControl = [[UIRefreshControl alloc] initWithFrame:customRefreshControlFrame];
//    
//    refreshControl.tintColor = [UIColor colorWithRed:197/255.0f green:111/255.0f blue:69/255.0f alpha:1.0f];
//    self.refreshControl = refreshControl;
 //   [_groupTblView addSubview:refreshControl];
   
    [self.groupTblView reloadData];
    
    
  
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getPrivateGroupLists];
    //[_groupTblView reloadData];
}

//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    _needToRefreshComments = YES;
//    
//    [refreshControl endRefreshing];
//    
//}

-(void)createBtnView
{
    int y = [UIScreen mainScreen].bounds.size.height;
    int width = [UIScreen mainScreen].bounds.size.width;
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0,y-120, width, 60)];
    btn.backgroundColor = [UIColor orangeColor];
    [btn setTitle:@"Create New" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(createBtnClickedAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (_groupsList == nil)
    {
    
        self.groupTblView.separatorStyle = UITableViewCellSeparatorStyleNone;

        return 0;
    }
    else
    {
        self.groupTblView.backgroundView = nil;
        return _groupsList.count;
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 62.5f;
    
}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"%ld",(long)indexPath.row);
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSLog(@"is selected");
   
    PFObject *userObj = _groupsList[indexPath.row];
    PrivateDetailGrpViewController *groupVC = [[PrivateDetailGrpViewController alloc]init];
    groupVC.stringTitle = [userObj objectForKey:@"groupTitle"];
    groupVC.object = userObj;
    [self.navigationController pushViewController:groupVC animated:YES];
    
//    //PFObject *userObj = _groupsList[indexPath.row];
//       
//    PrivateDetailGroupViewController *group = [[PrivateDetailGroupViewController alloc]initWithNibName:@"PrivateDetailGroupViewController" bundle:nil];
//    //group.stringTitle = [userObj objectForKey:@"groupTitle"];
//    //group.object = userObj;
//    
//   [self.navigationController pushViewController:group animated:YES];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"group";
    
    GroupTableViewCell *cell = (GroupTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[GroupTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    }
    
    PFObject *user = _groupsList[indexPath.row];
    cell.object = user;
    if(cell.userInteractionEnabled)
    {NSLog(@"is enabled");
    }else
    {
        NSLog(@"is not enabled");
    }


    //cell.nameButton.titleLabel = user[@"groupTitle"];
    
    
    
    return cell;
}


- (IBAction)createBtnClickedAction
{
    
    CreatGroupViewController *createGrpCV = [[CreatGroupViewController alloc]initWithNibName:@"CreatGroupViewController" bundle:nil];
    createGrpCV.user = self.user;
    [self.navigationController pushViewController:createGrpCV animated:YES];
    
    
}

//-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{ return nil;
//}

-(void)getPrivateGroupLists
{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:@"loading.." On:self.view];
    

    PFQuery *privategrp = [PFQuery queryWithClassName:@"PrivateGroup"];
    [privategrp findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [pi hideProgressIndicator];
     
        if (!error) {
            NSLog(@"objects %@",objects);
            
            NSMutableArray * userArry;
             _groupsList = [[NSMutableArray alloc]init];
            for (PFObject *obj in objects)
            {
                //NSArray *arr = [obj objectForKey:@"groupTitle"];
              userArry= [[NSMutableArray alloc] initWithArray:[obj objectForKey:@"groupUsers"]] ;
                if ([userArry containsObject:[PFUser currentUser].objectId])
                {
                    NSLog(@"Users Group:%@",[obj objectForKey:@"groupUsers"]);
                    
                    
                    //_groupsList = [[NSMutableArray alloc]init];
                    [_groupsList addObject:obj];
                                             //_groupsList = [[NSArray alloc] initWithArray:obj];
                    
                                            // return;
                }
                 //return
                
            }
            
//            _groupsList = [[NSArray alloc] initWithArray:objects];
            [self.groupTblView reloadData];

        }
        else
        {
            
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
            messageLabel.text = @"You have no Private Groups. Create a Group..";
            messageLabel.textColor = [UIColor blackColor];
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
            [messageLabel sizeToFit];
            self.groupTblView.backgroundView = messageLabel;
            self.groupTblView.separatorStyle = UITableViewCellSeparatorStyleNone;
        }
        
    }];
    
}

@end
