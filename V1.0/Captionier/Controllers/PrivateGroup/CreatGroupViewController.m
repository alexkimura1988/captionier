//
//  CreatGroupViewController.m
//  Captionier
//
//  Created by Rahul Sharma on 21/10/15.
//
//

#import "CreatGroupViewController.h"
# import "creategroupTableViewCell.h"
#import "UIImage+ResizeAdditions.h"
#import <Pinterest/Pinterest.h>
#import "PlacesViewController.h"
#import "AmazonTransfer.h"
#import "UploadProgress.h"
#define Bucket @"captionierapp"

@interface CreatGroupViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,creategroupTableViewCellDelegate,UITextFieldDelegate>
@property (nonatomic, strong) NSArray *followerList;
@property (nonatomic, strong) NSMutableArray *addedUsersList;
@property(nonatomic,assign)BOOL isImageEdited;
@property(nonatomic, assign)BOOL isSelected;
@property (strong, nonatomic) NSString *uploadedThumbnailMediaLink;
@property (strong, nonatomic) NSString *uploadedMediaLink;

@end

@implementation CreatGroupViewController
@synthesize followerList;
@synthesize isSelected;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Create Group";
    isSelected = NO;
    //[_groupImgView setHidden:YES];
    UIImage *defaultImage = [UIImage imageNamed:@"default_image_frame.png"];
    self.groupImgView.image = defaultImage;
    self.groupImgView.layer.cornerRadius = _groupImgView.frame.size.width/2;
    self.groupImgView.clipsToBounds = YES;
    [self.groupImgView setClipsToBounds:YES];
    _addedUsersList = [[NSMutableArray alloc] init];
    _groupTitle.delegate = self;
    [_topView setHidden:YES];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismiss)];
    [self.view addGestureRecognizer:tap];
    
    
    [self followerListQuery];
    _followerlistView.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
}

-(void)dismiss
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (followerList == nil)
    {
        self.followerlistView.separatorStyle = UITableViewCellSeparatorStyleNone;
       
        return 0;
    }
    else
    {
        self.followerlistView.backgroundView = nil;
        [_topView setHidden:NO];
        [self createBtnView];
        return followerList.count;
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 62.5f;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    PFUser *user = _chatUsers[indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //    PFUser *new = [user objectForKey:kPAPActivityToUserKey] ;
    //    if ([new.objectId isEqual:[PFUser currentUser].objectId]) {
    //        new = [user objectForKey:kPAPActivityFromUserKey];
    //    }
    //    ChatView *chatView = [[ChatView alloc] initWithUser:new];
    //
    //    //ChatView *chatView = [[ChatView alloc] initWithUser:[user objectForKey:kPAPActivityToUserKey]];
    //    chatView.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:chatView animated:YES];
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"ceateGroup";
    
    creategroupTableViewCell *cell = (creategroupTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[creategroupTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setDelegate:self];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    }
    PFObject *user = followerList[indexPath.row];
    cell.object = user;
    
    return cell;
}

-(void)cell:(creategroupTableViewCell *)cellView didTapAddButton:(PFUser *)aUser
{
    
        [cellView.addButton setSelected:YES];
        
        if ([_addedUsersList containsObject:aUser.objectId]) {
            return;
        }
        else
        {
         [_addedUsersList addObject:aUser.objectId];
        }
       
  
    
    
    
    
}



-(void)followerListQuery
{    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:@"loading.." On:self.view];
    
    PFQuery *query = [PFQuery queryWithClassName:kPAPActivityClassKey];
    [query whereKey:kPAPActivityTypeKey equalTo:kPAPActivityTypeFollow];
    [query whereKey:kPAPActivityToUserKey equalTo:self.user];
    //[query whereKey:kPAPActivityToAlbumKey equalTo:NULL];
    [query includeKey:kPAPActivityFromUserKey];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array , NSError *error){
        [pi hideProgressIndicator];
        if (!error) {
            //[pi hideProgressIndicator];
            followerList = [[NSArray alloc] initWithArray:array];
            [self.followerlistView reloadData];
        }
        if (array.count < 1) {
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
            messageLabel.text = @"No Followers..";
            messageLabel.textColor = [UIColor blackColor];
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:20];
            [messageLabel sizeToFit];
            self.followerlistView.backgroundView = messageLabel;
            
        }
    }];
    
}

-(void)createBtnView
{
    int y = [UIScreen mainScreen].bounds.size.height;
    int width = [UIScreen mainScreen].bounds.size.width;
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0,y-120, width, 60)];
    btn.backgroundColor = [UIColor orangeColor];
    [btn setTitle:@"Create" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    // btn.titleLabel.text = @"Create";
    // btn.backgroundColor = [UIColor greenColor];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(createBtnClickedAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
}

- (void)createBtnClickedAction
{
    
    if (!_groupTitle.text.length) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Missing" message:@"Please set title for this group" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }else{
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showMessage:@"Creating.." On:self.view];

        [_addedUsersList addObject:[PFUser currentUser].objectId];
        
        //[self uploadImageOnAmezon:_groupImgView.image];
        NSData* data = UIImageJPEGRepresentation(_groupImgView.image, 0.5f);
        PFFile *imageFile = [PFFile fileWithName:@"groupImage.jpg" data:data];
        PFObject *object = [PFObject objectWithClassName:@"PrivateGroup"];
        [object setObject:_addedUsersList forKey:@"groupUsers"];
        [object setObject:_groupTitle.text forKey:@"groupTitle"];
        [object setObject:imageFile forKey:@"groupImage"];
        [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            if (succeeded) {
                [pi hideProgressIndicator];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        }];
    }
    
}


//UITextfield delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)ImagePickerBtnClicked:(id)sender {
    
    UIActionSheet *acctionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Library", nil];
    acctionSheet.tag = 200;
    [acctionSheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 200) {
        switch (buttonIndex) {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            default:
                break;
        }
    }
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage  *albumImg = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    
    _groupImgView.image = albumImg;
    // profileImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"FacebookProfilePicture.jpg"];
    
    NSData *data = UIImageJPEGRepresentation(albumImg,1.0);
    [data writeToFile:getImagePath atomically:YES];
    
    _isImageEdited = YES;
    
    
}
/**
 *  This method will open device camera to capture image
 *
 *  @param sender cameraBtn press
 */
-(void)cameraButtonClicked:(id)sender
{
    
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.allowsEditing = YES;
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerCameraDeviceFront;
    [self presentViewController:picker animated:YES completion:nil];
}

/**
 *  This method will open device image library to pick image
 *
 *  @param sender libraryBtn press
 */
-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
    
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}


@end
