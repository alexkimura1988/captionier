//
//  PrivateDetailGrpViewController.h
//  Captionier
//
//  Created by Rahul Sharma on 30/10/15.
//
//

#import "PAPPhotoTimelineViewController.h"

@interface PrivateDetailGrpViewController : PAPPhotoTimelineViewController
@property (nonatomic,strong) NSString *stringTitle;
@property (nonatomic, strong) PFObject *object;
@end
