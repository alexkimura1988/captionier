//
//  creategroupTableViewCell.m
//  Captionier
//
//  Created by Rahul Sharma on 21/10/15.
//
//

#import "creategroupTableViewCell.h"
#import "PAPProfileImageView.h"


@implementation creategroupTableViewCell
@synthesize user;
@synthesize object;
@synthesize avatarImageView;
@synthesize nameButton;
@synthesize addButton;
@synthesize separatorImage;
@synthesize mainView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - NSObject

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        

        
        //cellInsetWidth = 0.0f;
        //hideSeparator = NO;
        self.clipsToBounds = YES;
        //horizontalTextSpace =  [PAPBaseTextCell horizontalTextSpaceForInsetWidth:cellInsetWidth];
        
        self.opaque = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        self.backgroundColor = [UIColor clearColor];
        
        mainView = [[UIView alloc] initWithFrame:self.contentView.frame];
        [mainView setBackgroundColor:[UIColor whiteColor]];
        NSLog(@"weidth %f",mainView.frame.size.width);
        
        self.avatarImageView = [[PAPProfileImageView alloc] init];
        self.avatarImageView.layer.borderWidth = 2;
        self.avatarImageView.layer.borderColor =[UIColor colorWithWhite:0.705 alpha:1.0000].CGColor;
        [self.avatarImageView setBackgroundColor:[UIColor clearColor]];
        [self.avatarImageView setOpaque:YES];
        [mainView addSubview:self.avatarImageView];
        
        
        self.addButton = [[UIButton alloc]init];
        [self.addButton setBackgroundImage:[UIImage imageNamed:@"add_friend_add_icon_off.png"] forState:UIControlStateNormal];
        [self.addButton setBackgroundImage:[UIImage imageNamed:@"add_friend_check_mark_icon_on.png"] forState:UIControlStateSelected];
        [mainView addSubview:self.addButton];
        
        
        nameButton = [UIButton buttonWithType:UIButtonTypeCustom];
        nameButton.frame = CGRectMake(avatarDim+horiBorderSpacing, avatarY+8, 275, 20);
        [nameButton setBackgroundColor:[UIColor whiteColor]];
        nameButton.titleLabel.font = [UIFont fontWithName:robo_bold size:16];
        [nameButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [nameButton setTitleColor:[UIColor colorWithRed:0.341 green:0.832 blue:0.979 alpha:1.000] forState:UIControlStateHighlighted];
        [mainView addSubview:nameButton];
        
        
        

        
        self.separatorImage = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"profile_screen_filter_line-568h"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 1, 0, .5)]];
        [mainView addSubview:separatorImage];
        
        
        
        
        
        
        
        [self.contentView addSubview:mainView];
    }
    
    return self;
}
- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    [mainView setFrame:CGRectMake(0, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.contentView.frame.size.height)];
    
    // Layout avatar image
    //[self.avatarImageView setFrame:CGRectMake(avatarX, avatarY, avatarDim, avatarDim)];
    [self.avatarImageView setFrame:CGRectMake(8.0f,8.0f,45.0f,45.0f)];
    //self.avatarImageView.frame = CGRectMake( 5.0f, 11.0f, 45.0f, 45.0f);
    self.avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width/2;
    self.avatarImageView.clipsToBounds = YES;
    [self.avatarImageView setClipsToBounds:YES];
    
    
    [self.addButton setFrame:CGRectMake(236+37, vertBorderSpacing+8, 27,27)];
    [self.addButton addTarget:self action:@selector(didTapAddButton:) forControlEvents:UIControlEventTouchUpInside];
    
    NSLog(@"user %@",[self.object objectForKey:kPAPActivityFromUserKey]);
    user = [self.object objectForKey:kPAPActivityFromUserKey];
    NSLog(@"user name %@",user.username);
    
    if ([user.objectId isEqual:[PFUser currentUser].objectId]) {
        user = [self.object objectForKey:kPAPActivityToUserKey];
        
    }
    
    
    
    // NSLog(@"user name %@",user2.username);
    
    [nameButton setTitle:[user objectForKey:kPAPUserDisplayNameKey] forState:UIControlStateNormal];
    [self.avatarImageView setFile:[user objectForKey:kPAPUserProfilePicMediumKey]];
    
    // [postObject objectForKey:@"toUser"][@"username"]
    
    
    NSLog(@"user %@",nameButton.titleLabel.text);
    
    
    // Layout the name button
    CGSize nameSize = [nameButton.titleLabel.text sizeWithFont:[UIFont fontWithName:robo_bold size:16] forWidth:275 lineBreakMode:NSLineBreakByTruncatingTail];
    
    [nameButton setFrame:CGRectMake(timeX+15, vertBorderSpacing+10, nameSize.width, nameSize.height)];
   
    
    //[self.nameButton addTarget:self action:@selector(didTapUserButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    nameButton.backgroundColor = [UIColor clearColor];
    
    mainView.backgroundColor = [UIColor clearColor];
    
    // Layour separator
    [self.separatorImage setFrame:CGRectMake(0, self.frame.size.height-2, self.frame.size.width, 2)];
    //[self.separatorImage setHidden:hideSeparator];
    
    
    
}

/* Inform delegate that the follow button was tapped */
- (void)didTapAddButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapAddButton:)])
    {
        
        [self.delegate cell:self didTapAddButton:self.user];
        
    }
}

@end
