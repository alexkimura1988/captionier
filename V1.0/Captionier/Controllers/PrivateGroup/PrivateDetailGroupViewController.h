//
//  PrivateDetailGroupViewController.h
//  Captionier
//
//  Created by Rahul Sharma on 24/10/15.
//
//

#import <UIKit/UIKit.h>

@interface PrivateDetailGroupViewController : UIViewController
@property (nonatomic,strong) NSString *stringTitle;
@property (nonatomic, strong) PFObject *object;

@end
