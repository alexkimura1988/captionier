//
//  PrivateGroupListViewController.h
//  Captionier
//
//  Created by Rahul Sharma on 21/10/15.
//
//

#import <UIKit/UIKit.h>

@interface PrivateGroupListViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *groupTblView;
@property (strong, nonatomic) IBOutlet UIButton *createBtn;
@property (strong, nonatomic) PFUser *user;

- (IBAction)createBtnClicked:(id)sender;
@end
