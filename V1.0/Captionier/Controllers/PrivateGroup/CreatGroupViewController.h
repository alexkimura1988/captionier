//
//  CreatGroupViewController.h
//  Captionier
//
//  Created by Rahul Sharma on 21/10/15.
//
//

#import <UIKit/UIKit.h>

@interface CreatGroupViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *groupImgView;
@property (strong, nonatomic) IBOutlet UITableView *followerlistView;
@property (strong, nonatomic) IBOutlet UIButton *doneBtn;
@property (strong, nonatomic) PFUser *user;
- (IBAction)doneBtnClicked:(id)sender;
- (IBAction)ImagePickerBtnClicked:(id)sender;
//@property (strong, nonatomic) IBOutlet UITextView *groupTitle;
@property (strong, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIButton *imagePickerBtn;
@property (strong, nonatomic) IBOutlet UITextField *groupTitle;
//Group title
@end
