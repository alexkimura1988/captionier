//
//  PrivateDetailGrpViewController.m
//  Captionier
//
//  Created by Rahul Sharma on 30/10/15.
//
//

#import "PrivateDetailGrpViewController.h"
#import "CustomCameraViewController.h"

@interface PrivateDetailGrpViewController ()

@end

@implementation PrivateDetailGrpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _stringTitle;
    //[self displayMessage];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self customizeNavigationBarButtons];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)displayMessage
{
    UILabel *message = [[UILabel alloc]init];
    message.frame = CGRectMake(0,[UIScreen mainScreen].bounds.size.height-200, 320, 100);
    message.textAlignment = NSTextAlignmentCenter;
    message.text = @"No post to display";
    message.textColor = BLACK_COLOR;
    //message.backgroundColor = GREEN_COLOR;
    [self.view addSubview:message];
}

-(void)privateGroupClicked:(id)sender{
    
    CustomCameraViewController *camCV = [[CustomCameraViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:camCV];
    camCV.hidesBottomBarWhenPushed = YES;
    camCV.fromController = @"PrivateGrp";
    camCV.groupName = _stringTitle;
    camCV.isPrivateGrp = YES;
    camCV.group = _object;
    // [self.navigationController pushViewController:navController animated:YES];
    [self.navigationController presentViewController:navController animated:YES completion:nil];
    
    
    
}


-(void)backBtnClicked:(id)sender{
    
    NSLog(@"BackClicked");
    [self.navigationController popViewControllerAnimated:YES];
    
}


/**
 *  Configures the navigation bar right side button as custome button .
 */
- (void)customizeNavigationBarButtons {
    UIImage *imgButton = [UIImage imageNamed:@"homescreen_add_icon_off.png"];
    
    UIButton *rightBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBarButton setBackgroundImage:imgButton forState:UIControlStateNormal];
    [rightBarButton setBackgroundImage:[UIImage imageNamed:@"homescreen_add_icon_on.png"] forState:UIControlStateHighlighted];
    [rightBarButton setFrame:CGRectMake(0, 0, imgButton.size.width, imgButton.size.height)];
    rightBarButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    [rightBarButton addTarget:self action:@selector(privateGroupClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButton];
    
    UIButton *leftBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *lftimgButton = [UIImage imageNamed:@"sharescreen_back_btn@2x.png"];
    //sharescreen_back_btn@2x.png
    [leftBarButton setBackgroundImage:lftimgButton  forState:UIControlStateNormal];
    [leftBarButton setFrame:CGRectMake(0, 0, lftimgButton.size.width, lftimgButton.size.height)];
    leftBarButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    [leftBarButton addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBarButton];
    
 
    
}


#pragma mark - PFQueryTableViewController

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    // self.tableView.tableHeaderView = self.headerView;
    //[self.tableView reloadData];
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
}

- (PFQuery *)queryForTable {
    
    PFQuery *query;
    
    query = [PFQuery queryWithClassName:kPAPPrivateFeedClassKey];
    [query orderByDescending:@"createdAt"];
    [query includeKey:kPAPPrivateGroupUsersKey];
    [query includeKey:kPAPPhotoUserKey];
    [query whereKey:kPAPPrivateGroupUsersKey equalTo:_object];
    //[query whereKey:kPAPInstallationUserKey equalTo:[PFUser currentUser]];
    //[query whereKey:@"groupTitle" equalTo:_stringTitle];

    return query;
}



@end
