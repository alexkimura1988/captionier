//
//  creategroupTableViewCell.h
//  Captionier
//
//  Created by Rahul Sharma on 21/10/15.
//
//

#import <UIKit/UIKit.h>

@class PAPProfileImageView;
@protocol creategroupTableViewCellDelegate;


@interface creategroupTableViewCell : UITableViewCell
{
    id _delegate;
}
@property (nonatomic, strong) id<creategroupTableViewCellDelegate> delegate;
@property (nonatomic, strong) PFUser *user;
@property (nonatomic, strong) PFObject *object;
@property (nonatomic, strong) UILabel *photoLabel;
@property (nonatomic, strong) UIButton *addButton;
@property (nonatomic, strong) UIButton *nameButton;
@property (nonatomic, strong) PAPProfileImageView *avatarImageView;
@property (nonatomic, strong) UIImageView *separatorImage;
@property (nonatomic, strong) UIView *mainView;
@end
@protocol creategroupTableViewCellDelegate <NSObject>
@optional

- (void)cell:(creategroupTableViewCell *)cellView didTapAddButton:(PFUser *)aUser;

@end
/*! Layout constants */
#define vertBorderSpacing 8.0f
#define vertElemSpacing 0.0f

#define horiBorderSpacing 8.0f
#define horiBorderSpacingBottom 9.0f
#define horiElemSpacing 5.0f

#define vertTextBorderSpacing 10.0f

#define avatarX horiBorderSpacing
#define avatarY vertBorderSpacing
#define avatarDim 33.0f

#define nameX avatarX+avatarDim+horiElemSpacing
#define nameY vertTextBorderSpacing
#define nameMaxWidth 200.0f

#define timeX avatarX+avatarDim+horiElemSpacing
