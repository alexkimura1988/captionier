//
//  GWExploreViewController.m
//  VIND
//
//  Created by Vinay Raja on 01/09/14.
//
//

#import "GWExploreViewController.h"
#import "ExploreViewController.h"
#import "CustomCameraViewController.h"
#import "NavigationViewController.h"

@interface GWExploreViewController ()

@end

@implementation GWExploreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIButton *menuBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn addTarget:self.navigationController action:@selector(toggleMenu) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setFrame:CGRectMake(0.0f,0.0f, 44,44)];
    [Helper setButton:menuBtn Text:@"" WithFont:@"HelveticaNeue" FSize:17 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [menuBtn setImage:[UIImage imageNamed:@"menu_icon_off.png"] forState:UIControlStateNormal];
    [menuBtn setImage:[UIImage imageNamed:@"menu_icon_on.png"] forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingBarButton1 = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    
    self.navigationItem.rightBarButtonItem = containingBarButton1;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake( 0.0f, 0.0f,35, 35);
    [backButton addTarget:self action:@selector(cameraButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:[UIImage imageNamed:@"camera_nav_bar_icon_off"] forState:UIControlStateNormal];
    //[backButton setBackgroundImage:[UIImage imageNamed:@"camera_btn_on"] forState:UIControlStateHighlighted];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
}

-(void)cameraButtonAction:(id)sender {
    NavigationViewController *navigationController = (NavigationViewController *)self.navigationController;
    navigationController.menu.isOpen = YES;
    [navigationController toggleMenu];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if (IS_IPHONE_5) {
        CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController" bundle:nil];
        [self.navigationController pushViewController:obj animated:YES];
    }
    else {
        CustomCameraViewController *obj = [[CustomCameraViewController alloc] initWithNibName:@"CustomCameraViewController_ip4" bundle:nil];
        
        [self.navigationController pushViewController:obj animated:YES];
    }}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loadGpicChannel:(id)sender
{
    ExploreViewController *exploreViewVC = [[ExploreViewController alloc] initWithStyle:UITableViewStylePlain];
    exploreViewVC.category = nil;
    exploreViewVC.title = @"Gpic Channel";
    [self.navigationController pushViewController:exploreViewVC animated:YES];
}

- (IBAction)loadGerardWayWebsite:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.gerardway.com"]];
}

- (IBAction)loadiTunes:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://smarturl.it/gwha"]];
}

- (IBAction)loadFacebook:(id)sender
{
    NSURL *facebookURL = [NSURL URLWithString:@"fb://profile/248329335369517"];
    if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
        [[UIApplication sharedApplication] openURL:facebookURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/gerardway"]];
    }
    
}

- (IBAction)loadTwitter:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/gerardway"]];
}

- (IBAction)loadInstagram:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://instagram.com/gerardway"]];
}


@end
