//
//  AlarmSelectViewController.m
//  VIND
//
//  Created by Rathore on 26/11/14.
//
//

#import "AlarmSelectViewController.h"

@interface AlarmSelectViewController ()
@property(nonatomic,strong)IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *arrayAlarmSchedule;
@end

@implementation AlarmSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _arrayAlarmSchedule = @[@"Every Sunday",@"Every Monday",@"Every Tuesday",@"Every Wednesday",@"Every Thursday",@"Every Friday",@"Every Saturday"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - UITableViewDelegates

-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    
    return _arrayAlarmSchedule.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *SimpleIndentifier = @"customCell";    //must be indentical
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SimpleIndentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleIndentifier];
        
    }
    cell.textLabel.text = _arrayAlarmSchedule[indexPath.row];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud boolForKey:_arrayAlarmSchedule[indexPath.row]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
    
}



-(void)tableView : (UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([ud boolForKey:_arrayAlarmSchedule[indexPath.row]]) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [ud setBool:NO forKey:_arrayAlarmSchedule[indexPath.row]];
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [ud setBool:YES forKey:_arrayAlarmSchedule[indexPath.row]];
    }
    
    [ud synchronize];
}
@end
