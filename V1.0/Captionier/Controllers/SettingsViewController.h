//
//  SettingsViewController.h
//  Anypic
//
//  Created by rahul Sharma on 08/08/13.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <AddressBookUI/AddressBookUI.h>
@interface SettingsViewController : UIViewController <ABPeoplePickerNavigationControllerDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, UIActionSheetDelegate>



@end
