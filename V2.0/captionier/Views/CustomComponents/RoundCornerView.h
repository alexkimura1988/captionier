//
//  RoundCornerView.h
//  Salesconsultant
//
//  Created by dev on 2015. 11. 24..
//  Copyright © 2015년 Cubastion. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface RoundCornerView : UIControl

@property (nonatomic) IBInspectable NSInteger cornerRadius;
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;

@end
