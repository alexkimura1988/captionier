//
//  UITextField+PlaceHolder.m
//  captionier
//
//  Created by kenji on 2015. 12. 15..
//  Copyright © 2015년 embed. All rights reserved.
//

#import "UITextField+PlaceHolder.h"

@implementation UITextField_PlaceHolder

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        _placeColor = [UIColor grayColor];
    }
    
    return self;
}

- (void) drawPlaceholderInRect:(CGRect)rect {
    NSDictionary *attributes = @{NSForegroundColorAttributeName: _placeColor, NSFontAttributeName: self.font};
    CGRect boundingRect = [self.placeholder boundingRectWithSize:rect.size options:0 attributes:attributes context:nil];
    [self.placeholder drawAtPoint:CGPointMake((rect.size.width/2) - boundingRect.size.width / 2, (rect.size.height/2) - boundingRect.size.height / 2) withAttributes:attributes];
}

@end
