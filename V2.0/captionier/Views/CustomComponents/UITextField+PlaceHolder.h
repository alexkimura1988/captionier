//
//  UITextField+PlaceHolder.h
//  captionier
//
//  Created by kenji on 2015. 12. 15..
//  Copyright © 2015년 embed. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface UITextField_PlaceHolder : UITextField

@property (nonatomic) IBInspectable UIColor *placeColor;

@end
