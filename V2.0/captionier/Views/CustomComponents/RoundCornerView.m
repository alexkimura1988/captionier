//
//  RoundCornerView.m
//  Salesconsultant
//
//  Created by dev on 2015. 11. 24..
//  Copyright © 2015년 Cubastion. All rights reserved.
//

#import "RoundCornerView.h"

@implementation RoundCornerView

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.cornerRadius = 10;
        self.borderWidth = 1.0;
        self.borderColor = [UIColor grayColor];
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect
{
    self.layer.masksToBounds = true;
    self.layer.cornerRadius = _cornerRadius;
    self.layer.borderWidth = _borderWidth;
    self.layer.borderColor = _borderColor.CGColor;
    self.clipsToBounds = true;
}

@end
