//
//  PAPConstants.m
//  Anypic
//
//  Created by Mattieu Gamache-Asselin on 5/25/12.
//

#import "PAPConstants.h"

NSString *const kPAPUserDefaultsActivityFeedViewControllerLastRefreshKey    = @"com.parse.Anypic.userDefaults.activityFeedViewController.lastRefresh";
NSString *const kPAPUserDefaultsCacheFacebookFriendsKey                     = @"com.parse.Anypic.userDefaults.cache.facebookFriends";


#pragma mark - Launch URLs

NSString *const kPAPLaunchURLHostTakePicture = @"camera";


#pragma mark - NSNotification

NSString *const PAPAppDelegateApplicationDidReceiveRemoteNotification           = @"com.parse.Anypic.appDelegate.applicationDidReceiveRemoteNotification";
NSString *const PAPUtilityUserFollowingChangedNotification                      = @"com.parse.Anypic.utility.userFollowingChanged";
NSString *const PAPUtilityUserLikedUnlikedPhotoCallbackFinishedNotification     = @"com.parse.Anypic.utility.userLikedUnlikedPhotoCallbackFinished";
NSString *const PAPUtilityDidFinishProcessingProfilePictureNotification         = @"com.parse.Anypic.utility.didFinishProcessingProfilePictureNotification";
NSString *const PAPTabBarControllerDidFinishEditingPhotoNotification            = @"com.parse.Anypic.tabBarController.didFinishEditingPhoto";
NSString *const PAPTabBarControllerDidFinishImageFileUploadNotification         = @"com.parse.Anypic.tabBarController.didFinishImageFileUploadNotification";
NSString *const PAPPhotoDetailsViewControllerUserDeletedPhotoNotification       = @"com.parse.Anypic.photoDetailsViewController.userDeletedPhoto";
NSString *const PAPPhotoDetailsViewControllerUserLikedUnlikedPhotoNotification  = @"com.parse.Anypic.photoDetailsViewController.userLikedUnlikedPhotoInDetailsViewNotification";
NSString *const PAPPhotoDetailsViewControllerUserCommentedOnPhotoNotification   = @"com.parse.Anypic.photoDetailsViewController.userCommentedOnPhotoInDetailsViewNotification";


#pragma mark - User Info Keys
NSString *const PAPPhotoDetailsViewControllerUserLikedUnlikedPhotoNotificationUserInfoLikedKey = @"liked";
NSString *const kPAPEditPhotoViewControllerUserInfoCommentKey = @"comment";

#pragma mark - Installation Class

// Field keys
NSString *const kPAPInstallationUserKey = @"user";

#pragma mark - Activity Class
// Class key
NSString *const kPAPActivityClassKey = @"Activity";


// Field keys
NSString *const kPAPActivityTypeKey                     = @"type";
NSString *const kPAPActivityFromUserKey                 = @"fromUser";
NSString *const kPAPActivityToUserKey                   = @"toUser";
NSString *const kPAPActivityContentKey                  = @"content";
NSString *const kPAPActivityPhotoKey                    = @"photo";
NSString *const kPAPActivityRatingCountKey              = @"ratingCount";
NSString *const kPAPActivityUpvoteCommentsCount         = @"upvoteCount";
NSString *const kPAPActivityUpvoteCommentsUsersArrayKey = @"upvoteCommentUser";



// Type values
NSString *const kPAPActivityTypeLike        = @"like";
NSString *const kPAPActivityTypeFollow      = @"follow";
NSString *const kPAPActivityTypeComment     = @"comment";
NSString *const kPAPActivityTypeJoined      = @"joined";
NSString *const kPAPActivityTypeRating      = @"rating";
NSString *const kPAPActivityTypeCommentTag  =  @"commentTag";

#pragma mark - User Class
// Field keys
NSString *const kPAPUserDisplayNameKey                          = @"displayName";
NSString *const kPAPUserDisplayLocationKey                      = @"displayLocation";
NSString *const kPAPUserFacebookIDKey                           = @"facebookId";
NSString *const kPAPUserPhotoIDKey                              = @"photoId";
NSString *const kPAPUserProfilePicSmallKey                      = @"profilePictureSmall";
NSString *const kPAPUserProfilePicMediumKey                     = @"profilePictureMedium";
NSString *const kPAPUserFacebookFriendsKey                      = @"facebookFriends";
NSString *const kPAPUserAlreadyAutoFollowedFacebookFriendsKey   = @"userAlreadyAutoFollowedFacebookFriends";
NSString *const kPAPUserPrivateChannelKey                       = @"privateChannelKey";
NSString *const kPAPInstallationChannelsKey                     = @"channels";
NSString *const kPAPPrivateGroupUsersKey                        = @"privateGroupUsers";

#pragma mark - Photo Class
// Class key
NSString *const kPAPPhotoClassKey = @"Photo";

// Field keys
NSString *const kPAPPhotoPictureKey         = @"image";
NSString *const kPAPPhotoThumbnailKey       = @"thumbnail";
NSString *const kPAPPhotoUserKey            = @"user";
NSString *const kPAPPhotoOpenGraphIDKey     = @"fbOpenGraphID";
NSString *const kPAPPhotoDescriptionTextKey = @"descreptionText";
NSString *const kPAPPhotoCommentsCount      = @"commentsCount";
NSString *const kPAPPhotoLikeCountKey       = @"likesCount";
NSString *const kPAPPhotoTotalRating        = @"totalRating";
NSString *const kPAPPhotoTotalUsersRated    = @"usersRated";
NSString *const kPAPPhotoCategory           = @"category";
NSString *const kPAPPhotoUpperPrice         = @"upperPrice";
NSString *const kPAPPhotoLowerPrice         = @"lowerPrice";
NSString *const kPAPPhotoBought             = @"isBought";
NSString *const kPAPPhotoBlock              = @"isBlocked";
NSString *const kPAPPhotoMediaTypeKey       = @"mediaType";
NSString *const kPAPPhotoVidoKey            = @"videoFile";
NSString *const kPAPPhotoAudioKey           = @"AudioFile";
NSString *const kPAPPhotoMediaPhotoKey      = @"photoMedia";
NSString *const kPAPPhotoMediaVideoKey      = @"videoMedia";
NSString *const kPAPPhotoMediaAudioKey      = @"audioMedia";
NSString *const kPAPPhotoCommentArrayKey    = @"comments";




#pragma mark - Cached Photo Attributes
// keys
NSString *const kPAPPhotoAttributesIsLikedByCurrentUserKey = @"isLikedByCurrentUser";
NSString *const kPAPPhotoAttributesIsRatedByCurrentUserKey = @"isRatedByCurrentUser";
NSString *const kPAPPhotoAttributesCurrentUserRatingKey = @"userRating";
NSString *const kPAPPhotoAttributesLikeCountKey            = @"likeCount";
NSString *const kPAPPhotoAttributesLikersKey               = @"likers";
NSString *const kPAPPhotoAttributesCommentCountKey         = @"commentCount";
NSString *const kPAPPhotoAttributesCommentersKey           = @"commenters";


NSString *const kPAPhotoAttributesCommetnterUserskey        = @"commentedUsers";
NSString *const kPAPPhotoAttributesCommentsKey              = @"comments";


#pragma mark - Cached User Attributes
// keys
NSString *const kPAPUserAttributesPhotoCountKey                 = @"photoCount";
NSString *const kPAPUserAttributesIsFollowedByCurrentUserKey    = @"isFollowedByCurrentUser";


#pragma mark - Push Notification Payload Keys

NSString *const kAPNSAlertKey = @"alert";
NSString *const kAPNSBadgeKey = @"badge";
NSString *const kAPNSSoundKey = @"sound";

// the following keys are intentionally kept short, APNS has a maximum payload limit
NSString *const kPAPPushPayloadPayloadTypeKey          = @"p";
NSString *const kPAPPushPayloadPayloadTypeActivityKey  = @"a";
NSString *const kPAPPushPayloadPayloadTypeChatKey = @"ch";


NSString *const kPAPPushPayloadActivityTaggedInPhotoKey = @"tp";
NSString *const kPAPPushPayloadActivityTaggedInCommentKey = @"tc";
NSString *const kPAPPushPayloadActivityTypeKey     = @"t";
NSString *const kPAPPushPayloadActivityLikeKey     = @"l";
NSString *const kPAPPushPayloadActivityCommentKey  = @"c";
NSString *const kPAPPushPayloadActivityFollowKey   = @"f";
NSString *const kPAPPushPayloadActivityUpVoteKey   = @"v";
NSString *const kPAPPushPayloadChatUserIdKey       = @"cUid";

NSString *const kPAPPushPayloadFromUserObjectIdKey = @"fu";
NSString *const kPAPPushPayloadToUserObjectIdKey   = @"tu";
NSString *const kPAPPushPayloadPhotoObjectIdKey    = @"pid";
NSString *const kPAPSaveOriginalPhoto              = @"saveOriginalPhoto";


#pragma mark - Feed Class
// Class key
NSString *const kPAPFeedClassKey = @"Feed";



// Field keys
NSString *const kPAPFeedUserKey            = @"user";
NSString *const kPAPFeedCommentsCount      = @"commentsCount";

NSString *const kPAPFeedLikeCountKey       = @"likesCount";
NSString *const kPAPFeedBlock              = @"isBlocked";
NSString *const kPAPFeedMediaTypeKey       = @"mediaType";
NSString *const kPAPFeedMediaTypeVideoKey  = @"video";
NSString *const kPAPFeedMediaTypeImageKey  = @"image";
NSString *const kPAPFeedMediaLinkKey       = @"mediaLink";
NSString *const kPAPFeedMediaThumbnailLikKey = @"thumbnailLink";
NSString *const kPAPFeedCommentersArrayKey = @"commenters";
NSString *const kPAPFeedLocationKey        = @"location";
NSString *const kPAPFeedCommentsArrayKey   = @"comments";
NSString *const kPAPFeedLikedByArrayKey    = @"likedBy";
NSString *const kPAPFeedActivityArrayKey   = @"activity";
NSString *const kPAPFeedChannelKey         = @"channel";
NSString *const kPAPFeedPhotoCurrentLocaiton = @"photoLocation";
NSString *const kPAPFeedPhotoTaggedLocation = @"taggedLocation";
NSString *const kPAPFeedPhotoTaggedLocationName = @"taggedLocationName";

#pragma mark - Follower Class

NSString *const kPAPFollowersClassKey = @"Followers";

// Field keys
NSString *const kPAPFollowersFollowedByKey            = @"followedBy";
NSString *const kPAPFollowersFollowingKey          = @"followingUser";

#pragma mark - PrivateFeed

NSString *const kPAPPrivateFeedClassKey = @"PrivateFeed";

#pragma mark - PrivateGroup

NSString *const kPAPPrivateGroupClassKey = @"PrivateGroup";

#pragma mark - PrivateGroupActivity
NSString *const kPAPPrivateGroupActivityClassKey = @"PrivateGroupActivity";
