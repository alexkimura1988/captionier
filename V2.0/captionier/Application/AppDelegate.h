//
//  AppDelegate.h
//  captionier
//
//  Created by kenji on 2015. 12. 15..
//  Copyright © 2015년 embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

