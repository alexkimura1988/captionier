//
//  main.m
//  captionier
//
//  Created by dev on 2015. 12. 15..
//  Copyright © 2015년 embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
