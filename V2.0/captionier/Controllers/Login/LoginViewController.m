//
//  LoginViewController.m
//  captionier
//
//  Created by kenji on 2015. 12. 15..
//  Copyright © 2015년 embed. All rights reserved.
//

#import "LoginViewController.h"
#import <Parse/Parse.h>
#import <ParseTwitterUtils/ParseTwitterUtils.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "PAPConstants.h"

@interface LoginViewController ()
{
    UITextField *inputEmailTextField;
}

@property (weak, nonatomic) IBOutlet UITextField *textUsername;
@property (weak, nonatomic) IBOutlet UITextField *textPassword;

@property (weak, nonatomic) IBOutlet UIImageView *imgBkUsername;
@property (weak, nonatomic) IBOutlet UIImageView *imgBkPassword;
@property (weak, nonatomic) IBOutlet UIImageView *imgBkLogin;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image = [UIImage imageNamed:@"login_tab_off"];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 20.0, 0.0, 20.0)];
    _imgBkUsername.image = image;
    _imgBkPassword.image = image;
    _imgBkLogin.image = image;
}

- (IBAction)onClickLogin:(id)sender {
    if (_textUsername.text.length == 0 || _textPassword.text.length == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Missing Information"
                                                                       message:@"Make sure you fill out all of the information!"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [SVProgressHUD show];
    [PFUser logInWithUsernameInBackground:_textUsername.text
                                 password:_textPassword.text
                                    block:^(PFUser * _Nullable user, NSError * _Nullable error) {
                                        [SVProgressHUD dismiss];
                                        
                                        if (user != nil) {
                                            [self didLogInUser:user];
                                        }
                                        else {
                                            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login failed!"
                                                                                                           message:@"invalid login credentials."
                                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                                            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                                                         style:UIAlertActionStyleDefault
                                                                                       handler:nil];
                                            
                                            [alert addAction:ok];
                                            [self presentViewController:alert animated:YES completion:nil];
                                        }
                                    }];
}

- (IBAction)onClickEmail:(id)sender {
    
}

- (IBAction)onClickTwitter:(id)sender {
    
}

- (IBAction)onClickForgotPassword:(id)sender {
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Enter Email"
                                                                    message:@"Please enter the email address for your account."
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                   
                                                   NSLog(@"%@", inputEmailTextField.text);
                                               }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:cancel];
    [alert addAction:ok];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Email";
        inputEmailTextField = textField;
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) didLogInUser:(PFUser *)user {
    
    NSString *privateChannelName = [NSString stringWithFormat:@"captionier_%@",[user objectId]];
    // Add the user to the installation so we can track the owner of the deviceu
    [[PFInstallation currentInstallation] setObject:[PFUser currentUser] forKey:kPAPInstallationUserKey];
    // Subscribe user to private channel
    [[PFInstallation currentInstallation] addUniqueObject:privateChannelName forKey:kPAPInstallationChannelsKey];
    // Save installation object
    [[PFInstallation currentInstallation] saveEventually];
    
    [user setObject:privateChannelName forKey:kPAPUserPrivateChannelKey];
    [user saveInBackground];
    
    if ([PFUser currentUser].email.length == 0) {
        if([PFTwitterUtils isLinkedWithUser:user]) {
            NSError *error;
            NSURL *verify = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:verify];
            [[PFTwitterUtils twitter] signRequest:request];
            NSURLResponse *response = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if (!error) {
                [self twitterRequestDidLoad:dictionary];
            }
            
            NSLog(@"%@",dictionary);
        }
    }
    else {
        [self promptEULAToUser];
    }
}

- (void)promptEULAToUser {
    
    PFUser *user = [PFUser currentUser];
    
    BOOL eulaAccepted = NO;
    if ([user objectForKey:@"eulaAccepted"]) {
        eulaAccepted = [[user objectForKey:@"eulaAccepted"] boolValue];
    }
    
    if ([user isNew] || !eulaAccepted) {
        
        WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webView.title = @"Terms of Service";
        webView.weburl = TermsOfService;//[[NSUserDefaults standardUserDefaults] objectForKey:@"EulaURL"];;
        webView.delegate = self;
        // webView.hidesBottomBarWhenPushed = YES;
        //[self.navigationController pushViewController:webView animated:YES];
        UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:webView];
        [self.welcomeViewController.presentedViewController presentViewController:navC animated:NO completion:nil];
    }
    else {
        [self completeLogin:user];
    }
}

@end
